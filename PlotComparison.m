function PlotComparison(Est1,Est2,Est3,Est4,Est5,fine, fineCss)

t_obs = Est1.t;

[SQRT_X1, SQRT_V1, P_t_C_20_1, P_t_C_22_1, mean1, std1, meanV1, stdV1,...
    meanLib1, stdLib1, meanC201, stdC201, meanC221, stdC221] = Envelopes_Plot(Est1, fine, fineCss);
[SQRT_X2, SQRT_V2, P_t_C_20_2, P_t_C_22_2, mean2, std2, meanV2, stdV2,...
    meanLib2, stdLib2, meanC202, stdC202, meanC222, stdC222] = Envelopes_Plot(Est2, fine, fineCss);
[SQRT_X3, SQRT_V3, P_t_C_20_3, P_t_C_22_3, mean3, std3, meanV3, stdV3,...
    meanLib3, stdLib3, ~, stdC203, meanC223, stdC223] = Envelopes_Plot(Est3, fine, fineCss);
[SQRT_X4, SQRT_V4, P_t_C_20_4, P_t_C_22_4, mean4, std4, meanV4, stdV4,...
    meanLib4, stdLib4, meanC204, stdC204, meanC224, stdC224] = Envelopes_Plot(Est4, fine, fineCss);
[SQRT_X5, SQRT_V5, P_t_C_20_5, P_t_C_22_5, mean5, std5, meanV5,...
    stdV5, meanLib5, stdLib5, meanC205, stdC205, meanC225, stdC225] = Envelopes_Plot(Est5, fine, fineCss);

figure()
subplot(1,2,1)
semilogy(t_obs(1:end-1)/3600,SQRT_X1,'LineWidth',1.2)
grid on;
hold on;
semilogy(t_obs(1:end-1)/3600,SQRT_X2,'LineWidth',1.2)
semilogy(t_obs(1:end-1)/3600,SQRT_X3,'LineWidth',1.2)
semilogy(t_obs(1:end-1)/3600,SQRT_X4,'LineWidth',1.2)
semilogy(t_obs(1:end-1)/3600,SQRT_X5,'LineWidth',1.2)
title('Position vector $3\sigma$ RMS','FontSize',30)
xlabel('Time $[hour]$','Fontsize',26)
ylabel('$[km]$','Fontsize',26)
legend('QSO-H','QSO-M','QSO-La','QSO-Lb','QSO-Lc', 'Interpreter','latex','Fontsize',26)
subplot(1,2,2)
semilogy(t_obs(1:end-1)/3600,SQRT_V1,'LineWidth',1.2)
grid on;
hold on;
semilogy(t_obs(1:end-1)/3600,SQRT_V2,'LineWidth',1.2)
semilogy(t_obs(1:end-1)/3600,SQRT_V3,'LineWidth',1.2)
semilogy(t_obs(1:end-1)/3600,SQRT_V4,'LineWidth',1.2)
semilogy(t_obs(1:end-1)/3600,SQRT_V5,'LineWidth',1.2)
title('Velocity vector $3\sigma$ RMS','FontSize',30)
xlabel('Time $[hour]$','Fontsize',26)
ylabel('$[km/s]$','Fontsize',26)
legend('QSO-H','QSO-M','QSO-La','QSO-Lb','QSO-Lc', 'Interpreter','latex','Fontsize',26)

figure()
subplot(1,2,1)
semilogy(t_obs(1:end-1)/3600,3.*real(sqrt(P_t_C_20_1)),'LineWidth',1.2);
grid on;
hold on;
semilogy(t_obs(1:end-1)/3600,3.*real(sqrt(P_t_C_20_2)),'LineWidth',1.2);
semilogy(t_obs(1:end-1)/3600,3.*real(sqrt(P_t_C_20_3)),'LineWidth',1.2);
semilogy(t_obs(1:end-1)/3600,3.*real(sqrt(P_t_C_20_4)),'LineWidth',1.2);
semilogy(t_obs(1:end-1)/3600,3.*real(sqrt(P_t_C_20_5)),'LineWidth',1.2);
xlabel('Time $[hour]$','Fontsize',26)
ylabel('$C_{20}$','Fontsize',26)
legend('QSO-H','QSO-M','QSO-La','QSO-Lb','QSO-Lc', 'Interpreter','latex','Fontsize',26)
subplot(1,2,2)
semilogy(t_obs(1:end-1)/3600,3.*real(sqrt(P_t_C_22_1)),'LineWidth',1.2);
grid on;
hold on;
semilogy(t_obs(1:end-1)/3600,3.*real(sqrt(P_t_C_22_2)),'LineWidth',1.2);
semilogy(t_obs(1:end-1)/3600,3.*real(sqrt(P_t_C_22_3)),'LineWidth',1.2);
semilogy(t_obs(1:end-1)/3600,3.*real(sqrt(P_t_C_22_4)),'LineWidth',1.2);
semilogy(t_obs(1:end-1)/3600,3.*real(sqrt(P_t_C_22_5)),'LineWidth',1.2);
xlabel('Time $[hour]$','Fontsize',26)
ylabel('$C_{22}$','Fontsize',26)
legend('QSO-H','QSO-M','QSO-La','QSO-Lb','QSO-Lc', 'Interpreter','latex','Fontsize',26)


figure()
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(Est1.P_t(13,13,1:end-1)))),'LineWidth',1.2)
grid on;
hold on;
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(Est2.P_t(13,13,1:end-1)))),'LineWidth',1.2)
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(Est3.P_t(13,13,1:end-1)))),'LineWidth',1.2)
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(Est4.P_t(13,13,1:end-1)))),'LineWidth',1.2)
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(Est5.P_t(13,13,1:end-1)))),'LineWidth',1.2)
title('Libration angle $3\sigma$ envelopes','Interpreter','latex','Fontsize',30)
xlabel('Time [hour]', 'Interpreter','latex','Fontsize',26)
ylabel('$\Phi_{Ph}$ [deg]', 'Interpreter','latex','Fontsize',26)
legend('QSO-H','QSO-M','QSO-La','QSO-Lb','QSO-Lc', 'Interpreter','latex','Fontsize',26)

% Phobos & Mars states

% figure()
% subplot(2,4,1)
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est6.P_t(7,7,1:end-1)))),'LineWidth',1.2)
% grid on;
% hold on;
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est7.P_t(7,7,1:end-1)))),'LineWidth',1.2)
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est8.P_t(7,7,1:end-1)))),'LineWidth',1.2)
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est9.P_t(7,7,1:end-1)))),'LineWidth',1.2)
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est10.P_t(7,7,1:end-1)))),'LineWidth',1.2)
% xlabel('Time [hour]')
% ylabel('$R_{Ph}$ [km]')
% subplot(2,4,2)
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est6.P_t(9,9,1:end-1)))),'LineWidth',1.2)
% grid on;
% hold on;
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est7.P_t(9,9,1:end-1)))),'LineWidth',1.2)
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est8.P_t(9,9,1:end-1)))),'LineWidth',1.2)
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est9.P_t(9,9,1:end-1)))),'LineWidth',1.2)
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est10.P_t(9,9,1:end-1)))),'LineWidth',1.2)
% xlabel('Time [hour]')
% ylabel('$\theta_{Ph}$ [deg]')
% subplot(2,4,3)
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est6.P_t(11,11,1:end-1)))),'LineWidth',1.2)
% grid on;
% hold on;
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est7.P_t(11,11,1:end-1)))),'LineWidth',1.2)
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est8.P_t(11,11,1:end-1)))),'LineWidth',1.2)
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est9.P_t(11,11,1:end-1)))),'LineWidth',1.2)
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est10.P_t(11,11,1:end-1)))),'LineWidth',1.2)
% xlabel('Time [hour]')
% ylabel('$\Phi_{M}$ [deg]')
% subplot(2,4,4)
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est6.P_t(13,13,1:end-1)))),'LineWidth',1.2)
% grid on;
% hold on;
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est7.P_t(13,13,1:end-1)))),'LineWidth',1.2)
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est8.P_t(13,13,1:end-1)))),'LineWidth',1.2)
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est9.P_t(13,13,1:end-1)))),'LineWidth',1.2)
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est10.P_t(13,13,1:end-1)))),'LineWidth',1.2)
% xlabel('Time [hour]')
% ylabel('$\Phi_{Ph}$ [deg]')
% 
% subplot(2,4,5)
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est6.P_t(8,8,1:end-1)))),'LineWidth',1.2)
% grid on;
% hold on;
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est7.P_t(8,8,1:end-1)))),'LineWidth',1.2)
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est8.P_t(8,8,1:end-1)))),'LineWidth',1.2)
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est9.P_t(8,8,1:end-1)))),'LineWidth',1.2)
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est10.P_t(8,8,1:end-1)))),'LineWidth',1.2)
% xlabel('Time [hour]')
% ylabel('$\dot{R}_{Ph}$ [km/s]')
% subplot(2,4,6)
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est6.P_t(10,10,1:end-1)))),'LineWidth',1.2)
% grid on;
% hold on;
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est7.P_t(10,10,1:end-1)))),'LineWidth',1.2)
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est8.P_t(10,10,1:end-1)))),'LineWidth',1.2)
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est9.P_t(10,10,1:end-1)))),'LineWidth',1.2)
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est10.P_t(10,10,1:end-1)))),'LineWidth',1.2)
% xlabel('Time [hour]')
% ylabel('$\dot{\theta}_{Ph}$ [deg/s]')
% subplot(2,4,7)
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est6.P_t(12,12,1:end-1)))),'LineWidth',1.2)
% grid on;
% hold on;
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est7.P_t(12,12,1:end-1)))),'LineWidth',1.2)
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est8.P_t(12,12,1:end-1)))),'LineWidth',1.2)
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est9.P_t(12,12,1:end-1)))),'LineWidth',1.2)
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est10.P_t(12,12,1:end-1)))),'LineWidth',1.2)
% xlabel('Time [hour]')
% ylabel('$\dot{\Phi}_{M}$ [deg/s]')
% subplot(2,4,8)
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est6.P_t(14,14,1:end-1)))),'LineWidth',1.2)
% grid on;
% hold on;
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est7.P_t(14,14,1:end-1)))),'LineWidth',1.2)
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est8.P_t(14,14,1:end-1)))),'LineWidth',1.2)
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est9.P_t(14,14,1:end-1)))),'LineWidth',1.2)
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est10.P_t(14,14,1:end-1)))),'LineWidth',1.2)
% xlabel('Time [hour]')
% ylabel('$\dot{\Phi}_{Ph}$ [deg/s]')

% Phobos states withoutMars

figure()
subplot(2,3,1)
semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est1.P_t(7,7,1:end-1)))),'LineWidth',1.2)
grid on;
hold on;
semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est2.P_t(7,7,1:end-1)))),'LineWidth',1.2)
semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est3.P_t(7,7,1:end-1)))),'LineWidth',1.2)
semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est4.P_t(7,7,1:end-1)))),'LineWidth',1.2)
semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est5.P_t(7,7,1:end-1)))),'LineWidth',1.2)
xlabel('Time [hour]','FontSize',26)
ylabel('$r_{Ph}$ [km]','FontSize',26)
legend('QSO-H','QSO-M','QSO-La','QSO-Lb','QSO-Lc', 'Interpreter','latex','Fontsize',20)
subplot(2,3,2)
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(Est1.P_t(9,9,1:end-1)))),'LineWidth',1.2)
grid on;
hold on;
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(Est2.P_t(9,9,1:end-1)))),'LineWidth',1.2)
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(Est3.P_t(9,9,1:end-1)))),'LineWidth',1.2)
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(Est4.P_t(9,9,1:end-1)))),'LineWidth',1.2)
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(Est5.P_t(9,9,1:end-1)))),'LineWidth',1.2)
xlabel('Time [hour]','FontSize',26)
ylabel('$\theta_{Ph}$ [deg]','FontSize',26)
legend('QSO-H','QSO-M','QSO-La','QSO-Lb','QSO-Lc', 'Interpreter','latex','Fontsize',20)
subplot(2,3,3)
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(Est1.P_t(13,13,1:end-1)))),'LineWidth',1.2)
grid on;
hold on;
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(Est2.P_t(13,13,1:end-1)))),'LineWidth',1.2)
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(Est3.P_t(13,13,1:end-1)))),'LineWidth',1.2)
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(Est4.P_t(13,13,1:end-1)))),'LineWidth',1.2)
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(Est5.P_t(13,13,1:end-1)))),'LineWidth',1.2)
xlabel('Time [hour]','FontSize',26)
ylabel('$\Phi_{Ph}$ [deg]','FontSize',26)
legend('QSO-H','QSO-M','QSO-La','QSO-Lb','QSO-Lc', 'Interpreter','latex','Fontsize',20)

subplot(2,3,4)
semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est1.P_t(8,8,1:end-1)))),'LineWidth',1.2)
grid on;
hold on;
semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est2.P_t(8,8,1:end-1)))),'LineWidth',1.2)
semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est3.P_t(8,8,1:end-1)))),'LineWidth',1.2)
semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est4.P_t(8,8,1:end-1)))),'LineWidth',1.2)
semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est5.P_t(8,8,1:end-1)))),'LineWidth',1.2)
xlabel('Time [hour]','FontSize',26)
ylabel('$\dot{r}_{Ph}$ [km/s]','FontSize',26)
legend('QSO-H','QSO-M','QSO-La','QSO-Lb','QSO-Lc', 'Interpreter','latex','Fontsize',20)
subplot(2,3,5)
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(Est1.P_t(10,10,1:end-1)))),'LineWidth',1.2)
grid on;
hold on;
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(Est2.P_t(10,10,1:end-1)))),'LineWidth',1.2)
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(Est3.P_t(10,10,1:end-1)))),'LineWidth',1.2)
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(Est4.P_t(10,10,1:end-1)))),'LineWidth',1.2)
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(Est5.P_t(10,10,1:end-1)))),'LineWidth',1.2)
xlabel('Time [hour]','FontSize',26)
ylabel('$\dot{\theta}_{Ph}$ [deg/s]','FontSize',26)
legend('QSO-H','QSO-M','QSO-La','QSO-Lb','QSO-Lc', 'Interpreter','latex','Fontsize',20)
subplot(2,3,6)
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(Est1.P_t(14,14,1:end-1)))),'LineWidth',1.2)
grid on;
hold on;
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(Est2.P_t(14,14,1:end-1)))),'LineWidth',1.2)
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(Est3.P_t(14,14,1:end-1)))),'LineWidth',1.2)
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(Est4.P_t(14,14,1:end-1)))),'LineWidth',1.2)
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(Est5.P_t(14,14,1:end-1)))),'LineWidth',1.2)
xlabel('Time [hour]','FontSize',26)
ylabel('$\dot{\Phi}_{Ph}$ [deg/s]','FontSize',26)
legend('QSO-Lc','QSO-Lb','QSO-La','QSO-H','QSO-H', 'Interpreter','latex','Fontsize',20)


% Moments of inertia

% figure()
% subplot(2,3,1)
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est1.P_t(15,15,1:end-1)))),'LineWidth',1);
% grid on;
% hold on;
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est2.P_t(15,15,1:end-1)))),'LineWidth',1);
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est3.P_t(15,15,1:end-1)))),'LineWidth',1);
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est4.P_t(15,15,1:end-1)))),'LineWidth',1);
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est5.P_t(15,15,1:end-1)))),'LineWidth',1);
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est6.P_t(15,15,1:end-1)))),'LineWidth',1);
% xlabel('time $[hour]$')
% ylabel('$I_{PhX}$')
% legend('QSO-H','QSO-M','QSO-La','QSO-Lb','QSO-Lc', 'Interpreter','latex','Fontsize',26)
% subplot(2,3,2)
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est1.P_t(16,16,1:end-1)))),'LineWidth',1);
% grid on;
% hold on;
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est2.P_t(16,16,1:end-1)))),'LineWidth',1);
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est3.P_t(16,16,1:end-1)))),'LineWidth',1);
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est4.P_t(16,16,1:end-1)))),'LineWidth',1);
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est5.P_t(16,16,1:end-1)))),'LineWidth',1);
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est6.P_t(16,16,1:end-1)))),'LineWidth',1);
% xlabel('time $[hour]$')
% ylabel('$I_{PhY}$')
% legend('QSO-H','QSO-M','QSO-La','QSO-Lb','QSO-Lc', 'Interpreter','latex','Fontsize',26)
% subplot(2,3,3)
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est1.P_t(17,17,1:end-1)))),'LineWidth',1);
% grid on;
% hold on;
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est2.P_t(17,17,1:end-1)))),'LineWidth',1);
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est3.P_t(17,17,1:end-1)))),'LineWidth',1);
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est4.P_t(17,17,1:end-1)))),'LineWidth',1);
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est5.P_t(17,17,1:end-1)))),'LineWidth',1);
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est6.P_t(17,17,1:end-1)))),'LineWidth',1);
% xlabel('time $[hour]$')
% ylabel('$I_{PhZ}$')
% legend('QSO-H','QSO-M','QSO-La','QSO-Lb','QSO-Lc', 'Interpreter','latex','Fontsize',26)
% subplot(2,3,4)
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est1.P_t(16,15,1:end-1)))),'LineWidth',1);
% grid on;
% hold on;
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est2.P_t(16,15,1:end-1)))),'LineWidth',1);
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est3.P_t(16,15,1:end-1)))),'LineWidth',1);
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est4.P_t(16,15,1:end-1)))),'LineWidth',1);
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est5.P_t(16,15,1:end-1)))),'LineWidth',1);
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est6.P_t(16,15,1:end-1)))),'LineWidth',1);
% xlabel('time $[hour]$')
% ylabel('$C_{I_{PhX}I_{PhY}}$')
% legend('QSO-H','QSO-M','QSO-La','QSO-Lb','QSO-Lc', 'Interpreter','latex','Fontsize',26)
% subplot(2,3,5)
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est1.P_t(17,15,1:end-1)))),'LineWidth',1);
% grid on;
% hold on;
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est2.P_t(17,15,1:end-1)))),'LineWidth',1);
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est3.P_t(17,15,1:end-1)))),'LineWidth',1);
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est4.P_t(17,15,1:end-1)))),'LineWidth',1);
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est5.P_t(17,15,1:end-1)))),'LineWidth',1);
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est6.P_t(17,15,1:end-1)))),'LineWidth',1);
% xlabel('time $[hour]$')
% ylabel('$C_{I_{PhX}I_{PhZ}}$')
% legend('QSO-H','QSO-M','QSO-La','QSO-Lb','QSO-Lc', 'Interpreter','latex','Fontsize',26)
% subplot(2,3,6)
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est1.P_t(17,16,1:end-1)))),'LineWidth',1);
% grid on;
% hold on;
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est2.P_t(17,16,1:end-1)))),'LineWidth',1);
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est3.P_t(17,16,1:end-1)))),'LineWidth',1);
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est4.P_t(17,16,1:end-1)))),'LineWidth',1);
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est5.P_t(17,16,1:end-1)))),'LineWidth',1);
% semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est6.P_t(17,16,1:end-1)))),'LineWidth',1);
% xlabel('time $[hour]$')
% ylabel('$C_{I_{PhY}I_{PhZ}}$')
% legend('QSO-H','QSO-M','QSO-La','QSO-Lb','QSO-Lc', 'Interpreter','latex','Fontsize',26)



end