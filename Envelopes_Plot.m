function [SQRT_X, SQRT_V, P_t_C_20, P_t_C_22, meanX, sigmaX, meanV, sigmaV,...
    meanLib, sigmaLib, meanC20, sigmaC20, meanC22, sigmaC22] = Envelopes_Plot(Est,fine,fineCss)


sqx = squeeze(Est.P_t(1,1,1:end-1));
sqy = squeeze(Est.P_t(2,2,1:end-1));
sqz = squeeze(Est.P_t(3,3,1:end-1));
SQRT_X = 3*real(sqrt(sqx + sqy + sqz));
sqvx = squeeze(Est.P_t(4,4,1:end-1));
sqvy = squeeze(Est.P_t(5,5,1:end-1));
sqvz = squeeze(Est.P_t(6,6,1:end-1));
SQRT_V = 3*real(sqrt(sqvx + sqvy + sqvz));

switch size(Est.X_t,1)
    case 20
        P_t_I2x  = squeeze(Est.P_t(15,15,1:end-1));
        P_t_I2y  = squeeze(Est.P_t(16,16,1:end-1));
        P_t_I2z  = squeeze(Est.P_t(17,17,1:end-1));
        Cov_I2xI2y = squeeze(Est.P_t(15,16,1:end-1));
        Cov_I2yI2z = squeeze(Est.P_t(16,17,1:end-1));
        Cov_I2xI2z = squeeze(Est.P_t(15,17,1:end-1));
    case 18
        P_t_I2x  = squeeze(Est.P_t(13,13,1:end-1));
        P_t_I2y  = squeeze(Est.P_t(14,14,1:end-1));
        P_t_I2z  = squeeze(Est.P_t(15,15,1:end-1));
        Cov_I2xI2y = squeeze(Est.P_t(13,14,1:end-1));
        Cov_I2yI2z = squeeze(Est.P_t(14,15,1:end-1));
        Cov_I2xI2z = squeeze(Est.P_t(13,15,1:end-1));
end

P_t_C_20 = .25*(P_t_I2y + P_t_I2x - 2*Cov_I2xI2y);
P_t_C_22 = .5*(4*P_t_I2z + P_t_I2y + P_t_I2x - 4*Cov_I2xI2z - 4*Cov_I2yI2z + 2*Cov_I2xI2y);


SQRT_X2 = SQRT_X(fine:end);
meanX   = mean(SQRT_X2);
% min    = min(SQRT_X2);
% max    = max(SQRT_X2);
% err    = max-min;
sigmaX    = std(SQRT_X2);

SQRT_V2 = SQRT_V(fine:end);
meanV   = mean(SQRT_V2);
% minV    = min(SQRT_V2);
% maxV    = max(SQRT_V2);
% errV    = maxV-minV;
sigmaV    = std(SQRT_V2);

Lib    = 3*real(sqrt(Est.P_t(13,13,fine:end)));
meanLib   = mean(Lib);
% minLib    = min(Lib);
% maxLib    = max(Lib);
% errLib    = maxLib-minLib;
sigmaLib    = std(Lib);

meanC20 = mean(3.*real(sqrt(P_t_C_20(fineCss:end))));
sigmaC20= std(3.*real(sqrt(P_t_C_20(fineCss:end))));

meanC22 = mean(3.*real(sqrt(P_t_C_22(end))));
sigmaC22= std(3.*real(sqrt(P_t_C_22(fineCss:end))));

end