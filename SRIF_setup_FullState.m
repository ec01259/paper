clear
close all 
clc
warning off

%%  Phobos properties

    alpha       = 13.03;                          %km
    beta        = 11.4;                           %km
    gamma       = 9.14;                           %km
    Geom        = [alpha; beta; gamma];
    muPh        = 7.12611310492722e-4;            %km^3/s^2
    muM         = 4.28283736206991e4;             %km^3/s^2
    aPh         = 9378.24190780618;               %km
    L           = aPh;
    omega       = sqrt((muM + muPh)/L^3);
    T           = 1/omega;
    

    
%%  Filter MMX and Phobos

%   Inputs of the analysis
    fprintf('Insert the name of the orbit to analyse.\n');
    fprintf('Valid options are:\n');
    fprintf('- QSO-H\n');
    fprintf('- QSO-M\n');
    fprintf('- QSO-Lb\n');
    fprintf('- 3DQSO-M\n');
    fprintf('- 3DQSO-Lb\n');
    fprintf('- Swing_QSO-Lb\n\n');
    orbit   = input('Insert the orbit name: ','s');

%   Kernels loading    
    addpath(genpath('../MICE/mice'));
    switch orbit
        case   'QSO-H'    
            cspice_furnsh( {'C:\Program Files\MATLAB\Matlab\WarmUp\NBP-Check\kernels_QSOH.txt'});       
        case   'QSO-M'    
            cspice_furnsh( {'C:\Program Files\MATLAB\Matlab\WarmUp\NBP-Check\kernels_QSOM.txt'}); 
        case   'QSO-Lb'    
            cspice_furnsh( {'C:\Program Files\MATLAB\Matlab\WarmUp\NBP-Check\kernels_QSOLb.txt'});
        case   '3DQSO-M'    
            cspice_furnsh( {'C:\Program Files\MATLAB\Matlab\WarmUp\NBP-Check\kernels_3DQSOM.txt'});
        case   '3DQSO-Lb'    
            cspice_furnsh( {'C:\Program Files\MATLAB\Matlab\WarmUp\NBP-Check\kernels_3DQSOLb.txt'});
        case   'Swing_QSO-Lb'    
            cspice_furnsh( {'C:\Program Files\MATLAB\Matlab\WarmUp\NBP-Check\kernels_SwingQSOLb.txt'});
    end
    
%   Initial Epoch
    date0   = input('\nInsert the epoch for the beginning of the analysis\n(yyyy-mm-dd hh:mm:ss (Time))): ','s');
    date_end= input('\nInsert the epoch for the end of the analysis\n(yyyy-mm-dd hh:mm:ss (Time))): ','s');
    
%   Generation of the observables
    fprintf('\nGive me one moment to simulate the observables...\n');
    YObs_Full = Observations_Generation(orbit, date0, date_end);

%   Selection of the set of observables
    fprintf('\nOk, ready...');
    fprintf('\nHere I go with the analysis...');
    
    t_obs   = YObs_Full(:,1);
    YObs    = YObs_Full;
         
    
%   Prepare for N-Body propagation
    data        = cspice_str2et(date0);
    pars        = nbpset(data, '499', 'J2000', {'499'}, {'401'}, [4, 4], aPh, T, true);
    pars.model  = 'Harmonics';

    MMX         = cspice_spkezr('-33', data, 'J2000', 'none', 'MARS');
    Phobos      = cspice_spkezr('401', data, 'J2000', 'none', 'MARS');
    
    Clm = [];
    Slm = [];
    
    for i=1:size(pars.SH{1}.Clm,1)
        Clm = [Clm; pars.SH{1}.Clm(i,1:i)'];
    end
    for i=2:size(pars.SH{1}.Clm,1)
        Slm = [Slm; pars.SH{1}.Clm(i,2:i)'];
    end
    
    muM         = muM/pars.lsf^3*pars.tsf^2;
    muPh        = muPh/pars.lsf^3*pars.tsf^2;
    
%   Initial guess


    Est0.X      = [MMX./pars.sfVec; Phobos./pars.sfVec; Clm; Slm];
    pars.Dim    = [pars.sfVec; pars.sfVec; ones(25,1)];
    Est0.dx     = zeros(37,1);
    Est0.P0     = diag(([1*ones(3,1); 1e-3*ones(3,1);...
        1*ones(3,1); 1e-3*ones(3,1); 1*ones(25,1)]./pars.Dim).^2);
    pars.nCoeff = size(Clm,1) + size(Slm,1);
    pars.nConsCoeff = 0;
    Est0.t0     = pars.et0;
        
    propagation = @Mars_Phobos_Circular_FullState;
    observations= @Range_Speed_Lidar_Camera_FullState;
        
    
%   Weighting Matrix  
    sigma_range     = 5e-3/pars.lsf;              % [km]
    sigma_range_rate= 5e-7/pars.lsf*pars.tsf;     % [km/s]
    sigma_lidar     = 1e-3/pars.lsf;              % [km]
    sigma_cam       = 400*1e-6;                   % [rad/pixel]
 
    R      = zeros(5,5);
    R(1,1) = sigma_range^2;
    R(2,2) = sigma_range_rate^2;
    R(3,3) = sigma_lidar^2;
    R(4,4) = sigma_cam^2;
    R(5,5) = sigma_cam^2;

%   Analysis
%     [Est]  = SRIF_Ad_FullState(Est0,propagation,observations,R,YObs,pars);
%     fprintf('\nAnd here are the results...');
%     
    pars.sigma = 1e-7/pars.lsf*pars.tsf^2;
    [Est]  = SRIF_SNC_Paper(Est0,propagation,observations,R,YObs,pars);
    fprintf('\nAnd here are the results...');
    
    
%%  Results

    
%   Plot of the post-fit residuals
    figure()
    subplot(2,5,1)
    plot(t_obs/3600,Est.pre(1,:),'x')
    grid on;
    hold on;
    xlabel('$[hour]$','Interpreter','latex')
    ylabel('$[km]$','Interpreter','latex')
    title('Range Pre-fit residual','Interpreter','latex')
    subplot(2,5,2)
    plot(t_obs/3600,Est.pre(2,:),'x','Color','r')
    grid on;
    hold on;
    xlabel('$[hour]$','Interpreter','latex')
    ylabel('$[km/s]$','Interpreter','latex')    
    title('RangeRate Pre-fit residual','Interpreter','latex') 
    subplot(2,5,3)
    plot(t_obs/3600,Est.pre(3,:),'x','Color','g')
    grid on;
    hold on;
    xlabel('$[hour]$','Interpreter','latex')
    ylabel('$[km]$','Interpreter','latex')
    title('Lidar Pre-fit residual','Interpreter','latex') 
    subplot(2,5,4)
    plot(t_obs/3600,Est.pre(4,:),'x','Color','m')
    grid on;
    hold on;
    xlabel('$[hour]$','Interpreter','latex')
    ylabel('$[rad]$','Interpreter','latex')
    title('Camera Pre-fit residual','Interpreter','latex') 
    subplot(2,5,5)
    plot(t_obs/3600,Est.pre(5,:),'x','Color','k')
    grid on;
    hold on;
    xlabel('$[hour]$','Interpreter','latex')
    ylabel('$[rad]$','Interpreter','latex')
    title('Camera Pre-fit residual','Interpreter','latex') 
    
    subplot(2,5,6)
    plot(t_obs/3600,Est.err(1,:),'x')
    grid on;
    hold on;
    xlabel('$[hour]$','Interpreter','latex')
    ylabel('$[km]$','Interpreter','latex')
    title('Range Post-fit residual','Interpreter','latex')
    subplot(2,5,7)
    plot(t_obs/3600,Est.err(2,:),'x','Color','r')
    grid on;
    hold on;
    xlabel('$[hour]$','Interpreter','latex')
    ylabel('$[km/s]$','Interpreter','latex')
    title('RangeRate Post-fit residual','Interpreter','latex')   
    subplot(2,5,8)
    plot(t_obs/3600,Est.err(3,:),'x','Color','g')
    grid on;
    hold on;
    xlabel('$[hour]$','Interpreter','latex')
    ylabel('$[km]$','Interpreter','latex')
    title('Lidar Post-fit residual','Interpreter','latex')
    subplot(2,5,9)
    plot(t_obs/3600,Est.err(4,:),'x','Color','m')
    grid on;
    hold on;
    xlabel('$[hour]$','Interpreter','latex')
    ylabel('$[rad]$','Interpreter','latex')
    title('Camera Post-fit residual','Interpreter','latex')  
    subplot(2,5,10)
    plot(t_obs/3600,Est.err(5,:),'x','Color','k')
    grid on;
    hold on;
    xlabel('$[hour]$','Interpreter','latex')
    ylabel('$[rad]$','Interpreter','latex')
    title('Camera Post-fit residual','Interpreter','latex')  
    
% %   Difference between pre-fit and post-fit residuals
%     figure()
%     subplot(1,5,1)
%     plot(t_obs/3600,Est.pre(1,:) - Est.err(1,:),'x')
%     grid on;
%     hold on;
%     xlabel('$[hour]$','Interpreter','latex')
%     ylabel('$[km]$','Interpreter','latex')
%     title('Range Pre-fit residual','Interpreter','latex')
%     subplot(1,5,2)
%     plot(t_obs/3600,Est.pre(2,:) - Est.err(2,:),'x','Color','r')
%     grid on;
%     hold on;
%     xlabel('$[hour]$','Interpreter','latex')
%     ylabel('$[km/s]$','Interpreter','latex')    
%     title('RangeRate Pre-fit residual','Interpreter','latex') 
%     subplot(1,5,3)
%     plot(t_obs/3600,Est.pre(3,:) - Est.err(3,:),'x','Color','g')
%     grid on;
%     hold on;
%     xlabel('$[hour]$','Interpreter','latex')
%     ylabel('$[km]$','Interpreter','latex')
%     title('Lidar Pre-fit residual','Interpreter','latex') 
%     subplot(1,5,4)
%     plot(t_obs/3600,Est.pre(4,:) - Est.err(4,:),'x','Color','m')
%     grid on;
%     hold on;
%     xlabel('$[hour]$','Interpreter','latex')
%     ylabel('$[rad]$','Interpreter','latex')
%     title('Camera Pre-fit residual','Interpreter','latex') 
%     subplot(1,5,5)
%     plot(t_obs/3600,Est.pre(5,:) - Est.err(5,:),'x','Color','k')
%     grid on;
%     hold on;
%     xlabel('$[hour]$','Interpreter','latex')
%     ylabel('$[rad]$','Interpreter','latex')
%     title('Camera Pre-fit residual','Interpreter','latex') 
    
    figure()
    subplot(2,5,1)
    histfit(Est.pre(1,:))
    xlabel('$[km]$','Interpreter','latex')
    title('Range Pre-fit residual','Interpreter','latex') 
    subplot(2,5,2)
    histfit(Est.pre(2,:))
    xlabel('$[km/s]$','Interpreter','latex')
    title('Range Rate Pre-fit residual','Interpreter','latex') 
    subplot(2,5,3)
    histfit(Est.pre(3,:))
    xlabel('$[km]$','Interpreter','latex')
    title('Lidar Pre-fit residual','Interpreter','latex')
    subplot(2,5,4)
    histfit(Est.pre(4,:))
    xlabel('$[rad]$','Interpreter','latex')
    title('Camera Pre-fit residual','Interpreter','latex')
    subplot(2,5,5)
    histfit(Est.pre(5,:))
    xlabel('$[rad]$','Interpreter','latex')
    title('Camera Pre-fit residual','Interpreter','latex')
    subplot(2,5,6)
    histfit(Est.err(1,:))
    xlabel('$[km]$','Interpreter','latex')
    title('Range Post-fit residual','Interpreter','latex') 
    subplot(2,5,7)
    histfit(Est.err(2,:))
    xlabel('$[km/s]$','Interpreter','latex')
    title('Range Rate Post-fit residual','Interpreter','latex') 
    subplot(2,5,8)
    histfit(Est.err(3,:))
    xlabel('$[km]$','Interpreter','latex')
    title('Lidar Post-fit residual','Interpreter','latex')   
    subplot(2,5,9)
    histfit(Est.err(4,:))
    xlabel('$[rad]$','Interpreter','latex')
    title('Camera Post-fit residual','Interpreter','latex')   
    subplot(2,5,10)
    histfit(Est.err(5,:))
    xlabel('$[rad]$','Interpreter','latex')
    title('Camera Post-fit residual','Interpreter','latex')   
    
%   Position deviations and errors

    err_States  = Est.x_t(1:6,:)';
    
    figure()
    subplot(1,3,1)
    plot(t_obs/3600,-err_States(:,1),'*','Color','b')
    hold on
    grid on
    plot(t_obs/3600,3*squeeze(real(sqrt(Est.P_t(1,1,:)))),'--','Color','b')
    plot(t_obs/3600,-3*squeeze(real(sqrt(Est.P_t(1,1,:)))),'--','Color','b')
    xlabel('time $[hour]$','FontSize',16,'Interpreter','latex')
    ylabel('$\Delta X$ (km)','FontSize',16,'Interpreter','latex')
    subplot(1,3,2)
    plot(t_obs/3600,-err_States(:,2),'*','Color','r')
    hold on
    grid on
    plot(t_obs/3600,3*squeeze(real(sqrt(Est.P_t(2,2,:)))),'--','Color','r')
    plot(t_obs/3600,-3*squeeze(real(sqrt(Est.P_t(2,2,:)))),'--','Color','r')
    xlabel('time $[hour]$','FontSize',16,'Interpreter','latex')
    ylabel('$\Delta Y$ (km)','FontSize',16,'Interpreter','latex')
    subplot(1,3,3)
    plot(t_obs/3600,-err_States(:,3),'*','Color','g')
    hold on
    grid on
    plot(t_obs/3600,3*squeeze(real(sqrt(Est.P_t(3,3,:)))),'--','Color','g')
    plot(t_obs/3600,-3*squeeze(real(sqrt(Est.P_t(3,3,:)))),'--','Color','g')
    xlabel('time $[hour]$','FontSize',16,'Interpreter','latex')
    ylabel('$\Delta Z$ (km)','FontSize',16,'Interpreter','latex')


%   speed deviations and errors
    figure()
    subplot(1,3,1)
    plot(t_obs/3600,-err_States(:,4),'*','Color','b')
    hold on
    grid on
    plot(t_obs/3600,3*squeeze(real(sqrt(Est.P_t(4,4,:)))),'--','Color','b')
    plot(t_obs/3600,-3*squeeze(real(sqrt(Est.P_t(4,4,:)))),'--','Color','b')
    xlabel('time $[hour]$','FontSize',16,'Interpreter','latex')
    ylabel('$\Delta V_x$ (km/s)','FontSize',16,'Interpreter','latex')
    subplot(1,3,2)
    plot(t_obs/3600, -err_States(:,5),'*','Color','r')
    hold on
    grid on
    plot(t_obs/3600,3*squeeze(real(sqrt(Est.P_t(5,5,:)))),'--','Color','r')
    plot(t_obs/3600,-3*squeeze(real(sqrt(Est.P_t(5,5,:)))),'--','Color','r')
    xlabel('time $[hour]$','FontSize',16,'Interpreter','latex')
    ylabel('$\Delta V_y$ (km/s)','FontSize',16,'Interpreter','latex')
    subplot(1,3,3)
    plot(t_obs/3600, -err_States(:,6),'*','Color','g')
    hold on
    grid on
    plot(t_obs/3600,3*squeeze(real(sqrt(Est.P_t(6,6,:)))),'--','Color','g')
    plot(t_obs/3600,-3*squeeze(real(sqrt(Est.P_t(6,6,:)))),'--','Color','g')
    xlabel('time $[hour]$','FontSize',16,'Interpreter','latex')
    ylabel('$\Delta V_z$ (km/s)','FontSize',16,'Interpreter','latex')
    
    
%   3sigma Envelopes
    figure()
    subplot(1,2,1)
    semilogy(t_obs/3600,3.*squeeze(real(sqrt(Est.P_t(1,1,:)))),'Color','b','LineWidth',1)
    hold on;
    grid on;
    semilogy(t_obs/3600,3*squeeze(real(sqrt(Est.P_t(2,2,:)))),'Color','r','LineWidth',1)
    semilogy(t_obs/3600,3*squeeze(real(sqrt(Est.P_t(3,3,:)))),'Color','g','LineWidth',1)
    xlabel('time $[hour]$','FontSize',16,'Interpreter','latex')
    ylabel('Position vector $3\sigma$ envelopes (km)','FontSize',16,'Interpreter','latex')
    legend('$X_x$','$X_y$','$X_z$','FontSize',14,'Interpreter','latex')
    subplot(1,2,2)
    semilogy(t_obs/3600,3*squeeze(real(sqrt(Est.P_t(4,4,:)))),'Color','b','LineWidth',1)
    hold on;
    grid on;
    semilogy(t_obs/3600,3*squeeze(real(sqrt(Est.P_t(5,5,:)))),'Color','r','LineWidth',1)
    semilogy(t_obs/3600,3*squeeze(real(sqrt(Est.P_t(6,6,:)))),'Color','g','LineWidth',1)
    xlabel('time $[hour]$','FontSize',16,'Interpreter','latex')
    ylabel('Velocity vector $3\sigma$ envelopes (km/s)','FontSize',16,'Interpreter','latex')
    legend('$V_x$','$V_y$','$V_z$','FontSize',14,'Interpreter','latex')
    
%   Square-Root of the MMX covariance error

    SQRT_X = zeros(size(Est.P_t,3),1);
    SQRT_V = zeros(size(Est.P_t,3),1);
    
for i=1:size(Est.P_t,3)
    
    SQRT_X(i) = sqrt(3*trace(squeeze(real(Est.P_t(1:3,1:3,i)))));
    SQRT_V(i) = sqrt(3*trace(squeeze(real(Est.P_t(4:6,4:6,i)))));
    
end

    figure()
    subplot(1,2,1)
    semilogy(t_obs/3600,SQRT_X,'Color','b','LineWidth',1)
    xlabel('time $[hour]$','Interpreter','latex')
    ylabel('MMX Position Vector $\sqrt{\sigma_{x}^2+\sigma_{y}^2+\sigma_{z}^2}$','Interpreter','latex')
    grid on;
    subplot(1,2,2)
    semilogy(t_obs/3600,SQRT_V,'Color','b','LineWidth',1)
    xlabel('time $[hour]$','Interpreter','latex')
    ylabel('MMX Position Vector $\sqrt{\sigma_{x}^2+\sigma_{y}^2+\sigma_{z}^2}$','Interpreter','latex')    
    grid on;
    
    
%   PHOBOS Position deviations and errors

    err_States  = Est.x_t(7:12,:)';

    figure()
    subplot(1,3,1)
    plot(t_obs/3600,-err_States(:,1),'*','Color','b')
    hold on;
    grid on;
    plot(t_obs/3600,3*squeeze(real(sqrt(Est.P_t(7,7,:)))),'--','Color','b')
    plot(t_obs/3600,-3*squeeze(real(sqrt(Est.P_t(7,7,:)))),'--','Color','b')
    xlabel('time $[hour]$','FontSize',16,'Interpreter','latex')
    ylabel('$\Delta X$ (km)','FontSize',16,'Interpreter','latex')
    subplot(1,3,2)
    plot(t_obs/3600,-err_States(:,2),'*','Color','r')
    hold on;
    grid on;
    plot(t_obs/3600,3*squeeze(real(sqrt(Est.P_t(8,8,:)))),'--','Color','r')
    plot(t_obs/3600,-3*squeeze(real(sqrt(Est.P_t(8,8,:)))),'--','Color','r')
    xlabel('time $[hour]$','FontSize',16,'Interpreter','latex')
    ylabel('$\Delta Y$ (km)','FontSize',16,'Interpreter','latex')
    subplot(1,3,3)
    plot(t_obs/3600,-err_States(:,3),'*','Color','g')
    hold on;
    grid on;
    plot(t_obs/3600,3*squeeze(real(sqrt(Est.P_t(9,9,:)))),'--','Color','g')
    plot(t_obs/3600,-3*squeeze(real(sqrt(Est.P_t(9,9,:)))),'--','Color','g')
    xlabel('time $[hour]$','FontSize',16,'Interpreter','latex')
    ylabel('$\Delta Z$ (km)','FontSize',16,'Interpreter','latex')


%   speed deviations and errors
    figure()
    subplot(1,3,1)
    plot(t_obs/3600,-err_States(:,4),'*','Color','b')
    hold on;
    grid on;
    plot(t_obs/3600,3*squeeze(real(sqrt(Est.P_t(10,10,:)))),'--','Color','b')
    plot(t_obs/3600,-3*squeeze(real(sqrt(Est.P_t(10,10,:)))),'--','Color','b')
    xlabel('time $[hour]$','FontSize',16,'Interpreter','latex')
    ylabel('$\Delta V_x$ (km/s)','FontSize',16,'Interpreter','latex')
    subplot(1,3,2)
    plot(t_obs/3600, -err_States(:,5),'*','Color','r')
    hold on;
    grid on;
    plot(t_obs/3600,3*squeeze(real(sqrt(Est.P_t(11,11,:)))),'--','Color','r')
    plot(t_obs/3600,-3*squeeze(real(sqrt(Est.P_t(11,11,:)))),'--','Color','r')
    xlabel('time $[hour]$','FontSize',16,'Interpreter','latex')
    ylabel('$\Delta V_y$ (km/s)','FontSize',16,'Interpreter','latex')
    subplot(1,3,3)
    plot(t_obs/3600, -err_States(:,6),'*','Color','g')
    hold on;
    grid on;
    plot(t_obs/3600,3*squeeze(real(sqrt(Est.P_t(12,12,:)))),'--','Color','g')
    plot(t_obs/3600,-3*squeeze(real(sqrt(Est.P_t(12,12,:)))),'--','Color','g')
    xlabel('time $[hour]$','FontSize',16,'Interpreter','latex')
    ylabel('$\Delta V_z$ (km/s)','FontSize',16,'Interpreter','latex')


%   3sigma Envelopes
    figure()
    subplot(1,2,1)
    semilogy(t_obs/3600,3.*squeeze(real(sqrt(Est.P_t(7,7,:)))),'Color','b','LineWidth',1)
    hold on
    grid on
    semilogy(t_obs/3600,3*squeeze(real(sqrt(Est.P_t(8,8,:)))),'Color','r','LineWidth',1)
    semilogy(t_obs/3600,3*squeeze(real(sqrt(Est.P_t(9,9,:)))),'Color','g','LineWidth',1)
    xlabel('time $[hour]$','FontSize',16,'Interpreter','latex')
    ylabel('Position vector $3\sigma$ envelopes (km)','FontSize',16,'Interpreter','latex')
    legend('$X_x$','$X_y$','$X_z$','FontSize',14,'Interpreter','latex')
    subplot(1,2,2)
    semilogy(t_obs/3600,3*squeeze(real(sqrt(Est.P_t(10,10,:)))),'Color','b','LineWidth',1)
    hold on
    grid on
    semilogy(t_obs/3600,3*squeeze(real(sqrt(Est.P_t(11,11,:)))),'Color','r','LineWidth',1)
    semilogy(t_obs/3600,3*squeeze(real(sqrt(Est.P_t(12,12,:)))),'Color','g','LineWidth',1)
    xlabel('time $[hour]$','FontSize',16,'Interpreter','latex')
    ylabel('Velocity vector $3\sigma$ envelopes (km/s)','FontSize',16,'Interpreter','latex')
    legend('$V_x$','$V_y$','$V_z$','FontSize',14,'Interpreter','latex')

    % %   Phobos Envelopes
    %     figure()
    %     subplot(2,3,1)
    %     semilogy(t_obs/3600,3*squeeze(real(sqrt(Est.P_t(7,7,:)))),'Color','b','LineWidth',1)
    %     hold on;
    %     grid on;
    %     xlabel('time $[hour]$','FontSize',16,'Interpreter','latex')
    %     legend('$X_{x}$','FontSize',14,'Interpreter','latex')
    %     subplot(2,3,2)
    %     semilogy(t_obs/3600,3*squeeze(real(sqrt(Est.P_t(8,8,:)))),'Color','r','LineWidth',1)
    %     hold on;
    %     grid on;
    %     xlabel('time $[hour]$','FontSize',16,'Interpreter','latex')
    %     legend('$X_{y}$','FontSize',14,'Interpreter','latex')
    %     subplot(2,3,3)
    %     semilogy(t_obs/3600,3*squeeze(real(sqrt(Est.P_t(9,9,:)))),'Color','g','LineWidth',1)
    %     hold on;
    %     grid on;
    %     xlabel('time $[hour]$','FontSize',16,'Interpreter','latex')
    %     legend('$X_{z}$','FontSize',14,'Interpreter','latex')
    %     subplot(2,3,4)
    %     semilogy(t_obs/3600,3*squeeze(real(sqrt(Est.P_t(10,10,:)))),'Color','b','LineWidth',1)
    %     hold on;
    %     grid on;
    %     xlabel('time $[hour]$','FontSize',16,'Interpreter','latex')
    %     legend('$V_{x}$','FontSize',14,'Interpreter','latex')
    %     subplot(2,3,5)
    %     semilogy(t_obs/3600,3*squeeze(real(sqrt(Est.P_t(11,11,:)))),'Color','r','LineWidth',1)
    %     hold on;
    %     grid on;
    %     xlabel('time $[hour]$','FontSize',16,'Interpreter','latex')
    %     legend('$V_{y}$','FontSize',14,'Interpreter','latex')
    %     subplot(2,3,6)
    %     semilogy(t_obs/3600,3*squeeze(real(sqrt(Est.P_t(12,12,:)))),'Color','g','LineWidth',1)
    %     hold on;
    %     grid on;
    %     xlabel('time $[hour]$','FontSize',16,'Interpreter','latex')
    %     legend('$V_{z}$','FontSize',14,'Interpreter','latex')



    %   Square-Root of the MMX covariance error

        SQRT_X = zeros(size(Est.P_t,3),1);
        SQRT_V = zeros(size(Est.P_t,3),1);

    for i=1:size(Est.P_t,3)

        SQRT_X(i) = sqrt(3*trace(squeeze(real(Est.P_t(7:9,7:9,i)))));
        SQRT_V(i) = sqrt(3*trace(squeeze(real(Est.P_t(10:12,10:12,i)))));

    end

        figure()
        subplot(1,2,1)
        semilogy(t_obs/3600,SQRT_X,'Color','b','LineWidth',1)
        xlabel('time $[hour]$','Interpreter','latex')
        ylabel('Phobos Position Vector $\sqrt{\sigma_{x}^2+\sigma_{y}^2+\sigma_{z}^2}$','Interpreter','latex')
        grid on;
        subplot(1,2,2)
        semilogy(t_obs/3600,SQRT_V,'Color','b','LineWidth',1)
        xlabel('time $[hour]$','Interpreter','latex')
        ylabel('Phobos Velocity Vector $\sqrt{\sigma_{x}^2+\sigma_{y}^2+\sigma_{z}^2}$','Interpreter','latex')
        grid on;
       
        
        
    
    % %   Harmonics coefficients deviations and errors
    % 
    %     err_States  = Est.x_t(13:37)';
    %     
    %     figure()
    %     subplot(1,5,1)
    %     plot(t_obs/3600,-err_States(:,16),'*','Color','b')
    %     hold on;
    %     grid on;
    %     plot(t_obs/3600,3*squeeze(real(sqrt(Est.P_t(16,16,:)))),'--','Color','b')
    %     plot(t_obs/3600,-3*squeeze(real(sqrt(Est.P_t(16,16,:)))),'--','Color','b')
    %     xlabel('time $[hour]$','FontSize',16,'Interpreter','latex')
    %     ylabel('$\Delta C_{20}$','FontSize',16,'Interpreter','latex')
    %     subplot(1,5,2)
    %     plot(t_obs/3600,-err_States(:,18),'*','Color','r')
    %     hold on;
    %     grid on;
    %     plot(t_obs/3600,3*squeeze(real(sqrt(Est.P_t(18,18,:)))),'--','Color','r')
    %     plot(t_obs/3600,-3*squeeze(real(sqrt(Est.P_t(18,18,:)))),'--','Color','r')
    %     xlabel('time $[hour]$','FontSize',16,'Interpreter','latex')
    %     ylabel('$\Delta C_{22}$','FontSize',16,'Interpreter','latex')
    %     subplot(1,5,3)
    %     plot(t_obs/3600,-err_States(:,23),'*','Color','g')
    %     hold on;
    %     grid on;
    %     plot(t_obs/3600,3*squeeze(real(sqrt(Est.P_t(23,23,:)))),'--','Color','g')
    %     plot(t_obs/3600,-3*squeeze(real(sqrt(Est.P_t(23,23,:)))),'--','Color','g')
    %     xlabel('time $[hour]$','FontSize',16,'Interpreter','latex')
    %     ylabel('$\Delta C_{40}$','FontSize',16,'Interpreter','latex')
    %     subplot(1,5,4)
    %     plot(t_obs/3600,-err_States(:,25),'*','Color','m')
    %     hold on;
    %     grid on;
    %     plot(t_obs/3600,3*squeeze(real(sqrt(Est.P_t(25,25,:)))),'--','Color','m')
    %     plot(t_obs/3600,-3*squeeze(real(sqrt(Est.P_t(25,25,:)))),'--','Color','m')
    %     xlabel('time $[hour]$','FontSize',16,'Interpreter','latex')
    %     ylabel('$\Delta C_{42}$','FontSize',16,'Interpreter','latex')
    %     subplot(1,5,5)
    %     plot(t_obs/3600,-err_States(:,27),'*','Color','k')
    %     hold on;
    %     grid on;
    %     plot(t_obs/3600,3*squeeze(real(sqrt(Est.P_t(27,27,:)))),'--','Color','k')
    %     plot(t_obs/3600,-3*squeeze(real(sqrt(Est.P_t(27,27,:)))),'--','Color','k')
    %     xlabel('time $[hour]$','FontSize',16,'Interpreter','latex')
    %     ylabel('$\Delta C_{44}$','FontSize',16,'Interpreter','latex')


%   Harmonics 3sigma Envelopes

    place0 = size(Est0.X,1)-pars.nCoeff;

    figure()
    subplot(1,5,1)
    semilogy(t_obs/3600,3.*squeeze(real(sqrt(Est.P_t(place0+1,place0+1,:)))),'Color','b','LineWidth',1)
    grid on;
    hold on;
    xlabel('time $[hour]$','FontSize',16,'Interpreter','latex')
    legend('$C_{00}$','FontSize',14,'Interpreter','latex')
    subplot(1,5,2)
    semilogy(t_obs/3600,3.*squeeze(real(sqrt(Est.P_t(place0+2,place0+2,:)))),'Color','b','LineWidth',1)
    hold on;
    grid on;
    semilogy(t_obs/3600,3.*squeeze(real(sqrt(Est.P_t(place0+3,place0+3,:)))),'Color','r','LineWidth',1)
    xlabel('time $[hour]$','FontSize',16,'Interpreter','latex')
    legend('$C_{10}$','$C_{11}$','FontSize',14,'Interpreter','latex')
    subplot(1,5,3)
    semilogy(t_obs/3600,3.*squeeze(real(sqrt(Est.P_t(place0+4,place0+4,:)))),'Color','b','LineWidth',1)
    hold on;
    grid on;
    semilogy(t_obs/3600,3.*squeeze(real(sqrt(Est.P_t(place0+5,place0+5,:)))),'Color','r','LineWidth',1)
    semilogy(t_obs/3600,3.*squeeze(real(sqrt(Est.P_t(place0+6,place0+6,:)))),'Color','g','LineWidth',1)
    xlabel('time $[hour]$','FontSize',16,'Interpreter','latex')
    legend('$C_{20}$','$C_{21}$','$C_{21}$','FontSize',14,'Interpreter','latex')
    subplot(1,5,4)
    semilogy(t_obs/3600,3.*squeeze(real(sqrt(Est.P_t(place0+7,place0+7,:)))),'Color','b','LineWidth',1)
    hold on;
    grid on;
    semilogy(t_obs/3600,3.*squeeze(real(sqrt(Est.P_t(place0+8,place0+8,:)))),'Color','r','LineWidth',1)
    semilogy(t_obs/3600,3.*squeeze(real(sqrt(Est.P_t(place0+9,place0+9,:)))),'Color','g','LineWidth',1)
    semilogy(t_obs/3600,3.*squeeze(real(sqrt(Est.P_t(place0+10,place0+10,:)))),'Color','m','LineWidth',1)
    xlabel('time $[hour]$','FontSize',16,'Interpreter','latex')
    legend('$C_{30}$','$C_{31}$','$C_{32}$','$C_{33}$','FontSize',14,'Interpreter','latex')
    subplot(1,5,5)
    semilogy(t_obs/3600,3*squeeze(real(sqrt(Est.P_t(place0+11,place0+11,:)))),'Color','b','LineWidth',1)
    hold on;
    grid on;
    semilogy(t_obs/3600,3*squeeze(real(sqrt(Est.P_t(place0+12,place0+12,:)))),'Color','r','LineWidth',1)
    semilogy(t_obs/3600,3*squeeze(real(sqrt(Est.P_t(place0+13,place0+13,:)))),'Color','g','LineWidth',1)
    semilogy(t_obs/3600,3*squeeze(real(sqrt(Est.P_t(place0+14,place0+14,:)))),'Color','m','LineWidth',1)
    semilogy(t_obs/3600,3*squeeze(real(sqrt(Est.P_t(place0+15,place0+15,:)))),'Color','k','LineWidth',1)
    xlabel('time $[hour]$','FontSize',16,'Interpreter','latex')
    legend('$C_{40}$','$C_{41}$','$C_{42}$','$C_{43}$','$C_{44}$','FontSize',14,'Interpreter','latex')


    figure()
    subplot(1,4,1)
    semilogy(t_obs/3600,3.*squeeze(real(sqrt(Est.P_t(place0+16,place0+16,:)))),'Color','b','LineWidth',1)
    grid on;
    hold on;
    xlabel('time $[hour]$','FontSize',16,'Interpreter','latex')
    legend('$S_{11}$','FontSize',14,'Interpreter','latex')
    subplot(1,4,2)
    semilogy(t_obs/3600,3.*squeeze(real(sqrt(Est.P_t(place0+17,place0+17,:)))),'Color','b','LineWidth',1)
    hold on;
    grid on;
    semilogy(t_obs/3600,3.*squeeze(real(sqrt(Est.P_t(place0+18,place0+18,:)))),'Color','r','LineWidth',1)
    xlabel('time $[hour]$','FontSize',16,'Interpreter','latex')
    legend('$S_{21}$','$S_{22}$','FontSize',14,'Interpreter','latex')
    subplot(1,4,3)
    semilogy(t_obs/3600,3.*squeeze(real(sqrt(Est.P_t(place0+19,place0+19,:)))),'Color','b','LineWidth',1)
    hold on;
    grid on;
    semilogy(t_obs/3600,3.*squeeze(real(sqrt(Est.P_t(place0+20,place0+20,:)))),'Color','r','LineWidth',1)
    semilogy(t_obs/3600,3.*squeeze(real(sqrt(Est.P_t(place0+21,place0+21,:)))),'Color','g','LineWidth',1)
    xlabel('time $[hour]$','FontSize',16,'Interpreter','latex')
    legend('$S_{31}$','$S_{32}$','$S_{33}$','FontSize',14,'Interpreter','latex')
    subplot(1,4,4)
    semilogy(t_obs/3600,3.*squeeze(real(sqrt(Est.P_t(place0+22,place0+22,:)))),'Color','b','LineWidth',1)
    hold on;
    grid on;
    semilogy(t_obs/3600,3.*squeeze(real(sqrt(Est.P_t(place0+23,place0+23,:)))),'Color','r','LineWidth',1)
    semilogy(t_obs/3600,3.*squeeze(real(sqrt(Est.P_t(place0+24,place0+24,:)))),'Color','g','LineWidth',1)
    semilogy(t_obs/3600,3.*squeeze(real(sqrt(Est.P_t(place0+25,place0+25,:)))),'Color','m','LineWidth',1)
    xlabel('time $[hour]$','FontSize',16,'Interpreter','latex')
    legend('$S_{41}$','$S_{42}$','$S_{43}$','$S_{44}$','FontSize',14,'Interpreter','latex')
