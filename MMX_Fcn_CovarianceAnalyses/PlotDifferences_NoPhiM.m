function PlotDifferences_NoPhiM(Est,YObs_Full,pars,units)


    set(0,'DefaultTextInterpreter','latex');
    set(0,'DefaultAxesFontSize', 16);


   t_obs = YObs_Full(1,:);

    [Ph, pars]   = Phobos_States_NewModel(pars.et0,pars);
    MMX0    = cspice_spkezr('-34',pars.et0,'MarsIAU','none','MARS');
    St0     = [MMX0./units.sfVec; Ph./units.sfVec2; pars.IPhx_bar; pars.IPhy_bar; pars.IPhz_bar];
    tspan   = t_obs*units.tsf;
    RelTol  = 1e-13;
    AbsTol  = 1e-16; 
    opt     = odeset('RelTol',RelTol,'AbsTol',AbsTol);
    [~,X]   = ode113(@(t,X) Dynamics_MPHandMMX_Inertia(t,X,pars,units),tspan,St0,opt);
    Phobos_true = X(:,7:14)'.*units.sfVec2;
    Phobos_true = [Phobos_true(1:4,:); Phobos_true(7:8,:)];
    MMX_true    = X(:,1:6)'.*units.sfVec;
    MMX_BSP     = cspice_spkezr('-34',pars.et0+t_obs,'MARSIAU','none','MARS');    

%   Errors between true values and estimates
    err_MMX         = MMX_BSP - Est.X_t(1:6,:);
    err_Phobos      = Phobos_true - [Est.X_t(7:12,:)];
    err_Coeff       = Est.X_t(13:15,:) - [pars.IPhx_bar; pars.IPhy_bar; pars.IPhz_bar];
    err_bias        = Est.X_t(16:end,:);

%%  Plote of the results

%   MMX

    %   MMX

    figure(99)
    subplot(3,1,1)
    semilogy(t_obs/3600,abs(err_MMX(1,:)),'*','Color','b')
    hold on
    grid on
    semilogy(t_obs/3600,3*squeeze(real(sqrt(Est.P_t(1,1,:)))),'--','Color','b')
    xlabel('time[hour]')
    ylabel('$\Delta$ x [km]')
    subplot(3,1,2)
    semilogy(t_obs/3600,abs(err_MMX(2,:)),'*','Color','r')
    hold on
    grid on
    semilogy(t_obs/3600,3*squeeze(real(sqrt(Est.P_t(2,2,:)))),'--','Color','r')
    xlabel('time[hour]')
    ylabel('$\Delta$ y [km]')
    subplot(3,1,3)
    semilogy(t_obs/3600,abs(err_MMX(3,:)),'*','Color','g')
    hold on
    grid on
    semilogy(t_obs/3600,3*squeeze(real(sqrt(Est.P_t(3,3,:)))),'--','Color','g')
    xlabel('time[hour]')
    ylabel('$\Delta$ z [km]')

    figure(98)
    subplot(3,1,1)
    semilogy(t_obs/3600,abs(err_MMX(4,:)),'*','Color','b')
    hold on
    grid on
    semilogy(t_obs/3600,3*squeeze(real(sqrt(Est.P_t(4,4,:)))),'--','Color','b')
    xlabel('time[hour]')
    ylabel('$\Delta\dot{x}$ [km/s]')
    subplot(3,1,2)
    semilogy(t_obs/3600, abs(err_MMX(5,:)),'*','Color','r')
    hold on
    grid on
    semilogy(t_obs/3600,3*squeeze(real(sqrt(Est.P_t(5,5,:)))),'--','Color','r')
    xlabel('time[hour]')
    ylabel('$\Delta\dot{y}$ [km/s]')
    subplot(3,1,3)
    semilogy(t_obs/3600, abs(err_MMX(6,:)),'*','Color','g')
    hold on
    grid on
    semilogy(t_obs/3600,3*squeeze(real(sqrt(Est.P_t(6,6,:)))),'--','Color','g')
    xlabel('time[hour]')
    ylabel('$\Delta\dot{z}$ [km/s]')

%   Phobos

    figure(97)
    subplot(2,3,1)
    semilogy(t_obs/3600,abs(err_Phobos(1,:)),'*','Color','b')
    hold on
    grid on
    semilogy(t_obs/3600,3*squeeze(real(sqrt(Est.P_t(7,7,:)))),'--','Color','b')
    xlabel('time[hour]')
    ylabel('$\Delta R_{Ph}$ [km]')
    subplot(2,3,2)
    semilogy(t_obs/3600,abs(err_Phobos(3,:)),'*','Color','r')
    hold on
    grid on
    semilogy(t_obs/3600,3*squeeze(real(sqrt(Est.P_t(9,9,:)))),'--','Color','r')
    xlabel('time[hour]')
    ylabel('$\Delta\theta$ [rad]')
    subplot(2,3,3)
    semilogy(t_obs/3600,abs(err_Phobos(5,:)),'*','Color','m')
    hold on
    grid on
    semilogy(t_obs/3600,3*squeeze(real(sqrt(Est.P_t(11,11,:)))),'--','Color','m')
    xlabel('time[hour]')
    ylabel('$\Delta\Phi_{Ph}$ [rad]')
    subplot(2,3,4)
    semilogy(t_obs/3600,abs(err_Phobos(2,:)),'*','Color','b')
    hold on
    grid on
    semilogy(t_obs/3600,3*squeeze(real(sqrt(Est.P_t(8,8,:)))),'--','Color','b')
    xlabel('time[hour]')
    ylabel('$\Delta\dot{R}_{Ph}$ [km/s]')
    subplot(2,3,5)
    semilogy(t_obs/3600, abs(err_Phobos(4,:)),'*','Color','r')
    hold on
    grid on
    semilogy(t_obs/3600,3*squeeze(real(sqrt(Est.P_t(10,10,:)))),'--','Color','r')
    xlabel('time[hour]')
    ylabel('$\Delta\dot{\theta}$ [rad]')
    subplot(2,3,6)
    semilogy(t_obs/3600, abs(err_Phobos(6,:)),'*','Color','m')
    hold on
    grid on
    semilogy(t_obs/3600,3*squeeze(real(sqrt(Est.P_t(12,12,:)))),'--','Color','m')
    xlabel('time[hour]')
    ylabel('$\Delta\dot{\Phi}_{Ph}$ [rad/s]')



%   Phobos's moment of inertia
    figure(96)
    subplot(1,3,1)
    semilogy(t_obs/60,abs(err_Coeff(1,:)),'*','Color','b')
    hold on
    grid on
    semilogy(t_obs/60,3*squeeze(real(sqrt(Est.P_t(13,13,:)))),'--','Color','b')
    xlabel('time[min]')
    ylabel('$\Delta I_{PhX}$')
    subplot(1,3,2)
    semilogy(t_obs/60,abs(err_Coeff(2,:)),'*','Color','r')
    hold on
    grid on
    semilogy(t_obs/60,3*squeeze(real(sqrt(Est.P_t(14,14,:)))),'--','Color','r')
    xlabel('time[min]')
    ylabel('$\Delta I_{PhY}$')
    subplot(1,3,3)
    semilogy(t_obs/60,abs(err_Coeff(3,:)),'*','Color','g')
    hold on
    grid on
    semilogy(t_obs/60,3*squeeze(real(sqrt(Est.P_t(15,15,:)))),'--','Color','g')
    xlabel('time[min]')
    ylabel('$\Delta I_{PhZ}$')


%   Bias
    figure(95)
    subplot(1,3,1)
    semilogy(t_obs/60,abs(err_bias(1,:)),'*','Color','b')
    hold on
    grid on
    semilogy(t_obs/60,3*squeeze(real(sqrt(Est.P_t(16,16,:)))),'--','Color','b')
    xlabel('time[min]')
    ylabel('$\Delta b_\rho$')
    subplot(1,3,2)
    semilogy(t_obs/60,abs(err_bias(2,:)),'*','Color','r')
    hold on
    grid on
    semilogy(t_obs/60,3*squeeze(real(sqrt(Est.P_t(17,17,:)))),'--','Color','r')
    xlabel('time[min]')
    ylabel('$\Delta b_{\dot{\rho}}$')
    subplot(1,3,3)
    semilogy(t_obs/60,abs(err_bias(3,:)),'*','Color','g')
    hold on
    grid on
    semilogy(t_obs/60,3*squeeze(real(sqrt(Est.P_t(18,18,:)))),'--','Color','g')
    xlabel('time[min]')
    ylabel('$\Delta b_{lidar}$')


end