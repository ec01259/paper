function [Est] = SRIF_SNC(Est0,f,G_dG,O,Measures,pars,units)
%==========================================================================
% [Est] = SRIF(Est0,f,G_dG,R,Measures,par)
%
% Square Root Inforrmation Filter
%
% INPUT: Description Units
%
% Est0      - Apriori initial guess of
%               .X,     States
%               .dx,    States' deviation vector
%               .P0,    Covariance matrix
%               .t0,    initial time  
% 
% @f        - Dynamical model
% 
% @G_DG     - Observation's model and partial derivatives wrt the states
% 
% O         - Weight matrix
% Measures  - Observations matrix: the first column contains observation
%               times, then every column has a different type of observaion
%               type
% par       - Structure containing parameters required by @f and @G_dG 
%
% OUTPUT:
%
% Est       - Estimation of:
%               .X, States at final t
%               .x, States' deviation vector at final t
%               .P, Covariance matrix at final t
%               .t, Time observation vector
%               .err, Post-fit residuals
%               .pre, Pre-fit residuals
%               .y_t, Pre-fit residuals
%               .X_t, States at different t
%               .x_t, States deviation vector at different t
%               .Phi_t, STM at different t
%               .P_t, Covariance matrix at different t  
%               .Pbar_t, Covariance matrix time update at different t
% 
%
% Coupling:
% None
%
% Author: Edoardo Ciccarelli
% Date: 01/2021
%
%==========================================================================


%   Apriori initial guess, deviation and covariance matrix.
    Xstar0  = Est0.X;
    dx0bar  = Est0.dx;
    P0bar   = Est0.P0;
    told    = Est0.t0;
    
%   Column of the measures time.
    tobs    = Measures(1,:);

%   The measures must be organized in colums depending on the type of
%   measure; each row is reffered to a given measure's time.
    St_Id   = Measures(2,:);
    obs     = Measures(3:end,:)./units.Observations;
    
%   n,l and m are respectively the number of states, number of different
%   types of measures and number of measures per each type. q represents the
%   dimension of the process noise vector
    n       = size(Xstar0,1);
    m       = size(obs,2);
    q       = 3;
    
%   err is the vector of residuals, the others are needed to build the 
%   fitting residuals.
    err     = NaN(size(O,1),m);
    prefit  = NaN(size(O,1),m);
    y_t     = NaN(size(O,1),m);
    X_t     = zeros(n,m);
    x_t     = zeros(n,m);
    Phi_t   = zeros(n,n,m);
    Gamma_t = zeros(n,q,m);
    P_t     = zeros(n,n,m);
    Pbar_t  = zeros(n,n,m);
    
%   Initialization
    Xold    = Xstar0;
    x_est   = dx0bar;
    Pold    = P0bar;
    R       = chol(inv(Pold));
    b       = R*x_est;
    Info    = zeros(size(Pold));
    
    
%   Initial conditions for the propagation
    Phi0    = eye(n);
    St0     = [Xold; reshape(Phi0,[n^2,1])];
    interval_Range      = pars.interval_Range;
    interval_Range_rate = pars.interval_Range_rate;
    interval_lidar      = pars.interval_lidar;
    interval_camera     = pars.interval_camera;
    min_interval        = min([interval_Range; interval_Range_rate; interval_lidar; interval_camera]);

    tspan   = (0:min_interval:tobs(end))/units.tsf;

%   Propagation of the reference trajectory
    RelTol  = 1e-13;
    AbsTol  = 1e-16;
    opt     = odeset('RelTol',RelTol,'AbsTol',AbsTol);
    [t,X]   = ode113(@(t,X) f(t,X,pars,units),tspan,St0,opt);
    fprintf('\nOrbit propagation complete...');
    
%   For-loop on the observations
    for i = 1:m
        
        deltaT  = (tobs(i)-told)/units.tsf;
        Q       = pars.sigma^2*eye(q);
        Ru      = chol(inv(Q));
        
        if tobs(i) == told-Est0.t0
            Xstar   = Xold;
            Phi     = eye(n);
            Phi_new = eye(n);
        elseif tobs(i) > told
            idx     = find(round(t*units.tsf)==tobs(i));
            Xstar   = X(idx,1:n)';
            Phi_old = Phi_new;
            Phi_new = reshape(X(idx,n+1:end),[n,n]);
            Phi     = Phi_new/Phi_old;
        else
            Phi_new = reshape(X(idx,n+1:end),[n,n]);
            Phi     = eye(n);
        end
          
%       Time update

        if deltaT < 300
            Gamma       = [eye(q)*deltaT^2/2; eye(q)*deltaT; zeros(n-6,q)];
            bu_bar      = zeros(q,1);
            x_bar       = dx0bar;
            R_tilda     = R/Phi;
            M           = [Ru, zeros(q,n), bu_bar; -R_tilda*Gamma, R_tilda, b];
            M           = HH(M);
            R_bar       = M(q+1:q+n,q+1:q+n);
            P_bar       = (R_bar'*R_bar)\eye(n);
            b_bar       = M(q+1:end,end);
        else
            Gamma       = [eye(q); eye(q); zeros(n-6,q)];
            x_bar       = dx0bar;
            R_bar       = HH(R/Phi);
            P_bar       = (R_bar'*R_bar)\eye(n);
            b_bar       = R_bar*x_bar;
        end
        
%       Observations
        
        where       = isnan(obs(:,i));
        count       = sum(~where);
        IDX         = [];
        Y           = zeros(count,1);
        W           = zeros(count, count);

        if count ~= 0
            for k=1:size(where,1)
                    if where(k)==0
                        IDX = [IDX; k];
                    end
            end
    
            for j=1:count
                Y(j)    = obs(IDX(j),i);
                W(j,j)  = O(IDX(j),IDX(j));
            end
        else
            Y           = obs(i,:);
            W           = O;
        end

        
        et          = Est0.t0*units.tsf + tobs(i);
        [G,H_tilde] = G_dG(et,[St_Id(i); obs(:,i)]',Xstar,pars,units);
        
        y           = (Y - G');

%       Whitening
        S           = chol(W);
        y_k         = S\y;
        H_k         = S\H_tilde;

        Info       = Info + Phi'*H_tilde'/W*H_tilde*Phi;
        
%       Measurement update
        A           = [R_bar, b_bar; H_k, y_k];
        A           = HH(A);
        R           = A(1:n,1:n);
        b           = A(1:n,end);
        
        x_est       = R\b;
        P           = (R'*R)\eye(n);
        
%       Next iteration preparation
        Xold        = Xstar;
        told        = tobs(i);
        
%       Storing the results


    if count ~= 0
        for j=1:count
            err(IDX(j),i)    = (y(j) - H_tilde(j,:)*x_bar)*units.Observations(IDX(j));
            prefit(IDX(j),i) = (y(j) - H_tilde(j,:)*x_bar)*units.Observations(IDX(j));
            y_t(IDX(j),i)    = y(j)*units.Observations(IDX(j));
        end
    else
        err(:,i)    = (y - H_tilde*x_est).*units.Observations;
        prefit(:,i) = (y - H_tilde*x_bar).*units.Observations;
        y_t(:,i)    = y.*units.Observations;
    end
    
        Phi_t(:,:,i)    = Phi;
        Gamma_t(:,:,i)  = Gamma;
        P_t(:,:,i)      = P.*units.CovDim;
        Pbar_t(:,:,i)   = P_bar.*units.CovDim;
        X_t(:,i)        = (Xstar + x_est).*units.Dim;
        x_t(:,i)        = x_est.*units.Dim;
        
    end

    fprintf('\nAnalysis complete!\n');    
    
%   Check on the observability of the problem
    Est.flagNO = 0;    
    if det(Info) < 0
            fprintf('Look, the problem is not observable!!');
            fprintf('The determinant of the information matrix is:');
            det(Info)
            Est.flagNO = 1;
    end
    
    e = eig(Info);
    
    if sum(e<0)>0
            fprintf('Look, the problem is not observable!!');
            fprintf('One of the eigenvalues of the information matrix is negative');
            Est.flagNO = 1;
    end
    
    
%   Initial states' estimation, deviation and covariance matrix
    Est.X       = (Xstar + x_est).*units.Dim;
    Est.x       = x_est.*units.Dim;
    Est.P       = P.*units.CovDim;
    
%   Residuals and fit-residuals at different observation times
    Est.t       = tobs;
    Est.err     = err;
    Est.pre     = prefit;
    Est.y_t     = y_t;
    Est.X_t     = X_t;
    Est.x_t     = x_t;
    Est.Phi_t   = Phi_t;
    Est.Gamma   = Gamma_t;
    Est.P_t     = P_t;
    Est.Pbar_t  = Pbar_t;
    
    
end