function dXdt = TwoBP(~,X,par)

    dXdt = zeros(6,1);

    dXdt(1:3) = X(4:6);
    dXdt(4:6) = -par.mu/norm(X(1:3))^3*X(1:3);


end