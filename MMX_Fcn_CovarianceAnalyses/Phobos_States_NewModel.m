function [Ph, par] = Phobos_States_NewModel(data,par)

    Phobos_0    = cspice_spkezr('-401', data, 'MARSIAU', 'none', '499');
   
    r           = Phobos_0(1:3);
    v           = Phobos_0(4:6);
    h           = cross(r,v);
    theta_dot   = norm(h)/norm(r)^2;
    vr          = dot(r,v)/norm(r);
   
%   For the true anomaly I need to move to the orbit's plane
    eh = h/norm(h);
    ey = cross(v,h)/par.mu - r/norm(r);
    ey = ey/norm(ey);
    Y  = cross(eh,ey)/norm(cross(eh,ey));
    par.perifocal2MARSIAU = [ey,Y,eh];
    par.MARSIAU2perifocal = par.perifocal2MARSIAU';

    Ph_0    = par.MARSIAU2perifocal*Phobos_0(1:3);
    theta   = atan2(Ph_0(2),Ph_0(1));

    Ph_0_ORB    = cspice_spkezr('401', data, 'MARPHOFRZ', 'none', '499');
    er          = -Ph_0_ORB(1:3)/norm(Ph_0_ORB(1:3));
    eh          = cross(Ph_0_ORB(1:3),Ph_0_ORB(4:6))/...
        norm(cross(Ph_0_ORB(1:3),Ph_0_ORB(4:6)));
    ey          = cross(eh,er);
    NO          = [er,ey,eh];
    no          = [-er,cross(eh,-er),eh];
    par.no      = no;

%   Mars's libration angle
    BN          = cspice_sxform('MARPHOFRZ','IAU_MARS',data);
    BO          = BN(1:3,1:3)*no;
    [Phi1,ea2,ea3]  = cspice_m2eul(BO,3,2,1);

    Wtilde      = -BN(4:6, 1:3)*BN(1:3, 1:3)';
    w_BN        = [-Wtilde(2,3); Wtilde(1,3); -Wtilde(1,2)];
    w_BO        = w_BN - BO*[0; 0; theta_dot];
    Phi1_dot    = (sin(ea3)*w_BO(2) + cos(ea3)*w_BO(3))/cos(ea2);

    
%   Phobos's libration angle
    par.PB = [0.999240107370417        0.0378550984643587       0.00928436010779999
            -0.0379006415569816          0.99927008291848       0.00477940934253335
            -0.00909665828350325      -0.00512766070913186         0.999945477465509];

    BN          = cspice_sxform('MARPHOFRZ','IAU_PHOBOS',data);
    R_In2PhPA   = par.PB*BN(1:3,1:3);
    BO          = R_In2PhPA*NO;
    [Phi2,ea2,ea3]  = cspice_m2eul(BO,3,2,1);

    Wtilde      = -BN(4:6, 1:3)*BN(1:3, 1:3)';
    w_BN        = [-Wtilde(2,3); Wtilde(1,3); -Wtilde(1,2)];
    w_BO        = w_BN - BO*[0; 0; theta_dot];
    Phi2_dot    = (sin(ea3)*w_BO(2) + cos(ea3)*w_BO(3))/cos(ea2);

%   New state model
    Ph = [norm(r); vr; theta; theta_dot; Phi1; Phi1_dot; Phi2; Phi2_dot];

end