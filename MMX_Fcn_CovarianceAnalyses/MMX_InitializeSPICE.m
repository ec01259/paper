function MMX_InitializeSPICE()

cspice_furnsh(which('naif0012.tls'));
cspice_furnsh(which('pck00010.tpc'));
cspice_furnsh(which('gm_de431.tpc'));
cspice_furnsh(which('de430.bsp'));
cspice_furnsh(which('earth_topo_050714_v2.tf'));
cspice_furnsh(which('earth_topo_050714.tf'));
cspice_furnsh(which('earth_topo_201023.tf'));
cspice_furnsh(which('earthstns_itrf93_201023.bsp'));
cspice_furnsh(which('earth_000101_221026_220802.bpc'));
cspice_furnsh(which('earth_200101_990628_predict.bpc'));
cspice_furnsh(which('earth_latest_high_prec.bpc'));


end