function dx = MP_Circular_all(~,x,par,units)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% dx = MP_Circular_all(t,x,par,units)
%
% Calculate the acceleration vector in the N-Body Problem with Mars and
% Phobos only and the derivative of the coefficients to be estimated
%
% Input:
% .t        Elapsed time since epoch (-)
% .x        State Vector 
% .pars     Problem Parameters
%           .d          Dimension of the Problem
%           .et0        Initial epoch (-)
%           .refcenter  Origin of Frame (e.g., '399')
%           .refframe   Coordinate Frame (e.g., 'ECLIPJ2000' or 'J2000')
%           .Ntb        No. of third bodies
%           .BodiesID   Bodies' SPICE ID (e.g., '10','399', ...)
%           .GM         Bodies' Gravitational Parameters (-)
%           .Nsh        No. of Spherical Harmonic Bodies
%           .SH_BodiesID Spherical Harmonic Bodies' SPICE ID (e.g., '10','399', ...)
%           .SH         Spherical Harmonic Structure
%           .lsf        Length scale factor (km)
%           .tsf        Time scale factor (s)
%           .STM        true or false depending if you calculate STM or not
%           .nCoeff     Number if coefficients in the state vector
%
% Output:
% .dx       Vector containing accelerations and STM (d x 2 + nCoeff, -)
%
% Author: E.Ciccarelli
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%  Dimensions

    d           = par.d;                 % Dimension of the problem
    n           = d/2;
    nCoeff      = par.nCoeff;
    nBias       = par.nBias;

%%  Radius
    r = x(1:n);     % Pos. vector wrt central body
    R = norm(r);    % Norm of pos. vector wrt central body
    
%% 	Spherical harmonic bodies


%   Calculate Phobos pos wrt refcenter
    rPh = x(d+1:d+n);
    RPh = norm(rPh);
    vPh = x(d+n+1:2*d);
    VPh = norm(vPh);
    
%   Calculate spacecraft pos wrt Phobos
    rsb = r - rPh;
    Rsb = norm(rsb);
    
%   Rotate vector in body-fixed coordinates
    i       = rPh/RPh;
    s       = vPh/VPh;
    k       = cross(i,s)/norm(cross(i,s));
    j       = cross(k,i);
    NB      = [i,j,k];
    BN      = NB';
    
%%  MMX 2BP with Mars + 3rd body Phobos 

    [gb, Gb, ~, GClm, GSlm] = CGM(BN*rsb, par.SH);
    g       = - par.GM1*r/R^3 + NB * gb - par.SH.GM*rPh/RPh^3;
    G       = - par.GM1*(eye(n)/R^3 - 3*(r*r')/R^5) + NB * Gb * BN;
    GdPh    = - par.SH.GM*(eye(n)/RPh^3 - 3*(rPh*rPh')/RPh^5)  - NB * Gb * BN;
    GdClm   = NB * GClm;
    GdSlm   = NB * GSlm;    
    
%%  Phobos Circular orbit

    gPh         = - (par.SH.GM + par.GM1)*rPh/RPh^3;
    GPh         = - (par.SH.GM + par.GM1)*(eye(3)/RPh^3 - 3*(rPh*rPh')/RPh^5);
    
%%  Jacobian Matrix


    A   = [zeros(n),   eye(n), zeros(n,d+nCoeff), zeros(n,nBias); ...
        G, zeros(n), GdPh, zeros(n), GdClm, GdSlm, zeros(n,nBias);...
        zeros(n,n+d), eye(n), zeros(n,nCoeff), zeros(n,nBias);...
        zeros(n,d), GPh, zeros(n,n+nCoeff), zeros(n,nBias);...
        zeros(nCoeff+nBias,2*d+nCoeff+nBias)];


%%  EOM


%   Equations of motion + variational equations
    Phi     = reshape(x(2*d+nCoeff+nBias+1:end),(2*d+nCoeff+nBias),(2*d+nCoeff+nBias));
    dPhi    = A * Phi;

%   1-6 are the MMX states, then 7-12 Phobos + variational equations
%     dx                                  = zeros((2*d+nCoeff+nBias),1);
    dx                                  = zeros((2*d+nCoeff+nBias)*(2*d+nCoeff+nBias+1),1);
    dx(1:n)                             = x(n+1:d);
    dx(n+1:d)                           = g;
    dx(d+1:d+n)                         = x(d+n+1:2*d);
    dx(d+n+1:2*d)                       = gPh;
    dx(2*d+1:2*d+nCoeff)                = 0;
    dx(2*d+nCoeff+1:2*d+nCoeff+nBias)   = 0;
    dx(2*d+nCoeff+nBias+1:end)          = dPhi(:);

 

    
end