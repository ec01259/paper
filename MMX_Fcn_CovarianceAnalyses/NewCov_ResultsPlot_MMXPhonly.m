function NewCov_ResultsPlot_MMXPhonly(Est, YObs_Full, pars, units)
%==========================================================================
% NewCov_ResultsPlot_MMXPhonly(Est, YObs_Full, pars)
%
% This function plots the results of the covariance analysis
%
% INPUT: Description Units
%
% Est       - Estimation of:
%               .X, States at final t
%               .x, States' deviation vector at final t
%               .P, Covariance matrix at final t
%               .t, Time observation vector
%               .err, Post-fit residuals
%               .pre, Pre-fit residuals
%               .y_t, Pre-fit residuals
%               .X_t, States at different t
%               .x_t, States deviation vector at different t
%               .Phi_t, STM at different t
%               .P_t, Covariance matrix at different t  
%               .Pbar_t, Covariance matrix time update at different t
% 
% YObs_Full     Vector of observations and time of observations
%
% pars       - Structure containing problem's parameters
%
%
% Coupling:
% None
%
% Author: Edoardo Ciccarelli
% Date: 09/2021
%
%==========================================================================

    set(0,'DefaultTextInterpreter','latex');
    set(0,'DefaultAxesFontSize', 16);

    t_obs   = YObs_Full(1,:);

%   Plot of the post-fit residuals
    
    figure()
    subplot(2,5,1)
    plot(t_obs/3600,Est.pre(1,:),'x','Color','b')
    grid on;
    hold on;
    plot(t_obs/3600,Est.pre(3,:),'x','Color','b')
    plot(t_obs/3600,Est.pre(5,:),'x','Color','b')
    plot(t_obs/3600,3*pars.ObsNoise.range*units.lsf*ones(size(Est.pre(1,:),2)),'.-','Color','b')
    plot(t_obs/3600,-3*pars.ObsNoise.range*units.lsf*ones(size(Est.pre(1,:),2)),'.-','Color','b')
    plot(t_obs/3600,3*pars.ObsNoise.range*units.lsf*ones(size(Est.pre(3,:),2)),'.-','Color','b')
    plot(t_obs/3600,-3*pars.ObsNoise.range*units.lsf*ones(size(Est.pre(3,:),2)),'.-','Color','b')
    plot(t_obs/3600,3*pars.ObsNoise.range*units.lsf*ones(size(Est.pre(5,:),2)),'.-','Color','b')
    plot(t_obs/3600,-3*pars.ObsNoise.range*units.lsf*ones(size(Est.pre(5,:),2)),'.-','Color','b')
    ylim([-10*pars.ObsNoise.range*units.lsf, 10*pars.ObsNoise.range*units.lsf])
    xlabel('$[hour]$')
    ylabel('$[km]$')
    title('Range Pre-fit residual')
    subplot(2,5,2)
    plot(t_obs/3600,Est.pre(2,:),'x','Color','r')
    grid on;
    hold on;
    plot(t_obs/3600,Est.pre(4,:),'x','Color','r')
    plot(t_obs/3600,Est.pre(6,:),'x','Color','r')
    plot(t_obs/3600,3*pars.ObsNoise.range_rate*units.vsf*ones(size(Est.pre(2,:),2)),'.-','Color','r')
    plot(t_obs/3600,-3*pars.ObsNoise.range_rate*units.vsf*ones(size(Est.pre(2,:),2)),'.-','Color','r')
    plot(t_obs/3600,3*pars.ObsNoise.range_rate*units.vsf*ones(size(Est.pre(4,:),2)),'.-','Color','r')
    plot(t_obs/3600,-3*pars.ObsNoise.range_rate*units.vsf*ones(size(Est.pre(4,:),2)),'.-','Color','r')
    plot(t_obs/3600,3*pars.ObsNoise.range_rate*units.vsf*ones(size(Est.pre(6,:),2)),'.-','Color','r')
    plot(t_obs/3600,-3*pars.ObsNoise.range_rate*units.vsf*ones(size(Est.pre(6,:),2)),'.-','Color','r')
    ylim([-10*pars.ObsNoise.range_rate*units.vsf, 10*pars.ObsNoise.range_rate*units.vsf])
    xlabel('$[hour]$')
    ylabel('$[km/s]$')    
    title('RangeRate Pre-fit residual') 
    subplot(2,5,3)
    plot(t_obs/3600,Est.pre(7,:),'x','Color','g')
    grid on;
    hold on;
    plot(t_obs/3600,3*pars.ObsNoise.lidar*units.lsf*ones(size(Est.pre(7,:),2)),'.-','Color','g')
    plot(t_obs/3600,-3*pars.ObsNoise.lidar*units.lsf*ones(size(Est.pre(7,:),2)),'.-','Color','g')
    ylim([-10*pars.ObsNoise.lidar*units.lsf, 10*pars.ObsNoise.lidar*units.lsf])
    xlabel('$[hour]$')
    ylabel('$[km]$')
    title('Lidar Pre-fit residual') 
    subplot(2,5,4)
    plot(t_obs/3600,Est.pre(8,:),'x','Color','m')
    grid on;
    hold on;
    plot(t_obs/3600,3*pars.ObsNoise.camera*ones(size(Est.pre(8,:),2)),'.-','Color','m')
    plot(t_obs/3600,-3*pars.ObsNoise.camera*ones(size(Est.pre(8,:),2)),'.-','Color','m')
    ylim([-10*pars.ObsNoise.camera, 10*pars.ObsNoise.camera])
    xlabel('$[hour]$')
    ylabel('$[rad]$')
    title('Camera Pre-fit residual') 
    subplot(2,5,5)
    plot(t_obs/3600,Est.pre(9,:),'x','Color','k')
    grid on;
    hold on;
    plot(t_obs/3600,3*pars.ObsNoise.camera*ones(size(Est.pre(9,:),2)),'.-','Color','k')
    plot(t_obs/3600,-3*pars.ObsNoise.camera*ones(size(Est.pre(9,:),2)),'.-','Color','k')
    ylim([-10*pars.ObsNoise.camera, 10*pars.ObsNoise.camera])
    xlabel('$[hour]$')
    ylabel('$[rad]$')
    title('Camera Pre-fit residual') 


    subplot(2,5,6)
    histfit([Est.pre(1,:), Est.pre(3,:), Est.pre(5,:)])
    xlabel('$[km]$')
    title('Range Pre-fit residual') 
    subplot(2,5,7)
    histfit([Est.pre(2,:),Est.pre(4,:), Est.pre(6,:)])
    xlabel('$[km/s]$')
    title('Range Rate Pre-fit residual') 
    subplot(2,5,8)
    histfit(Est.pre(7,:))
    xlabel('$[km]$')
    title('Lidar Pre-fit residual')
    subplot(2,5,9)
    histfit(Est.pre(8,:))
    xlabel('$[rad]$')
    title('Camera Pre-fit residual')
    subplot(2,5,10)
    histfit(Est.pre(9,:))
    xlabel('$[rad]$')
    title('Camera Pre-fit residual')


%   Square-Root of the MMX covariance error

    sqx = squeeze(Est.P_t(1,1,1:end-1));
    sqy = squeeze(Est.P_t(2,2,1:end-1));
    sqz = squeeze(Est.P_t(3,3,1:end-1));
    SQRT_X = 3*sqrt(sqx + sqy + sqz);
    sqvx = squeeze(Est.P_t(4,4,1:end-1));
    sqvy = squeeze(Est.P_t(5,5,1:end-1));
    sqvz = squeeze(Est.P_t(6,6,1:end-1));
    SQRT_V = 3*sqrt(sqvx + sqvy + sqvz);

    
%   3sigma Envelopes
    figure()
    subplot(1,2,1)
    semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(1,1,1:end-1)))),'Color','b','LineWidth',1)
    hold on;
    grid on;
    semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(Est.P_t(2,2,1:end-1)))),'Color','r','LineWidth',1)
    semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(Est.P_t(3,3,1:end-1)))),'Color','g','LineWidth',1)
    semilogy(t_obs(1:end-1)/3600,SQRT_X,'Color','k','LineWidth',2)
    xlabel('time $[hour]$')
    ylabel('$[km]$')
    title('MMX position vector $3\sigma$ envelopes','Interpreter','latex','FontSize',16)
    legend('$3\sigma_{x}$','$3\sigma_{y}$','$3\sigma_{z}$','$3 RMS$','Interpreter','latex','FontSize',14)
    subplot(1,2,2)
    semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(Est.P_t(4,4,1:end-1)))),'Color','b','LineWidth',1)
    hold on;
    grid on;
    semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(Est.P_t(5,5,1:end-1)))),'Color','r','LineWidth',1)
    semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(Est.P_t(6,6,1:end-1)))),'Color','g','LineWidth',1)
    semilogy(t_obs(1:end-1)/3600,SQRT_V,'Color','k','LineWidth',2)
    xlabel('time $[hour]$')
    ylabel('$[km/s]$')
    title('MMX velocity vector $3\sigma$ envelopes','Interpreter','latex','FontSize',16)
    legend('$3\sigma_{\dot{x}}$','$3\sigma_{\dot{y}}$','$3\sigma_{\dot{z}}$','$3 RMS$','Interpreter','latex','FontSize',14)


%   Phobos's states uncertainties evolution

%   3sigma Envelopes
    figure()
    subplot(2,4,1)
    semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(7,7,1:end-1)))),'Color','b','LineWidth',1)
    grid on
    xlabel('time [hour]')
    ylabel('$R_{Ph}$ [km]')
    subplot(2,4,2)
    semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(9,9,1:end-1)))),'Color','r','LineWidth',1)
    grid on
    xlabel('time [hour]')
    ylabel('$\theta_{Ph}$ [rad]')
    subplot(2,4,3)
    semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(11,11,1:end-1)))),'Color','g','LineWidth',1)
    grid on
    xlabel('time [hour]')
    ylabel('$\Phi_{M}$ [rad]')
    subplot(2,4,4)
    semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(13,13,1:end-1)))),'Color','m','LineWidth',1)
    grid on
    xlabel('time [hour]')
    ylabel('$\Phi_{Ph}$ [rad]')
    
    subplot(2,4,5)
    semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(8,8,1:end-1)))),'Color','b','LineWidth',1)
    grid on
    xlabel('time [hour]')
    ylabel('$\dot{R}_{Ph}$ [km/s]')
    subplot(2,4,6)
    semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(10,10,1:end-1)))),'Color','r','LineWidth',1)
    grid on
    xlabel('time [hour]')
    ylabel('$\dot{\theta}_{Ph}$ [rad/s]')
    subplot(2,4,7)
    semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(12,12,1:end-1)))),'Color','g','LineWidth',1)
    grid on
    xlabel('time [hour]')
    ylabel('$\dot{\Phi}_{M}$ [rad/s]')
    subplot(2,4,8)
    semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(14,14,1:end-1)))),'Color','m','LineWidth',1)
    grid on
    xlabel('time [hour]')
    ylabel('$\dot{\Phi}_{Ph}$ [rad/s]')

%   Harmonics 3sigma Envelopes

    corr_label = {'$x$','$y$','$z$','$\dot{x}$','$\dot{y}$','$\dot{z}$',...
        '$R_{Ph}$','$\dot{R}_{Ph}$','$\theta_{Ph}$','$\dot{\theta}_{Ph}$',...
        '$\Phi_{M}$','$\dot{\Phi}_{M}$','$\Phi_{Ph}$','$\dot{\Phi}_{Ph}$'};

%   Correlations coefficients

    [~,corr] = readCovMatrix(real(Est.P));

    figure();
    imagesc(corr)
    title('Correlation Coefficients','FontSize',16);
    set(gca,'FontSize',16);
    colormap(hot);
    colorbar;
    set(gca,'TickLabelInterpreter','latex');
    set(gca,'XTick',(1:size(Est.X,1)));
    set(gca,'XTickLabel',corr_label);
    set(gca,'YTick',(1:size(Est.X,1)));
    set(gca,'YTickLabel',corr_label);
    axis square;
    freezeColors;


end