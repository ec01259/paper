function [X1, Xph] = MMX_CalculateInitialConditionsInInertialFrame(X0, Model)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% [X1, Xph] = MMX_CalculateInitialConditionsInInertialFrame(X0, Model)
%
% Calculate Initial conditions of MMX in Inertial Frame
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%% Load SPICE kernels
MMX_InitializeSPICE;
cspice_furnsh(which('mar097.bsp'));



%% Calculate Phobos' initial orbit elements
et0 = cspice_str2et(Model.epoch);
GM12 = (Model.pars.GM1 + Model.pars.SH.GM);
coe = cspice_oscelt(cspice_spkezr('401', et0, 'MARSIAU', 'NONE', '499'), et0, GM12*Model.units.lsf^3/Model.units.tsf^2);



%% Enforce circular orbit
a   = Model.pars.a;
e   = 0.0;
M0  = mod(coe(5) + coe(6), 2*pi);
coe = [a; e; coe(3); coe(4); 0; M0];



%% Rotate to inertial frame
Xph = COE2RV(coe, GM12);
rph = Xph(1:3);
vph = Xph(4:6);
hph = cross(rph, vph);
eR = rph/norm(rph);
eH = hph/norm(hph);
eT = cross(eH, eR);
NS = [eR, eT, eH];



%% Calculate Initial Conditions in Mars-centered Inertial Frame
X1 = zeros(6,1);
X1(1:3) = NS*(X0(1:3) + [Model.pars.a; 0; 0]);
X1(4:6) = NS*(X0(4:6) + cross([0; 0; Model.pars.n], X0(1:3) + [Model.pars.a; 0; 0]));


%% Uload Spice kernels
cspice_kclear;
end