function [pars, units] = New_MMX_CovarianceAnalysisParameters(pars, units)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% [pars, units] = New_MMX_CovarianceAnalysisParameters(pars, units)
%
% This function adds some units and parameters useful to the covariance
% analysis
% 
% Inputs:
% pars          Structure which contains parameters
% units         Structure with units 
% 
% Outputs:
% pars.ObsNoise             Observations noises standard deviation
% pars.ObsNoise.range       Standard deviation of the relative distance measure
% pars.ObsNoise.range_rate  Standard deviation of the relative velocity measure
% pars.ObsNoise.lidar       Standard deviation of the lidar measure
% pars.ObsNoise.camera      Standard deviation of the camera measure
% pars.interval             Seconds between observations
% pars.Clm                  Vector with all the Clm coefficients
% pars.Slm                  Vector with all the Slm coefficients
% pars.nCoeff               Number of the harmonics coefficients
% pars.P0                   A-priori covariance
% pars.sigma                PN diffusion coefficient
%
% units.Dim                 Vector of normalizing units [sfVec, sfVec, ones(pars.nCoeff,1)]
% units.CovDim              Matrix of normalizing units
%
% Author: STAG Team
% Date: Jun 2021
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

%% Parameters
    
% Observation noise
pars.ObsNoise.range     = 5e-3/units.lsf;         % [km]
pars.ObsNoise.range_rate= 5e-7/units.vsf;         % [km/s]
pars.ObsNoise.lidar     = 1e-2/units.lsf;         % [km]

pars.FOV     = deg2rad(45);
pixels  = 2048;
pars.ObsNoise.camera    = pars.FOV/pixels;               % [rad/pixel]

% Useful to adimensionalize the observables
units.Observations = [units.lsf; units.vsf; units.lsf; units.vsf; units.lsf; units.vsf; units.lsf; 1; 1];

% Matrix of the observation noise
pars.R      = zeros(9,9);
pars.R(1,1) = pars.ObsNoise.range^2;
pars.R(2,2) = pars.ObsNoise.range_rate^2;
pars.R(3,3) = pars.ObsNoise.range^2;
pars.R(4,4) = pars.ObsNoise.range_rate^2;
pars.R(5,5) = pars.ObsNoise.range^2;
pars.R(6,6) = pars.ObsNoise.range_rate^2;
pars.R(7,7) = pars.ObsNoise.lidar^2;
pars.R(8,8) = pars.ObsNoise.camera^2;
pars.R(9,9) = pars.ObsNoise.camera^2;

% Seconds between observation
pars.interval_Range         = 3600;     % s
pars.interval_Range_rate    = 600;      % s
pars.interval_lidar         = 600;      % s
pars.interval_camera        = 3600;     % s


pars.cutoffLIDAR    = 200;  % km
pars.elevation      = 80;   % deg

% Gravitational harmonics' parameters
pars.I2 = [pars.IPhx_bar; pars.IPhy_bar; pars.IPhz_bar];

% Measurements biases
% bias = [0; 0; 0]...
%     ./[units.lsf; units.vsf; units.lsf];
% pars.bias = bias;

bias = [0; 0; 0; 0; 0]...
    ./[units.lsf; units.vsf; units.lsf; 1; 1];
pars.bias = bias;

% Unit vector and parameters number useful for later use
pars.nCoeff     = size(pars.I2,1);
pars.nBias      = size(bias,1);
% pars.nBias      = 0;
% units.Dim       = [units.sfVec; units.lsf; units.vsf; 1; units.tsf;...
%      1; units.tsf; 1; units.tsf; ones(pars.nCoeff,1); units.lsf; units.vsf;...
%      units.lsf];
units.Dim       = [units.sfVec; units.lsf; units.vsf; 1; units.tsf;...
     1; units.tsf; 1; units.tsf; ones(pars.nCoeff,1); units.lsf; units.vsf;...
     units.lsf; 1; 1];
units.CovDim    = (repmat(units.Dim, 1, length(units.Dim)).*...
    repmat(units.Dim', length(units.Dim), 1));


% A-priori covariance
k = 1e-1;
% pars.P0         = diag(([.3*ones(3,1); .3e-3*ones(3,1);...    
%     1e-1; 1e-4; 1e-5; 1e-7; 3e-1; 1e-3; 3e-1; 1e-3; 1e-2*ones(pars.nCoeff,1);...
%     1; 1e-3; 1]./units.Dim).^2);
pars.P0         = diag(([.3*ones(3,1); .3e-3*ones(3,1);...    
    1e-1; 1e-4; 1e-5; 1e-7; 3e-1; 1e-3; 3e-1; 1e-3; 1e-2*ones(pars.nCoeff,1);...
    1; 1e-3; 1; 1e-1; 1e-1]./units.Dim).^2);
% pars.P0         = diag(([.3*ones(3,1); .3e-3*ones(3,1);...    
%     1e-1; 1e-4; 1e-6; 1e-8; 3e-1; 1e-3; 3e-1; 1e-3; 1e-2*ones(pars.nCoeff,1)]./units.Dim).^2); 


% PN coefficients
SRP         = 4.5e-6;                                               %[Pa]
AU          = 149597871;                                            %[km]
phi         = 1367e-6;                                              %[W/km^2]
c           = 299792.458;                                           %[km/s]
VMMO_S      = 2e-4*3e-4;                                            %[km^2]
VMMO_M      = 24;                                                   %[kg]
pars.sigma  = SRP*VMMO_S/VMMO_M/(units.vsf*units.tsf);              %[km/s^2]

% r_VMM0      = cspice_spkezr('-34', pars.et0, 'J2000', 'none', 'Sun');
% 
% pars.sigma  = norm(-(phi/c)*AU^2*(VMMO_S/VMMO_M)*r_VMM0/norm(r_VMM0)^3)/(units.vsf*units.tsf);


end