function [Est] = Batch(Est0, f, G_dG, O, Measures, Niter, pars, units)
%==========================================================================
% [Est] = Batch(Est0,f,G_dG,O,Measures,Niter,par)
%
% Batch Processor
%
% INPUT: Description Units
%
% Est0      - Apriori initial guess of
%               .X, State's 
%               .dx0bar, deviation
%               .P0bar, Covariance matrix
%               .told, initial time  
% 
% @f        - Dynamical model
% 
% @G_DG     - Observation's model and partial derivatives wrt the states
% 
% R         - Weight matrix
% Measures  - Observations matrix: the first column contains observation
%               times, then every column has a different type of observaion
%               type
% 
% Niter     - Number of the processor's iterations
% 
% par       - Structure containing parameters required by @f and @G_dG 
%
% OUTPUT:
%
% Est       - Estimation of:
%               .X,     States at final t 
%               .dx,    States' deviation vector at final t
%               .P,     Covariance matrix at final t
%               .err,   Post-fit residuals
%               .pre,   Pre-fit residuals
%               .y_tot, Deviation from observations
%               .H_tot, H_tilde(t) concatenated in vertical
%               .P_t,   Covariance matrix behaviour in time  
% 
% 
% DO NOT FORGET TO USE RESHAPE LATER IN THE PROJECT
%
% Coupling:
% None
%
% Author: Edoardo Ciccarelli
% Date: 01/2021
%
%==========================================================================


%   Apriori initial guess, deviation and covariance matrix; told is the
%   time at which is required the state estimation.
    Xstar0      = Est0.X;
    x_bar       = Est0.dx;
    P0bar       = Est0.P0;
    told        = Est0.t0;
   
%   Column of the measures time.
    tobs        = Measures(:,1);
    
%   The measures must be organized in colums depending on the type of
%   measure; each row is reffered to a given measure's time.
    obs         = Measures(:,2:end);
    
%   n,l and m are respectively the number of states, number of different
%   types of measures and number of measures per each type.
    n           = size(Xstar0,1);
    m           = size(obs,1);
    l           = size(obs,2)-1;

%   err is the vector of post-fit residuals, the others are needed to build the 
%   fitting residuals.
    err         = zeros(n,size(tobs,1));
    prefit      = zeros(n,size(tobs,1));
    y_tot       = zeros(size(obs')-1);
    H_tot       = zeros(l*m,n);
    P0_i        = zeros(n,n,Niter);
    
%   Integration setup
    interval_DSN    = pars.interval_DSN;
    interval_lidar  = pars.interval_lidar;
    interval_camera = pars.interval_camera;
    min_interval    = min([interval_DSN; interval_lidar; interval_camera]);

    RelTol  = 1e-13;
    AbsTol  = 1e-16; 
    

%   For-loop on the number of iterations required
    for i = 1:Niter
        
        Lam     = inv(P0bar);
        N       = P0bar\x_bar;

        Phi0    = eye(n);
        St0     = [Xstar0; reshape(Phi0,[n^2,1])];

        tspan   = (0:min_interval:tobs(end))/units.tsf;
        opt     = odeset('RelTol',RelTol,'AbsTol',AbsTol);
        [t,X]   = ode113(@(t,X) f(t,X,pars,units),tspan,St0,opt);
        fprintf('\nOrbit propagation complete...');


%       For-loop on the observations
        for j = 1:size(tobs,1)

            idx     = find(round(t*units.tsf)==tobs(j));
            Xstar   = X(idx,1:n)';
            Phi     = reshape(X(idx,n+1:end),[n,n]);
            

%           Observations
            
            count       = sum(isnan(obs(j,:)));
            where       = isnan(obs(j,:));
            
            switch count
                case 0
                    Y = obs(j,2:end)./[units.lsf, units.vsf, units.lsf, 1, 1];
                    W = O;
                case 1
                    Y = [obs(j,2:3), obs(j,5:end)]./[units.lsf, units.vsf, 1, 1];
                    W = diag([O(1,1), O(2,2), O(4,4), O(5,5)]);
                case 2 
                    Y = obs(j,2:4)./[units.lsf, units.vsf, units.lsf];
                    W = O(1:3,1:3);
                case 3 
                    if where(1) == 0
                        Y = obs(j,2:3)./[units.lsf, units.vsf];
                        W = O(1:2,1:2);
                    else                    
                        Y = obs(j,4:end)./[units.lsf, 1, 1];
                        W = O(3:end,3:end);
                    end
                case 4 
                    Y = obs(j,5:end)./[1, 1];
                    W = O(4:5,4:5);
                case 5 
                    Y = obs(j,4)./units.lsf;
                    W = O(3,3);
            end
            
            k           = size(Y,2);
            et          = Est0.t0*units.tsf + tobs(j);
            [G,H_tilde] = G_dG(et,obs(j,:),Xstar,pars,units);
            
            y           = (Y - G)';
            H           = H_tilde*Phi;
                
            Lam         = Lam + H'/W*H;           
            N           = N + H'/W*y;
            
            ind         = k*(j-1)+1:k*j;
%             H_tot(ind,:)= H;
%             y_tot(:,j)  = y;
            
        end

        Est.flagNO = 0;
        
%       Check on the observability of the system.
        if det(Lam) < 0
            fprintf('\nLook, the problem is not observable!!');
            fprintf('\nThe determinant of the information matrix is:');
            det(Lam)
            Est.flagNO = 1;
        end
        
        e = eig(Lam);
        
        if sum(e<0)>0
                fprintf('\nLook, the problem is not observable!!');
                fprintf('\nOne of the eigenvalues of the information matrix is negative');
                Est.flagNO = 1;
        end

        x_est       = Lam\N;
        P0          = inv(Lam);

        Xstar0      = Xstar0 + x_est;
%         prefit      = reshape(y_tot(:) - H_tot*x_bar,[l,m]);
        x_bar       = x_bar - x_est;
%         err         = reshape(y_tot(:) - H_tot*x_bar,[l,m]);
%         P0_i(:,:,i) = P0;
        
    end
    
%   Initial states' estimation, deviation and covariance matrix
    Est.X0  = Xstar0.*units.Dim;
    Est.x   = x_bar.*units.Dim;
    Est.P0  = P0.*units.CovDim;
    
%   Residuals and fit-residuals at different observation times
    Est.err = err;
    Est.pre = prefit;
    Est.y   = y_tot;
    Est.H   = H_tot;
    Est.P0_i= P0_i.*units.CovDim;

end