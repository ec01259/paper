function [z, Err] = MMX_CheckRelativeErrorInSynodicFrame(x, y, Model)


%% Check Relative Error
z = MMX_FromInertial2SynodicFrame(y, Model);
h = figure();
PlotTrajectories(h, z, Model, false);



%% Plot Magnitude of Relative Error
Err = x - z;

figure()
semilogy(vecnorm(Err'))
ylabel('Relative Error (-)');
end