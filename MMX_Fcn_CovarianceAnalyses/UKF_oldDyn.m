function [Est] = UKF_oldDyn(Est0,f,fsigma,G,O,Measures,par,units)
%==========================================================================
% [Est] = UKF_oldDyn(Est0,f,G_dG,R,Measures,par)
%
% Unscented Kalman Filter
%
% INPUT: Description Units
%
% Est0      - Apriori initial guess of
%               .X,     States 
%               .P0,    Covariance matrix
%               .t0,    initial time  
% 
% @f        - Dynamical model
% 
% @G        - Observation's model and partial derivatives wrt the states
% 
% O         - Weight matrix
% Nsigma    - Number of sigma points
% Measures  - Observations matrix: the first column contains observation
%               times, then every column has a different type of observaion
%               type
% par       - Structure containing parameters required by @f and @G_dG 
% units     - Structure containing lenght and time units 
%
% OUTPUT:
%
% Est       - Estimation of:
%               .X, States at final t
%               .P, Covariance matrix at final t
%               .t, Time observation vector
%               .X_t, States at different t
%               .P_t, Covariance matrix at different t  
%               .Pbar_t, Covariance matrix time update at different t
% 
%
% Coupling:
% None
%
% Author: Edoardo Ciccarelli
% Date: 04/2021
%
%==========================================================================


%   Apriori initial guess, deviation and covariance matrix.
    Xstar0  = Est0.X;
    P0bar   = Est0.P0;
    told    = Est0.t0;
    
%   Column of the measures time.
    tobs    = Measures(1,:);

%   The measures must be organized in colums depending on the type of
%   measure; each row is reffered to a given measure's time.
    obs     = Measures(2:end,:)./units.Observations;
    
%   n,l and m are respectively the number of states, number of different
%   types of measures and number of measures per each type.
    n       = size(Xstar0,1);
    m       = size(obs,2);
    q       = 3;

%   err is the vector of residuals, the others are needed to build the 
%   fitting residuals.
    err     = NaN(size(O,1),m);
    prefit  = NaN(size(O,1),m);
    y_t     = NaN(size(O,1),m);
    X_t     = zeros(n,size(tobs,1));
    P_t     = zeros(n,n,m);
    Pbar_t  = zeros(n,n,m);
    
%   Initialization of the filter
    k       = 3 - n;
    alpha   = par.alpha;
    lambda  = alpha^2*(n+k) - n;
    gamma   = sqrt(n+lambda);

%   Initialization of the filter
    Xold    = Xstar0;
    Pold    = P0bar;
    
%   Propagation of the reference trajectory
    Phi0    = eye(n);
    St0     = [Xold; reshape(Phi0,[n^2,1])];
    interval_Range      = par.interval_Range;
    interval_Range_rate = par.interval_Range_rate;
    interval_lidar      = par.interval_lidar;
    interval_camera     = par.interval_camera;
    min_interval        = min([interval_Range; interval_Range_rate; interval_lidar; interval_camera]);

    tspan   = (0:min_interval:tobs(end))/units.tsf;
    RelTol  = 1e-13;
    AbsTol  = 1e-16; 
    opt     = odeset('RelTol',RelTol,'AbsTol',AbsTol);
    [t,X]   = ode113(@(t,X) f(t,X,par,units),tspan,St0,opt);
    fprintf('\nOrbit propagation complete...');

%   Sigma points' weights
    W0c     = zeros(2*n, 1);
    W0c(:)   = 1./(2*(n+lambda));

       
%   For-loop on the observations
    for i = 1:m
        

        if tobs(i) == told-Est0.t0
            idx         = 1;
            Xmean_bar   = X(idx,1:n)';
        elseif tobs(i) > told
            idx         = find(round(t*units.tsf)==tobs(i));
            Xmean_bar   = X(idx,1:n)';
        end
    

%       Useful quantities's initialization
        par.et  = Est0.t0*units.tsf + tobs(i);
        
%       TIME UPDATE
%       Sigma points' definition
        S0 = real(sqrtm(Pold)); 
        X0 = [(repmat(Xold,1,n)-gamma.*S0),...
            (repmat(Xold,1,n)+gamma.*S0)];

%       Need to know how many observations are available
        where       = isnan(obs(1:end,i));
        count       = sum(~where);
        IDX         = [];
        Y_obs       = zeros(count,1);
        Y           = [];
        R           = zeros(count, count);

        if count ~= 0
            for z=1:size(where,1)
                 if where(z)==0
                     IDX = [IDX; z];
                 end
            end
    
            for j=1:count
                Y_obs(j)= obs(IDX(j),i);
                R(j,j)  = O(IDX(j),IDX(j));
            end
        else
            Y_obs       = obs(:,i);
            R           = O;
        end

        if tobs(i) == told-Est0.t0

%           Analysis of the sigma points' trajectories
            Xbar    = X0;

        elseif tobs(i) > told

%       Propagation of the sigma points' trajectories
            St0     = reshape(X0,[n*(2*n),1]);
            tspan   = (told:tobs(i))/units.tsf;
            opt     = odeset('RelTol',1e-13,'AbsTol',1e-16);
            [~,X_sigma] = ode113(@(t,X) fsigma(t,X,par,units),tspan,St0,opt);
            Xbar = reshape(X_sigma(end,1:n*(2*n))',[n,(2*n)]);

        end
        i

%       Apriori quantities
        Q       = par.sigma^2*eye(3);
        deltaT  = (tobs(i)-told)/units.tsf;
        Gamma   = [eye(q)*deltaT^2/2; eye(q)*deltaT; zeros(n-6,q)];        
        Q       = Gamma*Q*Gamma';
        P_bar   = Q;

        for j = 1:2*n
            P_bar   = P_bar + W0c(j)*((Xbar(:,j) - Xmean_bar) * (Xbar(:,j) - Xmean_bar)');
        end

%       Sigma point redefinition
        X0  = [Xmean_bar, (repmat(Xmean_bar,1,n)-gamma.*real(sqrtm(P_bar))),...
            (repmat(Xmean_bar,1,n)+gamma.*real(sqrtm(P_bar)))];

        for j = 1:2*n
%           G(j-th sigma point)
            Y_sigma = G(par.et,obs(:,i)',X0(:,j),par,units);
            Y       = [Y, Y_sigma'];
        end

%       Mean predicted measurement
        Y_mean  = G(par.et,obs(:,i)',Xmean_bar,par,units)';
        
%       Innovation covariance and cross covariance

        Py      = R;
        Pxy     = zeros(n,count);

        for j = 1:2*n
            Py  = Py + W0c(j)*(Y(:,j) - Y_mean)*(Y(:,j) - Y_mean)';
            Pxy = Pxy + W0c(j)*(X0(:,j) - Xmean_bar)*(Y(:,j) - Y_mean)';
        end
        
%       Kalman gain
        K   = Pxy/Py;

%       MEASUREMENT UPDATE
        y           = (Y_obs - Y_mean);
        Xstar       = Xmean_bar + K*y;
        P           = P_bar - K*Py*K';
        
%       Next iteration preparation
        Xold        = Xstar;
        told        = tobs(i);
        Pold        = P;
    
        err(IDX,i)    = (Y_obs - G(par.et,obs(:,i)',Xstar,par,units)').*units.Observations(IDX);
        prefit(IDX,i) = (Y_obs - G(par.et,obs(:,i)',Xmean_bar,par,units)').*units.Observations(IDX);
        y_t(IDX,i)    = y.*units.Observations(IDX);

%       Storing the results
        P_t(:,:,i)  = P.*units.CovDim;
        Pbar_t(:,:,i)  = P_bar.*units.CovDim;
        X_t(:,i)    = Xstar.*units.Dim;
        
    end
    
    fprintf('\nAnalysis complete!\n');
    
%   Initial states' estimation, deviation and covariance matrix
    Est.X       = Xstar.*units.Dim;
    Est.P       = P.*units.CovDim;
    
%   Residuals and fit-residuals at different observation times
    Est.t       = tobs;
    Est.err     = err;
    Est.pre     = prefit;
    Est.y_t     = y_t;
    Est.X_t     = X_t;
    Est.P_t     = P_t;
    Est.Pbar_t  = Pbar_t;
    
end