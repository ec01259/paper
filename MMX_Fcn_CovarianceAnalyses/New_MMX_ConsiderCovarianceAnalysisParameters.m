function [pars, units] = New_MMX_ConsiderCovarianceAnalysisParameters(pars, units)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% [pars, units] = New_MMX_ConsiderCovarianceAnalysisParameters(pars, units)
%
% This function adds some units and parameters useful to the covariance
% analysis
% 
% Inputs:
% pars          Structure which contains parameters
% units         Structure with units 
% 
% Outputs:
% pars.ObsNoise             Observations noises standard deviation
% pars.ObsNoise.range       Standard deviation of the relative distance measure
% pars.ObsNoise.range_rate  Standard deviation of the relative velocity measure
% pars.ObsNoise.lidar       Standard deviation of the lidar measure
% pars.ObsNoise.camera      Standard deviation of the camera measure
% pars.interval             Seconds between observations
% pars.Clm                  Vector with all the Clm coefficients
% pars.Slm                  Vector with all the Slm coefficients
% pars.nCoeff               Number of the harmonics coefficients
% pars.P0                   A-priori covariance
% pars.sigma                PN diffusion coefficient
%
% units.Dim                 Vector of normalizing units [sfVec, sfVec, ones(pars.nCoeff,1)]
% units.CovDim              Matrix of normalizing units
%
% Author: STAG Team
% Date: Jun 2021
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

%% Parameters
    
% Observation noise
pars.ObsNoise.range     = 5e-3/units.lsf;         % [km]
pars.ObsNoise.range_rate= 5e-7/units.vsf;         % [km/s]
pars.ObsNoise.lidar     = 1e-2/units.lsf;         % [km]

FOV     = 45*pi/180;
pixels  = 2048;
pars.ObsNoise.camera    = FOV/pixels;               % [rad/pixel]

% Useful to adimensionalize the observables
units.Observations = [units.lsf; units.vsf; units.lsf; units.vsf; units.lsf; units.vsf; units.lsf; 1; 1];

% Matrix of the observation noise
pars.R      = zeros(9,9);
pars.R(1,1) = pars.ObsNoise.range^2;
pars.R(2,2) = pars.ObsNoise.range_rate^2;
pars.R(3,3) = pars.ObsNoise.range^2;
pars.R(4,4) = pars.ObsNoise.range_rate^2;
pars.R(5,5) = pars.ObsNoise.range^2;
pars.R(6,6) = pars.ObsNoise.range_rate^2;
pars.R(7,7) = pars.ObsNoise.lidar^2;
pars.R(8,8) = pars.ObsNoise.camera^2;
pars.R(9,9) = pars.ObsNoise.camera^2;

% Seconds between observation
pars.interval_Range         = 3600;     % s
pars.interval_Range_rate    = 600;      % s
pars.interval_lidar         = 100;       % s
pars.interval_camera        = 3600;     % s

pars.cutoffLIDAR    = 200;  % km
pars.elevation      = 80;   % deg

% Gravitational harmonics' parameters
pars.I2 = [pars.IPhx_bar; pars.IPhy_bar; pars.IPhz_bar];

% Measurements biases
bias = [0; 0; 0]...
    ./[units.lsf; units.vsf; units.lsf];
pars.bias = bias;

% Unit vector and parameters number useful for later use
pars.nCoeff     = size(pars.I2,1);
pars.nBias      = size(bias,1);
units.Dim       = [units.sfVec; ones(pars.nCoeff,1); units.lsf; units.vsf;...
     units.lsf];
units.DimCons   = [units.lsf; units.vsf; 1; units.tsf;...
     1; units.tsf; 1; units.tsf];
units.CovDim    = (repmat(units.Dim, 1, length(units.Dim)).*...
    repmat(units.Dim', length(units.Dim), 1));


% A-priori covariance
pars.P0         = diag(([1*ones(3,1); 1e-3*ones(3,1); 1e-3*ones(pars.nCoeff,1);...
    1; 1e-3; 1]./units.Dim).^2); 
% pars.Pcc        = diag(([3e-3; 6e-7; 5e-7; 1e-12; 1; 1e-3; 1; 5e-7;]*1e0./units.DimCons).^2); 
pars.Pcc        = diag(([5e-4; 3e-9; 5e-7; 1e-12; 1e-2; 5e-5; 1e-2; 5e-7;]./units.DimCons).^2); 
pars.Pxc        = zeros(12,8);

end