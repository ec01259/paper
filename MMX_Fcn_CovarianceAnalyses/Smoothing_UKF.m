function Smooth = Smoothing_UKF(Est,par,units,fsigma,varargin)
%--------------------------------------------------------------------------
%   This function performs the smoothing of a certain sequential filter
%   estimate
%   
%   Smooth = Smoothing(Est)
%   Smooth = Smoothing(Est,tk)
%
%   INPUTs
%   - Est           .t          Observations' epoch
%                   .x_t        State deviations at epoch "t" from filter
%                   .P_t        Covariance matrix at epoch "t" from filter
%                   .Pbar_t     Time update of the Covariance matrix at 
%                               epoch "t" from filter
%                   .Phi_t      STM at epoch "t" from filter
% 
%   - tk                        Epoch till when is required the smoothing
%                               (it is Est.t(1) if not specified)
% 
%
%   OUTPUTs
%   Smooth      	.x_lk       State deviation vector smoothed at epoch tk
%                   .P_lk       Covariance matrix smoothed at epoch tk
%                   .x_lk_t     State deviation vector smoothed at all epochs
%                   .P_lk_t     Covariance matrix smoothed at all epochs
% 
% 
%   Author: Edoardo Ciccarelli
%   Date: 02/2020


    switch nargin
        case 5
            tk = varargin{1};
        case 4
            tk = Est.t(1);
    otherwise
        error('Check the help of this function');
    end

%   Initialization of the filter
    n       = size(Est.X_t,1);
    k       = 3 - n;
    alpha   = par.alpha;
    beta    = 2;
    lambda  = alpha^2*(n+k) - n;
    gamma   = sqrt(alpha^2*(n+k));

%   Sigma points' weights
    W0m     = zeros(2*n+1, 1);
    W0m(1)  = lambda/(lambda+n);
    W0m(2:2*n+1)   = 1./(2*(n+lambda));

    W0c     = zeros(2*n+1, 1);
    W0c(1)  = lambda/(lambda+n)+(1-alpha^2+beta);
    W0c(2:2*n+1)   = 1./(2*(n+lambda));
    
%   Observations time's vector
    tobs    = Est.t;
    len     = length(tobs) - find(Est.t==tk) + 1;
    len     = len(1);
    
%   Preallocation of the vectors with everything
    P_lk_t = zeros(size(Est.P,1),size(Est.P,2),len);
    
%   First step
    P_lk1  = Est.P_t(:,:,end)./units.CovDim;
    P_lk_t(:,:,end) = Est.P_t(:,:,end)./units.CovDim;
    
    for i = 1:len-1

%       Next step: k = kold - i
        X_kk  = Est.X_t(:,end-i)./units.Dim;
        X_kk1 = Est.X_t(:,end-i+1)./units.Dim;
        P_kk  = Est.P_t(:,:,end-i)./units.CovDim;
        Pbar_kk1 = Est.Pbar_t(:,:,end-i+1)./units.CovDim;
        
        S_kk1 = real(sqrtm(Pbar_kk1)); 
        X_sigma_kk1 = [X_kk1, (repmat(X_kk1,1,n)-gamma.*S_kk1),...
            (repmat(X_kk1,1,n)+gamma.*S_kk1)];

%       Propagation of the sigma points' trajectories
        St0     = reshape(X_sigma_kk1,[n*(2*n+1),1]);
        tspan   = [0, tobs(end-i+1)-tobs(end-i)]*units.tsf;
        opt     = odeset('RelTol',1e-13,'AbsTol',1e-16);
        [~,X_sigma_kk] = ode113(@(t,X) -fsigma(t,X,par,units),tspan,St0,opt);
        X_sigma_kk  = reshape(X_sigma_kk(end,1:n*(2*n+1))',[n,(2*n+1)]);

        P_kk_back   = zeros(n,n);
        C_kk1       = zeros(n,n);

        for j = 1:2*n+1
            P_kk_back   = P_kk_back + W0c(j)*((X_sigma_kk(:,j) - X_kk) *...
                (X_sigma_kk(:,j) - X_kk)');
            C_kk1   = C_kk1 + W0c(j)*((X_sigma_kk1(:,j) - X_kk1) *...
                (X_sigma_kk(:,j) - X_kk)');
        end

        
%       Smoothing algorithm
        S       = C_kk1/P_kk_back;
        P_lk    = P_kk + S*(P_lk1 - P_kk_back)*S';

        if any(isnan(P_lk))
            error(['Something happened at iteration %d during the ' ...
                'smoothing operations...'],i);
        end

%       Filling the time by time values
        P_lk_t(:,:,end-i) = P_lk.*units.CovDim;
        
%       Update of the counter
%         if all(eig(P_lk)>0)
            P_lk1 = P_lk;
%         end

        len-i
    end

%   Complete vector over time
    Smooth.P_lk_t = P_lk_t;

end