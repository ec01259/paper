function [Est] = ConsiderUKF_CovarianceAnalysis(Est0,f,fsigma,f_Cons_sigma,G,O,Measures,par,units)
%==========================================================================
% [Est] = ConsiderUKF_CovarianceAnalysis(Est0,f,G_dG,R,Measures,par)
%
% Consider Unscented Kalman Filter
%
% INPUT: Description Units
%
% Est0      - Apriori initial guess of
%               .X,     States 
%               .P0,    Covariance matrix
%               .t0,    initial time  
% 
% @f        - Dynamical model
% 
% @G        - Observation's model and partial derivatives wrt the states
% 
% O         - Weight matrix
% Nsigma    - Number of sigma points
% Measures  - Observations matrix: the first column contains observation
%               times, then every column has a different type of observaion
%               type
% par       - Structure containing parameters required by @f and @G_dG 
% units     - Structure containing lenght and time units 
%
% OUTPUT:
%
% Est       - Estimation of:
%               .X, States at final t
%               .P, Covariance matrix at final t
%               .t, Time observation vector
%               .X_t, States at different t
%               .P_t, Covariance matrix at different t  
%               .Pbar_t, Covariance matrix time update at different t
% 
%
% Coupling:
% None
%
% Author: Edoardo Ciccarelli
% Date: 04/2021
%
%==========================================================================


%   Apriori initial guess, deviation and covariance matrix.
    Xold    = Est0.X;
    c       = Est0.c;
    Xc_old  = [Xold; c];
    Pold    = Est0.P0;
    Pxx_old = Pold;
    Pxc_old = Est0.Pxc;
    Pcc     = Est0.Pcc;

%   Column of the measures time.
    tobs    = Measures(1,:);
    told    = tobs(1);

%   The measures must be organized in colums depending on the type of
%   measure; each row is reffered to a given measure's time.
    obs     = Measures(2:end,:)./units.Observations;
    
%   n,l and m are respectively the number of states, number of different
%   types of measures and number of measures per each type.
    n       = size(Xold,1);
    p       = size(Pcc,1);
    m       = size(obs,2);
    q       = 3;

%   err is the vector of residuals, the others are needed to build the 
%   fitting residuals.
    err     = NaN(size(O,1),m);
    prefit  = NaN(size(O,1),m);
    y_t     = NaN(size(O,1),m);
    X_t     = zeros(n,size(tobs,1));
    x_t     = zeros(n,size(tobs,1));
    P_t         = zeros(n,n,m);
    dP_t        = zeros(n,n,m);
    Pxx_cons_t  = zeros(n,n,m);
    Pbar_t      = zeros(n,n,m);
    
%   Initialization of the filter
    k       = 3 - n;
    alpha   = par.alpha;
    alpha_c = par.alpha_c;
    beta    = par.beta;
    lambda  = alpha^2*(n+k) - n;
    lambda_c    = alpha_c*3 - p;
    gamma   = n + lambda;
    gamma_c = p + lambda_c;

%   Sigma points' weights
    W0c     = zeros(2*n+1, 1);
    W0c(1)  = lambda/(lambda+n)+(1-alpha^2+beta);
    W0c(2:end)  = 1./(2*(n+lambda));

    W0m     = zeros(2*n+1, 1);
    W0m(1)  = lambda/(lambda+n);
    W0m(2:end)  = W0c(2:end);

    W0cons  = zeros(2*p+1, 1);
    W0cons(1)  = lambda_c/(lambda_c+p)+(1-alpha^2+beta);
    W0cons(2:end)  = 1./(2*(p+lambda_c));

    Wconsm  = zeros(2*p+1, 1);
    Wconsm(1)  = lambda_c/(lambda_c+p);
    Wconsm(2:end)  = W0cons(2:end);

%   Reference orbit propagation
    interval_Range      = par.interval_Range;
    interval_Range_rate = par.interval_Range_rate;
    interval_lidar      = par.interval_lidar;
    interval_camera     = par.interval_camera;
    min_interval        = min([interval_Range; interval_Range_rate; interval_lidar; interval_camera]);

    St0     = [Xold(1:6); c; Xold(7:9)];
    tspan   = (tobs(1):min_interval:tobs(end))*units.tsf;
    RelTol  = 1e-13;
    AbsTol  = 1e-16; 
    opt     = odeset('RelTol',RelTol,'AbsTol',AbsTol);
    [t,Xp]   = ode113(@(t,X) f(t,X,par,units),tspan,St0,opt);
    fprintf('\nOrbit propagation complete...');
    
    X       = [Xp(:,1:6), Xp(:,15:end)];
    Xcc     = Xp(:,7:14);
    idx_old = 1;

%   For-loop on the observations
    for i = 1:m
        
%       Look for the observation to process
        if tobs(i) == told
            idx     = idx_old;
            Xold    = [X(idx,:)';0;0;0];
            Xc_old  = [Xold; Xcc(idx,:)'];
        elseif tobs(i) > told
            idx     = find(round(t/units.tsf)==tobs(i));
            Xold    = [X(idx,:)';0;0;0];
            Xc_old  = [Xold; Xcc(idx,:)'];
        end

%       Useful quantities's initialization
        et  = Est0.t0/units.tsf + tobs(i);
        
%       TIME UPDATE
%       Sigma points' definition
        S0 = real(sqrtm(gamma.*Pold)); 
        X0 = [Xold, (repmat(Xold,1,n)-S0),...
            (repmat(Xold,1,n)+S0)];

%       Consider sigma points' definition
        Scc     = real(sqrtm(gamma_c.*Pcc)); 
        Scx     = gamma_c*(Scc\Pxc_old'); 
        Sxx     = real(sqrtm(3.*Pxx_old - Scx'*Scx)); 
        S_cons  = [Sxx, zeros(n,p); Scx, Scc]';

        Xc0 = [Xc_old, (repmat(Xc_old,1,p)-S_cons(:,n+1:end)),...
            (repmat(Xc_old,1,p)+S_cons(:,n+1:end))];
        

%       Need to know how many observations are available
        where       = isnan(obs(1:end,idx));
        count       = sum(~where);
        IDX         = [];
        Y_obs       = zeros(count,1);
        R           = zeros(count, count);

        if count ~= 0
            for z=1:size(where,1)
                 if where(z)==0
                     IDX = [IDX; z];
                 end
            end
    
            for j=1:count
                Y_obs(j)= obs(IDX(j),idx);
                R(j,j)  = O(IDX(j),IDX(j));
            end
        else
            Y_obs       = obs(:,idx);
            R           = O;
        end

        if tobs(idx) == told

%           The sigma point dont need to be proppagated
            Xbar    = X0;
            Xc_bar  = Xc0;

        elseif tobs(idx) > told

%           Propagation of the sigma points' trajectories
            St0     = reshape([X0(1:6,:); repmat(Xc_old(n+1:end),1,2*n+1); X0(7:end,:)],...
                [(n+p)*(2*n+1),1]);
%             St0     = reshape([X0(1:6,:); repmat(Xcc(idx-1,:)',1,2*n+1); X0(7:end,:)],...
%                 [(n+p)*(2*n+1),1]);
            tspan   = (told:tobs(idx))*units.tsf;
            opt     = odeset('RelTol',1e-13,'AbsTol',1e-16);
            [T1,X_sigma] = ode113(@(t,X) fsigma(t,X,par,units),tspan,St0,opt);
            Xbar    = reshape(X_sigma(end,1:(n+p)*(2*n+1))',[n+p,(2*n+1)]);
            
%             Xbar(:,1)-[X(idx,1:6) Xcc(idx,:) X(idx,7:9) 0 0 0]'
            
            Xbar    = [Xbar(1:6,:); Xbar(15:end,:)];

%           Propagation of the consider sigma points' trajectories
            St0     = reshape(Xc0,[(n + p)*(2*p+1),1]);
            tspan   = (told:tobs(idx))*units.tsf;
            opt     = odeset('RelTol',1e-13,'AbsTol',1e-16);
            [~,X_sigma] = ode113(@(t,X) f_Cons_sigma(t,X,par,units),tspan,St0,opt);
            Xc_bar  = reshape(X_sigma(end,1:(n + p)*(2*p+1))',[n+p,(2*p+1)]);

%             Xc_bar(:,1)-[X(idx,1:9) 0 0 0 Xcc(idx-1,:)]'

        end
        i

        Xmean_bar   = sum(W0m'.*Xbar,2);
        Xc_mean_bar = sum(Wconsm'.*Xc_bar,2);

%       Process noise if needed
        deltaT  = (tobs(i)-told)*units.tsf;
        Q       = par.sigma^2*eye(3);
        Gamma   = [eye(q)*deltaT^2/2; eye(q)*deltaT; zeros(n-6,q)];
        Q       = Gamma*Q*Gamma';  

%       Covariance matrix's time update 
        P_bar       = Q;
        Pcons_bar   = zeros(n+p,n+p);

        for j = 1:2*n+1
            P_bar   = P_bar + W0c(j)*((Xbar(:,j) - Xmean_bar) *...
                (Xbar(:,j) - Xmean_bar)');
        end
        
        for j = 1:2*p+1
            Pcons_bar   = Pcons_bar + W0cons(j)*((Xc_bar(:,j) - Xc_mean_bar) *...
                (Xc_bar(:,j) - Xc_mean_bar)');
        end

        Pxc_bar     = Pcons_bar(1:n,n+1:end);

%       Sigma point redefinition
        S_bar   = real(sqrtm(gamma.*P_bar));
        X0  = [Xmean_bar, (repmat(Xmean_bar,1,n)-S_bar),...
                    (repmat(Xmean_bar,1,n)+S_bar)];
        
        S_bar_cons = real(sqrtm(gamma.*Pcons_bar));
        Xc0 = [Xc_mean_bar, (repmat(Xc_mean_bar,1,p+n)-S_bar_cons),...
                (repmat(Xc_mean_bar,1,p+n)+S_bar_cons)];
        

        Y = zeros(count,2*n+1);
        Yc  = zeros(count,2*p+1);

        for j = 1:2*n+1
%           G(j-th sigma point)
            Y_sigma = G(et,obs(:,idx),X0(:,j),Xcc(idx,:)',par,units);
            Y(:,j)  = Y_sigma';
        end

       for j = 1:2*p+1
%           G(j-th sigma point)
            Y_sigma = G(et,obs(:,idx),Xc0(1:n,j),Xc0(n+1:end,j),par,units);
            Y(:,j)  = Y_sigma';
        end

%       Mean predicted measurement
        Y_mean  = sum(W0m'.*Y,2);
        Yc_mean = sum(Wconsm'.*Yc,2);
        
%       Innovation covariance and cross covariance

        Py      = R;
        Pxy     = zeros(n,count);

        for j = 1:2*n
            Py  = Py + W0c(j)*(Y(:,j) - Y_mean)*(Y(:,j) - Y_mean)';
            Pxy = Pxy + W0c(j)*(X0(:,j) - Xmean_bar)*(Y(:,j) - Y_mean)';
        end
        
        Pcy = zeros(p,count);
        for j = 1:2*p
            Pcy = Pcy + W0cons(j)*(Xc_bar(n+1:end,j) - Xc_mean_bar(n+1:end))*...
                (Yc(:,j) - Yc_mean)';
        end

%       Kalman gain
        K   = Pxy/Py;

%       MEASUREMENT UPDATE
        y       = (Y_obs - Y_mean);
        Xstar   = Xmean_bar + K*y;
        P       = P_bar - K*Py*K';
        Pxc     = Pxc_bar - K*Pxy'/P_bar*Pxc_bar - K*Pcy';
        dP      = Pxc/Pcc*Pxc';
        Pxx_cons= P + dP;

%       Next iteration preparation
%         Xold    = Xmean_bar;
%         Xc_old  = Xc_mean_bar;
        Xold    = Xstar;
        Xc_old  = [Xstar; Xcc(idx,:)'];
        told    = tobs(idx);
        Pold    = P;
        Pxx_old = Pxx_cons;
        Pxc_old = Pxc;
       
        err(IDX,i)    = (Y_obs - G(et,obs(:,i),Xstar,Xc_old(n+1:end)',par,units)').*units.Observations(IDX);
        prefit(IDX,i) = (Y_obs - G(et,obs(:,i),Xmean_bar,Xc_old(n+1:end)',par,units)').*units.Observations(IDX);
        y_t(IDX,i)    = y.*units.Observations(IDX);


%       Storing the results
        P_t(:,:,i)          = P.*units.CovDim;
        dP_t(:,:,i)         = dP.*units.CovDim;
        Pxx_cons_t(:,:,i)   = Pxx_cons.*units.CovDim;
        Pbar_t(:,:,i)       = P_bar.*units.CovDim;
        X_t(:,i)    = Xold.*units.Dim;
        x_t(:,i)    = K*y.*units.Dim;
        
    end
    
    fprintf('\nAnalysis complete!\n');
    
%   Initial states' estimation, deviation and covariance matrix
    Est.X       = Xold.*units.Dim;
    Est.P       = P.*units.CovDim;
    
%   Residuals and fit-residuals at different observation times
    Est.t       = tobs;
    Est.err     = err;
    Est.pre     = prefit;
    Est.y_t     = y_t;
    Est.X_t     = X_t;
    Est.x_t     = x_t;
    Est.P_t         = P_t;
    Est.dP_t        = dP_t;
    Est.Pxx_cons_t  = Pxx_cons_t;
    Est.Pbar_t      = Pbar_t;
    
end