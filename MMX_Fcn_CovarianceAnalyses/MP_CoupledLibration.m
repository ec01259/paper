function dx = MP_CoupledLibration(~,x,par,~)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% dx = MP_CoupledLibration(t,x,pars)
%
% Calculate the acceleration vector in the N-Body Problem with Mars and
% Phobos only and the derivative of the coefficients to be estimated
%
% Input:
% .t        Elapsed time since epoch (-)
% .x        State Vector 
% .pars     Problem Parameters
%           .d          Dimension of the Problem
%           .et0        Initial epoch (-)
%           .refcenter  Origin of Frame (e.g., '399')
%           .refframe   Coordinate Frame (e.g., 'ECLIPJ2000' or 'J2000')
%           .Ntb        No. of third bodies
%           .BodiesID   Bodies' SPICE ID (e.g., '10','399', ...)
%           .GM         Bodies' Gravitational Parameters (-)
%           .Nsh        No. of Spherical Harmonic Bodies
%           .SH_BodiesID Spherical Harmonic Bodies' SPICE ID (e.g., '10','399', ...)
%           .SH         Spherical Harmonic Structure
%           .lsf        Length scale factor (km)
%           .tsf        Time scale factor (s)
%           .STM        true or false depending if you calculate STM or not
%           .nCoeff     Number if coefficients in the state vector
%
% Output:
% .dx       Vector containing accelerations and STM (d x 2 + nCoeff, -)
%
% Author: E.Ciccarelli
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%  Dimensions

    Ph          = par.d2;                  % Secondary body's N. of states

%%  Useful quantities

%   Phobos states
    RPh         = x(1);
    RPh_dot     = x(2);
    theta_dot   = x(4);
    Phi_dot     = x(6);
    Xsi         = x(7);
    Xsi_dot     = x(8);
    
    PHI         = reshape(x(9:end),[Ph, Ph]);
       
    
%%  Phobos's orbit

%   Potential's first order partials
    dVdRPh      = par.ni/RPh^2*(1 + 3/(2*RPh^2)*((par.IMz_bar - par.Is) -...
        .5*par.IPhx_bar - .5*par.IPhy_bar + par.IPhz_bar + ...
        1.5*(par.IPhy_bar - par.IPhx_bar)*cos(2*Xsi)));
    dVdXsi      = 1.5*par.ni/RPh^3 * (par.IPhy_bar - par.IPhx_bar)*sin(2*Xsi);

%   Phobos equations of motions
    RPh_ddot    = theta_dot^2*RPh - dVdRPh/par.ni;
    theta_ddot  = dVdXsi/(par.ni*RPh^2) - 2*RPh_dot*theta_dot/RPh;
    Phi_ddot    = - dVdXsi/(par.ni*RPh^2) + 2*RPh_dot*theta_dot/RPh;
    Xsi_ddot    = -(1 + par.ni*RPh^2/par.IPhz_bar)*dVdXsi/(par.ni*RPh^2) +...
                    + 2*RPh_dot*theta_dot/RPh;

%%  Jacobian of Mars wrt its own states

%   Potential's second order partials
    dVdRPhsqrd          = - 2*par.ni/RPh^5*(RPh^2+3*((par.IMz_bar - par.Is) -...
        .5*par.IPhx_bar - .5*par.IPhy_bar + par.IPhz_bar + ...
        1.5*(par.IPhy_bar - par.IPhx_bar)*cos(2*Xsi)));
    dVdRPhdXsi          = (-3*(par.IPhy_bar - par.IPhx_bar)*sin(2*Xsi))*1.5*par.ni/RPh^4;
    dVdXsisqrd          = 3*par.ni/RPh^3*(par.IPhy_bar - par.IPhx_bar)*cos(2*Xsi);

%   DRPh_ddotDX
    DRPh_ddotDRPh       = theta_ddot^2 - dVdRPhsqrd/par.ni;
    DRPh_ddotDtheta_dot = 2*theta_ddot*RPh;
    DRPh_ddotDXsi       = - dVdRPhdXsi/par.ni;
    DRPh_ddotDX         = [DRPh_ddotDRPh, 0, 0, DRPh_ddotDtheta_dot, 0, 0, DRPh_ddotDXsi, 0];

%   Dtheta_ddotDX
    Dtheta_ddotDRPh         = (dVdRPhdXsi/RPh^2 - 2/RPh^3*dVdXsi)/par.ni + 2*RPh_dot*theta_dot/RPh^2;
    Dtheta_ddotDRPh_dot     = - 2*theta_dot/RPh;
    Dtheta_ddotDtheta_dot   = - 2*RPh_dot/RPh;
    Dtheta_ddotDXsi         = dVdXsisqrd/(par.ni*RPh^2);
    Dtheta_ddotDX           = [Dtheta_ddotDRPh, Dtheta_ddotDRPh_dot, 0, Dtheta_ddotDtheta_dot, 0, 0, Dtheta_ddotDXsi, 0];
    
%   DPhi_ddotDX
    DPhi_ddotDRPh       = - Dtheta_ddotDRPh;
    DPhi_ddotDRPh_dot   = - Dtheta_ddotDRPh_dot;
    DPhi_ddotDtheta_dot = - Dtheta_ddotDtheta_dot;
    DPhi_ddotDXsi       = - Dtheta_ddotDXsi;
    DPhi_ddotDX         = [DPhi_ddotDRPh, DPhi_ddotDRPh_dot, 0, DPhi_ddotDtheta_dot, 0, 0, DPhi_ddotDXsi, 0];

%   DXsi_ddotDX
    DXsi_ddotDRPh       = - dVdRPhdXsi/par.IPhz_bar - 2*RPh_dot*theta_dot/RPh^2;
    DXsi_ddotDRPh_dot   = 2*theta_dot/RPh;
    DXsi_ddotDtheta_dot = 2*RPh_dot/RPh;
    DXsi_ddotDXsi       = -(1 + par.ni*RPh^2/par.IPhz_bar)*dVdXsisqrd/(par.ni*RPh^2);
    DXsi_ddotDX         = [DXsi_ddotDRPh, DXsi_ddotDRPh_dot, 0, DXsi_ddotDtheta_dot, 0, 0, DXsi_ddotDXsi, 0];

%   Speeds' derivatives wrt states
    DRPh_dotDX      = [0, 1, 0, 0, 0, 0, 0, 0];
    Dtheta_dotDX    = [0, 0, 0, 1, 0, 0, 0, 0];
    DPhi_dotDX      = [0, 0, 0, 0, 0, 1, 0, 0];
    DXsi_dotDX      = [0, 0, 0, 0, 0, 0, 0, 1];

%   Jacobian summariz
    Jacobian_Ph = [DRPh_dotDX; DRPh_ddotDX; Dtheta_dotDX; Dtheta_ddotDX;...
        DPhi_dotDX; DPhi_ddotDX; DXsi_dotDX; DXsi_ddotDX];
    
    PHI_dot = Jacobian_Ph*PHI;
    PHI_dot = reshape(PHI_dot,[Ph^2,1]);

%%  Output for the integrator
    
    dx = [RPh_dot; RPh_ddot; theta_dot; theta_ddot; Phi_dot; Phi_ddot; Xsi_dot; Xsi_ddot; PHI_dot];

end