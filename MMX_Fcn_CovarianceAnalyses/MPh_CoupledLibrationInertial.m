function dx = MPh_CoupledLibrationInertial(~,x,par,~)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% dx = MP_CoupledLibrationInertial(t,x,pars)
%
% Calculate the acceleration vector in the N-Body Problem with Mars and
% Phobos only and the derivative of the coefficients to be estimated
%
% Input:
% .t        Elapsed time since epoch (-)
% .x        State Vector 
% .pars     Problem Parameters
%           .d          Dimension of the Problem
%           .et0        Initial epoch (-)
%           .refcenter  Origin of Frame (e.g., '399')
%           .refframe   Coordinate Frame (e.g., 'ECLIPJ2000' or 'J2000')
%           .Ntb        No. of third bodies
%           .BodiesID   Bodies' SPICE ID (e.g., '10','399', ...)
%           .GM         Bodies' Gravitational Parameters (-)
%           .Nsh        No. of Spherical Harmonic Bodies
%           .SH_BodiesID Spherical Harmonic Bodies' SPICE ID (e.g., '10','399', ...)
%           .SH         Spherical Harmonic Structure
%           .lsf        Length scale factor (km)
%           .tsf        Time scale factor (s)
%           .STM        true or false depending if you calculate STM or not
%           .nCoeff     Number if coefficients in the state vector
%
% Output:
% .dx       Vector containing accelerations and STM (d x 2 + nCoeff, -)
%
% Author: E.Ciccarelli
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%  Dimensions

    d           = par.d;                    % Dimension of the problem

%%  Useful quantities
    
%   Phobos 2 states
    RPh         = x(d+1);
    RPh_dot     = x(d+2);
    theta       = x(d+3);
    theta_dot   = x(d+4);
    Phi         = x(d+5);
    Phi_dot     = x(d+6);
    Xsi         = x(d+7);
    Xsi_dot     = x(d+8);


%%  Phobos's orbit

%   Potential's first order partials
    dVdRPh      = par.ni/RPh^2*(1 + 3/(2*RPh^2)*((par.IMz_bar - par.Is) -...
        .5*par.IPhx_bar - .5*par.IPhy_bar + par.IPhz_bar + ...
        1.5*(par.IPhy_bar - par.IPhx_bar)*cos(2*Xsi)));
    dVdXsi      = 1.5*par.ni/RPh^3 * (par.IPhy_bar - par.IPhx_bar)*sin(2*Xsi);

%   Phobos equations of motions
    RPh_ddot    = theta_dot^2*RPh - dVdRPh/par.ni;
    theta_ddot  = dVdXsi/(par.ni*RPh^2) - 2*RPh_dot*theta_dot/RPh;
    Phi_ddot    = - dVdXsi/(par.ni*RPh^2) + 2*RPh_dot*theta_dot/RPh;
    Xsi_ddot    = -(1 + par.ni*RPh^2/par.IPhz_bar)*dVdXsi/(par.ni*RPh^2) +...
                    + 2*RPh_dot*theta_dot/RPh;



%%  Phobos motion from that weird potential 

%   Tutto adimensionale
    G   = par.G;
    M2  = par.MPho;
    M1  = par.MMars; 

    I1x = par.IMx_bar*M1;
    I1y = par.IMy_bar*M1;
    I1z = par.IMz_bar*M1;
    
    I2x = par.IPhx_bar;
    I2y = par.IPhy_bar;
    I2z = par.IPhz_bar;
    
%   Need those quantities 
    Theta1 = theta+Phi;
    Theta2 = theta+Xsi;
    
    A1          = [cos(Theta1), sin(Theta1), 0; -sin(Theta1), cos(Theta1), 0; 0, 0, 1];
    dA1dtheta   = [-sin(Theta1), cos(Theta1), 0; -cos(Theta1), -sin(Theta1), 0; 0, 0, 0];
    A2          = [cos(Theta2), sin(Theta2), 0; -sin(Theta2), cos(Theta2), 0; 0, 0, 1];
    dA2dtheta   = [-sin(Theta2), cos(Theta2), 0; -cos(Theta2), -sin(Theta2), 0; 0, 0, 0];


    I1_bar  = diag([I1x, I1y, I1z])/M1;
    I2_bar  = diag([I2x, I2y, I2z])/M2;

%   Chain rule terms
    dRPhdx      = x(1)/norm(x(1:3)); 
    dRPhdy      = x(2)/norm(x(1:3));
    dRPhdz      = x(3)/norm(x(1:3));
    dthetadx    = -x(2)/(x(1)^2 + x(2)^2);
    dthetady    = x(1)/(x(1)^2 + x(2)^2);
    dthetadz    = 0;
    dbetadx     = -x(3)*x(1)/((1 - x(3)^2/norm(x(1:3))^2)^(1/2)*norm(x(1:3))^3);
    dbetady     = -x(3)*x(2)/((1 - x(3)^2/norm(x(1:3))^2)^(1/2)*norm(x(1:3))^3);
    dbetadz     = (1/norm(x(1:3)) - x(3)^2/norm(x(1:3))^3)/(1 - x(3)^2/norm(x(1:3))^2)^(1/2);
 
    
    dVdtheta    = G*M1*M2/norm(x(1:3))*(1.5/norm(x(1:3))^4*x(1:3)'*...
        ((dA1dtheta'*I1_bar*A1 + A1'*I1_bar*dA1dtheta) + (dA2dtheta'*I2_bar*A2 + A2'*I2_bar*dA2dtheta))*x(1:3));

%   Gravitational acceleration in x,y,z
    g_M =  -[dRPhdx, dthetadx, dbetadx; dRPhdy, dthetady, dbetady; dRPhdz, dthetadz, dbetadz]*[dVdRPh; dVdtheta; 0];
    g_tot   = g_M;


%%  Output for the integrator

    dx = [x(4:6); g_tot; RPh_dot; RPh_ddot; theta_dot; theta_ddot; Phi_dot; Phi_ddot; Xsi_dot; Xsi_ddot];
    
    
end