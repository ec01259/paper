function z = MMX_FromInertial2SynodicFrame(y, Model)

N = size(y, 1);
Xpm = y(:, 7:12)';


% Define axes of synodic frame
eR = Xpm(1:3, :)./vecnorm(Xpm(1:3, :));
eH = cross(Xpm(1:3, :), Xpm(4:6, :))./vecnorm(cross(Xpm(1:3, :), Xpm(4:6, :)));
eT = cross(eH, eR);


% Calculate pos. and velocity vector wrt Phobos barycenter
x1 = [dot(eR, y(:, 1:3)'); dot(eT, y(:, 1:3)'); dot(eH, y(:, 1:3)')] - [Model.pars.a; 0; 0];
v1 = [dot(eR, y(:, 4:6)'); dot(eT, y(:, 4:6)'); dot(eH, y(:, 4:6)')] - cross(repmat([0; 0; Model.pars.n], 1, N), x1 + [Model.pars.a; 0; 0]);
z  = [x1', v1'];