function dx = MPhMMX_CoupledLibration_Cartesian(~,x,par,~)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% dx = MPhMMX_CoupledLibration_Cartesian(t,x,pars)
%
% Calculate the acceleration vector in the N-Body Problem with Mars and
% Phobos only and the derivative of the coefficients to be estimated
%
% Input:
% .t        Elapsed time since epoch (-)
% .x        State Vector 
% .pars     Problem Parameters
%           .d          Dimension of the Problem
%           .et0        Initial epoch (-)
%           .refcenter  Origin of Frame (e.g., '399')
%           .refframe   Coordinate Frame (e.g., 'ECLIPJ2000' or 'J2000')
%           .Ntb        No. of third bodies
%           .BodiesID   Bodies' SPICE ID (e.g., '10','399', ...)
%           .GM         Bodies' Gravitational Parameters (-)
%           .Nsh        No. of Spherical Harmonic Bodies
%           .SH_BodiesID Spherical Harmonic Bodies' SPICE ID (e.g., '10','399', ...)
%           .SH         Spherical Harmonic Structure
%           .lsf        Length scale factor (km)
%           .tsf        Time scale factor (s)
%           .STM        true or false depending if you calculate STM or not
%           .nCoeff     Number if coefficients in the state vector
%
% Output:
% .dx       Vector containing accelerations and STM (d x 2 + nCoeff, -)
%
% Author: E.Ciccarelli
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%  Dimensions

    d           = par.d;                    % Dimension of the problem

%%  Useful quantities
    
%   MMX states
    
    r = x(1:3);     % Pos. vector wrt central body
    v = x(4:6);     % Pos. vector wrt central body
    R = norm(r);    % Norm of pos. vector wrt central body

%   Phobos states
    
    Phi         = x(13);
    Phi_dot     = x(14);
    Xsi         = x(15);
    Xsi_dot     = x(16);

    r_Ph        = x(7:9);
    v_Ph        = x(10:12);
    
    RPh         = norm(r_Ph);
    h           = cross(r_Ph,v_Ph);
    theta_dot   = norm(h)/RPh^2;
    RPh_dot     = dot(r_Ph,v_Ph)/RPh;
    Ph_0    = par.MARSIAU2perifocal*r_Ph;
    theta   = atan2(Ph_0(2),Ph_0(1));

%   Position of MMX wrt Phobos
    rsb     = r - r_Ph;

%   Rotation matrix from orbit's to Phobos fixed coordinates frame    
    i       = r_Ph/RPh;
    k       = par.perifocal2MARSIAU*[0; 0; 1];
    j       = cross(k,i);
    NO      = [i,j,k];
    ON      = NO';

%   Rotation matrix from orbit's to Phobos fixed coordinates frame
    BO      = [cos(Xsi), sin(Xsi), 0; -sin(Xsi), cos(Xsi), 0; 0, 0, 1];
    OB      = BO';

%%  Phobos's Motion

%   Potential's first order partials
    dVdRPh      = par.ni/RPh^2*(1 + 3/(2*RPh^2)*((par.IMz_bar - par.Is) -...
        .5*par.IPhx_bar - .5*par.IPhy_bar + par.IPhz_bar + ...
        1.5*(par.IPhy_bar - par.IPhx_bar)*cos(2*Xsi)));
    dVdXsi      = 1.5*par.ni/RPh^3 * (par.IPhy_bar - par.IPhx_bar)*sin(2*Xsi);

%   Phobos equations of motions
    Phi_ddot    = - dVdXsi/(par.ni*RPh^2) + 2*RPh_dot*theta_dot/RPh;
    Xsi_ddot    = -(1 + par.ni*RPh^2/par.IPhz_bar)*dVdXsi/(par.ni*RPh^2) +...
                    + 2*RPh_dot*theta_dot/RPh;

%   Tutto adimensionale
    G   = par.G;
    M2  = par.MPho;
    M1  = par.MMars; 

    I1x = par.IMx_bar*M1;
    I1y = par.IMy_bar*M1;
    I1z = par.IMz_bar*M1;
    
    I2x = par.IPhx_bar;
    I2y = par.IPhy_bar;
    I2z = par.IPhz_bar;
    
%   Need those quantities 
    Theta1 = theta+Phi;
    Theta2 = theta+Xsi;
    
    A1          = [cos(Theta1), sin(Theta1), 0; -sin(Theta1), cos(Theta1), 0; 0, 0, 1];
    dA1dtheta   = [-sin(Theta1), cos(Theta1), 0; -cos(Theta1), -sin(Theta1), 0; 0, 0, 0];
    A2          = [cos(Theta2), sin(Theta2), 0; -sin(Theta2), cos(Theta2), 0; 0, 0, 1];
    dA2dtheta   = [-sin(Theta2), cos(Theta2), 0; -cos(Theta2), -sin(Theta2), 0; 0, 0, 0];

     I1  = diag([I1x, I1y, I1z]);
    I2  = diag([I2x, I2y, I2z]);

%   Chain rule terms
    dRPhdx      = r_Ph(1)/RPh; 
    dRPhdy      = r_Ph(2)/RPh;
    dRPhdz      = r_Ph(3)/RPh;
    dthetadx    = -r_Ph(2)/(r_Ph(1)^2 + r_Ph(2)^2);
    dthetady    = r_Ph(1)/(r_Ph(1)^2 + r_Ph(2)^2);
    dthetadz    = 0;
    dbetadx     = -r_Ph(3)*r_Ph(1)/((1 - r_Ph(3)^2/RPh^2)^(1/2)*RPh^3);
    dbetady     = -r_Ph(3)*r_Ph(2)/((1 - r_Ph(3)^2/RPh^2)^(1/2)*RPh^3);
    dbetadz     = (1/RPh - r_Ph(3)^2/RPh^3)/(1 - r_Ph(3)^2/RPh^2)^(1/2);
 
    dVdtheta    = 3*G/(2*norm(r_Ph(1:3))^5)*r_Ph(1:3)'*(M2*(dA1dtheta'*I1*A1 +...
        A1'*I1*dA1dtheta) + M1*(dA2dtheta'*I2*A2 + A2'*I2*dA2dtheta))*r_Ph(1:3);


%   Gravitational acceleration in x,y,z
    g_M_on_PH   =  -[dRPhdx, dthetadx, dbetadx; dRPhdy, dthetady, dbetady; dRPhdz, dthetadz, dbetadz]*[dVdRPh; dVdtheta; 0];
    g_Ph_on_M   = g_M_on_PH*(-(M2+M1)/(M2*M1))/M1;

%%  MMX 2BP with Mars + 3rd body Phobos 

%   The gravitational effect of Mars on MMX is 2BP+J2

    gM      = - par.G*par.MMars*r/R^3;
    gJ2M    = - 1.5*par.G*par.MMars*par.J2*par.RMmean/R^5*...
        [(1-5*(r(3)/R)^2)*r(1); (1-5*(r(3)/R)^2)*r(2);...
        (3-5*(r(3)/R)^2)*r(3)];

    gM      = gM + gJ2M;

%   The gravitational effect of Phobos on MMX includes the Ellipsoid
%   harmonics

    [gb,~,~,~] = SHGM_Full(BO*ON*rsb, par.SH);
    gPh = NO*OB*gb;

%   Combined effect on MMX
    g_MMX   = gM + gPh - g_Ph_on_M;

%%  Output for the integrator

    dx = [v; g_MMX; v_Ph; g_M_on_PH; Phi_dot; Phi_ddot; Xsi_dot; Xsi_ddot];
    
    
end