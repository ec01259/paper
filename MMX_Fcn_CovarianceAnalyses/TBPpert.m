function f = TBPpert(t, X, pars)

% Parameters
SH  = pars.SH;
GMp = SH.GM;
GMm = pars.GM1;


% Retrieve Phobos coordinates
Xpm = X(7:12);
rpm = Xpm(1:3); Rpm = norm(rpm);
vpm = Xpm(4:6);
hpm = cross(rpm, vpm);


% Calculate rotation matrix from inertial 2 synodic frame
eR = rpm/norm(rpm);
eH = hpm/norm(hpm);
eT = cross(eH, eR);
SN = [eR'; eT'; eH'];
NS = SN';


% pos. vector of the spacecraft with respect to Mars
r = X(1:3); 
R = norm(r);
v = X(4:6);


% pos. vector of the spacecraft with respect to Phobos
rp = r - rpm; 
g2 = NS*CGM(SN*rp, SH);


% Dynamics
f = zeros(12,1);
f(1:3) = v;
f(4:6) = - GMm*r/R^3 + g2 - GMp*rpm/Rpm^3;
f(7:9) = Xpm(4:6);
f(10:12) = - (GMp + GMm)*Xpm(1:3)/Rpm^3;