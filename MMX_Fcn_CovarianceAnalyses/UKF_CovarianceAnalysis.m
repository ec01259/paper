function [Est] = UKF_CovarianceAnalysis(Est0,f,fsigma,G,O,Measures,par,units)
%==========================================================================
% [Est] = UKF_CovarianceAnalysis(Est0,f,G_dG,R,Measures,par)
%
% Unscented Kalman Filter
%
% INPUT: Description Units
%
% Est0      - Apriori initial guess of
%               .X,     States 
%               .P0,    Covariance matrix
%               .t0,    initial time  
% 
% @f        - Dynamical model
% 
% @G        - Observation's model and partial derivatives wrt the states
% 
% O         - Weight matrix
% Nsigma    - Number of sigma points
% Measures  - Observations matrix: the first column contains observation
%               times, then every column has a different type of observaion
%               type
% par       - Structure containing parameters required by @f and @G_dG 
% units     - Structure containing lenght and time units 
%
% OUTPUT:
%
% Est       - Estimation of:
%               .X, States at final t
%               .P, Covariance matrix at final t
%               .t, Time observation vector
%               .X_t, States at different t
%               .P_t, Covariance matrix at different t  
%               .Pbar_t, Covariance matrix time update at different t
% 
%
% Coupling:
% None
%
% Author: Edoardo Ciccarelli
% Date: 04/2021
%
%==========================================================================


%   Apriori initial guess, deviation and covariance matrix.
    Xold    = Est0.X;
    Pold    = Est0.P0;
    
%   Column of the measures time.
    tobs    = Measures(1,:);
    told    = tobs(1);

%   The measures must be organized in colums depending on the type of
%   measure; each row is reffered to a given measure's time.
    obs     = Measures(2:end,:)./units.Observations;
    
%   n,l and m are respectively the number of states, number of different
%   types of measures and number of measures per each type.
    n       = size(Xold,1);
    m       = size(obs,2);
    q       = 3;

%   err is the vector of residuals, the others are needed to build the 
%   fitting residuals.
    err     = NaN(size(O,1),m);
    prefit  = NaN(size(O,1),m);
    y_t     = NaN(size(O,1),m);
    X_t     = zeros(n,size(tobs,1));
    x_t     = zeros(n,size(tobs,1));
    P_t     = zeros(n,n,m);
    Pbar_t  = zeros(n,n,m);
    
%   Initialization of the filter
    k       = 3 - n;
    alpha   = par.alpha;
    lambda  = alpha^2*(n+k) - n;
    gamma   = sqrt(n+lambda);

%   Sigma points' weights
    W0c     = zeros(2*n, 1);
    W0c(:)  = 1./(2*(n+lambda));
    
%   Reference orbit propagation
    interval_Range      = par.interval_Range;
    interval_Range_rate = par.interval_Range_rate;
    interval_lidar      = par.interval_lidar;
    interval_camera     = par.interval_camera;
    min_interval        = min([interval_Range; interval_Range_rate; interval_lidar; interval_camera]);

    switch n
        case 14
            St0 = [Xold; par.IPhx_bar; par.IPhy_bar; par.IPhz_bar];
        case 17
            St0 = Xold(1:17);
        case 18
            St0 = par.X0_reference;
        case 20
            St0 = Xold(1:17);
        case 22
            St0 = Xold(1:17);
    end

    tspan   = (0:min_interval:tobs(end))*units.tsf;
    RelTol  = 1e-13;
    AbsTol  = 1e-16; 
    opt     = odeset('RelTol',RelTol,'AbsTol',AbsTol,'event',@(t,X) landing_Phobos(t,X,par,units));
    [t,X]   = ode113(@(t,X) f(t,X,par,units),tspan,St0,opt);
    fprintf('\nOrbit propagation complete...');

%     err1 = zeros(6,size(X,1));
%     err2 = zeros(6,size(X,1));
%     for i = 1:size(X,1)
%         et  = Est0.t0/units.tsf + tobs(i);
%         MMX = cspice_spkezr('-34', et, 'MarsIAU', 'none', '499')./units.sfVec;
%         err1(:,i) = ((X(i,1:6)' - MMX)).*units.sfVec;
%     end

%     figure()
%     subplot(2,1,1)
%     plot(tobs,err1(1,:))
%     hold on
%     grid on
%     plot(tobs,err1(2,:))
%     plot(tobs,err1(3,:))
%     plot(tobs,vecnorm(err1(1:3,:),2),'LineWidth',1.5)
%     subplot(2,1,2)
%     plot(tobs,err1(4,:))
%     hold on
%     grid on
%     plot(tobs,err1(5,:))
%     plot(tobs,err1(6,:))
%     plot(tobs,vecnorm(err1(4:6,:),2),'LineWidth',1.5)

%   For-loop on the observations
    for i = 1:m
        
        fprintf('\nProcesso osservazione n.: %d', i)

%       Look for the observation to process
        if tobs(i) == told
                idx     = 1;
            switch n
                case 14
                    Xmean_bar   = X(idx,1:size(Est0.X,1))';
                case 17
                    Xmean_bar   = X(idx,:)';
                case 18
                    Xmean_bar   = [X(idx,1:10)'; X(idx,13:end)'; 0; 0; 0];
                    par.PhiM    = X(idx,11);
                case 20
                    Xmean_bar   = [X(idx,:)'; 0; 0; 0];
                case 22
                    Xmean_bar   = [X(idx,:)'; 0; 0; 0; 0; 0];
            end
        elseif tobs(i) > told
            idx     = find(round(t/units.tsf)==tobs(i));
            switch n
                case 14
                    Xmean_bar   = X(idx,1:size(Est0.X,1))';
                case 17
                    Xmean_bar   = X(idx,:)';
                case 18
                    Xmean_bar   = [X(idx,1:10)'; X(idx,13:end)'; 0; 0; 0];
                case 20
                    Xmean_bar   = [X(idx,:)'; 0; 0; 0];
                case 22
                    Xmean_bar   = [X(idx,:)'; 0; 0; 0; 0; 0];    
            end
        end

%       Useful quantities's initialization
        et  = Est0.t0/units.tsf + tobs(i);
        
%       TIME UPDATE
%       Sigma points' definition
        S0 = real(sqrtm(Pold)); 
        X0 = [(repmat(Xold,1,n)-gamma.*S0),...
            (repmat(Xold,1,n)+gamma.*S0)];

%       Need to know how many observations are available
        where       = isnan(obs(1:end,idx));
        count       = sum(~where);
        IDX         = [];
        Y_obs       = zeros(count,1);
        R           = zeros(count, count);

        if count ~= 0
            for z=1:size(where,1)
                 if where(z)==0
                     IDX = [IDX; z];
                 end
            end
    
            for j=1:count
                Y_obs(j)= obs(IDX(j),idx);
                R(j,j)  = O(IDX(j),IDX(j));
            end
        else
            Y_obs       = obs(:,idx);
            R           = O;
        end

        if tobs(idx) == told

%           The sigma point dont need to be proppagated
            Xbar    = X0;

        elseif tobs(idx) > told

%           Propagation of the sigma points' trajectories
            St0     = reshape(X0,[n*(2*n),1]);
            tspan   = (told:tobs(idx))*units.tsf;
            opt     = odeset('RelTol',1e-13,'AbsTol',1e-16,'event',@(t,X) landing_Phobos_UKF(t,X,par,units));
            [~,X_sigma] = ode113(@(t,X) fsigma(t,X,par,units),tspan,St0,opt);
            Xbar    = reshape(X_sigma(end,1:n*(2*n))',[n,(2*n)]);

        end


%       Process noise if needed
        deltaT  = (tobs(i)-told)*units.tsf;
        Q       = par.sigma^2*eye(3);
        switch n
            case 14
                Gamma   = [eye(q)*deltaT^2/2; eye(q)*deltaT; zeros(n-6,q)];
                Q = Gamma*Q*Gamma' + [zeros(6,n); zeros(n-6,6), par.sigmaPh^2*...
                    diag([deltaT^2/2, deltaT, deltaT^2/2, deltaT, deltaT^2/2, deltaT, deltaT^2/2, deltaT])];
            case 17
                Gamma   = [eye(q)*deltaT^2/2; eye(q)*deltaT; zeros(n-6,q)];
                Q = Gamma*Q*Gamma' + [zeros(6,n); zeros(n-6,6), par.sigmaPh^2*...
                    diag([deltaT^2/2, deltaT, deltaT^2/2, deltaT, deltaT^2/2, deltaT, deltaT^2/2, deltaT, zeros(1,n-14)])];  
            case 18
                Gamma   = [eye(q)*deltaT^2/2; eye(q)*deltaT; zeros(n-6,q)];
                Q = Gamma*Q*Gamma' + [zeros(6,n); zeros(n-6,6), par.sigmaPh^2*...
                    diag([deltaT^2/2, deltaT, deltaT^2/2, deltaT, deltaT^2/2, deltaT, zeros(1,n-12)])];  
            case 20
                Gamma   = [eye(q)*deltaT^2/2; eye(q)*deltaT; zeros(n-6,q)];
                Q = Gamma*Q*Gamma' + [zeros(6,n); zeros(n-6,6), par.sigmaPh^2*...
                    diag([deltaT^2/2, deltaT, deltaT^2/2, deltaT, deltaT^2/2, deltaT, deltaT^2/2, deltaT, zeros(1,n-14)])];  
            case 22
                Gamma   = [eye(q)*deltaT^2/2; eye(q)*deltaT; zeros(n-6,q)];
                Q = Gamma*Q*Gamma' + [zeros(6,n); zeros(n-6,6), par.sigmaPh^2*...
                    diag([deltaT^2/2, deltaT, deltaT^2/2, deltaT, deltaT^2/2, deltaT, deltaT^2/2, deltaT, zeros(1,n-14)])];  
        end

%       Covariance matrix's time update 
        P_bar   = Q;

        for j = 1:2*n
            P_bar   = P_bar + W0c(j)*((Xbar(:,j) - Xmean_bar) * (Xbar(:,j) - Xmean_bar)');
        end
        
%       Sigma point redefinition
        S_bar = real(sqrtm(P_bar));
        X0  = [(repmat(Xmean_bar,1,n)-gamma.*S_bar),...
            (repmat(Xmean_bar,1,n)+gamma.*S_bar)];
        
        Y = zeros(count,2*n);

        for j = 1:2*n
%           G(j-th sigma point)
            Y_sigma = G(et,obs(:,idx),X0(:,j),par,units);
            Y(:,j)  = Y_sigma';
        end

%       Mean predicted measurement
        Y_mean  = G(et,obs(:,idx),Xmean_bar,par,units)';
        
%       Innovation covariance and cross covariance

        Py      = R;
        Pxy     = zeros(n,count);

        for j = 1:2*n
            Py  = Py + W0c(j)*(Y(:,j) - Y_mean)*(Y(:,j) - Y_mean)';
            Pxy = Pxy + W0c(j)*(X0(:,j) - Xmean_bar)*(Y(:,j) - Y_mean)';
        end
        
%       Kalman gain
        K   = Pxy/Py;

%       MEASUREMENT UPDATE
        y      = (Y_obs - Y_mean);
        Xstar  = Xmean_bar + K*y;
        P      = P_bar - K*Py*K';
        
%       Next iteration preparation
        Xold   = Xmean_bar;
        told   = tobs(idx);
        Pold   = P;
    

        err(IDX,i)    = (Y_obs - G(et,obs(:,idx),Xstar,par,units)').*units.Observations(IDX);
        prefit(IDX,i) = (Y_obs - G(et,obs(:,idx),Xmean_bar,par,units)').*units.Observations(IDX);
        y_t(IDX,i)    = y.*units.Observations(IDX);


%       Storing the results
        P_t(:,:,i)  = P.*units.CovDim;
        Pbar_t(:,:,i)  = P_bar.*units.CovDim;
        X_t(:,i)    = Xold.*units.Dim;
        x_t(:,i)    = K*y.*units.Dim;
        
        
%         et  = Est0.t0/units.tsf + tobs(i);
%         MMX = cspice_spkezr('-34', et, 'MarsIAU', 'none', '499')./units.sfVec;
%         err2(:,i) = ((Xmean_bar(1:6) - MMX)).*units.sfVec;
        

    end
    
    fprintf('\nAnalysis complete!\n');
    
%     figure()
%     subplot(2,1,1)
%     plot(tobs,err2(1,:))
%     hold on
%     grid on
%     plot(tobs,err2(2,:))
%     plot(tobs,err2(3,:))
%     plot(tobs,vecnorm(err2(1:3,:),2),'LineWidth',1.5)
%     subplot(2,1,2)
%     plot(tobs,err2(4,:))
%     hold on
%     grid on
%     plot(tobs,err2(5,:))
%     plot(tobs,err2(6,:))
%     plot(tobs,vecnorm(err2(4:6,:),2),'LineWidth',1.5)


%   Initial states' estimation, deviation and covariance matrix
    Est.X       = Xold.*units.Dim;
    Est.P       = P.*units.CovDim;
    
%   Residuals and fit-residuals at different observation times
    Est.t       = tobs;
    Est.err     = err;
    Est.pre     = prefit;
    Est.y_t     = y_t;
    Est.X_t     = X_t;
    Est.x_t     = x_t;
    Est.P_t     = P_t;
    Est.Pbar_t  = Pbar_t;
    
end