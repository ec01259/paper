function G = New_Observables_model(et,Obs,X,par,units)
%==========================================================================
% [G] = New_Observables_model(t,Obs,X,par,units)
%
% Compute the Range, Range Rate, Lidar's and camera's measures 
%
% INPUT: Description Units
%
% et        - Time Epoch                                                s
%
% stationID - Available Observables at the epoch t
%
% X         - State Vector defined in the Phobos rotating frame 
%               at the time being considered (42x1)
%               .MMX Position Vector (3x1)                              km
%               .MMX Velocity Vector (3x1)                              km/s
%               .Phobos Position Vector (3x1)                           km
%               .Phobos Velocity Vector (3x1)                           km/s
%               .Clm harmonics coefficients
%               .Slm Hamronics coefficients
%               .State Transition Matrix 
%
% par       -  Parameters structure 
% 
%
% OUTPUT:
%
% G         - G
% 
% DO NOT FORGET TO USE RESHAPE LATER IN THE PROJECT
%
% Coupling:
% None
%
% Author: Edoardo Ciccarelli
% Date: 06/2022
%
%==========================================================================
    
%   Dimensions of the problem
    n       = par.d/2;
    d       = par.d;
   
%   Output initialization
    range_Camb       = NaN;
    range_rate_Camb  = NaN;
    range_Gold       = NaN;
    range_rate_Gold  = NaN;
    range_Mad        = NaN;
    range_rate_Mad   = NaN;
    lidar            = NaN;
    camera           = [NaN, NaN];

%   Unpacking of the state vector
    X_MMX       = X(1:d);
    r_MMX       = X_MMX(1:3);
    v_MMX       = X_MMX(4:6);
    r_Phobos    = par.perifocal2MARSIAU*X(d+1)*[cos(X(d+3)); sin(X(d+3)); 0];
    
    switch size(X,1)
        case 17
            bias        = par.bias;
        case 18
            bias        = X(16:end);
        case 20
            bias        = X(18:end);
        case 22
            bias        = X(18:end);
    end

%   Definition of Mars position WRT Earth J2000
    [Mars, ~]   = cspice_spkezr('499',et,'MARSIAU','none','399');
    Mars        = Mars./units.sfVec;

%   Useful to understand the observables that are available
    where       = isnan(Obs);
    
%   Which station is working
%   Camberra's Position in the J2000 Earth-centered
    [Camb, ~]  = cspice_spkezr('DSS-45', et, 'MARSIAU', 'none', 'EARTH');
    Camb       = Camb./units.sfVec;    
%   Goldstone's Position in the J2000 Earth-centered
    [Gold, ~]  = cspice_spkezr('DSS-24', et, 'MARSIAU', 'none', 'EARTH');
    Gold       = Gold./units.sfVec;
%   Madrid's Position in the J2000 Earth-centered
    [Mad, ~]   = cspice_spkezr('DSS-65', et, 'MARSIAU', 'none', 'EARTH');
    Mad        = Mad./units.sfVec;

    if where(1) == 0
%       Camberra antennas available
        r           = norm(Mars(1:n) + r_MMX - Camb(1:n));
        range_Camb  = r + bias(1);
                    
    end
    
    if where(2) == 0
%       Camberra antennas available
        R       = norm(Mars(1:n) + r_MMX - Camb(1:n));
        r_dot   = 1/R*sum((Mars(n+1:d) + v_MMX - Camb(n+1:d)).*(Mars(1:n) + r_MMX - Camb(1:n)));
        range_rate_Camb = r_dot + bias(2);
                    
    end

    if where(3) == 0
%       Goldstone antennas available
        r           = norm(Mars(1:n) + r_MMX - Gold(1:n));
        range_Gold  = r + bias(1);
                    
    end
    
    if where(4) == 0
%       Goldstone antennas available
        R       = norm(Mars(1:n) + r_MMX - Gold(1:n));
        r_dot   = 1/R*sum((Mars(n+1:d) + v_MMX - Gold(n+1:d)).*(Mars(1:n) + r_MMX - Gold(1:n)));
        range_rate_Gold = r_dot + bias(2);
                    
    end

    if where(5) == 0
%       Madrid antennas available
        r     = norm(Mars(1:n) + r_MMX - Mad(1:n));
        range_Mad = r + bias(1);
                    
    end
    
    if where(6) == 0
%       Camberra antennas available
        R       = norm(Mars(1:n) + r_MMX - Mad(1:n));
        r_dot   = 1/R*sum((Mars(n+1:d) + v_MMX - Mad(n+1:d)).*(Mars(1:n) + r_MMX - Mad(1:n)));
        range_rate_Mad = r_dot + bias(2);
                    
    end
    
    if where(7) == 0

%      Lidar available
%        lidar   = norm(r_MMX - r_Phobos) + bias(3);

%      Lidar rispetto alla superficie
       I    = r_Phobos/X(7);
       k    = par.perifocal2MARSIAU*[0; 0; 1];
       j    = cross(k,I);
       NO   = [I,j,k];
       ON   = NO';

       Xsi  = X(13);
       BO   = [cos(Xsi), sin(Xsi), 0; -sin(Xsi), cos(Xsi), 0; 0, 0, 1];
        
       rsb  = r_MMX-r_Phobos;

%      Posizione di MMX nel Phobos Body-fixed reference frame
       r_bf = BO*ON*rsb;
       
       lat  = asin(r_bf(3)/norm(r_bf));
       lon  = atan2(r_bf(2), r_bf(1));

%      Phobos radius as function of latitude and longitude
       alpha = par.Phobos.alpha;
       beta  = par.Phobos.beta;
       gamma = par.Phobos.gamma;
       
       R_latlon = (alpha*beta*gamma)/sqrt(beta^2*gamma^2*cos(lon)^2 +...
           gamma^2*alpha^2*sin(lon)^2*cos(lat)^2 + alpha^2*beta^2*sin(lon)^2*sin(lat));

       lidar = norm(rsb) - R_latlon/units.lsf + bias(3);

        
    end
    
    if where(8) == 0
%       Camera available
        rsb     = X_MMX(1:3) - r_Phobos;
        Rsb     = norm(rsb);

        I       = -rsb/Rsb;
        v       = [0; 0; 1];
        j       = cross(v, I)/norm(cross(v, I));
        k       = cross(I, j)/norm(cross(I, j));
        
        T       = [I, j, k]';

        switch size(X,1)
            case 20
                camera  = ([0 1 0; 0 0 1]*T*I)';
            case 22
                camera  = ([0 1 0; 0 0 1]*T*I)' + [bias(4), bias(5)];
        end
        
    end
   
    G           = [range_Camb, range_rate_Camb, range_Gold, range_rate_Gold, ...
        range_Mad, range_rate_Mad, lidar, camera];
    G(isnan(G)) = [];

     
end