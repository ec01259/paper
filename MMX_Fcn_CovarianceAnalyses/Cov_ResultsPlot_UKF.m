function Cov_ResultsPlot_UKF(PhobosGravityField, Est, YObs_Full, pars, units)
%==========================================================================
% Cov_ResultsPlot(Est, YObs_Full, pars)
%
% This function plots the results of the covariance analysis
%
% INPUT: Description Units
%
% Est       - Estimation of:
%               .X, States at final t
%               .x, States' deviation vector at final t
%               .P, Covariance matrix at final t
%               .t, Time observation vector
%               .err, Post-fit residuals
%               .pre, Pre-fit residuals
%               .y_t, Pre-fit residuals
%               .X_t, States at different t
%               .x_t, States deviation vector at different t
%               .Phi_t, STM at different t
%               .P_t, Covariance matrix at different t  
%               .Pbar_t, Covariance matrix time update at different t
% 
% YObs_Full     Vector of observations and time of observations
%
% pars       - Structure containing problem's parameters
%
%
% Coupling:
% None
%
% Author: Edoardo Ciccarelli
% Date: 09/2021
%
%==========================================================================


    t_obs   = YObs_Full(1,:);

%   Residuals

    figure()
    subplot(2,5,1)
    plot(t_obs/3600,Est.pre(1,:),'x','Color','b')
    grid on;
    hold on;
    plot(t_obs/3600,Est.pre(3,:),'x','Color','b')
    plot(t_obs/3600,Est.pre(5,:),'x','Color','b')
    plot(t_obs/3600,3*pars.ObsNoise.range*units.lsf*ones(size(Est.pre(1,:),2)),'.-','Color','b')
    plot(t_obs/3600,-3*pars.ObsNoise.range*units.lsf*ones(size(Est.pre(1,:),2)),'.-','Color','b')
    plot(t_obs/3600,3*pars.ObsNoise.range*units.lsf*ones(size(Est.pre(3,:),2)),'.-','Color','b')
    plot(t_obs/3600,-3*pars.ObsNoise.range*units.lsf*ones(size(Est.pre(3,:),2)),'.-','Color','b')
    plot(t_obs/3600,3*pars.ObsNoise.range*units.lsf*ones(size(Est.pre(5,:),2)),'.-','Color','b')
    plot(t_obs/3600,-3*pars.ObsNoise.range*units.lsf*ones(size(Est.pre(5,:),2)),'.-','Color','b')
    xlabel('$[hour]$')
    ylabel('$[km]$')
    title('Range Pre-fit residual')
    subplot(2,5,2)
    plot(t_obs/3600,Est.pre(2,:),'x','Color','r')
    grid on;
    hold on;
    plot(t_obs/3600,Est.pre(4,:),'x','Color','r')
    plot(t_obs/3600,Est.pre(6,:),'x','Color','r')
    plot(t_obs/3600,3*pars.ObsNoise.range_rate*units.vsf*ones(size(Est.pre(2,:),2)),'.-','Color','r')
    plot(t_obs/3600,-3*pars.ObsNoise.range_rate*units.vsf*ones(size(Est.pre(2,:),2)),'.-','Color','r')
    plot(t_obs/3600,3*pars.ObsNoise.range_rate*units.vsf*ones(size(Est.pre(4,:),2)),'.-','Color','r')
    plot(t_obs/3600,-3*pars.ObsNoise.range_rate*units.vsf*ones(size(Est.pre(4,:),2)),'.-','Color','r')
    plot(t_obs/3600,3*pars.ObsNoise.range_rate*units.vsf*ones(size(Est.pre(6,:),2)),'.-','Color','r')
    plot(t_obs/3600,-3*pars.ObsNoise.range_rate*units.vsf*ones(size(Est.pre(6,:),2)),'.-','Color','r')
    xlabel('$[hour]$')
    ylabel('$[km/s]$')    
    title('RangeRate Pre-fit residual') 
    subplot(2,5,3)
    plot(t_obs/3600,Est.pre(7,:),'x','Color','g')
    grid on;
    hold on;
    plot(t_obs/3600,3*pars.ObsNoise.lidar*units.lsf*ones(size(Est.pre(7,:),2)),'.-','Color','g')
    plot(t_obs/3600,-3*pars.ObsNoise.lidar*units.lsf*ones(size(Est.pre(7,:),2)),'.-','Color','g')
    xlabel('$[hour]$')
    ylabel('$[km]$')
    title('Lidar Pre-fit residual') 
    subplot(2,5,4)
    plot(t_obs/3600,Est.pre(8,:),'x','Color','m')
    grid on;
    hold on;
    plot(t_obs/3600,3*pars.ObsNoise.camera*ones(size(Est.pre(8,:),2)),'.-','Color','m')
    plot(t_obs/3600,-3*pars.ObsNoise.camera*ones(size(Est.pre(8,:),2)),'.-','Color','m')
    xlabel('$[hour]$')
    ylabel('$[rad]$')
    title('Camera Pre-fit residual') 
    subplot(2,5,5)
    plot(t_obs/3600,Est.pre(9,:),'x','Color','k')
    grid on;
    hold on;
    plot(t_obs/3600,3*pars.ObsNoise.camera*ones(size(Est.pre(9,:),2)),'.-','Color','k')
    plot(t_obs/3600,-3*pars.ObsNoise.camera*ones(size(Est.pre(9,:),2)),'.-','Color','k')
    xlabel('$[hour]$')
    ylabel('$[rad]$')
    title('Camera Pre-fit residual') 


%     subplot(2,5,6)
%     histfit([Est.pre(1,:), Est.pre(3,:), Est.pre(5,:)])
%     xlabel('$[km]$')
%     title('Range Pre-fit residual') 
%     subplot(2,5,7)
%     histfit([Est.pre(2,:),Est.pre(4,:), Est.pre(6,:)])
%     xlabel('$[km/s]$')
%     title('Range Rate Pre-fit residual') 
%     subplot(2,5,8)
%     histfit(Est.pre(7,:))
%     xlabel('$[km]$')
%     title('Lidar Pre-fit residual')
%     subplot(2,5,9)
%     histfit(Est.pre(8,:))
%     xlabel('$[rad]$')
%     title('Camera Pre-fit residual')
%     subplot(2,5,10)
%     histfit(Est.pre(9,:))
%     xlabel('$[rad]$')
%     title('Camera Pre-fit residual')

%   Square-Root of the MMX covariance error

    sqx = squeeze(Est.P_t(1,1,1:end-1));
    sqy = squeeze(Est.P_t(2,2,1:end-1));
    sqz = squeeze(Est.P_t(3,3,1:end-1));
    SQRT_X = 3*sqrt(sqx + sqy + sqz);
    sqvx = squeeze(Est.P_t(4,4,1:end-1));
    sqvy = squeeze(Est.P_t(5,5,1:end-1));
    sqvz = squeeze(Est.P_t(6,6,1:end-1));
    SQRT_V = 3*sqrt(sqvx + sqvy + sqvz);

    
%   3sigma Envelopes
    figure()
    subplot(1,2,1)
    semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(1,1,1:end-1)))),'Color','b','LineWidth',1)
    hold on;
    grid on;
    semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(Est.P_t(2,2,1:end-1)))),'Color','r','LineWidth',1)
    semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(Est.P_t(3,3,1:end-1)))),'Color','g','LineWidth',1)
    semilogy(t_obs(1:end-1)/3600,SQRT_X,'Color','k','LineWidth',2)
    xlabel('time $[hour]$')
    ylabel('$[km]$')
    title('MMX position vector $3\sigma$ envelopes','Interpreter','latex','FontSize',16)
    legend('$3\sigma_{x}$','$3\sigma_{y}$','$3\sigma_{z}$','$3 RMS$','Interpreter','latex','FontSize',14)
    subplot(1,2,2)
    semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(Est.P_t(4,4,1:end-1)))),'Color','b','LineWidth',1)
    hold on;
    grid on;
    semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(Est.P_t(5,5,1:end-1)))),'Color','r','LineWidth',1)
    semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(Est.P_t(6,6,1:end-1)))),'Color','g','LineWidth',1)
    semilogy(t_obs(1:end-1)/3600,SQRT_V,'Color','k','LineWidth',2)
    xlabel('time $[hour]$')
    ylabel('$[km/s]$')
    title('MMX velocity vector $3\sigma$ envelopes','Interpreter','latex','FontSize',16)
    legend('$3\sigma_{\dot{x}}$','$3\sigma_{\dot{y}}$','$3\sigma_{\dot{z}}$','$3 RMS$','Interpreter','latex','FontSize',14)


%   Square-Root of the Phobos covariance error

    sqx = squeeze(Est.P_t(7,7,1:end-1));
    sqy = squeeze(Est.P_t(8,8,1:end-1));
    sqz = squeeze(Est.P_t(9,9,1:end-1));
    SQRT_X = 3*sqrt(sqx + sqy + sqz);
    sqvx = squeeze(Est.P_t(10,10,1:end-1));
    sqvy = squeeze(Est.P_t(11,11,1:end-1));
    sqvz = squeeze(Est.P_t(12,12,1:end-1));
    SQRT_V = 3*sqrt(sqvx + sqvy + sqvz);

%   3sigma Envelopes
    figure()
    subplot(1,2,1)
    semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(7,7,1:end-1)))),'Color','b','LineWidth',1)
    hold on
    grid on
    semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(Est.P_t(8,8,1:end-1)))),'Color','r','LineWidth',1)
    semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(Est.P_t(9,9,1:end-1)))),'Color','g','LineWidth',1)
    semilogy(t_obs(1:end-1)/3600,SQRT_X,'Color','k','LineWidth',2)
    xlabel('time $[hour]$')
    ylabel('$[km]$')
    title('Phobos position vector $3\sigma$ envelopes','Interpreter','latex','FontSize',16)
    legend('$3\sigma_{x}$','$3\sigma_{y}$','$3\sigma_{z}$','$3 RMS$','Interpreter','latex','FontSize',14)
    subplot(1,2,2)
    semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(Est.P_t(10,10,1:end-1)))),'Color','b','LineWidth',1)
    hold on
    grid on
    semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(Est.P_t(11,11,1:end-1)))),'Color','r','LineWidth',1)
    semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(Est.P_t(12,12,1:end-1)))),'Color','g','LineWidth',1)
    semilogy(t_obs(1:end-1)/3600,SQRT_V,'Color','k','LineWidth',2)
    xlabel('time $[hour]$')
    ylabel('$[km/s]$')
    title('Phobos velocity vector $3\sigma$ envelopes','Interpreter','latex','FontSize',16)
    legend('$3\sigma_{\dot{x}}$','$3\sigma_{\dot{y}}$','$3\sigma_{\dot{z}}$','$3 RMS$','Interpreter','latex','FontSize',14)



%   Harmonics 3sigma Envelopes

    place0 = size(Est.X,1)-pars.nCoeff-pars.nBias;
    corr_label = {'$x$','$y$','$z$','$\dot{x}$','$\dot{y}$','$\dot{z}$',...
        '$x_{Ph}$','$y_{Ph}$','$z_{Ph}$','$\dot{x}_{Ph}$','$\dot{y}_{Ph}$',...
        '$\dot{z}_{Ph}$'};

    figure()
    for i = 0:PhobosGravityField(1)
        subplot(1,PhobosGravityField(1)+1,i+1)
        for j = 0:i
            place0 = place0+1;
            semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(place0,place0,1:end-1)))),'LineWidth',1,...
                'DisplayName',['$C_{',num2str(i),num2str(j),'}$']);
            grid on;
            hold on;
            corr_label{end+1} = ['$C_{',num2str(i),num2str(j),'}$'];
        end
        xlabel('time $[hour]$')
        legend('Interpreter','latex','FontSize',14)
    end
    
%   Slm coefficients
    figure()
    for i = 1:PhobosGravityField(1)
        subplot(1,PhobosGravityField(1),i)
        for j = 1:i
            place0 = place0+1;
            semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(place0,place0,1:end-1)))),'LineWidth',1,...
                'DisplayName',['$S_{',num2str(i),num2str(j),'}$'])
            grid on;
            hold on;
            corr_label{end+1} = ['$S_{',num2str(i),num2str(j),'}$'];
        end
        xlabel('time $[hour]$')
        legend('Interpreter','latex','FontSize',14)
    end

%   Biases
    place0 = size(Est.X,1)-pars.nBias;
    corr_label(end+1) = {'$\rho_b$'};
    corr_label(end+1) = {'$\dot{\rho}_b$'};
    corr_label(end+1) = {'$LIDAR_b$'};
    corr_label(end+1) = {'$Cam1_b$'};
    corr_label(end+1) = {'$Cam2_b$'};

    figure()
    subplot(1,pars.nBias,1)
    semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(place0+1,place0+1,1:end-1)))),'LineWidth',1,...
                'DisplayName',('$\rho_b$'),'Color','b');
    grid on;
    hold on;
    xlabel('time $[hour]$')
    ylabel('$[km]$')
    subplot(1,pars.nBias,2)
    semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(place0+2,place0+2,1:end-1)))),'LineWidth',1,...
                'DisplayName',('$\dot{\rho}_b$'),'Color','r');
    grid on;
    hold on;
    xlabel('time $[hour]$')
    ylabel('$[km/s]$')
    subplot(1,pars.nBias,3)
    semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(place0+3,place0+3,1:end-1)))),'LineWidth',1,...
                'DisplayName',('$LIDAR_b$'),'Color','g');
    grid on;
    hold on;
    xlabel('time $[hour]$')
    ylabel('$[km]$')

    subplot(1,pars.nBias,4)
    semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(place0+4,place0+4,1:end-1)))),'LineWidth',1,...
                'DisplayName',('$Cam1_b$'),'Color','m');
    grid on;
    hold on;
    xlabel('time $[hour]$')
    ylabel('$[rad]$')

    subplot(1,pars.nBias,5)
    semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(place0+5,place0+5,1:end-1)))),'LineWidth',1,...
                'DisplayName',('$Cam2_b$'),'Color','k');
    grid on;
    hold on;
    xlabel('time $[hour]$')
    ylabel('$[rad]$')


%   Correlations coefficients

    [~,corr] = readCovMatrix(real(Est.P));

    figure();
    imagesc(corr)
    title('Correlation Coefficients','FontSize',16);
    set(gca,'FontSize',16);
    colormap(hot);
    colorbar;
    set(gca,'TickLabelInterpreter','latex');
    set(gca,'XTick',(1:size(Est.X,1)));
    set(gca,'XTickLabel',corr_label);
    set(gca,'YTick',(1:size(Est.X,1)));
    set(gca,'YTickLabel',corr_label);
    axis square;
    freezeColors;


end