function Smooth = Smoothing(Est,~,units,varargin)
%--------------------------------------------------------------------------
%   This function performs the smoothing of a certain sequential filter
%   estimate
%   
%   Smooth = Smoothing(Est)
%   Smooth = Smoothing(Est,tk)
%
%   INPUTs
%   - Est           .t          Observations' epoch
%                   .x_t        State deviations at epoch "t" from filter
%                   .P_t        Covariance matrix at epoch "t" from filter
%                   .Pbar_t     Time update of the Covariance matrix at 
%                               epoch "t" from filter
%                   .Phi_t      STM at epoch "t" from filter
% 
%   - tk                        Epoch till when is required the smoothing
%                               (it is Est.t(1) if not specified)
% 
%
%   OUTPUTs
%   Smooth      	.x_lk       State deviation vector smoothed at epoch tk
%                   .P_lk       Covariance matrix smoothed at epoch tk
%                   .x_lk_t     State deviation vector smoothed at all epochs
%                   .P_lk_t     Covariance matrix smoothed at all epochs
% 
% 
%   Author: Edoardo Ciccarelli
%   Date: 02/2020


    switch nargin
        case 4
            tk = varargin{1};
        case 3
            tk = Est.t(1);
    otherwise
        error('Check the help of this function');
    end


%   Observations time's vector
    tobs    = Est.t;
    len     = length(tobs) - find(Est.t==tk) + 1;
    len     = len(1);
    
%   Preallocation of the vectors with everything
    x_lk_t = zeros(size(Est.P,1),len);
    P_lk_t = zeros(size(Est.P,1),size(Est.P,2),len);
    
%   First step
    x_lk1  = Est.x_t(:,end)./units.Dim;
    P_lk1  = Est.P_t(:,:,end)./units.CovDim;
    x_lk_t(:,end)   = Est.x_t(:,end)./units.Dim;
    P_lk_t(:,:,end) = Est.P_t(:,:,end)./units.CovDim;
    
    for i = 1:len-1

%       Next step: k = kold - i
        x_kk  = Est.x_t(:,end-i)./units.Dim;
        P_kk  = Est.P_t(:,:,end-i)./units.CovDim;
        Pbar_kk1 = Est.Pbar_t(:,:,end-i+1)./units.CovDim;
        Phi   = Est.Phi_t(:,:,end-i+1);
        
%       Smoothing algorithm
        S     = P_kk*Phi'/Pbar_kk1;
        x_lk  = x_kk + S*(x_lk1 - Phi*x_kk);
        P_lk  = P_kk + S*(P_lk1 - Pbar_kk1)*S';

%       Filling the time by time values
        x_lk_t(:,end-i)   = x_lk.*units.Dim;
        P_lk_t(:,:,end-i) = abs(P_lk.*units.CovDim);
        
%       Update of the counter
        x_lk1 = x_lk;
        P_lk1 = P_lk;
        
    end


%   Last step
    Smooth.x_lk = x_lk.*units.Dim;
    Smooth.P_lk = P_lk.*units.CovDim;

%   Complete vector over time
    Smooth.x_lk_t = x_lk_t;
    Smooth.P_lk_t = P_lk_t;

end