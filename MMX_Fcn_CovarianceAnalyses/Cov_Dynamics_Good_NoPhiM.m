function dx = Cov_Dynamics_Good_NoPhiM(~,x,par,~)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% dx = Cov_Dynamics_Good(t,x,pars,units)
%
% Calculate the acceleration vector in the N-Body Problem with Mars and
% Phobos only and the derivative of the coefficients to be estimated
%
% Input:
% .t        Elapsed time since epoch (-)
% .x        State Vector 
% .pars     Problem Parameters
%           .d          Dimension of the Problem
%           .et0        Initial epoch (-)
%           .refcenter  Origin of Frame (e.g., '399')
%           .refframe   Coordinate Frame (e.g., 'ECLIPJ2000' or 'J2000')
%           .Ntb        No. of third bodies
%           .BodiesID   Bodies' SPICE ID (e.g., '10','399', ...)
%           .GM         Bodies' Gravitational Parameters (-)
%           .Nsh        No. of Spherical Harmonic Bodies
%           .SH_BodiesID Spherical Harmonic Bodies' SPICE ID (e.g., '10','399', ...)
%           .SH         Spherical Harmonic Structure
%           .lsf        Length scale factor (km)
%           .tsf        Time scale factor (s)
%           .STM        true or false depending if you calculate STM or not
%           .nCoeff     Number if coefficients in the state vector
%
% Output:
% .dx       Vector containing accelerations and STM (d x 2 + nCoeff, -)
%
% Author: E.Ciccarelli
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
%%  Useful quantities

    
%%  Dimensions

    d           = par.d;                    % Dimension of the problem
    nCoeff      = par.nCoeff;               % N. of gravitational parameters
    nBias       = par.nBias;

    size_St     = (d+6+nCoeff+nBias);
    

%%  Useful quantities

    for i = 0:2*size_St-1

%       Spacecraft state vector
        r = x(1+i*size_St:3+i*size_St);     % Pos. vector wrt central body
        v = x(4+i*size_St:6+i*size_St);     % Vel. vector wrt central body
        R = norm(r);                        % Norm of pos. vector wrt central body
    
%       Phobos states
        RPh         = x(7+i*size_St);
        RPh_dot     = x(8+i*size_St);
        theta       = x(9+i*size_St);
        theta_dot   = x(10+i*size_St);
        Xsi         = x(11+i*size_St);
        Xsi_dot     = x(12+i*size_St);
    
        Phi         = par.PhiM;

%       Phobos moment of inertia
        IPhx_bar    = x(13+i*size_St);
        IPhy_bar    = x(14+i*size_St);
        IPhz_bar    = x(15+i*size_St);

%       Position of MMX wrt Phobos
        rPh = par.MARSIAU2perifocal'*[RPh*cos(theta); RPh*sin(theta); 0];
        rsb = r - rPh;
    
%       Rotation matrix from orbit's to Phobos fixed coordinates frame    
        I       = rPh/RPh;
        k       = par.MARSIAU2perifocal'*[0; 0; 1];
        j       = cross(k,I);
        NO      = [I,j,k];
        ON      = NO';
    
%       Rotation matrix from orbit's to Phobos fixed coordinates frame
        OB      = [cos(Xsi), sin(Xsi), 0; -sin(Xsi), cos(Xsi), 0; 0, 0, 1];
        BO      = OB';
    
  %%  Phobos's orbit
    
    %   Potential's first order partials
        dVdRPh      = par.ni/RPh^2*(1 + 3/(2*RPh^2)*((par.IMz_bar - par.Is) +...
                -.5*IPhx_bar - .5*IPhy_bar + IPhz_bar + 1.5*(IPhy_bar - IPhx_bar)*cos(2*Xsi)));
        dVdXsi      = 1.5*par.ni/RPh^3 * (IPhy_bar - IPhx_bar)*sin(2*Xsi);
    
    %   Phobos equations of motions
        RPh_ddot    = theta_dot^2*RPh - dVdRPh/par.ni;
        theta_ddot  = dVdXsi/(par.ni*RPh^2) - 2*RPh_dot*theta_dot/RPh;
        Xsi_ddot    = -(1 + par.ni*RPh^2/IPhz_bar)*dVdXsi/(par.ni*RPh^2) +...
                    + 2*RPh_dot*theta_dot/RPh; 
          
%%      MMX 2BP with Mars + 3rd body Phobos 
    
%       The gravitational effect of Mars on MMX is 2BP+J2
    
        gM      = - par.G*par.MMars*r/R^3;
        gJ2M    = - 1.5*par.G*par.MMars*par.J2*par.RMmean/R^5*...
            [(1-5*(r(3)/R)^2)*r(1); (1-5*(r(3)/R)^2)*r(2);...
            (3-5*(r(3)/R)^2)*r(3)];
    
        gM      = gM + gJ2M;
    
%       The gravitational effect of Phobos on MMX includes the Ellipsoid
%       harmonics

        C_22_Ph     = .25*(IPhy_bar - IPhx_bar);
        C_20_Ph     = -.5*(2*IPhz_bar - IPhx_bar - IPhy_bar);
        par.SH.Clm  = [1, 0, 0; 0, 0, 0; C_20_Ph, 0, C_22_Ph]./par.SH.Norm(1:3,1:3);
    
        [gb,~,~,~] = SHGM_Full(BO*ON*rsb,par.SH);
        gPh = NO*OB*gb;
    
%       Trascinamento
%       Tutto adimensionale
        
        G   = par.G;
        M2  = par.MPho;
        M1  = par.MMars; 
    
        I1x = par.IMx_bar*M1;
        I1y = par.IMy_bar*M1;
        I1z = par.IMz_bar*M1;
        

%       Need those quantities 
        
        A1          = [cos(theta+Phi), sin(theta+Phi), 0; -sin(theta+Phi), cos(theta+Phi), 0; 0, 0, 1];
        dA1dtheta   = [-sin(theta+Phi), cos(theta+Phi), 0; -cos(theta+Phi), -sin(theta+Phi), 0; 0, 0, 0];
        A2          = [cos(theta+Xsi), sin(theta+Xsi), 0; -sin(theta+Xsi), cos(theta+Xsi), 0; 0, 0, 1];
        dA2dtheta   = [-sin(theta+Xsi), cos(theta+Xsi), 0; -cos(theta+Xsi), -sin(theta+Xsi), 0; 0, 0, 0];
    
        I1  = diag([I1x, I1y, I1z]);
        I2  = diag([IPhx_bar, IPhy_bar, IPhz_bar]);
    
%       Chain rule terms
        dRPhdx      = rPh(1)/norm(rPh(1:3)); 
        dRPhdy      = rPh(2)/norm(rPh(1:3));
        dRPhdz      = rPh(3)/norm(rPh(1:3));
        dthetadx    = -rPh(2)/(rPh(1)^2 + rPh(2)^2);
        dthetady    = rPh(1)/(rPh(1)^2 + rPh(2)^2);
        dthetadz    = 0;
        dbetadx     = -rPh(3)*rPh(1)/(sqrt(rPh(1)^2+rPh(2)^2)*sum(rPh(1:3).^2));
        dbetady     = -rPh(3)*rPh(2)/(sqrt(rPh(1)^2+rPh(2)^2)*sum(rPh(1:3).^2));
        dbetadz     = sqrt(rPh(1)^2+rPh(2)^2)/sum(rPh(1:3).^2);
    
        dVdtheta    = 3*G/(2*norm(rPh(1:3))^5)*rPh(1:3)'*(M2*(dA1dtheta'*I1*A1 + A1'*I1*dA1dtheta) + M1*(dA2dtheta'*I2*A2 + A2'*I2*dA2dtheta))*rPh(1:3);
    
%   Gravitational acceleration in x,y,z
        g_M_on_PH   =  -[dRPhdx, dthetadx, dbetadx; dRPhdy, dthetady, dbetady; dRPhdz, dthetadz, dbetadz]*[dVdRPh; dVdtheta; 0];
        g_Ph_on_M   = g_M_on_PH*(-(M2+M1)/(M2*M1))/M1;
    
%   Combined effect on MMX
        g_tot   = gM + gPh - g_Ph_on_M;


%%      Output for the integrator
    
        dx(1+i*size_St:size_St+i*size_St,1) = [v; g_tot; RPh_dot;...
            RPh_ddot; theta_dot; theta_ddot; Xsi_dot; Xsi_ddot;...
            zeros(nCoeff,1); zeros(nBias,1)];
    
    end

end