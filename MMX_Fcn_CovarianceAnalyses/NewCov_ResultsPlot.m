function NewCov_ResultsPlot(Est, YObs_Full, pars, units)
%==========================================================================
% NewCov_ResultsPlot(Est, YObs_Full, pars)
%
% This function plots the results of the covariance analysis
%
% INPUT: Description Units
%
% Est       - Estimation of:
%               .X, States at final t
%               .x, States' deviation vector at final t
%               .P, Covariance matrix at final t
%               .t, Time observation vector
%               .err, Post-fit residuals
%               .pre, Pre-fit residuals
%               .y_t, Pre-fit residuals
%               .X_t, States at different t
%               .x_t, States deviation vector at different t
%               .Phi_t, STM at different t
%               .P_t, Covariance matrix at different t  
%               .Pbar_t, Covariance matrix time update at different t
% 
% YObs_Full     Vector of observations and time of observations
%
% pars       - Structure containing problem's parameters
%
%
% Coupling:
% None
%
% Author: Edoardo Ciccarelli
% Date: 09/2021
%
%==========================================================================

    set(0,'DefaultTextInterpreter','latex');
    set(0,'DefaultAxesFontSize', 16);

    t_obs   = YObs_Full(1,:);

%   Plot of the post-fit residuals
    
%     figure(1)
%     subplot(1,2,1)
%     plot(t_obs/3600,Est.pre(1,:),'x','Color','b')
%     grid on;
%     hold on;
%     plot(t_obs/3600,Est.pre(3,:),'x','Color','b')
%     plot(t_obs/3600,Est.pre(5,:),'x','Color','b')
%     plot(t_obs/3600,3*pars.ObsNoise.range*units.lsf*ones(size(Est.pre(1,:),2)),'.-','Color','b')
%     plot(t_obs/3600,-3*pars.ObsNoise.range*units.lsf*ones(size(Est.pre(1,:),2)),'.-','Color','b')
%     plot(t_obs/3600,3*pars.ObsNoise.range*units.lsf*ones(size(Est.pre(3,:),2)),'.-','Color','b')
%     plot(t_obs/3600,-3*pars.ObsNoise.range*units.lsf*ones(size(Est.pre(3,:),2)),'.-','Color','b')
%     plot(t_obs/3600,3*pars.ObsNoise.range*units.lsf*ones(size(Est.pre(5,:),2)),'.-','Color','b')
%     plot(t_obs/3600,-3*pars.ObsNoise.range*units.lsf*ones(size(Est.pre(5,:),2)),'.-','Color','b')
%     xlabel('$[hour]$')
%     ylabel('$[km]$')
%     title('Range Pre-fit residual')
%     figure(2)
%     subplot(1,2,1)
%     plot(t_obs/3600,Est.pre(2,:),'x','Color','r')
%     grid on;
%     hold on;
%     plot(t_obs/3600,Est.pre(4,:),'x','Color','r')
%     plot(t_obs/3600,Est.pre(6,:),'x','Color','r')
%     plot(t_obs/3600,3*pars.ObsNoise.range_rate*units.vsf*ones(size(Est.pre(2,:),2)),'.-','Color','r')
%     plot(t_obs/3600,-3*pars.ObsNoise.range_rate*units.vsf*ones(size(Est.pre(2,:),2)),'.-','Color','r')
%     plot(t_obs/3600,3*pars.ObsNoise.range_rate*units.vsf*ones(size(Est.pre(4,:),2)),'.-','Color','r')
%     plot(t_obs/3600,-3*pars.ObsNoise.range_rate*units.vsf*ones(size(Est.pre(4,:),2)),'.-','Color','r')
%     plot(t_obs/3600,3*pars.ObsNoise.range_rate*units.vsf*ones(size(Est.pre(6,:),2)),'.-','Color','r')
%     plot(t_obs/3600,-3*pars.ObsNoise.range_rate*units.vsf*ones(size(Est.pre(6,:),2)),'.-','Color','r')
%     xlabel('$[hour]$')
%     ylabel('$[km/s]$')    
%     title('RangeRate Pre-fit residual') 
%     figure(3)
%     subplot(1,2,1)
%     plot(t_obs/3600,Est.pre(7,:),'x','Color','g')
%     grid on;
%     hold on;
%     plot(t_obs/3600,3*pars.ObsNoise.lidar*units.lsf*ones(size(Est.pre(7,:),2)),'.-','Color','g')
%     plot(t_obs/3600,-3*pars.ObsNoise.lidar*units.lsf*ones(size(Est.pre(7,:),2)),'.-','Color','g')
%     xlabel('$[hour]$')
%     ylabel('$[km]$')
%     title('Lidar Pre-fit residual') 
%     figure(4)
%     subplot(1,2,1)
%     plot(t_obs/3600,Est.pre(8,:),'x','Color','m')
%     grid on;
%     hold on;
%     plot(t_obs/3600,3*pars.ObsNoise.camera*ones(size(Est.pre(8,:),2)),'.-','Color','m')
%     plot(t_obs/3600,-3*pars.ObsNoise.camera*ones(size(Est.pre(8,:),2)),'.-','Color','m')
%     xlabel('$[hour]$')
%     ylabel('$[rad]$')
%     title('Camera Pre-fit residual') 
%     figure(5)
%     subplot(1,2,1)
%     plot(t_obs/3600,Est.pre(9,:),'x','Color','k')
%     grid on;
%     hold on;
%     plot(t_obs/3600,3*pars.ObsNoise.camera*ones(size(Est.pre(9,:),2)),'.-','Color','k')
%     plot(t_obs/3600,-3*pars.ObsNoise.camera*ones(size(Est.pre(9,:),2)),'.-','Color','k')
%     xlabel('$[hour]$')
%     ylabel('$[rad]$')
%     title('Camera Pre-fit residual') 


%     figure(1)
%     subplot(1,2,2)
%     histfit([Est.pre(1,:), Est.pre(3,:), Est.pre(5,:)])
%     xlabel('$[km]$')
%     title('Range Pre-fit residual') 
%     figure(2)
%     subplot(1,2,2)
%     histfit([Est.pre(2,:),Est.pre(4,:), Est.pre(6,:)])
%     xlabel('$[km/s]$')
%     title('Range Rate Pre-fit residual') 
%     figure(3)
%     subplot(1,2,2)
%     histfit(Est.pre(7,:))
%     xlabel('$[km]$')
%     title('Lidar Pre-fit residual')
%     figure(4)
%     subplot(1,2,2)
%     histfit(Est.pre(8,:))
%     xlabel('$[rad]$')
%     title('Camera Pre-fit residual')
%     figure(5)
%     subplot(1,2,2)
%     histfit(Est.pre(9,:))
%     xlabel('$[rad]$')
%     title('Camera Pre-fit residual')
    
%   Square-Root of the MMX covariance error

    sqx = squeeze(Est.P_t(1,1,1:end-1));
    sqy = squeeze(Est.P_t(2,2,1:end-1));
    sqz = squeeze(Est.P_t(3,3,1:end-1));
    SQRT_X = 3*sqrt(sqx + sqy + sqz);
    sqvx = squeeze(Est.P_t(4,4,1:end-1));
    sqvy = squeeze(Est.P_t(5,5,1:end-1));
    sqvz = squeeze(Est.P_t(6,6,1:end-1));
    SQRT_V = 3*sqrt(sqvx + sqvy + sqvz);

    
%   3sigma Envelopes
    figure()
    subplot(1,2,1)
    semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(1,1,1:end-1)))),'Color','b','LineWidth',1)
    hold on;
    grid on;
    semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(Est.P_t(2,2,1:end-1)))),'Color','r','LineWidth',1)
    semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(Est.P_t(3,3,1:end-1)))),'Color','g','LineWidth',1)
    semilogy(t_obs(1:end-1)/3600,SQRT_X,'Color','k','LineWidth',2)
    xlabel('time $[hour]$')
    ylabel('$[km]$')
    title('MMX position vector $3\sigma$ envelopes','Interpreter','latex','FontSize',16)
    legend('$3\sigma_{x}$','$3\sigma_{y}$','$3\sigma_{z}$','$3 RMS$','Interpreter','latex','FontSize',14)
    subplot(1,2,2)
    semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(Est.P_t(4,4,1:end-1)))),'Color','b','LineWidth',1)
    hold on;
    grid on;
    semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(Est.P_t(5,5,1:end-1)))),'Color','r','LineWidth',1)
    semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(Est.P_t(6,6,1:end-1)))),'Color','g','LineWidth',1)
    semilogy(t_obs(1:end-1)/3600,SQRT_V,'Color','k','LineWidth',2)
    xlabel('time $[hour]$')
    ylabel('$[km/s]$')
    title('MMX velocity vector $3\sigma$ envelopes','Interpreter','latex','FontSize',16)
    legend('$3\sigma_{\dot{x}}$','$3\sigma_{\dot{y}}$','$3\sigma_{\dot{z}}$','$3 RMS$','Interpreter','latex','FontSize',14)


%   Phobos's states uncertainties evolution

% %   3sigma Envelopes

    figure()
    subplot(2,4,1)
    semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(7,7,1:end-1)))),'Color','b','LineWidth',1)
    grid on
    xlabel('time [hour]')
    ylabel('$R_{Ph}$ [km]')
    subplot(2,4,2)
    semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(9,9,1:end-1)))),'Color','r','LineWidth',1)
    grid on
    xlabel('time [hour]')
    ylabel('$\theta_{Ph}$ [rad]')
    subplot(2,4,3)
    semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(11,11,1:end-1)))),'Color','g','LineWidth',1)
    grid on
    xlabel('time [hour]')
    ylabel('$\Phi_{M}$ [rad]')
    subplot(2,4,4)
    semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(13,13,1:end-1)))),'Color','m','LineWidth',1)
    grid on
    xlabel('time [hour]')
    ylabel('$\Phi_{Ph}$ [rad]')
    
    subplot(2,4,5)
    semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(8,8,1:end-1)))),'Color','b','LineWidth',1)
    grid on
    xlabel('time [hour]')
    ylabel('$\dot{R}_{Ph}$ [km/s]')
    subplot(2,4,6)
    semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(10,10,1:end-1)))),'Color','r','LineWidth',1)
    grid on
    xlabel('time [hour]')
    ylabel('$\dot{\theta}_{Ph}$ [rad/s]')
    subplot(2,4,7)
    semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(12,12,1:end-1)))),'Color','g','LineWidth',1)
    grid on
    xlabel('time [hour]')
    ylabel('$\dot{\Phi}_{M}$ [rad/s]')
    subplot(2,4,8)
    semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(14,14,1:end-1)))),'Color','m','LineWidth',1)
    grid on
    xlabel('time [hour]')
    ylabel('$\dot{\Phi}_{Ph}$ [rad/s]')

%   Harmonics 3sigma Envelopes

%     place0 = size(Est.X,1)-pars.nCoeff-pars.nBias;
%     corr_label = {'$x$','$y$','$z$','$\dot{x}$','$\dot{y}$','$\dot{z}$',...
%         '$R_{Ph}$','$\dot{R}_{Ph}$','$\theta_{Ph}$','$\dot{\theta}_{Ph}$',...
%         '$\Phi_{M}$','$\dot{\Phi}_{M}$','$\Phi_{Ph}$','$\dot{\Phi}_{Ph}$'};


%   Moments of inertia
%     figure()
%     subplot(1,3,1)
%     semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(15,15,1:end-1)))),'LineWidth',1);
%     grid on;
%     hold on;
%     corr_label{end+1} = ['$I_{PhX}$'];
%     xlabel('time $[hour]$')
%     legend('$I_{PhX}$','Interpreter','latex','FontSize',14)
%     subplot(1,3,2)
%     semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(16,16,1:end-1)))),'LineWidth',1);
%     grid on;
%     hold on;
%     corr_label{end+1} = ['$I_{PhY}$'];
%     xlabel('time $[hour]$')
%     legend('$I_{PhY}$','Interpreter','latex','FontSize',14)
%     subplot(1,3,3)
%     semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(16,16,1:end-1)))),'LineWidth',1);
%     grid on;
%     hold on;
%     corr_label{end+1} = ['$I_{PhZ}$'];
%     xlabel('time $[hour]$')
%     legend('$I_{PhZ}$','Interpreter','latex','FontSize',14)

        
%   Biases

    place0 = size(Est.X,1)-pars.nBias;
%     corr_label(end+1) = {'$\rho$'};
%     corr_label(end+1) = {'$\dot{\rho}$'};
%     corr_label(end+1) = {'$LIDAR_b$'};

    figure()
    subplot(1,pars.nBias,1)
    semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(place0+1,place0+1,1:end-1)))),'LineWidth',1,...
                'DisplayName',('$\rho$'),'Color','b');
    grid on;
    hold on;
    xlabel('time $[hour]$')
    ylabel('$[km]$')
    subplot(1,pars.nBias,2)
    semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(place0+2,place0+2,1:end-1)))),'LineWidth',1,...
                'DisplayName',('$\dot{\rho}$'),'Color','r');
    grid on;
    hold on;
    xlabel('time $[hour]$')
    ylabel('$[km/s]$')
    subplot(1,pars.nBias,3)
    semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(place0+3,place0+3,1:end-1)))),'LineWidth',1,...
                'DisplayName',('$LIDAR_b$'),'Color','g');
    grid on;
    hold on;
    xlabel('time $[hour]$')
    ylabel('$[km]$')

    subplot(1,pars.nBias,4)
    semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(place0+4,place0+4,1:end-1)))),'LineWidth',1,...
                'DisplayName',('$cam_x$'),'Color','m');
    grid on;
    hold on;
    xlabel('time $[hour]$')
    subplot(1,pars.nBias,5)
    semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est.P_t(place0+5,place0+5,1:end-1)))),'LineWidth',1,...
                'DisplayName',('$cam_y$'),'Color','b');
    grid on;
    hold on;
    xlabel('time $[hour]$')


%     
% %   Harmonics Coefficients
% 
    P_t_I2x  = squeeze(Est.P_t(15,15,1:end-1));
    P_t_I2y  = squeeze(Est.P_t(16,16,1:end-1));
    P_t_I2z  = squeeze(Est.P_t(17,17,1:end-1));
    Cov_I2xI2y = squeeze(Est.P_t(15,16,1:end-1));
    Cov_I2yI2z = squeeze(Est.P_t(16,17,1:end-1));
    Cov_I2xI2z = squeeze(Est.P_t(15,17,1:end-1));

    P_t_C_20 = .25*(P_t_I2y + P_t_I2x - 2*Cov_I2xI2y);
    P_t_C_22 = .5*(4*P_t_I2z + P_t_I2y + P_t_I2x - 4*Cov_I2xI2z - 4*Cov_I2yI2z + 2*Cov_I2xI2y);

    figure()
    subplot(1,2,1)
    semilogy(t_obs(1:end-1)/3600,3.*real(sqrt(P_t_C_20)),'LineWidth',1,'Color','b');
    grid on;
    hold on;
    xlabel('time $[hour]$')
    legend('$C_{20}$','Interpreter','latex','FontSize',14)
    subplot(1,2,2)
    semilogy(t_obs(1:end-1)/3600,3.*real(sqrt(P_t_C_22)),'LineWidth',1,'Color','r');
    grid on;
    hold on;
    xlabel('time $[hour]$')
    legend('$C_{22}$','Interpreter','latex','FontSize',14)

% 
% 
% %   Correlations coefficients
% 
%     [~,corr] = readCovMatrix(real(Est.P));
% 
%     figure();
%     imagesc(real(corr))
%     title('Correlation Coefficients','FontSize',16);
%     set(gca,'FontSize',16);
%     colormap(hot);
%     colorbar;
%     set(gca,'TickLabelInterpreter','latex');
%     set(gca,'XTick',(1:size(Est.X,1)));
%     set(gca,'XTickLabel',corr_label);
%     set(gca,'YTick',(1:size(Est.X,1)));
%     set(gca,'YTickLabel',corr_label);
%     axis square;
%     freezeColors;
% 

end