function [G,dGdX] = Observables_model(et,Obs,X,par,units)
%==========================================================================
% [G,dGdX] = Range_Speed_Lidar_Camera(t,Obs,X,par)
%
% Compute the Range, Range Rate, Lidar's and camera's measures and the
% H_tilde matrix 
%
% INPUT: Description Units
%
% et        - Time Epoch                                                s
%
% stationID - Available Observables at the epoch t
%
% X         - State Vector defined in the Phobos rotating frame 
%               at the time being considered (42x1)
%               .MMX Position Vector (3x1)                              km
%               .MMX Velocity Vector (3x1)                              km/s
%               .Phobos Position Vector (3x1)                           km
%               .Phobos Velocity Vector (3x1)                           km/s
%               .Clm harmonics coefficients
%               .Slm Hamronics coefficients
%               .State Transition Matrix 
%
% par       -  Parameters structure 
% 
%
% OUTPUT:
%
% G         - G
% dGdx      - H_tilda matrix 
% 
% DO NOT FORGET TO USE RESHAPE LATER IN THE PROJECT
%
% Coupling:
% None
%
% Author: Edoardo Ciccarelli
% Date: 06/2021
%
%==========================================================================
    
%   Dimensions of the problem
    n       = par.d/2;
    d       = par.d;
    nCoeff  = par.nCoeff;
    nBias   = par.nBias;

%   Output initialization
    range       = NaN;
    range_rate  = NaN;
    lidar       = NaN;
    camera      = [NaN, NaN];
    drdX        = NaN(1,size(X,1));
    dr_dotdX    = NaN(1,size(X,1));
    dlidardX    = NaN(1,size(X,1));
    dcameradX   = NaN(2,size(X,1));

%   Unpacking of the state vector
    r_MMX       = X(1:n);
    v_MMX       = X(n+1:d);
    r_Phobos    = X(d+1:d+n);

    bias        = X(2*d+nCoeff+1:end);
    
%   Definition of Mars position WRT Earth J2000
    [Mars, ~]   = cspice_spkezr('499', et, 'J2000', 'none', '399');
    Mars        = Mars./units.sfVec;

%   Useful to understand the observables that are available
    where       = isnan(Obs);
    
%   Which station is working
    switch Obs(1)
        case 1
%       Camberra's Position in the J2000 Earth-centered
            [X_st, ~]  = cspice_spkezr('DSS-45', et, 'J2000', 'none', '399');
            X_st       = X_st./units.sfVec;    
        case 2
%       Goldstone's Position in the J2000 Earth-centered
            [X_st, ~]  = cspice_spkezr('DSS-24', et, 'J2000', 'none', '399');
            X_st       = X_st./units.sfVec;
        case 3
%       Madrid's Position in the J2000 Earth-centered
            [X_st, ~]  = cspice_spkezr('DSS-65', et, 'J2000', 'none', '399');
            X_st       = X_st./units.sfVec;
    end

    if where(2) == 0
%       DSN antennas available
        r       = norm(Mars(1:n) + r_MMX - X_st(1:n));
                    
        range = r + bias(1);
                    
%       Partials
        drdX    = [(Mars(1:n) + r_MMX - X_st(1:n))/norm(Mars(1:n) + r_MMX - X_st(1:n)); zeros(d+n+nCoeff,1); 1; zeros(nBias-1,1)]';
    end
    
    if where(3) == 0
%       DSN antennas available
        R       = norm(Mars(1:n) + r_MMX - X_st(1:n));
        r_dot   = 1/R*sum((Mars(n+1:d) + v_MMX - X_st(n+1:d)).*(Mars(1:n) + r_MMX - X_st(1:n)));
                    
        range_rate = r_dot + bias(2);
                    
                    
%       Partials
        dr_dotdX= [(Mars(n+1:d) + v_MMX - X_st(n+1:d))/R -...
                (Mars(1:n) + r_MMX - X_st(1:n))*sum((Mars(n+1:d) + v_MMX - X_st(n+1:d)).*(Mars(1:n) + r_MMX - X_st(1:n)))/R^3;...
                (Mars(1:n) + r_MMX - X_st(1:n))/R; zeros(d+nCoeff,1); 0; 1; zeros(nBias-2,1)]';
    end
    
    if where(4) == 0
%       Lidar available
        lidar   = norm(r_MMX - r_Phobos) + bias(3);
        
%       Partials
        dlidardX= [(r_MMX - r_Phobos)/norm(r_MMX - r_Phobos); zeros(n,1);...
                    -(r_MMX - r_Phobos)/norm(r_MMX - r_Phobos); zeros(n+nCoeff,1);...
                    0; 0; 1; zeros(nBias-3,1)]';
    end
    
    if where(5) == 0
%       Camera available
        rsb     = r_MMX - r_Phobos;
        Rsb     = norm(rsb);

        I       = -rsb/Rsb;
        v       = [0; 0; 1];
        j       = cross(v, I)/norm(cross(v, I));
        k       = cross(I, j)/norm(cross(I, j));
        
        T       = [I, j, k]';
        camera  = ([0 1 0; 0 0 1]*T*I)' + bias(4:5)';
        
%       Partials
        H_los      = -1/Rsb*(eye(n) - (rsb*rsb')/Rsb^2);  
        dcameradX  = [[0 1 0; 0 0 1]*T*[H_los, zeros(n,n), -H_los, zeros(n,n+nCoeff)], zeros(2,nBias-2), eye(2)];
        
    end
   
    G           = [range, range_rate, lidar, camera];
    G(isnan(G)) = [];
    dGdX        = [drdX; dr_dotdX; dlidardX; dcameradX];
    dGdX(where(2:end),:) = [];
    

     
end