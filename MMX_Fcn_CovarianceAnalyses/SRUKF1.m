function [Est] = SRUKF1(Est0,f,G,O,Measures,par,units)
%==========================================================================
% [Est] = SRUKF(Est0,f,G_dG,R,Measures,par)
%
% Square Root Unscented Kalman Filter
%
% INPUT: Description Units
%
% Est0      - Apriori initial guess of
%               .X,     States 
%               .P0,    Covariance matrix
%               .t0,    initial time  
% 
% @f        - Dynamical model
% 
% @G        - Observation's model and partial derivatives wrt the states
% 
% O         - Weight matrix
% Nsigma    - Number of sigma points
% Measures  - Observations matrix: the first column contains observation
%               times, then every column has a different type of observaion
%               type
% par       - Structure containing parameters required by @f and @G_dG 
% units     - Structure containing lenght and time units 
%
% OUTPUT:
%
% Est       - Estimation of:
%               .X, States at final t
%               .P, Covariance matrix at final t
%               .t, Time observation vector
%               .X_t, States at different t
%               .P_t, Covariance matrix at different t  
%               .Pbar_t, Covariance matrix time update at different t
% 
%
% Coupling:
% None
%
% Author: Edoardo Ciccarelli
% Date: 04/2021
%
%==========================================================================


%   Apriori initial guess, deviation and covariance matrix.
    Xold    = Est0.X;
    Pold    = Est0.P0;
    
%   Column of the measures time.
    tobs    = Measures(1,:);
    told    = tobs(1);
    
%   The measures must be organized in colums depending on the type of
%   measure; each row is reffered to a given measure's time.
    obs     = Measures(2:end,:)./units.Observations;
    
%   n,l and m are respectively the number of states, number of different
%   types of measures and number of measures per each type.
    n       = size(Xold,1);
    m       = size(obs,2);
    q       = 3;

%   err is the vector of residuals, the others are needed to build the 
%   fitting residuals.
    X_t     = zeros(n,size(tobs,1));
    P_t     = zeros(n,n,m);
    Pbar_t  = zeros(n,n,m);
    
%   Initialization of the filter
    alpha   = 1e0;
    beta    = 2;
    lambda  = (alpha^2 - 1)*n;
    gamma   = sqrt(n+lambda);

%   Sigma points' weights
    W0c     = zeros(2*n+1, 1);
    W0c(1)  = lambda/(lambda+n)+(1-alpha^2+beta);
    W0c(2:2*n+1)   = 1./(2*(n+lambda));

    W0m     = zeros(2*n+1, 1);
    W0m(1)  = lambda/(lambda+n);
    W0m(2:2*n+1)   = W0c(2:2*n+1);
   
%   Useful quantities's initialization
    S0      = chol(Pold); 

%   For-loop on the observations
    for i = 1:m
    
%       Useful quantities's initialization
        par.et  = Est0.t0/units.tsf + tobs(i);

%       TIME UPDATE
%       Sigma points' definition
        X0 = [Xold, (repmat(Xold,1,n)-gamma.*S0),...
            (repmat(Xold,1,n)+gamma.*S0)];

%       Need to know how many observations are available
        where       = isnan(obs(1:end,i));
        count       = sum(~where);
        IDX         = [];
        Y_obs       = zeros(count,1);
        Y           = zeros(count,2*n+1);
        R           = zeros(count, count);

        if count ~= 0
            for k=1:size(where,1)
                    if where(k)==0
                        IDX = [IDX; k];
                    end
            end
    
            for j=1:count
                Y_obs(j)= obs(IDX(j),i);
                R(j,j)  = O(IDX(j),IDX(j));
            end
        else
            Y_obs       = obs(:,i);
            R           = O;
        end

        if tobs(i) == told

%           The sigma point dont need to be proppagated
            Xbar    = X0;

        elseif tobs(i) > told

%           Propagation of the sigma points' trajectories
            St0     = reshape(X0,[n*(2*n+1),1]);
            tspan   = (told:tobs(i))*units.tsf;
            opt     = odeset('RelTol',1e-13,'AbsTol',1e-16);
            [~,X_sigma] = ode113(@(t,X) f(t,X,par,units),tspan,St0,opt);
            Xbar = reshape(X_sigma(end,1:n*(2*n+1))',[n,(2*n+1)]);

        end

%       Apriori quantities
        Xmean_bar   = sum(W0m'.*Xbar,2);
        Q       = par.sigma^2*eye(3);
        deltaT  = (tobs(i)-told)*units.tsf;
        Gamma   = [eye(q)*deltaT^2/2; eye(q)*deltaT; zeros(n-6,q)];    
        Q       = Gamma*Q*Gamma';
        Sq      = real(sqrtm(Q));
%         Sq      = [zeros(3,n); zeros(3,3), Sq, zeros(3,n-6); zeros(n-6,n)];
        UG       = [zeros(n,2*n), Sq];

        parfor j = 1:2*n
            UG(:,j) = sqrt(W0c(j+1))*(Xbar(:,j+1) - Xmean_bar);
        end
        
%       VERSIONE 1
        [~,S_bar] = qr(UG);        
        S_bar = cholupdate(S_bar(1:n,1:n), (Xbar(:,1) - Xmean_bar), '+');
        P_bar = S_bar*S_bar';

        i
%       Sigma point redefinition
        X0  = [Xmean_bar, (repmat(Xmean_bar,1,n)-gamma.*S_bar),...
            (repmat(Xmean_bar,1,n)+gamma.*S_bar)];

        for j = 1:2*n+1
%           G(j-th sigma point)
            Y_sigma = G(par.et,obs(:,i),X0(:,j),par,units);
            Y(:,j)  = Y_sigma';
        end

%       Mean predicted measurement
        Y_mean  = sum(W0m'.*Y,2);
        
%       Innovation covariance and cross covariance

        Pxy     = zeros(n,count);
        Sr      = chol(R);
        M       = [zeros(count,2*n), Sr];

        parfor j = 1:2*n+1
            Pxy = Pxy + W0c(j)*(Xbar(:,j) - Xmean_bar)*(Y(:,j) - Y_mean)';
        end

        parfor j = 1:2*n
            M(:,j) = sqrt(W0c(j+1))*(Y(:,j+1) - Y_mean);
        end

%       VERSIONE 1
        [~,S_y] = qr(M);
        S_y = cholupdate(S_y(1:count,1:count), (Y(:,1) - Y_mean), '+');

%       Kalman gain
        Kt  = Pxy/S_y';
        K   = Kt/S_y;

%       MEASUREMENT UPDATE
        Xstar   = Xmean_bar + K*(Y_obs - Y_mean);
        S       = S_bar;

        for j=1:count
            S = cholupdate(S, Kt(:,j), '-');
        end

        P   = S*S';

%       Next iteration preparation
        Xold    = Xstar;
        told    = tobs(i);
        S0      = S;
    
%       Storing the results
        P_t(:,:,i)  = P.*units.CovDim;
        Pbar_t(:,:,i)  = P_bar.*units.CovDim;
        X_t(:,i)    = Xstar.*units.Dim;
        
    end
    
    fprintf('\nAnalysis complete!\n');
    
%   Initial states' estimation, deviation and covariance matrix
    Est.X       = Xstar.*units.Dim;
    Est.P       = P.*units.CovDim;
    
%   Residuals and fit-residuals at different observation times
    Est.t       = tobs;
    Est.X_t     = X_t;
    Est.P_t     = P_t;
    Est.Pbar_t  = Pbar_t;
    
end