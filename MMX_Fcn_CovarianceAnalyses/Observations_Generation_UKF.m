function YObs_Full = Observations_Generation_UKF(date0, date_end, pars, units)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% YObs_Full = Observations_Generation_Paper(date0, date_end, pars, units)
%
% Generate the real observations and the file for the UKF
%
% Input:
%     
% date0         Date of the beginning of the analysis
% date_end      Date of the end of the analysis
% pars          Structure containing some useful parameters for the analysis
% units         Structure containing units of lenght, time, etc. of the problem
%
% Output:
% 
% YObs_Full     Vector of observations and time of observations
%
% Author: E.Ciccarelli
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%   Generation properties
    
    elevation       = pars.elevation;
    LIDARlimit      = pars.cutoffLIDAR;

    interval_Range      = pars.interval_Range;
    interval_Range_rate = pars.interval_Range_rate;
    interval_lidar      = pars.interval_lidar;
    interval_camera     = pars.interval_camera;
    
%   Noise in the Observations
    sigma_range     = pars.ObsNoise.range*units.lsf;
    sigma_range_rate= pars.ObsNoise.range_rate*units.vsf;
    sigma_lidar     = pars.ObsNoise.lidar*units.lsf;
    sigma_cam       = pars.ObsNoise.camera;
    
    
%%  Observations

    et0             = date0;
    et_end          = date_end;
    
    YObs_Full       = [];
    Pic_count       = 0;
    
    min_interval    = min([interval_Range; interval_Range_rate; interval_lidar; interval_camera]);
    
    
    for i = et0:min_interval:et_end
        
        got_it          = 0;
        
%       CAMERA OBSERVABLE
        MMX     = cspice_spkezr('-33', i, 'J2000', 'none', 'MARS');
        Phobos  = cspice_spkezr('401', i, 'J2000', 'none', 'MARS');
        
        rsb     = MMX(1:3) - Phobos(1:3);
        Rsb     = norm(rsb);
        
        I       = -rsb/Rsb;
        v       = [0; 0; 1];
        j       = cross(v, I)/norm(cross(v, I));
        k       = cross(I, j)/norm(cross(I, j));
        
        T       = [I, j, k]';
        
%       Check if Phobos is in light or not
        Phobos  = cspice_spkezr('401', i, 'J2000', 'none', 'SUN');
        MMX     = cspice_spkezr('-33', i, 'J2000', 'none', 'SUN');
        R_PS    = -Phobos(1:3)/norm(Phobos(1:3));
        R_MMXP  = (MMX(1:3) - Phobos(1:3))/norm(MMX(1:3) - Phobos(1:3));
        
%       Is Mars between Phobos and the Sun?
        check3   = cspice_occult('401','POINT',' ',...
         '499','ELLIPSOID','IAU_MARS','none','SUN',i);
        
        if (dot(R_MMXP,R_PS)>0)&&(check3>=0)&&(rem(round(i-et0),interval_camera)==0)
            Y = ([0 1 0; 0 0 1]*T*I)+[random('Normal',0, sigma_cam);...
                               random('Normal',0, sigma_cam)];
            got_it = 1;

        else
            Y = [NaN; NaN];
        end
     
%       LIDAR OBSERVABLES
%       Position of MMX wrt J2000 Phobos-centered
        MMX     = cspice_spkezr('-33', i, 'J2000', 'none', '401');
        
        if (norm(MMX(1:3)) < LIDARlimit) && (rem((i-et0),interval_lidar)==0)
           Lidar    = norm(MMX(1:3)) + random('Normal',0, sigma_lidar);
           got_it   = 1;
        else
           Lidar    = NaN;
        end

        
%       Position of MMX wrt J2000 Earth-centered
        MMX = cspice_spkezr('-33', i, 'J2000', 'none', 'EARTH');
        
%       Check on MMX being behind Mars or Phobos
        check1  = cspice_occult('-33','POINT',' ',...
         '499','ELLIPSOID','IAU_MARS','none','EARTH',i);
        check2  = cspice_occult('-33','POINT',' ',...
         '401','ELLIPSOID','IAU_PHOBOS','none','EARTH',i);
        check   = check1 + check2;
        
%       Position of Camberra's GS
        [X_st1, ~]  = cspice_spkezr('DSS-45', i, 'J2000', 'none', 'EARTH');
        D           = MMX(1:3) - X_st1(1:3);
        v1          = X_st1(1:3)/norm(X_st1(1:3));
        
        
        if (check >= 0)&&(dot(v1,D)/norm(D) > (cos(elevation*pi/180))) && (rem((i-et0),interval_Range)==0)
            R_Cam   = norm(MMX(1:3) - X_st1(1:3)) + random('Normal',0, sigma_range);
        else
            R_Cam   = NaN;
        end

        if (check >= 0)&&(dot(v1,D)/norm(D) > (cos(elevation*pi/180))) && (rem((i-et0),interval_Range_rate)==0)
            r       = norm(MMX(1:3) - X_st1(1:3)) + random('Normal',0, sigma_range);
            R_dot_Cam = 1/r*sum((MMX(4:6) - X_st1(4:6)).*(MMX(1:3) - X_st1(1:3))) + random('Normal',0, sigma_range_rate);
            got_it  = 1;            
        else
            R_dot_Cam = NaN;
        end
        
           
%       Position of Goldstone's GS
        [X_st2, ~]  = cspice_spkezr('DSS-24', i, 'J2000', 'none', 'EARTH');
        D           = MMX(1:3) - X_st1(1:3);
        v2          = X_st2(1:3)/norm(X_st2(1:3));
        
        if (check >= 0)&&(dot(v2,D)/norm(D) > (cos(elevation*pi/180))) && (rem((i-et0),interval_Range)==0)
            R_Gold  = norm(MMX(1:3) - X_st2(1:3)) + random('Normal',0, sigma_range);
        else
            R_Gold  = NaN;
        end

        if (check >= 0)&&(dot(v2,D)/norm(D) > (cos(elevation*pi/180))) && (rem((i-et0),interval_Range_rate)==0)
            r       = norm(MMX(1:3) - X_st2(1:3)) + random('Normal',0, sigma_range);
            R_dot_Gold  = 1/r*sum((MMX(4:6) - X_st2(4:6)).*(MMX(1:3) - X_st2(1:3))) + random('Normal',0, sigma_range_rate);
            got_it  = 1;            
        else
            R_dot_Gold  = NaN;
        end

%       Position of Madrid's GS
        [X_st3, ~]  = cspice_spkezr('DSS-65', i, 'J2000', 'none', 'EARTH');
        D           = MMX(1:3) - X_st3(1:3);
        v3          = X_st3(1:3)/norm(X_st3(1:3));
        
        if (check >= 0)&&(dot(v3,D)/norm(D) > (cos(elevation*pi/180))) && (rem((i-et0),interval_Range)==0)
            R_Mad    = norm(MMX(1:3) - X_st3(1:3)) + random('Normal',0, sigma_range);
        else
            R_Mad    = NaN;
        end   

        if (check >= 0)&&(dot(v3,D)/norm(D) > (cos(elevation*pi/180))) && (rem((i-et0),interval_Range_rate)==0)
            r       = norm(MMX(1:3) - X_st3(1:3)) + random('Normal',0, sigma_range);
            R_dot_Mad   = 1/r*sum((MMX(4:6) - X_st3(4:6)).*(MMX(1:3) - X_st3(1:3))) + random('Normal',0, sigma_range_rate);
            got_it  = 1;            
        else
            R_dot_Mad   = NaN;
        end   
        
        if (got_it == 1)
            YObs_Full   = [YObs_Full, [i-et0; R_Cam; R_dot_Cam; R_Gold; R_dot_Gold; R_Mad; R_dot_Mad; Lidar; Y]];
        end
    end    
    

%%  Plot of the data

set(0,'DefaultTextInterpreter','latex');
set(0,'DefaultAxesFontSize', 16);

% Observables

% Observables

DSN_Cam   = [];
DSN_GS    = [];
DSN_Mad   = [];

for i = 1:size(YObs_Full,2)

    DSN_Cam   = [DSN_Cam, [YObs_Full(1,i); YObs_Full(2:3,i)]];
    DSN_GS    = [DSN_GS, [YObs_Full(1,i); YObs_Full(4:5,i)]];
    DSN_Mad   = [DSN_Mad, [YObs_Full(1,i); YObs_Full(6:7,i)]];

end


figure()

subplot(2,1,1)
plot(DSN_Cam(1,:)/3600,DSN_Cam(2,:),'*','Color','b')
grid on; hold on;
plot(DSN_GS(1,:)/3600,DSN_GS(2,:),'*','Color','r')
plot(DSN_Mad(1,:)/3600,DSN_Mad(2,:),'*','Color','g')
ylabel('Range $[km]$');
xlabel('Time $[hour]$')
legend('Camberra','GoldStone','Madrid')

subplot(2,1,2)
plot(DSN_Cam(1,:)/3600,DSN_Cam(3,:),'*','Color','b')
grid on; hold on;
plot(DSN_GS(1,:)/3600,DSN_GS(3,:),'*','Color','r')
plot(DSN_Mad(1,:)/3600,DSN_Mad(3,:),'*','Color','g')
ylabel('Range_Rate $[km/s]$');
xlabel('Time $[hour]$')
legend('Camberra','GoldStone','Madrid')

figure()
plot(YObs_Full(1,:)/3600,YObs_Full(8,:),'*')
grid on; hold on;
ylabel('Lidar $[km]$');
xlabel('Time $[hour]$')

figure()
subplot(2,1,1)
plot(YObs_Full(1,:)/3600,YObs_Full(9,:),'*')
grid on; hold on;
ylabel('Cam1 $[rad]$');
xlabel('Time $[hour]$')

subplot(2,1,2)
plot(YObs_Full(1,:)/3600,YObs_Full(10,:),'*')
grid on; hold on;
ylabel('Cam2 $[rad]$');
xlabel('Time $[hour]$')



    
    
end