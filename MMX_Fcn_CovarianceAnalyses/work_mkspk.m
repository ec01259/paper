function work_mkspk(x_M,outputfilename,OBJECT_ID,REF_FRAME_NAME,CENTER_ID,mkspk_dir)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% work_mkspk(x_M,outputfilename,OBJECT_ID,REF_FRAME_NAME,CENTER_ID,mkspk_dir)
%
% Make SPICE bsp kernel
%
% INPUT:
% - x_M           (nx7) matrix including time in (:,1) and states in (:,2:7)
% - outputfilename      string (e.g.: 'testcase.bsp')
% - OBJECT_ID           integer(e.g.: -100)
% - REF_FRAME_NAME      string (e.g.: 'ECLIPJ2000')
% - CENTER_ID           integer(e.g.: 10)
%
% OUTPUT:
% - NONE
%
% LOG:
% Crated SC 5 Jan 2012
% Modified SC 28 Dec 2016 change directory paths and tls from 9 to 11
% 2018 Jan 31 JHA changed paths to own PC
% 2018 May 15 NB changed paths to own PC
%
%
% Copyright 2018-19 EQUULEUS FD Team
% 
% Licensed under the Apache License, Version 2.0 (the "License"); you may
% not use this file except in compliance with the License. You may obtain a
% copy of the License <a
% href="matlab:web('http://www.apache.org/licenses/LICENSE-2.0','-browser')">here</a>.
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
% WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
% License for the specific language governing permissions and limitations
% under the License.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% USER PARAMETERS
leapsecondsfile = which('naif0012.tls');
PRODUCER_ID     = 'STAG - University of Surrey';


% CHECK FOR WRONG TIME TAGS
dt_C            = diff(x_M(:,1));
neg             = dt_C <= 0;
if ~isempty(dt_C(neg))
    disp('TIME MUST INCREASE! - output not computed');
    return
end


% CREATE INPUT FILE
inputfile   = 'w_mkspk_input';
fid         = fopen(inputfile,'w+');
[row,~]     = size(x_M);
for i=1:row
    % fprintf('Creating input %i/%i\n', i, row);
    fprintf(fid,num2str(x_M(i,:),20)); fprintf(fid,'\n');
end
fclose(fid);


% CREATE SETUP FILE
setupfile = 'w_mkspk_setup';
fid = fopen(setupfile,'w+');
fprintf(fid,'\\begindata \n');
fprintf(fid,'INPUT_DATA_TYPE   = ''STATES'' \n');
fprintf(fid,'OUTPUT_SPK_TYPE   = 9 \n');
fprintf(fid,'OBJECT_ID         = %i \n',OBJECT_ID);
fprintf(fid,'CENTER_ID         = %i \n',CENTER_ID);
fprintf(fid,'REF_FRAME_NAME    = ''%s'' \n',REF_FRAME_NAME);
fprintf(fid,'DATA_ORDER        = ''EPOCH X Y Z VX VY VZ'' \n');
fprintf(fid,'INPUT_DATA_UNITS  = (''ANGLES=DEGREES'' ''DISTANCES=km'') \n');
fprintf(fid,'INPUT_DATA_FILE   = ''%s'' \n', inputfile);
fprintf(fid,'OUTPUT_SPK_FILE   = ''%s'' \n', outputfilename);
fprintf(fid,'TIME_WRAPPER      = ''# ETSECONDS'' \n');
fprintf(fid,'PRODUCER_ID       = ''%s'' \n',PRODUCER_ID);
fprintf(fid,'LINES_PER_RECORD  =  1 \n');
fprintf(fid,'IGNORE_FIRST_LINE =  0 \n');
fprintf(fid,'LEAPSECONDS_FILE  = ''%s'' \n',leapsecondsfile);
fprintf(fid,'SEGMENT_ID        = ''SPK_STATES_09'' \n');
fprintf(fid,'DATA_DELIMITER    = ''  '' \n');
fprintf(fid,'POLYNOM_DEGREE    = 9 \n');
fprintf(fid,'\\begintext');
fclose(fid);

% CREATE .bsp; MOVE AND REMOVE FILES
if(isunix || ismac)
    unix( [mkspk_dir,'/mkspk -setup ',setupfile]);
elseif(ispc)
    system([mkspk_dir,'mkspk -setup ',setupfile]);
end

delete(setupfile);
delete(inputfile);   

end