function dx = MP_Circular_all_UKF(~,x,par,~)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% dx = MP_Circular_all(t,x,par,units)
%
% Calculate the acceleration vector in the N-Body Problem with Mars and
% Phobos only and the derivative of the coefficients to be estimated
%
% Input:
% .t        Elapsed time since epoch (-)
% .x        State Vector 
% .pars     Problem Parameters
%           .d          Dimension of the Problem
%           .et0        Initial epoch (-)
%           .refcenter  Origin of Frame (e.g., '399')
%           .refframe   Coordinate Frame (e.g., 'ECLIPJ2000' or 'J2000')
%           .Ntb        No. of third bodies
%           .BodiesID   Bodies' SPICE ID (e.g., '10','399', ...)
%           .GM         Bodies' Gravitational Parameters (-)
%           .Nsh        No. of Spherical Harmonic Bodies
%           .SH_BodiesID Spherical Harmonic Bodies' SPICE ID (e.g., '10','399', ...)
%           .SH         Spherical Harmonic Structure
%           .lsf        Length scale factor (km)
%           .tsf        Time scale factor (s)
%           .STM        true or false depending if you calculate STM or not
%           .nCoeff     Number if coefficients in the state vector
%
% Output:
% .dx       Vector containing accelerations and STM (d x 2 + nCoeff, -)
%
% Author: E.Ciccarelli
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%  Dimensions

    d           = par.d;                 % Dimension of the problem
    nCoeff      = par.nCoeff;
    nBias       = par.nBias;

    St_size     = 2*d+nCoeff+nBias;


%%  Radius

    for i = 0:2*St_size-1

        r = x(1+i*St_size:3+i*St_size);     % Pos. vector wrt central body
        v = x(4+i*St_size:6+i*St_size);     % Vel. vector wrt central body
        R = norm(r);    % Norm of pos. vector wrt central body
    
%% 	Spherical harmonic bodies

%       Calculate Phobos pos wrt refcenter
        rPh = x(7+i*St_size:9+i*St_size);
        RPh = norm(rPh);
        vPh = x(10+i*St_size:12+i*St_size);
        VPh = norm(vPh);
    
%       Calculate spacecraft pos wrt Phobos
        rsb = r - rPh;
    
%       Rotate vector in body-fixed coordinates
        I       = rPh/RPh;
        s       = vPh/VPh;
        k       = cross(I,s)/norm(cross(I,s));
        j       = cross(k,I);
        NB      = [I,j,k];
        BN      = NB';
    
%%  MMX 2BP with Mars + 3rd body Phobos 

        par.SH_sigma = par.SH;
        order   = size(par.SH.Clm,1)-1;
        
        count_Clm = 1;
        for j = 0:order
            for z = 0:j
                par.SH_sigma.Clm(j+1,z+1) = x(2*d+count_Clm+i*St_size);
                count_Clm = count_Clm+1;
            end
        end

        count_Slm = 0;
        for j = 1:order
            for z = 1:j
                par.SH_sigma.Slm(j+1,z+1) = x(2*d+count_Clm+count_Slm+i*St_size);
                count_Slm = count_Slm+1;
            end
        end

        [gb,~,~,~,~] = CGM(BN*rsb, par.SH_sigma);
        g   = - par.GM1*r/R^3 + NB * gb - par.SH.GM*rPh/RPh^3;
    
%%  Phobos Circular orbit

        gPh = - (par.SH.GM + par.GM1)*rPh/RPh^3;

%%  EOM

%       1-6 are the MMX states, then 7-12 Phobos + variational equations
        dx(1+i*St_size:St_size+i*St_size,1) = [v; g; vPh; gPh; zeros(nCoeff,1); zeros(nBias,1)];
        
    end
    
end