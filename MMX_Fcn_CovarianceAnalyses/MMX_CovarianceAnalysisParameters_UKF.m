function [pars, units] = MMX_CovarianceAnalysisParameters_UKF(pars, units)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% [pars, units] = MMX_CovarianceAnalysisParameters(pars, units)
%
% This function adds some units and parameters useful to the covariance
% analysis
% 
% Inputs:
% pars          Structure which contains parameters
% units         Structure with units 
% 
% Outputs:
% pars.ObsNoise             Observations noises standard deviation
% pars.ObsNoise.range       Standard deviation of the relative distance measure
% pars.ObsNoise.range_rate  Standard deviation of the relative velocity measure
% pars.ObsNoise.lidar       Standard deviation of the lidar measure
% pars.ObsNoise.camera      Standard deviation of the camera measure
% pars.interval             Seconds between observations
% pars.Clm                  Vector with all the Clm coefficients
% pars.Slm                  Vector with all the Slm coefficients
% pars.nCoeff               Number of the harmonics coefficients
% pars.P0                   A-priori covariance
% pars.sigma                PN diffusion coefficient
%
% units.Dim                 Vector of normalizing units [sfVec, sfVec, ones(pars.nCoeff,1)]
% units.CovDim              Matrix of normalizing units
%
% Author: STAG Team
% Date: Jun 2021
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

%% Parameters
    
% Observation noise
pars.ObsNoise.range     = 5e-3/units.lsf;         % [km]
pars.ObsNoise.range_rate= 5e-7/units.vsf;         % [km/s]
pars.ObsNoise.lidar     = 1e-3/units.lsf;         % [km]

FOV     = 45*pi/180;
pixels  = 2048;
pars.ObsNoise.camera    = FOV/pixels;               % [rad/pixel]

% Useful to adimensionalize the observables for UKF
units.Observations = [units.lsf; units.vsf; units.lsf; units.vsf;...
    units.lsf; units.vsf; units.lsf; 1; 1];

% Matrix of the observation noise for UKF
pars.R      = zeros(9,9);
pars.R(1,1) = pars.ObsNoise.range^2;
pars.R(2,2) = pars.ObsNoise.range_rate^2;
pars.R(3,3) = pars.ObsNoise.range^2;
pars.R(4,4) = pars.ObsNoise.range_rate^2;
pars.R(5,5) = pars.ObsNoise.range^2;
pars.R(6,6) = pars.ObsNoise.range_rate^2;
pars.R(7,7) = pars.ObsNoise.lidar^2;
pars.R(8,8) = pars.ObsNoise.camera^2;
pars.R(9,9) = pars.ObsNoise.camera^2;

% Seconds between observation
pars.interval_Range         = 1800;     % s
pars.interval_Range_rate    = 600;      % s
pars.interval_lidar         = 150;      % s
pars.interval_camera        = 3600;     % s

pars.cutoffLIDAR    = 200;  % km
pars.elevation      = 80;   % deg

% Gravitational harmonics' parameters
Clm = [];
Slm = [];

for i=1:size(pars.SH.Clm,1)
    Clm = [Clm; pars.SH.Clm(i,1:i)'];
end
for i=2:size(pars.SH.Clm,1)
    Slm = [Slm; pars.SH.Slm(i,2:i)'];
end

pars.Clm = Clm;
pars.Slm = Slm;

% Measurements biases
bias = [0; 0; 0; 0; 0]...
    ./[units.lsf; units.vsf; units.lsf; ones(2,1)];     % [km, km/s, km, rad, rad]
pars.bias = bias;

% Unit vector and parameters number useful for later use
pars.nCoeff     = size(Clm,1) + size(Slm,1);
pars.nBias      = size(bias,1);
units.Dim       = [units.sfVec; units.sfVec; ones(pars.nCoeff,1);...
    units.lsf; units.vsf; units.lsf; ones(2,1)];
units.CovDim    = (repmat(units.Dim, 1, length(units.Dim)).*...
    repmat(units.Dim', length(units.Dim), 1));


% A-priori covariance
pars.P0         = diag(([1*ones(3,1); 1e-3*ones(3,1);...    
    1*ones(3,1); 1e-3*ones(3,1); 1*ones(pars.nCoeff,1);...
    1; 1e-3; 1; 1e-3; 1e-3]./units.Dim).^2);     % [km, km/s, km, km/s, ones,km, km/s, km, rad, rad]


% PN coefficients
SRP         = 4.5e-6;                                               %[Pa]
AU          = 149597871;                                            %[km]
phi         = 1367e-6;                                              %[W/km^2]
c           = 299792.458;                                           %[km/s]
VMMO_S      = 2e-4*3e-4;                                            %[km^2]
VMMO_M      = 24;                                                   %[kg]
pars.sigma  = SRP*VMMO_S/VMMO_M/(units.vsf/units.tsf);              %[km/s^2]

% r_VMM0      = cspice_spkezr('-999', pars.date0, 'J2000', 'none', 'Sun');
% 
% pars.sigma  = norm(-(phi/c)*AU^2*(VMMO_S/VMMO_M)*r_VMM0/norm(r_VMM0)^3)/(units.vsf/units.tsf);


end