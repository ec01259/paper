clear
close all
clc

addpath('./ResultsHPC/');
addpath('../useful_functions/');
addpath(genpath('../mice/'));
addpath(genpath('../generic_kernels/'));
addpath('MMX_Fcn_CovarianceAnalyses/');
MMX_InitializeSPICE
cspice_furnsh(which('mar097.bsp'));
set(0,'DefaultTextInterpreter','latex');
set(0,'DefaultAxesFontSize', 16);

%% Confronto altezze con PN 1e-10 and personalized choice of alpha

name6   = 'QSOH_PN9.mat';
load(name6);
Est6    = Est;
name7   = 'QSOM_PN9.mat';
load(name7);
Est7    = Est;
name8   = 'QSOLa_PN9.mat';
load(name8);
Est8    = Est;
name9   = 'QSOLb_PN9.mat';
load(name9);
Est9    = Est;
name10  = 'QSOLc_PN9.mat';
load(name10);
Est10   = Est;

t_obs   = Est6.t(1,:);
coeff   = 0.9;
% coeff   = 0.75;
fine    = round(coeff*6000);
fineCss = round(coeff*6000);

% Con Process noice

[SQRT_X6, SQRT_V6, P_t_C_20_6, P_t_C_22_6, mean6, std6, meanV6, stdV6,...
    meanLib6, stdLib6, meanC206, stdC206, meanC226, stdC226] = Envelopes_Plot(Est6, fine, fineCss);
[SQRT_X7, SQRT_V7, P_t_C_20_7, P_t_C_22_7, mean7, std7, meanV7, stdV7,...
    meanLib7, stdLib7, meanC207, stdC207, meanC227, stdC227] = Envelopes_Plot(Est7, fine, fineCss);
[SQRT_X8, SQRT_V8, P_t_C_20_8, P_t_C_22_8, mean8, std8, meanV8, stdV8,...
    meanLib8, stdLib8, meanC208, stdC208, meanC228, stdC228] = Envelopes_Plot(Est8, fine, fineCss);
[SQRT_X9, SQRT_V9, P_t_C_20_9, P_t_C_22_9, mean9, std9, meanV9, stdV9,...
    meanLib9, stdLib9, meanC209, stdC209, meanC229, stdC229] = Envelopes_Plot(Est9, fine, fineCss);
[SQRT_X10, SQRT_V10, P_t_C_20_10, P_t_C_22_10, mean10, std10, meanV10,...
    stdV10, meanLib10, stdLib10, meanC2010, stdC2010, meanC2210, stdC2210] = Envelopes_Plot(Est10, fine, fineCss);


% Plot

PlotComparison(Est6,Est7,Est8,Est9,Est10,fine, fineCss)


% Tradeoff
x       = (1:5);
y       = [mean6; mean7; mean8; mean9; mean10];
ypos    = [std6; std7; std8; std9; std10];
yneg    = [std6; std7; std8; std9; std10];
yV      = [meanV6; meanV7; meanV8; meanV9; meanV10];
yposV   = [stdV6; stdV7; stdV8; stdV9; stdV10];
ynegV   = [stdV6; stdV7; stdV8; stdV9; stdV10];

xC20       = (1:5);
yC20       = [meanC206; meanC207; meanC208; meanC209; meanC2010];
yposC20    = [stdC206; stdC207; stdC208; stdC209; stdC2010];
ynegC20    = [stdC206; stdC207; stdC208; stdC209; stdC2010];
yC22      = [meanC226; meanC227; meanC228; meanC229; meanC2210];
yposC22   = [stdC226; stdC227; stdC228; stdC229; stdC2210];
ynegC22   = [stdC226; stdC227; stdC228; stdC229; stdC2210];

labels  = {'$QSO-H$','$QSO-M$','$QSO-La$','$QSO-Lb$','$QSO-Lc$'};

% figure()
% subplot(1,2,1)
% errorbar(x,y,yneg,ypos,'-s','LineWidth',1.2,'MarkerSize',10)
% set(gca,'YScale','log')
% set(gca,'TickLabelInterpreter','latex')
% set(gca,'XTick',(1:5));
% set(gca,'XTickLabel',labels);
% set(gca,'XTickLabelRotation',45)
% ylabel('$[km]$')
% xlim([0,6])
% grid on
% subplot(1,2,2)
% errorbar(x,yV,ynegV,yposV,'-s','LineWidth',1.2,'MarkerSize',10)
% grid on
% set(gca,'YScale','log')
% set(gca,'TickLabelInterpreter','latex')
% set(gca,'XTick',(1:5));
% set(gca,'XTickLabel',labels);
% set(gca,'XTickLabelRotation',45)
% ylabel('$[km/s]$')
% xlim([0,6])

% Same thing with the libration angle

xLib       = (1:5);
yLib       = [meanLib6; meanLib7; meanLib8; meanLib9; meanLib10].*180/pi;
yposLib    = [stdLib6; stdLib7; stdLib8; stdLib9; stdLib10].*180/pi;
ynegLib    = [stdLib6; stdLib7; stdLib8; stdLib9; stdLib10].*180/pi;
labelsLib  = {'$QSO-M$','$QSO-La$','$QSO-Lb$','$QSO-Lc$'};

% figure()
% errorbar(xLib,yLib,ynegLib,yposLib,'-s','LineWidth',1.2,'MarkerSize',10)
% set(gca,'YScale','log')
% set(gca,'TickLabelInterpreter','latex')
% set(gca,'XTick',(1:4));
% set(gca,'XTickLabel',labels);
% set(gca,'XTickLabelRotation',45)
% ylabel('$[deg]$')
% xlim([0,6])
% grid on

%%  Confronto altezze con PN 1e-10 and personalized choice of alpha

name1   = 'QSOH_PN10.mat';
load(name1);
Est1    = Est;
name2   = 'QSOM_PN10.mat';
load(name2);
Est2    = Est;
name3   = 'QSOLa_PN10.mat';
load(name3);
Est3    = Est;
name4   = 'QSOLb_PN10.mat';
load(name4);
Est4    = Est;
name5  = 'QSOLc_PN10.mat';
load(name5);
Est5   = Est;


% Con Process noice

[SQRT_X1, SQRT_V1, P_t_C_20_1, P_t_C_22_1, mean1, std1, meanV1, stdV1,...
    meanLib1, stdLib1, meanC201, stdC201, meanC221, stdC221] = Envelopes_Plot(Est1, fine, fineCss);
[SQRT_X2, SQRT_V2, P_t_C_20_2, P_t_C_22_2, mean2, std2, meanV2, stdV2,...
    meanLib2, stdLib2, meanC202, stdC202, meanC222, stdC222] = Envelopes_Plot(Est2, fine, fineCss);
[SQRT_X3, SQRT_V3, P_t_C_20_3, P_t_C_22_3, mean3, std3, meanV3, stdV3,...
    meanLib3, stdLib3, meanC203, stdC203, meanC223, stdC223] = Envelopes_Plot(Est3, fine, fineCss);
[SQRT_X4, SQRT_V4, P_t_C_20_4, P_t_C_22_4, mean4, std4, meanV4, stdV4,...
    meanLib4, stdLib4, meanC204, stdC204, meanC224, stdC224] = Envelopes_Plot(Est4, fine, fineCss);
[SQRT_X5, SQRT_V5, P_t_C_20_5, P_t_C_22_5, mean5, std5, meanV5,...
    stdV5, meanLib5, stdLib5, meanC205, stdC205, meanC225, stdC225] = Envelopes_Plot(Est5, fine, fineCss);


% Plot

PlotComparison(Est1,Est2,Est3,Est4,Est5,fine, fineCss);

% Tradeoff
x       = (1:5);
y10       = [mean1; mean2; mean3; mean4; mean5];
ypos10    = [std1; std2; std3; std4; std5];
yneg10    = [std1; std2; std3; std4; std5];
yV10      = [meanV1; meanV2; meanV3; meanV4; meanV5];
yposV10   = [stdV1; stdV2; stdV3; stdV4; stdV5];
ynegV10   = [stdV1; stdV2; stdV3; stdV4; stdV5];

xC2010       = (1:5);
yC2010       = [meanC201; meanC202; meanC203; meanC204; meanC205];
yposC2010    = [stdC201; stdC202; stdC203; stdC204; stdC205];
ynegC2010    = [stdC201; stdC202; stdC203; stdC204; stdC205];
yC2210      = [meanC221; meanC222; meanC223; meanC224; meanC225];
yposC2210   = [stdC221; stdC222; stdC223; stdC224; stdC225];
ynegC2210   = [stdC221; stdC222; stdC223; stdC224; stdC225];

labels  = {'$QSO-H$','$QSO-M$','$QSO-La$','$QSO-Lb$','$QSO-Lc$'};

% figure()
% subplot(1,2,1)
% errorbar(x,y,yneg,ypos,'-s','LineWidth',1.2,'MarkerSize',10)
% grid on
% hold on
% errorbar(x,y10,yneg10,ypos10,'-s','LineWidth',1.2,'MarkerSize',10)
% set(gca,'YScale','log')
% set(gca,'TickLabelInterpreter','latex')
% set(gca,'XTick',(1:5));
% set(gca,'XTickLabel',labels);
% set(gca,'XTickLabelRotation',45)
% ylabel('$[km]$')
% legend('PN 1e-9', 'PN 1e-10')
% xlim([0,6])
% grid on
% subplot(1,2,2)
% errorbar(x,yV,ynegV,yposV,'-s','LineWidth',1.2,'MarkerSize',10)
% grid on
% hold on
% errorbar(x,yV10,ynegV10,yposV10,'-s','LineWidth',1.2,'MarkerSize',10)
% set(gca,'YScale','log')
% set(gca,'TickLabelInterpreter','latex')
% set(gca,'XTick',(1:5));
% set(gca,'XTickLabel',labels);
% set(gca,'XTickLabelRotation',45)
% legend('PN 1e-9', 'PN 1e-10')
% ylabel('$[km/s]$')
% xlim([0,6])

% Same thing with the libration angle

xLib10       = (1:5);
yLib10       = [meanLib1; meanLib2; meanLib3; meanLib4; meanLib5].*180/pi;
yposLib10    = [stdLib1; stdLib2; stdLib3; stdLib4; stdLib5].*180/pi;
ynegLib10    = [stdLib1; stdLib2; stdLib3; stdLib4; stdLib5].*180/pi;
labelsLib10  = {'$QSO-M$','$QSO-La$','$QSO-Lb$','$QSO-Lc$'};

% figure()
% errorbar(xLib,yLib,ynegLib,yposLib,'-s','LineWidth',1.2,'MarkerSize',10)
% hold on
% grid on
% errorbar(xLib10,yLib10,ynegLib10,yposLib10,'-s','LineWidth',1.2,'MarkerSize',10)
% set(gca,'YScale','log')
% set(gca,'TickLabelInterpreter','latex')
% set(gca,'XTick',(1:4));
% set(gca,'XTickLabel',labels);
% set(gca,'XTickLabelRotation',45)
% legend('PN 1e-9', 'PN 1e-10')
% ylabel('$[deg]$')
% xlim([0,6])
% grid on


%%  Confronto altezze con PN 5e-11 and personalized choice of alpha

name11   = 'QSOH_PN511.mat';
load(name11);
Est11    = Est;
name12   = 'QSOM_PN511.mat';
load(name12);
Est12    = Est;
name13   = 'QSOLa_PN511.mat';
load(name13);
Est13    = Est;
name14   = 'QSOLb_PN511.mat';
load(name14);
Est14    = Est;
name15  = 'QSOLc_PN511.mat';
load(name15);
Est15   = Est;


% Con Process noice

[SQRT_X11, SQRT_V11, P_t_C_20_11, P_t_C_22_11, mean11, std11, meanV11, stdV11,...
    meanLib11, stdLib11, meanC2011, stdC2011, meanC2211, stdC2211] = Envelopes_Plot(Est11, fine, fineCss);
[SQRT_X12, SQRT_V12, P_t_C_20_12, P_t_C_22_12, mean12, std12, meanV12, stdV12,...
    meanLib12, stdLib12, meanC2012, stdC2012, meanC2212, stdC2212] = Envelopes_Plot(Est12, fine, fineCss);
[SQRT_X13, SQRT_V13, P_t_C_20_13, P_t_C_22_13, mean13, std13, meanV13, stdV13,...
    meanLib13, stdLib13, meanC2013, stdC2013, meanC2213, stdC2213] = Envelopes_Plot(Est13, fine, fineCss);
[SQRT_X14, SQRT_V14, P_t_C_20_14, P_t_C_22_14, mean14, std14, meanV14, stdV14,...
    meanLib14, stdLib14, meanC2014, stdC2014, meanC2214, stdC2214] = Envelopes_Plot(Est14, fine, fineCss);
[SQRT_X15, SQRT_V15, P_t_C_20_15, P_t_C_22_15, mean15, std15, meanV15,...
    stdV15, meanLib15, stdLib15, meanC2015, stdC2015, meanC2215, stdC2215] = Envelopes_Plot(Est15, fine, fineCss);


% Plot

PlotComparison(Est11,Est12,Est13,Est14,Est15,fine, fineCss)

% Tradeoff
x       = (1:5);
y511       = [mean11; mean12; mean13; mean14; mean15];
ypos511    = [std11; std12; std13; std14; std15];
yneg511    = [std11; std12; std13; std14; std15];
yV511      = [meanV11; meanV12; meanV13; meanV14; meanV15];
yposV511   = [stdV11; stdV12; stdV13; stdV14; stdV15];
ynegV511   = [stdV11; stdV12; stdV13; stdV14; stdV15];

xC20       = (1:5);
yC20511       = [meanC2011; meanC2012; meanC2013; meanC2014; meanC2015];
yposC20511    = [stdC2011; stdC2012; stdC2013; stdC2014; stdC2015];
ynegC20511    = [stdC2011; stdC2012; stdC2013; stdC2014; stdC2015];
yC22511      = [meanC2211; meanC2212; meanC2213; meanC2214; meanC2215];
yposC22511   = [stdC2211; stdC2212; stdC2213; stdC2214; stdC2215];
ynegC22511   = [stdC2211; stdC2212; stdC2213; stdC2214; stdC2215];

labels  = {'$QSO-H$','$QSO-M$','$QSO-La$','$QSO-Lb$','$QSO-Lc$'};

figure()
subplot(1,2,1)
errorbar(x,y,yneg,ypos,'-s','LineWidth',1.2,'MarkerSize',10)
hold on
grid on
errorbar(x,y10,yneg10,ypos10,'-s','LineWidth',1.2,'MarkerSize',10)
errorbar(x,y511,yneg511,ypos511,'-s','LineWidth',1.2,'MarkerSize',10)
set(gca,'YScale','log')
set(gca,'TickLabelInterpreter','latex')
set(gca,'XTick',(1:5));
set(gca,'XTickLabel',labels);
set(gca,'XTickLabelRotation',45)
legend('PN 1e-9','PN 1e-10','PN 5e-11')
ylabel('$[km]$')
xlim([0,6])
grid on
subplot(1,2,2)
errorbar(x,yV,ynegV,yposV,'-s','LineWidth',1.2,'MarkerSize',10)
grid on
hold on
errorbar(x,yV10,ynegV10,yposV10,'-s','LineWidth',1.2,'MarkerSize',10)
errorbar(x,yV511,ynegV511,yposV511,'-s','LineWidth',1.2,'MarkerSize',10)
set(gca,'YScale','log')
set(gca,'TickLabelInterpreter','latex')
set(gca,'XTick',(1:5));
set(gca,'XTickLabel',labels);
set(gca,'XTickLabelRotation',45)
legend('PN 1e-9','PN 1e-10','PN 5e-11')
ylabel('$[km/s]$')
xlim([0,6])

% Same thing with the libration angle

xLib       = (1:5);
yLib511       = [meanLib11; meanLib12; meanLib13; meanLib14; meanLib15].*180/pi;
yposLib511    = [stdLib11; stdLib12; stdLib13; stdLib14; stdLib15].*180/pi;
ynegLib511    = [stdLib11; stdLib12; stdLib13; stdLib14; stdLib15].*180/pi;
labelsLib511  = {'$QSO-M$','$QSO-La$','$QSO-Lb$','$QSO-Lc$'};

figure()
errorbar(xLib,yLib,ynegLib,yposLib,'-s','LineWidth',1.2,'MarkerSize',10)
hold on
grid on
errorbar(xLib10,yLib10,ynegLib10,yposLib10,'-s','LineWidth',1.2,'MarkerSize',10)
errorbar(xLib,yLib511,ynegLib511,yposLib511,'-s','LineWidth',1.2,'MarkerSize',10)
set(gca,'YScale','log')
set(gca,'TickLabelInterpreter','latex')
set(gca,'XTick',(1:4));
set(gca,'XTickLabel',labels);
set(gca,'XTickLabelRotation',45)
ylabel('$[deg]$')
legend('PN 1e-9','PN 1e-10','PN 5e-11')
xlim([0,6])
grid on


figure()
subplot(1,2,1)
errorbar(x,yC20,ynegC20,yposC20,'-s','LineWidth',1.2,'MarkerSize',5)
hold on
errorbar(x,yC2010,ynegC2010,yposC2010,'-s',...
    'LineWidth',1.2,'MarkerSize',5)
errorbar(x,yC20511,ynegC20511,yposC20511,'-s',...
    'LineWidth',1.2,'MarkerSize',5)
set(gca,'YScale','log')
set(gca,'TickLabelInterpreter','latex')
set(gca,'XTick',(1:5));
set(gca,'XTickLabel',labels,'Fontsize',26);
set(gca,'XTickLabelRotation',45)
legend('PN 1e-9','PN 1e-10','PN 5e-11')
ylabel('$3\sigma_{C_{20}}$','Fontsize',30)
xlim([0,6])
grid on
subplot(1,2,2)
errorbar(x,yC22,ynegC22,yposC22,'-s','LineWidth',1.2,'MarkerSize',5)
hold on
errorbar(x,yC2210,ynegC2210,yposC2210,'-s',...
    'LineWidth',1.2,'MarkerSize',5)
errorbar(x,yC22511,ynegC22511,yposC22511,'-s',...
    'LineWidth',1.2,'MarkerSize',5)
grid on
set(gca,'YScale','log')
set(gca,'TickLabelInterpreter','latex')
set(gca,'XTick',(1:5));
set(gca,'XTickLabel',labels,'Fontsize',26);
set(gca,'XTickLabelRotation',45)
legend('PN 1e-9','PN 1e-10','PN 5e-11')
ylabel('$3\sigma_{C_{22}}$','Fontsize',30)
xlim([0,6])
