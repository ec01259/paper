function [pars, units] = MMX_DefineModelParametersAndUnits(PhobosGravityField, varargin)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% [pars, units] = MMX_DefineModelParametersAndUnits(PhobosGravityField)
%
% Define model parameters and units for the MMX Toolbox
% 
% Inputs:
% PhobosGravityField    Order x Degree of Phobos' gravity field (max [8, 8])
% 
% Outputs:
% pars.d        No. of States
% pars.p        No. of model parameters
% pars.Body     Secondary's body structure:
%          .alp Secondary's largest semi-axis [Normalized Units]
%          .bet Secondary's intermediate semi-axis [Normalized Units]
%          .gam Secondary's smallest semi-axis [Normalized Units]
%
% units.lsf     Length unit
% units.tsf     Time unit
% units.vsf     Velocity unit
% units.sfVec   Vector of normalizing units [lsf, lsf, lsf, vsf, vsf, vsf]
%
% Author: STAG Team
% Date: Jun 2021
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 



%% Read Phobos Spherical Harmonics
SH = PhobosSphericalHarmonicCoefficients(PhobosGravityField(1),PhobosGravityField(2));
Rp = SH.Re;
GMp = SH.GM;



%% Problem Parameters
alp = 13.03;                 % Phobos' largest semi-axis (km) 
bet = 11.4;                  % Phobos' intermediate semi-axis (km)
gam = 9.14;                  % Phobos' smallest semi-axis (km)
a = 9378.24190780618;        % Phobos' semi-major axis (km)
GMm = 42828.3736206991;      % Mars grav. parameter [values obtained via cspice_bodvrd (km^3/s^2)]
n = sqrt((GMp + GMm)/a^3);   % Phobos' mean motion (rad/s) 



%% Units
if(nargin > 1)
    units.lsf = varargin{2};
    units.tsf = varargin{3};
else
    units.lsf = 100;
    units.tsf = 1/n;
end
units.GMsf = units.lsf^3/units.tsf^2;
units.vsf = units.lsf/units.tsf;
units.sfVec = [units.lsf; units.lsf; units.lsf; units.vsf; units.vsf; units.vsf];



%% Normalize Parameters
Rp = Rp/units.lsf;
GMp = GMp/units.GMsf;
GMm = GMm/units.GMsf;
a = a/units.lsf;
n = n*units.tsf;
SH.Re = Rp;
SH.GM = GMp;
alp = alp/units.lsf;
bet = bet/units.lsf;
gam = gam/units.lsf;



%% Store Normalized Parameters
pars.d = 6;
pars.p = 0;
pars.GM1 = GMm;
pars.a = a;
pars.n = n;
pars.SH = SH;
pars.alp = alp;
pars.bet = bet;
pars.gam = gam;