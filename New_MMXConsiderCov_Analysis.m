clear
close all
clc

%%  Setup

    warning('off', 'all'); format longG;
    set(0,'DefaultTextInterpreter','latex');
    set(0,'DefaultAxesFontSize', 16);

%   Mac
    restoredefaultpath
    addpath('MMX_Fcn_CovarianceAnalyses/');
    addpath('Model_check/');
    addpath('../useful_functions/');
    addpath(genpath('../mice/'));
    addpath(genpath('../generic_kernels/'));
    addpath(genpath('../paper_after_conf/MMX_Product/MMX_BSP_Files_GravLib/'));
    MMX_InitializeSPICE
    cspice_furnsh(which('mar097.bsp'));
    cspice_furnsh(which('MARPHOFRZ.txt'));
%     cspice_furnsh(which('MMX_QSO_094_2x2_826891269_831211269.bsp'));
    cspice_furnsh(which('MMX_3DQSO_094_009_2x2_826891269_831211269.bsp'));
%     cspice_furnsh(which('MMX_SwingQSO_094_006_2x2_826891269_831211269.bsp'));
    cspice_furnsh(which('Phobos_826891269_831211269.bsp'));
    


%%  Load observations and path for syntethic pictures

    load("Observations.mat");
    YObs_SB = YObs_Full;
    YObs_SB(2:7,:) = NaN;

%%  Initial conditions for the analysis

%   Model parameters
    [par, units] = MMX_DefineNewModelParametersAndUnits;

%   Time of the analysis
    data        = '2026-03-16 00:00:00 (UTC)';
    data        = cspice_str2et(data);
    day         = 86400;
    par.et0     = data;
    [Ph,par]    = Phobos_States_NewModel(data,par);

%   Covariance analysis parameters
    [par1, units1] = New_MMX_CovarianceAnalysisParameters(par, units);
    [par, units]  = New_MMX_ConsiderCovarianceAnalysisParameters(par, units);
    par.sigma     = 1e-9/(units.vsf*units.tsf);
    par.sigmaPh   = 0/(units.vsf*units.tsf);
    par1.sigma    = 1e-9/(units.vsf*units.tsf);
    par1.sigmaPh  = 0/(units.vsf*units.tsf);

%   Initial Phobos's state vector
    Ph0     = Ph./units.sfVec2;

%   Initial MMX's State vector
    MMX0   = cspice_spkezr('-34', data, 'MarsIAU', 'none', '499');
    MMX0   = MMX0./units.sfVec;

%   Analysis initial state vector
    St01   = [MMX0; Ph0; par.I2; par.bias];
    St0    = [MMX0; par.I2; par.bias];


    Est10.X  = St01;
    Est10.dx = zeros(size(St01,1),1);
    Est10.P0 = par1.P0;
    Est10.t0 = data*units.tsf;

    Est0.X  = St0;
    Est0.c  = Ph0;
    Est0.dx = zeros(size(St0,1),1);
    Est0.P0     = par.P0;
    Est0.Pcc    = par.Pcc;
    Est0.Pxc    = par.Pxc;
    Est0.t0     = data*units.tsf;

%%  Analysis
  
    
    par.alpha   = 1;
    par.alpha_c = 1;
    par.beta    = 2;
    par1.alpha  = 1;

%     [Est1] = UKF_CovarianceAnalysis(Est10,@Dynamics_MPHandMMX_Inertia,...
%         @Cov_Dynamics_Good, @New_Observables_model,...
%         par.R,YObs_Full,par1,units1);

    [Est] = ConsiderUKF_CovarianceAnalysis(Est0,@Dynamics_MPHandMMX_Inertia,...
        @Consider_cov_Dynamics_Good,@COnsider_Dynamics_Good, @Consider_Observables_model,...
        par.R,YObs_Full,par,units);

%     [Est1] = ConsiderUKF_CovarianceAnalysis(Est0,@Dynamics_MPHandMMX_Inertia,...
%         @Consider_cov_Dynamics_Good,@COnsider_Dynamics_Good, @Consider_Observables_model,...
%         par.R,YObs_SB,par,units);
    
%    NewCov_ResultsPlot(Est1, YObs_Full, par, units)
   ConsiderCov_ResultsPlot(Est, YObs_Full, par, units)
%    ConsiderCov_ResultsPlot(Est1, YObs_SB, par, units)