clear
close all
clc

%%  Setup

    warning('off', 'all'); format longG;
    set(0,'DefaultTextInterpreter','latex');
    set(0,'DefaultAxesFontSize', 16);

%   Mac
    restoredefaultpath
    addpath('MMX_Fcn_CovarianceAnalyses/');
    addpath('../useful_functions/');
    addpath(genpath('../mice/'));
    addpath(genpath('../generic_kernels/'));
    addpath(genpath('MMX_Product/MMX_BSP_Files_GravLib/'));
    MMX_InitializeSPICE
    cspice_furnsh(which('mar097.bsp'));
    cspice_furnsh(which('MMX_QSO_027_2x2_826891269_828619269.bsp'));
%     cspice_furnsh(which('MMX_3DQSO_031_009_2x2_826891269_828619269.bsp'));
%     cspice_furnsh(which('MMX_SwingQSO_031_011_2x2_826891269_828619269.bsp'));
    cspice_furnsh(which('Phobos_826891269_828619269.bsp'));
    


%%  Load observations and path for syntethic pictures

    addpath(genpath('~/Documents/MATLAB/Paper_after_Conf/Pictures'));
    load("Observations.mat");

%%  Initial conditions for the analysis

%   Model parameters
    [par, units] = MMX_DefineNewModelParametersAndUnits;

%   Time of the analysis
    data        = '2026-03-18 00:00:00 (UTC)';
    data        = cspice_str2et(data);
    par.et0     = data;
    [Ph,par]    = Phobos_States_NewModel(data,par);

%   Covariance analysis parameters
    [par, units] = New_MMX_CovarianceAnalysisParameters_NoPhiM(par, units);
    par.sigma    = 0/(units.vsf*units.tsf);
    par.sigmaPh  = 0/(units.vsf*units.tsf);

%   Initial Phobos's state vector
    Ph0     = Ph./units.sfVec2;

%   Initial MMX's State vector
    MMX0   = cspice_spkezr('-34', data, 'MarsIAU', 'none', '499');
    MMX0   = MMX0./units.sfVec;

%   Analysis initial state vector
    St0    = [MMX0; Ph0(1:4); Ph0(7:8); par.I2; par.bias];

    Est0.X  = St0;
    Est0.dx = zeros(size(St0,1),1);
    Est0.P0 = par.P0;
    Est0.t0 = data*units.tsf;
    par.X0_reference = [MMX0; Ph0; par.I2];


%%  Analysis
  
    
    par.alpha   = 1e0;

    [Est] = UKF_CovarianceAnalysis(Est0,@Dynamics_MPHandMMX_Inertia,...
        @Cov_Dynamics_Good_NoPhiM, @New_Observables_model,...
        par.R,YObs_Full,par,units);
    
    save('Risultati_QSOLc_alpha1_18.mat','Est');
    move('Risultati_QSOLc_alpha1_18.mat','./ResultsHPC/');
    
    NewCov_ResultsPlot_NoPhiM(Est, YObs_Full, par, units)
% %     PlotDifferences_NoPhiM(Est,YObs_Full,par,units)
