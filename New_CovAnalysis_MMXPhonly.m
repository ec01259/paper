clear
close all
clc

%%  Setup

    warning('off', 'all'); format longG;
    set(0,'DefaultTextInterpreter','latex');
    set(0,'DefaultAxesFontSize', 16);

%   Mac
    restoredefaultpath
    addpath('MMX_Fcn_CovarianceAnalyses/');
    addpath('MMX_Product/MMX_FullEphemeris_BSP_Files/');
    addpath('../useful_functions/');
    addpath(genpath('../mice/'));
    addpath(genpath('../generic_kernels/'));
    addpath(genpath('./MMX_Product/MMX_BSP_Files_GravLib/'));
    addpath(genpath('./Model_Check/'));
    MMX_InitializeSPICE
    cspice_furnsh(which('mar097.bsp'));
%     cspice_furnsh(which('MMX_QSO_031_2x2_826891269_828619269.bsp'));
%     cspice_furnsh(which('MMX_3DQSO_031_006_2x2_826891269_828619269.bsp'));
    cspice_furnsh(which('MMX_SwingQSO_031_001_2x2_826891269_828619269.bsp'));
    cspice_furnsh(which('Phobos_826891269_828619269.bsp'));
    cspice_furnsh(which('MARPHOFRZ.txt'));
    
%   Time of the analysis
    data        = '2026-03-16 00:00:00 (UTC)';
    data        = cspice_str2et(data);
    
%   Model parameters
    [par, units] = MMX_DefineNewModelParametersAndUnits;

%   Covariance analysis parameters
    par.date0    = data;
    [par, units] = New_MMX_CovarianceAnalysisParameters_MMXPhonly(par, units);
    par.sigma    = 8e-10/(units.vsf*units.tsf);
    par.sigmaPh  = 0/(units.vsf*units.tsf);


%%  Load observations and path for syntethic pictures

    addpath(genpath('~/Documents/MATLAB/Paper_after_Conf/Pictures'));
    load("Observations.mat");

%%  Initial conditions for the analysis

%   Initial Phobos's State vector
    [Ph, par]   = Phobos_States_NewModel(data,par);
    Ph0         = Ph./units.sfVec2;

%   Initial MMX's State vector
    MMX0    = cspice_spkezr('-34', data, 'MarsIAU', 'none', '499');
    MMX0    = MMX0./units.sfVec;

%   Analysis initial state vector
    St0     = [MMX0; Ph0];

    Est0.X  = St0;
    Est0.dx = zeros(size(St0,1));
    Est0.P0 = par.P0;
    Est0.t0 = data*units.tsf;



%%  Analysis

    par.alpha = 5e-1;

    [Est] = UKF_CovarianceAnalysis(Est0,@Dynamics_MPHandMMX_Inertia,...
        @New_Analysis_Dynamics_MMXPhonly, @New_Observables_model_MMXPhonly,...
        par.R,YObs_Full,par,units);
%     [Est] = UKF(Est0,@New_Analysis_Dynamics_smart_MMXPhonly,...
%         @New_Observables_model_MMXPhonly,par.R,YObs_Full,par,units);

    NewCov_ResultsPlot_MMXPhonly(Est, YObs_Full, par, units)
%     PlotDifferences_MMXPhonly(Est,YObs_Full,par,units)
