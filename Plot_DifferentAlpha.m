clear
close all
clc

addpath('./ResultsHPC/');
addpath('../useful_functions/');
addpath(genpath('../mice/'));
addpath(genpath('../generic_kernels/'));
addpath('MMX_Fcn_CovarianceAnalyses/');
set(0,'DefaultTextInterpreter','latex');

%% Confronto altezze con PN 1e-10 alpha 1e0

load('QSOH_PN10_alpha1.mat');
Est1    = Est;
load('QSOM_PN10_alpha1.mat');
Est2    = Est;
load('QSOLa_PN10_alpha1.mat');
Est3    = Est;
load('QSOLb_PN10_alpha1.mat');
Est4    = Est;
load('QSOLc_PN10_alpha1.mat');
Est5   = Est;

labels  = {'$QSO-H$','$QSO-M$','$QSO-La$','$QSO-Lb$','$QSO-Lc$'};
t_obs   = Est1.t(1,:);
coeff   = 0.8;
fine    = round(coeff*size(t_obs,2));
fineCss = round(coeff*size(t_obs,2));

% Con Process noice

[SQRT_X1, SQRT_V1, P_t_C_20_1, P_t_C_22_1, mean1, std1, meanV1, stdV1,...
    meanLib1, stdLib1, meanC201, stdC201, meanC221, stdC221] = Envelopes_Plot(Est1, fine, fineCss);
[SQRT_X2, SQRT_V2, P_t_C_20_2, P_t_C_22_2, mean2, std2, meanV2, stdV2,...
    meanLib2, stdLib2, meanC202, stdC202, meanC222, stdC222] = Envelopes_Plot(Est2, fine, fineCss);
[SQRT_X3, SQRT_V3, P_t_C_20_3, P_t_C_22_3, mean3, std3, meanV3, stdV3,...
    meanLib3, stdLib3, meanC203, stdC203, meanC223, stdC223] = Envelopes_Plot(Est3, fine, fineCss);
[SQRT_X4, SQRT_V4, P_t_C_20_4, P_t_C_22_4, mean4, std4, meanV4, stdV4,...
    meanLib4, stdLib4, meanC204, stdC204, meanC224, stdC224] = Envelopes_Plot(Est4, fine, fineCss);
[SQRT_X5, SQRT_V5, P_t_C_20_5, P_t_C_22_5, mean5, std5, meanV5,...
    stdV5, meanLib5, stdLib5, meanC205, stdC205, meanC225, stdC225] = Envelopes_Plot(Est5, fine, fineCss);

x        = (1:5);
y1       = [mean1; mean2; mean3; mean4; mean5];
ypos1    = [std1; std2; std3; std4; std5];
yneg1    = [std1; std2; std3; std4; std5];
yV1      = [meanV1; meanV2; meanV3; meanV4; meanV5];
yposV1   = [stdV1; stdV2; stdV3; stdV4; stdV5];
ynegV1   = [stdV1; stdV2; stdV3; stdV4; stdV5];

xC20     = (1:5);
yC201    = [meanC201; meanC202; meanC203; meanC204; meanC205];
yposC201 = [stdC201; stdC202; stdC203; stdC204; stdC205];
ynegC201 = [stdC201; stdC202; stdC203; stdC204; stdC205];
yC221    = [meanC221; meanC222; meanC223; meanC224; meanC225];
yposC221 = [stdC221; stdC222; stdC223; stdC224; stdC225];
ynegC221 = [stdC221; stdC222; stdC223; stdC224; stdC225];

xLib     = (1:5);
yLib1    = [meanLib1; meanLib2; meanLib3; meanLib4; meanLib5].*180/pi;
yposLib1 = [stdLib1; stdLib2; stdLib3; stdLib4; stdLib5].*180/pi;
ynegLib1 = [stdLib1; stdLib2; stdLib3; stdLib4; stdLib5].*180/pi;


%% Confronto altezze con PN 1e-10 alpha 1e-1

load('QSOH_PN10_alpha11.mat');
Est6    = Est;
load('QSOM_PN10_alpha11.mat');
Est7    = Est;
load('QSOLa_PN10_alpha11.mat');
Est8    = Est;
load('QSOLb_PN10_alpha11.mat');
Est9    = Est;
load('QSOLc_PN10_alpha11.mat');
Est10   = Est;


% Con Process noice

[SQRT_X6, SQRT_V6, P_t_C_20_6, P_t_C_22_6, mean6, std6, meanV6, stdV6,...
    meanLib6, stdLib6, meanC206, stdC206, meanC226, stdC226] = Envelopes_Plot(Est6, fine, fineCss);
[SQRT_X7, SQRT_V7, P_t_C_20_7, P_t_C_22_7, mean7, std7, meanV7, stdV7,...
    meanLib7, stdLib7, meanC207, stdC207, meanC227, stdC227] = Envelopes_Plot(Est7, fine, fineCss);
[SQRT_X8, SQRT_V8, P_t_C_20_8, P_t_C_22_8, mean8, std8, meanV8, stdV8,...
    meanLib8, stdLib8, meanC208, stdC208, meanC228, stdC228] = Envelopes_Plot(Est8, fine, fineCss);
[SQRT_X9, SQRT_V9, P_t_C_20_9, P_t_C_22_9, mean9, std9, meanV9, stdV9,...
    meanLib9, stdLib9, meanC209, stdC209, meanC229, stdC229] = Envelopes_Plot(Est9, fine, fineCss);
[SQRT_X10, SQRT_V10, P_t_C_20_10, P_t_C_22_10, mean10, std10, meanV10,...
    stdV10, meanLib10, stdLib10, meanC2010, stdC2010, meanC2210, stdC2210] = Envelopes_Plot(Est10, fine, fineCss);

y11       = [mean6; mean7; mean8; mean9; mean10];
ypos11    = [std6; std7; std8; std9; std10];
yneg11    = [std6; std7; std8; std9; std10];
yV11      = [meanV6; meanV7; meanV8; meanV9; meanV10];
yposV11   = [stdV6; stdV7; stdV8; stdV9; stdV10];
ynegV11   = [stdV6; stdV7; stdV8; stdV9; stdV10];

yC2011    = [meanC206; meanC207; meanC208; meanC209; meanC2010];
yposC2011 = [stdC206; stdC207; stdC208; stdC209; stdC2010];
ynegC2011 = [stdC206; stdC207; stdC208; stdC209; stdC2010];
yC2211    = [meanC226; meanC227; meanC228; meanC229; meanC2210];
yposC2211 = [stdC226; stdC227; stdC228; stdC229; stdC2210];
ynegC2211 = [stdC226; stdC227; stdC228; stdC229; stdC2210];

yLib11    = [meanLib6; meanLib7; meanLib8; meanLib9; meanLib10].*180/pi;
yposLib11 = [stdLib6; stdLib7; stdLib8; stdLib9; stdLib10].*180/pi;
ynegLib11 = [stdLib6; stdLib7; stdLib8; stdLib9; stdLib10].*180/pi;


%% Confronto altezze con PN 1e-10 alpha 1e-2

load('QSOH_PN10_alpha12.mat');
Est11    = Est;
load('QSOM_PN10_alpha12.mat');
Est12    = Est;
load('QSOLa_PN10_alpha12.mat');
Est13    = Est;
load('QSOLb_PN10_alpha12.mat');
Est14    = Est;
load('QSOLc_PN10_alpha12.mat');
Est15   = Est;


% Con Process noice

[SQRT_X11, SQRT_V11, P_t_C_20_11, P_t_C_22_11, mean11, std11, meanV11, stdV11,...
    meanLib11, stdLib11, meanC2011, stdC2011, meanC2211, stdC2211] = Envelopes_Plot(Est11, fine, fineCss);
[SQRT_X12, SQRT_V12, P_t_C_20_12, P_t_C_22_12, mean12, std12, meanV12, stdV12,...
    meanLib12, stdLib12, meanC2012, stdC2012, meanC2212, stdC2212] = Envelopes_Plot(Est12, fine, fineCss);
[SQRT_X13, SQRT_V13, P_t_C_20_13, P_t_C_22_13, mean13, std13, meanV13, stdV13,...
    meanLib13, stdLib13, meanC2013, stdC2013, meanC2213, stdC2213] = Envelopes_Plot(Est13, fine, fineCss);
[SQRT_X14, SQRT_V14, P_t_C_20_14, P_t_C_22_14, mean14, std14, meanV14, stdV14,...
    meanLib14, stdLib14, meanC2014, stdC2014, meanC2214, stdC2214] = Envelopes_Plot(Est14, fine, fineCss);
[SQRT_X15, SQRT_V15, P_t_C_20_15, P_t_C_22_15, mean15, std15, meanV15,...
    stdV15, meanLib15, stdLib15, meanC2015, stdC2015, meanC2215, stdC2215] = Envelopes_Plot(Est15, fine, fineCss);

x         = (1:5);
y12       = [mean11; mean12; mean13; mean14; mean15];
ypos12    = [std11; std12; std13; std14; std15];
yneg12    = [std11; std12; std13; std14; std15];
yV12      = [meanV11; meanV12; meanV13; meanV14; meanV15];
yposV12   = [stdV11; stdV12; stdV13; stdV14; stdV15];
ynegV12   = [stdV11; stdV12; stdV13; stdV14; stdV15];

yC2012    = [meanC2011; meanC2012; meanC2013; meanC2014; meanC2015];
yposC2012 = [stdC2011; stdC2012; stdC2013; stdC2014; stdC2015];
ynegC2012 = [stdC2011; stdC2012; stdC2013; stdC2014; stdC2015];
yC2212    = [meanC2211; meanC2212; meanC2213; meanC2214; meanC2215];
yposC2212 = [stdC2211; stdC2212; stdC2213; stdC2214; stdC2215];
ynegC2212 = [stdC2211; stdC2212; stdC2213; stdC2214; stdC2215];

yLib12    = [meanLib11; meanLib12; meanLib13; meanLib14; meanLib15].*180/pi;
yposLib12 = [stdLib11; stdLib12; stdLib13; stdLib14; stdLib15].*180/pi;
ynegLib12 = [stdLib11; stdLib12; stdLib13; stdLib14; stdLib15].*180/pi;

%% Confronto altezze con PN 1e-10 alpha 5e-1

load('QSOH_PN10_alpha51.mat');
Est16    = Est;
load('QSOM_PN10_alpha51.mat');
Est17    = Est;
load('QSOLa_PN10_alpha51.mat');
Est18    = Est;
load('QSOLb_PN10_alpha51.mat');
Est19    = Est;
load('QSOLc_PN10_alpha51.mat');
Est20   = Est;


% Con Process noice

[SQRT_X16, SQRT_V16, P_t_C_20_16, P_t_C_22_16, mean16, std16, meanV16, stdV16,...
    meanLib16, stdLib16, meanC2016, stdC2016, meanC2216, stdC2216] = Envelopes_Plot(Est16, fine, fineCss);
[SQRT_X17, SQRT_V17, P_t_C_20_17, P_t_C_22_17, mean17, std17, meanV17, stdV17,...
    meanLib17, stdLib17, meanC2017, stdC2017, meanC2217, stdC2217] = Envelopes_Plot(Est17, fine, fineCss);
[SQRT_X18, SQRT_V18, P_t_C_20_18, P_t_C_22_18, mean18, std18, meanV18, stdV18,...
    meanLib18, stdLib18, meanC2018, stdC2018, meanC2218, stdC2218] = Envelopes_Plot(Est18, fine, fineCss);
[SQRT_X19, SQRT_V19, P_t_C_20_19, P_t_C_22_19, mean19, std19, meanV19, stdV19,...
    meanLib19, stdLib19, meanC2019, stdC2019, meanC2219, stdC2219] = Envelopes_Plot(Est19, fine, fineCss);
[SQRT_X20, SQRT_V20, P_t_C_20_20, P_t_C_22_20, mean20, std20, meanV20,...
    stdV20, meanLib20, stdLib20, meanC2020, stdC2020, meanC2220, stdC2220] = Envelopes_Plot(Est20, fine, fineCss);

y51       = [mean16; mean17; mean18; mean19; mean20];
ypos51    = [std16; std17; std18; std19; std20];
yneg51    = [std16; std17; std18; std19; std20];
yV51      = [meanV16; meanV17; meanV18; meanV19; meanV20];
yposV51   = [stdV16; stdV17; stdV18; stdV19; stdV20];
ynegV51   = [stdV16; stdV17; stdV18; stdV19; stdV20];

yC2051    = [meanC2016; meanC2017; meanC2018; meanC2019; meanC2020];
yposC2051 = [stdC2016; stdC2017; stdC2018; stdC2019; stdC2020];
ynegC2051 = [stdC2016; stdC2017; stdC2018; stdC2019; stdC2020];
yC2251    = [meanC2216; meanC2217; meanC2218; meanC2219; meanC2220];
yposC2251 = [stdC2216; stdC2217; stdC2218; stdC2219; stdC2220];
ynegC2251 = [stdC2216; stdC2217; stdC2218; stdC2219; stdC2220];

yLib51    = [meanLib16; meanLib17; meanLib18; meanLib19; meanLib20].*180/pi;
yposLib51 = [stdLib16; stdLib17; stdLib18; stdLib19; stdLib20].*180/pi;
ynegLib51 = [stdLib16; stdLib17; stdLib18; stdLib19; stdLib20].*180/pi;

%% Confronto altezze con PN 1e-10 alpha 5e-2

load('QSOH_PN10_alpha52.mat');
Est21    = Est;
load('QSOM_PN10_alpha52.mat');
Est22    = Est;
load('QSOLa_PN10_alpha52.mat');
Est23    = Est;
load('QSOLb_PN10_alpha52.mat');
Est24    = Est;
load('QSOLc_PN10_alpha52.mat');
Est25   = Est;


% Con Process noice

[SQRT_X21, SQRT_V21, P_t_C_20_21, P_t_C_22_21, mean21, std21, meanV21, stdV21,...
    meanLib21, stdLib21, meanC2021, stdC2021, meanC2221, stdC2221] = Envelopes_Plot(Est21, fine, fineCss);
[SQRT_X22, SQRT_V22, P_t_C_20_22, P_t_C_22_22, mean22, std22, meanV22, stdV22,...
    meanLib22, stdLib22, meanC2022, stdC2022, meanC2222, stdC2222] = Envelopes_Plot(Est22, fine, fineCss);
[SQRT_X23, SQRT_V23, P_t_C_20_23, P_t_C_22_23, mean23, std23, meanV23, stdV23,...
    meanLib23, stdLib23, meanC2023, stdC2023, meanC2223, stdC2223] = Envelopes_Plot(Est23, fine, fineCss);
[SQRT_X24, SQRT_V24, P_t_C_20_24, P_t_C_22_24, mean24, std24, meanV24, stdV24,...
    meanLib24, stdLib24, meanC2024, stdC2024, meanC2224, stdC2224] = Envelopes_Plot(Est24, fine, fineCss);
[SQRT_X25, SQRT_V25, P_t_C_20_25, P_t_C_22_25, mean25, std25, meanV25,...
    stdV25, meanLib25, stdLib25, meanC2025, stdC2025, meanC2225, stdC2225] = Envelopes_Plot(Est25, fine, fineCss);

x         = (1:5);
y52       = [mean21; mean22; mean23; mean24; mean25];
ypos52    = [std21; std22; std23; std24; std25];
yneg52    = [std21; std22; std23; std24; std25];
yV52      = [meanV21; meanV22; meanV23; meanV24; meanV25];
yposV52   = [stdV21; stdV22; stdV23; stdV24; stdV25];
ynegV52   = [stdV21; stdV22; stdV23; stdV24; stdV25];

yC2052    = [meanC2021; meanC2022; meanC2023; meanC2024; meanC2025];
yposC2052 = [stdC2021; stdC2022; stdC2023; stdC2024; stdC2025];
ynegC2052 = [stdC2021; stdC2022; stdC2023; stdC2024; stdC2025];
yC2252    = [meanC2221; meanC2222; meanC2223; meanC2224; meanC2225];
yposC2252 = [stdC2221; stdC2222; stdC2223; stdC2224; stdC2225];
ynegC2252 = [stdC2221; stdC2222; stdC2223; stdC2224; stdC2225];

yLib52    = [meanLib21; meanLib22; meanLib23; meanLib24; meanLib25].*180/pi;
yposLib52 = [stdLib21; stdLib22; stdLib23; stdLib24; stdLib25].*180/pi;
ynegLib52 = [stdLib21; stdLib22; stdLib23; stdLib24; stdLib25].*180/pi;

%% Confronto altezze con PN 1e-10 alpha 5e-3

load('QSOH_PN10_alpha53.mat');
Est26    = Est;
load('QSOM_PN10_alpha53.mat');
Est27    = Est;
load('QSOLa_PN10_alpha53.mat');
Est28    = Est;
load('QSOLb_PN10_alpha53.mat');
Est29    = Est;
load('QSOLc_PN10_alpha53.mat');
Est30   = Est;


% Con Process noice

[SQRT_X26, SQRT_V26, P_t_C_20_26, P_t_C_22_26, mean26, std26, meanV26, stdV26,...
    meanLib26, stdLib26, meanC2026, stdC2026, meanC2226, stdC2226] = Envelopes_Plot(Est26, fine, fineCss);
[SQRT_X27, SQRT_V27, P_t_C_20_27, P_t_C_22_27, mean27, std27, meanV27, stdV27,...
    meanLib27, stdLib27, meanC2027, stdC2027, meanC2227, stdC2227] = Envelopes_Plot(Est27, fine, fineCss);
[SQRT_X28, SQRT_V28, P_t_C_20_28, P_t_C_22_28, mean28, std28, meanV28, stdV28,...
    meanLib28, stdLib28, meanC2028, stdC2028, meanC2228, stdC2228] = Envelopes_Plot(Est28, fine, fineCss);
[SQRT_X29, SQRT_V29, P_t_C_20_29, P_t_C_22_29, mean29, std29, meanV29, stdV29,...
    meanLib29, stdLib29, meanC2029, stdC2029, meanC2229, stdC2229] = Envelopes_Plot(Est29, fine, fineCss);
[SQRT_X30, SQRT_V30, P_t_C_20_30, P_t_C_22_30, mean30, std30, meanV30,...
    stdV30, meanLib30, stdLib30, meanC2030, stdC2030, meanC2230, stdC2230] = Envelopes_Plot(Est30, fine, fineCss);

y53       = [mean26; mean27; mean28; mean29; mean30];
ypos53    = [std26; std27; std28; std29; std30];
yneg53    = [std26; std27; std28; std29; std30];
yV53      = [meanV26; meanV27; meanV28; meanV29; meanV30];
yposV53   = [stdV26; stdV27; stdV28; stdV29; stdV30];
ynegV53   = [stdV26; stdV27; stdV28; stdV29; stdV30];

yC2053    = [meanC2026; meanC2027; meanC2028; meanC2029; meanC2030];
yposC2053 = [stdC2026; stdC2027; stdC2028; stdC2029; stdC2030];
ynegC2053 = [stdC2026; stdC2027; stdC2028; stdC2029; stdC2030];
yC2253    = [meanC2226; meanC2227; meanC2228; meanC2229; meanC2230];
yposC2253 = [stdC2226; stdC2227; stdC2228; stdC2229; stdC2230];
ynegC2253 = [stdC2226; stdC2227; stdC2228; stdC2229; stdC2230];

yLib53    = [meanLib26; meanLib27; meanLib28; meanLib29; meanLib30].*180/pi;
yposLib53 = [stdLib26; stdLib27; stdLib28; stdLib29; stdLib30].*180/pi;
ynegLib53 = [stdLib26; stdLib27; stdLib28; stdLib29; stdLib30].*180/pi;

%% Plots

PlotComparison(Est1,Est2,Est3,Est4,Est5,fine,fineCss);
% PlotComparison(Est11,Est12,Est13,Est14,Est15,fine,fineCss);

figure()
subplot(1,2,1)
errorbar(x,y1,yneg1,ypos1,'-s','LineWidth',1.2,'MarkerSize',10)
hold on
grid on
errorbar(x,y51,yneg51,ypos51,'-s','LineWidth',1.2,'MarkerSize',10)
errorbar(x,y11,yneg11,ypos11,'-s','LineWidth',1.2,'MarkerSize',10)
errorbar(x,y52,yneg52,ypos52,'-s','LineWidth',1.2,'MarkerSize',10)
errorbar(x,y12,yneg12,ypos12,'-s','LineWidth',1.2,'MarkerSize',10)
errorbar(x,y53,yneg53,ypos53,'-s','LineWidth',1.2,'MarkerSize',10)
set(gca,'YScale','log')
set(gca,'TickLabelInterpreter','latex')
set(gca,'XTick',(1:5));
set(gca,'XTickLabel',labels);
set(gca,'XTickLabelRotation',45)
legend('alpha 1','alpha 5e-1','alpha 1e-1','alpha 5e-2','alpha 1e-2','alpha 5e-3')
ylabel('$[km]$')
xlim([0,6])
grid on
subplot(1,2,2)
errorbar(x,yV1,ynegV1,yposV1,'-s','LineWidth',1.2,'MarkerSize',10)
grid on
hold on
errorbar(x,yV51,ynegV51,yposV51,'-s','LineWidth',1.2,'MarkerSize',10)
errorbar(x,yV11,ynegV11,yposV11,'-s','LineWidth',1.2,'MarkerSize',10)
errorbar(x,yV52,ynegV52,yposV52,'-s','LineWidth',1.2,'MarkerSize',10)
errorbar(x,yV12,ynegV12,yposV12,'-s','LineWidth',1.2,'MarkerSize',10)
errorbar(x,yV53,ynegV53,yposV53,'-s','LineWidth',1.2,'MarkerSize',10)
set(gca,'YScale','log')
set(gca,'TickLabelInterpreter','latex')
set(gca,'XTick',(1:5));
set(gca,'XTickLabel',labels);
set(gca,'XTickLabelRotation',45)
legend('alpha 1','alpha 5e-1','alpha 1e-1','alpha 5e-2','alpha 1e-2','alpha 5e-3')
ylabel('$[km/s]$')
xlim([0,6])


figure()
subplot(1,2,1)
errorbar(x,yC201,ynegC201,yposC201,'-s','LineWidth',1.2,'MarkerSize',5)
hold on
errorbar(x,yC2051,ynegC2051,yposC2051,'-s',...
    'LineWidth',1.2,'MarkerSize',5)
errorbar(x,yC2011,ynegC2011,yposC2011,'-s',...
    'LineWidth',1.2,'MarkerSize',5)
errorbar(x,yC2052,ynegC2052,yposC2052,'-s',...
    'LineWidth',1.2,'MarkerSize',5)
errorbar(x,yC2012,ynegC2012,yposC2012,'-s',...
    'LineWidth',1.2,'MarkerSize',5)
errorbar(x,yC2053,ynegC2053,yposC2053,'-s',...
    'LineWidth',1.2,'MarkerSize',5)
set(gca,'YScale','log')
set(gca,'TickLabelInterpreter','latex')
set(gca,'XTick',(1:5));
set(gca,'XTickLabel',labels,'Fontsize',26);
set(gca,'XTickLabelRotation',45)
legend('alpha 1','alpha 5e-1','alpha 1e-1','alpha 5e-2','alpha 1e-2','alpha 5e-3')
ylabel('$3\sigma_{C_{20}}$','Fontsize',30)
xlim([0,6])
grid on
subplot(1,2,2)
errorbar(x,yC221,ynegC221,yposC221,'-s','LineWidth',1.2,'MarkerSize',5)
hold on
errorbar(x,yC2251,ynegC2251,yposC2251,'-s',...
    'LineWidth',1.2,'MarkerSize',5)
errorbar(x,yC2211,ynegC2211,yposC2211,'-s',...
    'LineWidth',1.2,'MarkerSize',5)
errorbar(x,yC2252,ynegC2252,yposC2252,'-s',...
    'LineWidth',1.2,'MarkerSize',5)
errorbar(x,yC2212,ynegC2212,yposC2212,'-s',...
    'LineWidth',1.2,'MarkerSize',5)
errorbar(x,yC2253,ynegC2253,yposC2253,'-s',...
    'LineWidth',1.2,'MarkerSize',5)
grid on
set(gca,'YScale','log')
set(gca,'TickLabelInterpreter','latex')
set(gca,'XTick',(1:5));
set(gca,'XTickLabel',labels,'Fontsize',26);
set(gca,'XTickLabelRotation',45)
legend('alpha 1','alpha 5e-1','alpha 1e-1','alpha 5e-2','alpha 1e-2','alpha 5e-3')
ylabel('$3\sigma_{C_{22}}$','Fontsize',30)
xlim([0,6])



figure()
errorbar(xLib,yLib1,ynegLib1,yposLib1,'-s','LineWidth',1.2,'MarkerSize',10)
hold on
grid on
errorbar(xLib,yLib51,ynegLib51,yposLib51,'-s','LineWidth',1.2,'MarkerSize',10)
errorbar(xLib,yLib11,ynegLib11,yposLib11,'-s','LineWidth',1.2,'MarkerSize',10)
errorbar(xLib,yLib52,ynegLib52,yposLib52,'-s','LineWidth',1.2,'MarkerSize',10)
errorbar(xLib,yLib12,ynegLib12,yposLib12,'-s','LineWidth',1.2,'MarkerSize',10)
errorbar(xLib,yLib53,ynegLib53,yposLib53,'-s','LineWidth',1.2,'MarkerSize',10)
set(gca,'YScale','log')
set(gca,'TickLabelInterpreter','latex')
set(gca,'XTick',(1:4));
set(gca,'XTickLabel',labels);
set(gca,'XTickLabelRotation',45)
ylabel('$[deg]$')
legend('alpha 1','alpha 5e-1','alpha 1e-1','alpha 5e-2','alpha 1e-2','alpha 5e-3')
xlim([0,6])
grid on







%% Confronto altezze con PN 1e-10 alpha 1e-1

load('QSOH_PN10_alpha11_NOLidar.mat');
Est1    = Est;
load('QSOM_PN10_alpha11_NOLidar.mat');
Est2    = Est;
load('QSOLa_PN10_alpha11_NOLidar.mat');
Est3    = Est;
load('QSOLb_PN10_alpha11_NOLidar.mat');
Est4    = Est;
load('QSOLc_PN10_alpha11_NOLidar.mat');
Est5   = Est;

labels  = {'$QSO-H$','$QSO-M$','$QSO-La$','$QSO-Lb$','$QSO-Lc$'};
t_obs   = Est1.t(1,:);
coeff   = 0.8;
fine    = round(coeff*size(t_obs,2));
fineCss = round(coeff*size(t_obs,2));

% Con Process noice

[SQRT_X1, SQRT_V1, P_t_C_20_1, P_t_C_22_1, mean1, std1, meanV1, stdV1,...
    meanLib1, stdLib1, meanC201, stdC201, meanC221, stdC221] = Envelopes_Plot(Est1, fine, fineCss);
[SQRT_X2, SQRT_V2, P_t_C_20_2, P_t_C_22_2, mean2, std2, meanV2, stdV2,...
    meanLib2, stdLib2, meanC202, stdC202, meanC222, stdC222] = Envelopes_Plot(Est2, fine, fineCss);
[SQRT_X3, SQRT_V3, P_t_C_20_3, P_t_C_22_3, mean3, std3, meanV3, stdV3,...
    meanLib3, stdLib3, meanC203, stdC203, meanC223, stdC223] = Envelopes_Plot(Est3, fine, fineCss);
[SQRT_X4, SQRT_V4, P_t_C_20_4, P_t_C_22_4, mean4, std4, meanV4, stdV4,...
    meanLib4, stdLib4, meanC204, stdC204, meanC224, stdC224] = Envelopes_Plot(Est4, fine, fineCss);
[SQRT_X5, SQRT_V5, P_t_C_20_5, P_t_C_22_5, mean5, std5, meanV5,...
    stdV5, meanLib5, stdLib5, meanC205, stdC205, meanC225, stdC225] = Envelopes_Plot(Est5, fine, fineCss);

x        = (1:5);
y1       = [mean1; mean2; mean3; mean4; mean5];
ypos1    = [std1; std2; std3; std4; std5];
yneg1    = [std1; std2; std3; std4; std5];
yV1      = [meanV1; meanV2; meanV3; meanV4; meanV5];
yposV1   = [stdV1; stdV2; stdV3; stdV4; stdV5];
ynegV1   = [stdV1; stdV2; stdV3; stdV4; stdV5];

xC20     = (1:5);
yC201    = [meanC201; meanC202; meanC203; meanC204; meanC205];
yposC201 = [stdC201; stdC202; stdC203; stdC204; stdC205];
ynegC201 = [stdC201; stdC202; stdC203; stdC204; stdC205];
yC221    = [meanC221; meanC222; meanC223; meanC224; meanC225];
yposC221 = [stdC221; stdC222; stdC223; stdC224; stdC225];
ynegC221 = [stdC221; stdC222; stdC223; stdC224; stdC225];

xLib     = (1:5);
yLib1    = [meanLib1; meanLib2; meanLib3; meanLib4; meanLib5].*180/pi;
yposLib1 = [stdLib1; stdLib2; stdLib3; stdLib4; stdLib5].*180/pi;
ynegLib1 = [stdLib1; stdLib2; stdLib3; stdLib4; stdLib5].*180/pi;


%% Confronto altezze con PN 1e-10 alpha 1e-1

load('QSOH_PN10_alpha52_NOLidar.mat');
Est6    = Est;
load('QSOM_PN10_alpha52_NOLidar.mat');
Est7    = Est;
load('QSOLa_PN10_alpha52_NOLidar.mat');
Est8    = Est;
load('QSOLb_PN10_alpha52_NOLidar.mat');
Est9    = Est;
load('QSOLc_PN10_alpha52_NOLidar.mat');
Est10   = Est;


% Con Process noice

[SQRT_X6, SQRT_V6, P_t_C_20_6, P_t_C_22_6, mean6, std6, meanV6, stdV6,...
    meanLib6, stdLib6, meanC206, stdC206, meanC226, stdC226] = Envelopes_Plot(Est6, fine, fineCss);
[SQRT_X7, SQRT_V7, P_t_C_20_7, P_t_C_22_7, mean7, std7, meanV7, stdV7,...
    meanLib7, stdLib7, meanC207, stdC207, meanC227, stdC227] = Envelopes_Plot(Est7, fine, fineCss);
[SQRT_X8, SQRT_V8, P_t_C_20_8, P_t_C_22_8, mean8, std8, meanV8, stdV8,...
    meanLib8, stdLib8, meanC208, stdC208, meanC228, stdC228] = Envelopes_Plot(Est8, fine, fineCss);
[SQRT_X9, SQRT_V9, P_t_C_20_9, P_t_C_22_9, mean9, std9, meanV9, stdV9,...
    meanLib9, stdLib9, meanC209, stdC209, meanC229, stdC229] = Envelopes_Plot(Est9, fine, fineCss);
[SQRT_X10, SQRT_V10, P_t_C_20_10, P_t_C_22_10, mean10, std10, meanV10,...
    stdV10, meanLib10, stdLib10, meanC2010, stdC2010, meanC2210, stdC2210] = Envelopes_Plot(Est10, fine, fineCss);

y11       = [mean6; mean7; mean8; mean9; mean10];
ypos11    = [std6; std7; std8; std9; std10];
yneg11    = [std6; std7; std8; std9; std10];
yV11      = [meanV6; meanV7; meanV8; meanV9; meanV10];
yposV11   = [stdV6; stdV7; stdV8; stdV9; stdV10];
ynegV11   = [stdV6; stdV7; stdV8; stdV9; stdV10];

yC2011    = [meanC206; meanC207; meanC208; meanC209; meanC2010];
yposC2011 = [stdC206; stdC207; stdC208; stdC209; stdC2010];
ynegC2011 = [stdC206; stdC207; stdC208; stdC209; stdC2010];
yC2211    = [meanC226; meanC227; meanC228; meanC229; meanC2210];
yposC2211 = [stdC226; stdC227; stdC228; stdC229; stdC2210];
ynegC2211 = [stdC226; stdC227; stdC228; stdC229; stdC2210];

yLib11    = [meanLib6; meanLib7; meanLib8; meanLib9; meanLib10].*180/pi;
yposLib11 = [stdLib6; stdLib7; stdLib8; stdLib9; stdLib10].*180/pi;
ynegLib11 = [stdLib6; stdLib7; stdLib8; stdLib9; stdLib10].*180/pi;


%% Confronto altezze con PN 1e-10 alpha 1e-2

load('QSOH_PN10_alpha12_NOLidar.mat');
Est11    = Est;
load('QSOM_PN10_alpha12_NOLidar.mat');
Est12    = Est;
load('QSOLa_PN10_alpha12_NOLidar.mat');
Est13    = Est;
load('QSOLb_PN10_alpha12_NOLidar.mat');
Est14    = Est;
load('QSOLc_PN10_alpha12_NOLidar.mat');
Est15   = Est;


% Con Process noice

[SQRT_X11, SQRT_V11, P_t_C_20_11, P_t_C_22_11, mean11, std11, meanV11, stdV11,...
    meanLib11, stdLib11, meanC2011, stdC2011, meanC2211, stdC2211] = Envelopes_Plot(Est11, fine, fineCss);
[SQRT_X12, SQRT_V12, P_t_C_20_12, P_t_C_22_12, mean12, std12, meanV12, stdV12,...
    meanLib12, stdLib12, meanC2012, stdC2012, meanC2212, stdC2212] = Envelopes_Plot(Est12, fine, fineCss);
[SQRT_X13, SQRT_V13, P_t_C_20_13, P_t_C_22_13, mean13, std13, meanV13, stdV13,...
    meanLib13, stdLib13, meanC2013, stdC2013, meanC2213, stdC2213] = Envelopes_Plot(Est13, fine, fineCss);
[SQRT_X14, SQRT_V14, P_t_C_20_14, P_t_C_22_14, mean14, std14, meanV14, stdV14,...
    meanLib14, stdLib14, meanC2014, stdC2014, meanC2214, stdC2214] = Envelopes_Plot(Est14, fine, fineCss);
[SQRT_X15, SQRT_V15, P_t_C_20_15, P_t_C_22_15, mean15, std15, meanV15,...
    stdV15, meanLib15, stdLib15, meanC2015, stdC2015, meanC2215, stdC2215] = Envelopes_Plot(Est15, fine, fineCss);

x         = (1:5);
y12       = [mean11; mean12; mean13; mean14; mean15];
ypos12    = [std11; std12; std13; std14; std15];
yneg12    = [std11; std12; std13; std14; std15];
yV12      = [meanV11; meanV12; meanV13; meanV14; meanV15];
yposV12   = [stdV11; stdV12; stdV13; stdV14; stdV15];
ynegV12   = [stdV11; stdV12; stdV13; stdV14; stdV15];

yC2012    = [meanC2011; meanC2012; meanC2013; meanC2014; meanC2015];
yposC2012 = [stdC2011; stdC2012; stdC2013; stdC2014; stdC2015];
ynegC2012 = [stdC2011; stdC2012; stdC2013; stdC2014; stdC2015];
yC2212    = [meanC2211; meanC2212; meanC2213; meanC2214; meanC2215];
yposC2212 = [stdC2211; stdC2212; stdC2213; stdC2214; stdC2215];
ynegC2212 = [stdC2211; stdC2212; stdC2213; stdC2214; stdC2215];

yLib12    = [meanLib11; meanLib12; meanLib13; meanLib14; meanLib15].*180/pi;
yposLib12 = [stdLib11; stdLib12; stdLib13; stdLib14; stdLib15].*180/pi;
ynegLib12 = [stdLib11; stdLib12; stdLib13; stdLib14; stdLib15].*180/pi;

%% Confronto altezze con PN 1e-10 alpha 5e-1

load('QSOH_PN10_alpha53_NOLidar.mat');
Est16    = Est;
load('QSOM_PN10_alpha53_NOLidar.mat');
Est17    = Est;
load('QSOLa_PN10_alpha53_NOLidar.mat');
Est18    = Est;
load('QSOLb_PN10_alpha53_NOLidar.mat');
Est19    = Est;
load('QSOLc_PN10_alpha53_NOLidar.mat');
Est20   = Est;


% Con Process noice

[SQRT_X16, SQRT_V16, P_t_C_20_16, P_t_C_22_16, mean16, std16, meanV16, stdV16,...
    meanLib16, stdLib16, meanC2016, stdC2016, meanC2216, stdC2216] = Envelopes_Plot(Est16, fine, fineCss);
[SQRT_X17, SQRT_V17, P_t_C_20_17, P_t_C_22_17, mean17, std17, meanV17, stdV17,...
    meanLib17, stdLib17, meanC2017, stdC2017, meanC2217, stdC2217] = Envelopes_Plot(Est17, fine, fineCss);
[SQRT_X18, SQRT_V18, P_t_C_20_18, P_t_C_22_18, mean18, std18, meanV18, stdV18,...
    meanLib18, stdLib18, meanC2018, stdC2018, meanC2218, stdC2218] = Envelopes_Plot(Est18, fine, fineCss);
[SQRT_X19, SQRT_V19, P_t_C_20_19, P_t_C_22_19, mean19, std19, meanV19, stdV19,...
    meanLib19, stdLib19, meanC2019, stdC2019, meanC2219, stdC2219] = Envelopes_Plot(Est19, fine, fineCss);
[SQRT_X20, SQRT_V20, P_t_C_20_20, P_t_C_22_20, mean20, std20, meanV20,...
    stdV20, meanLib20, stdLib20, meanC2020, stdC2020, meanC2220, stdC2220] = Envelopes_Plot(Est20, fine, fineCss);

y51       = [mean16; mean17; mean18; mean19; mean20];
ypos51    = [std16; std17; std18; std19; std20];
yneg51    = [std16; std17; std18; std19; std20];
yV51      = [meanV16; meanV17; meanV18; meanV19; meanV20];
yposV51   = [stdV16; stdV17; stdV18; stdV19; stdV20];
ynegV51   = [stdV16; stdV17; stdV18; stdV19; stdV20];

yC2051    = [meanC2016; meanC2017; meanC2018; meanC2019; meanC2020];
yposC2051 = [stdC2016; stdC2017; stdC2018; stdC2019; stdC2020];
ynegC2051 = [stdC2016; stdC2017; stdC2018; stdC2019; stdC2020];
yC2251    = [meanC2216; meanC2217; meanC2218; meanC2219; meanC2220];
yposC2251 = [stdC2216; stdC2217; stdC2218; stdC2219; stdC2220];
ynegC2251 = [stdC2216; stdC2217; stdC2218; stdC2219; stdC2220];

yLib51    = [meanLib16; meanLib17; meanLib18; meanLib19; meanLib20].*180/pi;
yposLib51 = [stdLib16; stdLib17; stdLib18; stdLib19; stdLib20].*180/pi;
ynegLib51 = [stdLib16; stdLib17; stdLib18; stdLib19; stdLib20].*180/pi;


%% Plots

% PlotComparison(Est1,Est2,Est3,Est4,Est5,fine,fineCss);
% PlotComparison(Est11,Est12,Est13,Est14,Est15,fine,fineCss);

figure()
subplot(1,2,1)
errorbar(x,y1,yneg1,ypos1,'-s','LineWidth',1.2,'MarkerSize',10)
hold on
grid on
errorbar(x,y11,yneg11,ypos11,'-s','LineWidth',1.2,'MarkerSize',10)
errorbar(x,y12,yneg12,ypos12,'-s','LineWidth',1.2,'MarkerSize',10)
errorbar(x,y51,yneg51,ypos51,'-s','LineWidth',1.2,'MarkerSize',10)
set(gca,'YScale','log')
set(gca,'TickLabelInterpreter','latex')
set(gca,'XTick',(1:5));
set(gca,'XTickLabel',labels);
set(gca,'XTickLabelRotation',45)
legend('alpha 1e-1','alpha 5e-2','alpha 1e-2','alpha 5e-3')
ylabel('$[km]$')
xlim([0,6])
grid on
subplot(1,2,2)
errorbar(x,yV1,ynegV1,yposV1,'-s','LineWidth',1.2,'MarkerSize',10)
grid on
hold on
errorbar(x,yV11,ynegV11,yposV11,'-s','LineWidth',1.2,'MarkerSize',10)
errorbar(x,yV12,ynegV12,yposV12,'-s','LineWidth',1.2,'MarkerSize',10)
errorbar(x,yV51,ynegV51,yposV51,'-s','LineWidth',1.2,'MarkerSize',10)
set(gca,'YScale','log')
set(gca,'TickLabelInterpreter','latex')
set(gca,'XTick',(1:5));
set(gca,'XTickLabel',labels);
set(gca,'XTickLabelRotation',45)
legend('alpha 1e-1','alpha 5e-2','alpha 1e-2','alpha 5e-3')
ylabel('$[km/s]$')
xlim([0,6])


figure()
subplot(1,2,1)
errorbar(x,yC201,ynegC201,yposC201,'-s','LineWidth',1.2,'MarkerSize',5)
hold on
errorbar(x,yC2011,ynegC2011,yposC2011,'-s',...
    'LineWidth',1.2,'MarkerSize',5)
errorbar(x,yC2012,ynegC2012,yposC2012,'-s',...
    'LineWidth',1.2,'MarkerSize',5)
errorbar(x,yC2051,ynegC2051,yposC2051,'-s',...
    'LineWidth',1.2,'MarkerSize',5)
set(gca,'YScale','log')
set(gca,'TickLabelInterpreter','latex')
set(gca,'XTick',(1:5));
set(gca,'XTickLabel',labels,'Fontsize',26);
set(gca,'XTickLabelRotation',45)
legend('alpha 1e-1','alpha 5e-2','alpha 1e-2','alpha 5e-3')
ylabel('$3\sigma_{C_{20}}$','Fontsize',30)
xlim([0,6])
grid on
subplot(1,2,2)
errorbar(x,yC221,ynegC221,yposC221,'-s','LineWidth',1.2,'MarkerSize',5)
hold on
errorbar(x,yC2211,ynegC2211,yposC2211,'-s',...
    'LineWidth',1.2,'MarkerSize',5)
errorbar(x,yC2212,ynegC2212,yposC2212,'-s',...
    'LineWidth',1.2,'MarkerSize',5)
errorbar(x,yC2251,ynegC2251,yposC2251,'-s',...
    'LineWidth',1.2,'MarkerSize',5)
grid on
set(gca,'YScale','log')
set(gca,'TickLabelInterpreter','latex')
set(gca,'XTick',(1:5));
set(gca,'XTickLabel',labels,'Fontsize',26);
set(gca,'XTickLabelRotation',45)
legend('alpha 1e-1','alpha 5e-2','alpha 1e-2','alpha 5e-3')
ylabel('$3\sigma_{C_{22}}$','Fontsize',30)
xlim([0,6])



figure()
errorbar(xLib,yLib1,ynegLib1,yposLib1,'-s','LineWidth',1.2,'MarkerSize',10)
hold on
grid on
errorbar(xLib,yLib11,ynegLib11,yposLib11,'-s','LineWidth',1.2,'MarkerSize',10)
errorbar(xLib,yLib12,ynegLib12,yposLib12,'-s','LineWidth',1.2,'MarkerSize',10)
errorbar(xLib,yLib51,ynegLib51,yposLib51,'-s','LineWidth',1.2,'MarkerSize',10)
set(gca,'YScale','log')
set(gca,'TickLabelInterpreter','latex')
set(gca,'XTick',(1:4));
set(gca,'XTickLabel',labels);
set(gca,'XTickLabelRotation',45)
ylabel('$[deg]$')
legend('alpha 1e-1','alpha 5e-2','alpha 1e-2','alpha 5e-3')
xlim([0,6])
grid on
