clear
close all
clc

addpath('./Results/');
addpath('./ResultsHPC/');
addpath('./ObservationsHPC/');
addpath('../useful_functions/');
addpath(genpath('../mice/'));
addpath(genpath('../generic_kernels/'));
addpath('MMX_Fcn_CovarianceAnalyses/');
set(0,'DefaultTextInterpreter','latex');

%% Confronto fra diverse Lidar frequencies

load('QSOM_PN10_alpha1_16_Lidar20Min.mat');
Est20Min    = Est;
load('QSOM_PN10_alpha1_16_Lidar10Min.mat');
Est10Min     = Est;
load('QSOM_PN10_alpha1_16_Lidar5Min.mat');
Est5Min     = Est;
load('QSOM_PN10_alpha1_16.mat');
Est2Min     = Est;
load('QSOM_PN10_alpha1_16_Lidar1Min.mat');
Est1Min    = Est;
    

[SQRT_X99, SQRT_V99, P_t_C_20_99, P_t_C_22_99, mean99, std99, meanV99, stdV99,...
    meanLib99, stdLib99, meanC2099, stdC2099, meanC2299, stdC2299] = Envelopes_Plot(Est20Min, 500, 500);
[SQRT_X98, SQRT_V98, P_t_C_20_98, P_t_C_22_98, mean98, std98, meanV98, stdV98,...
    meanLib98, stdLib98, meanC2098, stdC2098, meanC2298, stdC2298] = Envelopes_Plot(Est10Min, 500, 500);
[SQRT_X97, SQRT_V97, P_t_C_20_97, P_t_C_22_97, mean97, std97, meanV97, stdV97,...
    meanLib97, stdLib97, meanC2097, stdC2097, meanC2297, stdC2297] = Envelopes_Plot(Est5Min, 1000, 1000);
[SQRT_X96, SQRT_V96, P_t_C_20_96, P_t_C_22_96, mean96, std96, meanV96, stdV96,...
    meanLib96, stdLib96, meanC2096, stdC2096, meanC2296, stdC2296] = Envelopes_Plot(Est2Min, 4000, 4000);
[SQRT_X95, SQRT_V95, P_t_C_20_95, P_t_C_22_95, mean95, std95, meanV95,...
    stdV95, meanLib95, stdLib95, meanC2095, stdC2095, meanC2295, stdC2295] = Envelopes_Plot(Est1Min, 8000, 8000);

xFreq     = (1:5);
yFreq     = [mean99; mean98; mean97; mean96; mean95];
yposFreq    = [std99; std98; std97; std96; std95];
ynegFreq    = [std99; std98; std97; std96; std95];
yVFreq      = [meanV99; meanV98; meanV97; meanV96; meanV95];
yposVFreq   = [stdV99; stdV98; stdV97; stdV96; stdV95];
ynegVFreq   = [stdV99; stdV98; stdV97; stdV96; stdV95];

labelsFreq  = {'$20 Min$','$10 Min$','$5 Min$','$2 Min$','$1 Min$'};


figure()
subplot(1,2,1)
errorbar(xFreq,yFreq,ynegFreq,yposFreq,'-s','LineWidth',1.2,'MarkerSize',5)
set(gca,'YScale','log')
set(gca,'TickLabelInterpreter','latex')
set(gca,'XTick',(1:5));
set(gca,'XTickLabel',labelsFreq,'Fontsize',26);
set(gca,'XTickLabelRotation',45)
ylabel('$[km]$','Fontsize',26)
xlim([0,6])
grid on
subplot(1,2,2)
errorbar(xFreq,yVFreq,ynegVFreq,yposVFreq,'-s',...
    'LineWidth',1.2,'MarkerSize',5)
grid on
set(gca,'YScale','log')
set(gca,'TickLabelInterpreter','latex')
set(gca,'XTick',(1:5));
set(gca,'XTickLabel',labelsFreq,'Fontsize',26);
set(gca,'XTickLabelRotation',45)
ylabel('$[km/s]$','Fontsize',26)
xlim([0,6])


%% Confronto fra diverse Range rate frequencies

load('QSOM_PN10_alpha1_16_Rate10Min.mat');
Est10Min_rate    = Est;
load('QSOM_PN10_alpha1_16_Rate5Min.mat');
Est5Min_rate     = Est;
load('QSOM_PN10_alpha81_16_Rate2Min.mat');
Est2Min_rate     = Est;
load('QSOM_PN10_alpha1_16_Rate1Min.mat');
Est1Min_rate     = Est;
    
t_obs   = Est10Min_rate.t(1,:);
coeff   = 0.8;
fine    = round(coeff*size(t_obs,2));
fineCss = round(coeff*size(t_obs,2));

[SQRT_X98, SQRT_V98, P_t_C_20_98, P_t_C_22_98, mean98, std98, meanV98, stdV98,...
    meanLib98, stdLib98, meanC2098, stdC2098, meanC2298, stdC2298] = Envelopes_Plot(Est10Min_rate, fine, fineCss);
[SQRT_X97, SQRT_V97, P_t_C_20_97, P_t_C_22_97, mean97, std97, meanV97, stdV97,...
    meanLib97, stdLib97, meanC2097, stdC2097, meanC2297, stdC2297] = Envelopes_Plot(Est5Min_rate, fine, fineCss);
[SQRT_X96, SQRT_V96, P_t_C_20_96, P_t_C_22_96, mean96, std96, meanV96, stdV96,...
    meanLib96, stdLib96, meanC2096, stdC2096, meanC2296, stdC2296] = Envelopes_Plot(Est2Min_rate, fine, fineCss);
[SQRT_X95, SQRT_V95, P_t_C_20_95, P_t_C_22_95, mean95, std95, meanV95, stdV95,...
    meanLib95, stdLib95, meanC2095, stdC2095, meanC2295, stdC2295] = Envelopes_Plot(Est1Min_rate, 7000, 7000);

xFreq     = (1:4);
yFreq     = [mean98; mean97; mean96; mean95];
yposFreq    = [std98; std97; std96; std95];
ynegFreq    = [std98; std97; std96; std95];
yVFreq      = [meanV98; meanV97; meanV96; meanV95];
yposVFreq   = [stdV98; stdV97; stdV96; stdV95];
ynegVFreq   = [stdV98; stdV97; stdV96; stdV95];

labelsFreq  = {'$10 Min$','$5 Min$','$2 Min$','$1 Min$'};


figure()
subplot(1,2,1)
errorbar(xFreq,yFreq,ynegFreq,yposFreq,'-s','LineWidth',1.2,'MarkerSize',5)
set(gca,'YScale','log')
set(gca,'TickLabelInterpreter','latex')
set(gca,'XTick',(1:4));
set(gca,'XTickLabel',labelsFreq,'Fontsize',26);
set(gca,'XTickLabelRotation',45)
ylabel('$[km]$','Fontsize',26)
xlim([0,5])
grid on
subplot(1,2,2)
errorbar(xFreq,yVFreq,ynegVFreq,yposVFreq,'-s',...
    'LineWidth',1.2,'MarkerSize',5)
grid on
set(gca,'YScale','log')
set(gca,'TickLabelInterpreter','latex')
set(gca,'XTick',(1:4));
set(gca,'XTickLabel',labelsFreq,'Fontsize',26);
set(gca,'XTickLabelRotation',45)
ylabel('$[km/s]$','Fontsize',26)
xlim([0,5])


%% Confronto fra diverse Range frequencies

load('QSOM_PN10_alpha81_16.mat');
Est1hour_range    = Est;
load('QSOM_PN10_alpha81_16_Range20Min.mat');
Est20Min_range     = Est;
load('QSOM_PN10_alpha81_16_Range10Min.mat');
Est10Min_range     = Est;
load('QSOM_PN10_alpha81_16_Range5Min.mat');
Est5Min_range     = Est;
load('QSOM_PN10_alpha81_16_Range2Min.mat');
Est2Min_range     = Est;
    
t_obs   = Est1hour_range.t(1,:);
coeff   = 0.8;
fine    = round(coeff*size(t_obs,2));
fineCss = round(coeff*size(t_obs,2));

[SQRT_X99, SQRT_V99, P_t_C_20_99, P_t_C_22_99, mean99, std99, meanV99, stdV99,...
    meanLib99, stdLib99, meanC2099, stdC2099, meanC2299, stdC2299] = Envelopes_Plot(Est1hour_range, fine, fineCss);
[SQRT_X98, SQRT_V98, P_t_C_20_98, P_t_C_22_98, mean98, std98, meanV98, stdV98,...
    meanLib98, stdLib98, meanC2098, stdC2098, meanC2298, stdC2298] = Envelopes_Plot(Est20Min_range, fine, fineCss);
[SQRT_X97, SQRT_V97, P_t_C_20_97, P_t_C_22_97, mean97, std97, meanV97, stdV97,...
    meanLib97, stdLib97, meanC2097, stdC2097, meanC2297, stdC2297] = Envelopes_Plot(Est10Min_range, fine, fineCss);
[SQRT_X96, SQRT_V96, P_t_C_20_96, P_t_C_22_96, mean96, std96, meanV96, stdV96,...
    meanLib96, stdLib96, meanC2096, stdC2096, meanC2296, stdC2296] = Envelopes_Plot(Est5Min_range, fine, fineCss);
[SQRT_X95, SQRT_V95, P_t_C_20_95, P_t_C_22_95, mean95, std95, meanV95,...
    stdV95, meanLib95, stdLib95, meanC2095, stdC2095, meanC2295, stdC2295] = Envelopes_Plot(Est2Min_range, fine, fineCss);

xFreq     = (1:5);
yFreq     = [mean99; mean98; mean97; mean96; mean95];
yposFreq    = [std99; std98; std97; std96; std95];
ynegFreq    = [std99; std98; std97; std96; std95];
yVFreq      = [meanV99; meanV98; meanV97; meanV96; meanV95];
yposVFreq   = [stdV99; stdV98; stdV97; stdV96; stdV95];
ynegVFreq   = [stdV99; stdV98; stdV97; stdV96; stdV95];

 PlotComparison(Est1hour_range, Est20Min_range, Est10Min_range, Est5Min_range, Est2Min_range, fine, fineCss);


labelsFreq  = {'$1 hour$','$20 Min$','$10 Min$','$5 Min$','$2 Min$'};


figure()
subplot(1,2,1)
errorbar(xFreq,yFreq,ynegFreq,yposFreq,'-s','LineWidth',1.2,'MarkerSize',5)
set(gca,'YScale','log')
set(gca,'TickLabelInterpreter','latex')
set(gca,'XTick',(1:5));
set(gca,'XTickLabel',labelsFreq,'Fontsize',26);
set(gca,'XTickLabelRotation',45)
ylabel('$[km]$','Fontsize',26)
xlim([0,6])
grid on
subplot(1,2,2)
errorbar(xFreq,yVFreq,ynegVFreq,yposVFreq,'-s',...
    'LineWidth',1.2,'MarkerSize',5)
grid on
set(gca,'YScale','log')
set(gca,'TickLabelInterpreter','latex')
set(gca,'XTick',(1:5));
set(gca,'XTickLabel',labelsFreq,'Fontsize',26);
set(gca,'XTickLabelRotation',45)
ylabel('$[km/s]$','Fontsize',26)
xlim([0,6])


%% Confronto fra diverse Camera frequencies

load('QSOM_PN10_alpha81_16.mat');
Est1hour_camera    = Est;
load('QSOM_PN10_alpha81_16_Camera20Min.mat');
Est20Min_camera     = Est;
load('QSOM_PN10_alpha81_16_Camera10Min.mat');
Est10Min_camera     = Est;
load('QSOM_PN10_alpha81_16_Camera2Min.mat');
Est2Min_camera     = Est;
    
t_obs   = Est1hour_camera.t(1,:);
coeff   = 0.8;
fine    = round(coeff*size(t_obs,2));
fineCss = round(coeff*size(t_obs,2));

[SQRT_X99, SQRT_V99, P_t_C_20_99, P_t_C_22_99, mean99, std99, meanV99, stdV99,...
    meanLib99, stdLib99, meanC2099, stdC2099, meanC2299, stdC2299] = Envelopes_Plot(Est1hour_camera, fine, fineCss);
[SQRT_X98, SQRT_V98, P_t_C_20_98, P_t_C_22_98, mean98, std98, meanV98, stdV98,...
    meanLib98, stdLib98, meanC2098, stdC2098, meanC2298, stdC2298] = Envelopes_Plot(Est20Min_camera, fine, fineCss);
[SQRT_X97, SQRT_V97, P_t_C_20_97, P_t_C_22_97, mean97, std97, meanV97, stdV97,...
    meanLib97, stdLib97, meanC2097, stdC2097, meanC2297, stdC2297] = Envelopes_Plot(Est10Min_camera, fine, fineCss);
[SQRT_X95, SQRT_V95, P_t_C_20_95, P_t_C_22_95, mean95, std95, meanV95,...
    stdV95, meanLib95, stdLib95, meanC2095, stdC2095, meanC2295, stdC2295] = Envelopes_Plot(Est2Min_camera, fine, fineCss);

xFreq     = (1:4);
yFreq     = [mean99; mean98; mean97;mean95];
yposFreq    = [std99; std98; std97; std95];
ynegFreq    = [std99; std98; std97; std95];
yVFreq      = [meanV99; meanV98; meanV97; meanV95];
yposVFreq   = [stdV99; stdV98; stdV97; stdV95];
ynegVFreq   = [stdV99; stdV98; stdV97; stdV95];

% PlotComparison(Est1hour_camera, Est20Min_camera, Est10Min_camera, Est5Min_camera, Est2Min_camera, fine, fineCss);


labelsFreq  = {'$1 hour$','$20 Min$','$10 Min$','$2 Min$'};


figure()
subplot(1,2,1)
errorbar(xFreq,yFreq,ynegFreq,yposFreq,'-s','LineWidth',1.2,'MarkerSize',5)
set(gca,'YScale','log')
set(gca,'TickLabelInterpreter','latex')
set(gca,'XTick',(1:4));
set(gca,'XTickLabel',labelsFreq,'Fontsize',26);
set(gca,'XTickLabelRotation',45)
ylabel('$[km]$','Fontsize',26)
xlim([0,5])
grid on
subplot(1,2,2)
errorbar(xFreq,yVFreq,ynegVFreq,yposVFreq,'-s',...
    'LineWidth',1.2,'MarkerSize',5)
grid on
set(gca,'YScale','log')
set(gca,'TickLabelInterpreter','latex')
set(gca,'XTick',(1:4));
set(gca,'XTickLabel',labelsFreq,'Fontsize',26);
set(gca,'XTickLabelRotation',45)
ylabel('$[km/s]$','Fontsize',26)
xlim([0,5])