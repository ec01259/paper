clear
close all
clc


rmpath(genpath('~/MMX_Fcn_CovarianceAnalyses'));
rmpath(genpath('~/MMX_Fcn_GenerateReferenceTrajectory'));
rmpath(genpath('~/MMX_Fcn_Miscellaneous'));
rmpath(genpath('~/MMX_Product'));



%% Test for the new Obs_Gen function


addpath(genpath('./MMX_Fcn_CovarianceAnalyses'));
addpath(genpath('./MMX_Fcn_GenerateReferenceTrajectory'));
addpath(genpath('./MMX_Fcn_Miscellaneous'));
addpath(genpath('./MMX_Product'));

% % Windows machine
% addpath(genpath('../MICE/mice'));
% cspice_furnsh( {'C:\Program Files\MATLAB\Matlab\WarmUp\NBP-Check\kernels_QSOM.txt'});

% Mac

addpath(genpath('~/Documents/MATLAB/mice'));
addpath(genpath('~/Documents/MATLAB/generic_kernels/'));
GravField = [6,6];
[pars, units]   = MMX_DefineModelParametersAndUnits(GravField);

MMX_InitializeSPICE
addpath(genpath('./MMX_Product/MMX_BSP_Files'));
cspice_furnsh(which('Mars_789739269_790603269.bsp'));
cspice_furnsh(which('Phobos_789739269_790603269.bsp'));
cspice_furnsh(which('MMX_QSO_027_6x6_789739269_790603269.bsp'));


et0   = cspice_str2et('2025-04-25 00:00:00 (UTC)');
et_end= cspice_str2et('2025-05-05 21:05:00 (UTC)');

[pars, units]   = MMX_CovarianceAnalysisParameters(pars, units);

YObs_Full       = Observations_Generation_Paper(et0, et_end, pars, units);

%% Test Smoothing validity

MMX     = cspice_spkezr('-33', et0, 'J2000', 'none', 'MARS');
Phobos  = cspice_spkezr('401', et0, 'J2000', 'none', 'MARS');    
    
Est0.X      = [MMX./units.sfVec; Phobos./units.sfVec; pars.Clm; pars.Slm; pars.bias];
Est0.dx     = zeros(size(Est0.X,1),1);
Est0.P0     = pars.P0;
Est0.t0     = et0/units.tsf;

% Sequential filter to be smoothed
[Est]  = SRIF(Est0,@MP_Circular_all,@FullSet_all,pars.R,YObs_Full,pars,units);

% if Est.flagNO == 0
%     
%     Smooth = Smoothing(Est,pars,units);
%     Est.x_t = Smooth.x_lk_t;
%     Est.P_t = Smooth.P_lk_t;
%         
% end

Cov_ResultsPlot(GravField, Est, YObs_Full, pars);

% Batch filter for comparison

[Est1] = Batch(Est0,@MP_Circular_all,@FullSet_all,pars.R,YObs_Full,1,pars,units);

if Est1.flagNO == 0
    % Forward propagation of the result
    MMX     = cspice_spkezr('-33', et0, 'J2000', 'none', 'MARS');
    Phobos  = cspice_spkezr('401', et0, 'J2000', 'none', 'MARS');    
        
    X0      = [MMX./units.sfVec; Phobos./units.sfVec; pars.Clm; pars.Slm; pars.bias];
    Phi0    = eye(size(X0,1));
    St0     = [X0; reshape(Phi0,[size(X0,1)^2,1])];
    interval_DSN    = pars.interval_DSN;
    interval_lidar  = pars.interval_lidar;
    interval_camera = pars.interval_camera;
    min_interval    = min([interval_DSN; interval_lidar; interval_camera]);
    
    t_obs   = YObs_Full(:,1);
    tspan   = (0:min_interval:t_obs(end))/units.tsf;
    RelTol  = 1e-13;
    AbsTol  = 1e-16; 
    opt     = odeset('RelTol',RelTol,'AbsTol',AbsTol);
    [t,X]   = ode113(@(t,X) MP_Circular_all(t,X,pars,units),tspan,St0,opt);
    
    P_t     = zeros(size(X0,1),size(X0,1),size(tspan,1));
    
    for i = 1:size(tspan,2)
    
        Phi = reshape(X(i,size(X0,1)+1:end),[size(X0,1),size(X0,1)]);
        P_t(:,:,i) = (Phi*(Est1.P0./units.CovDim)*Phi').*units.CovDim;
    
    end
    
    % Plots from the batch
    %   Square-Root of the MMX covariance error
    
        sqx = squeeze(P_t(1,1,1:end-1));
        sqy = squeeze(P_t(2,2,1:end-1));
        sqz = squeeze(P_t(3,3,1:end-1));
        SQRT_X = 3*sqrt(sqx + sqy + sqz);
        sqvx = squeeze(P_t(4,4,1:end-1));
        sqvy = squeeze(P_t(5,5,1:end-1));
        sqvz = squeeze(P_t(6,6,1:end-1));
        SQRT_V = 3*sqrt(sqvx + sqvy + sqvz);
    
        
    %   3sigma Envelopes
    
        t_obs = 0:min_interval:t_obs(end);
    
        figure()
        subplot(1,2,1)
        semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(P_t(1,1,1:end-1)))),'Color','b','LineWidth',1)
        hold on;
        grid on;
        semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_t(2,2,1:end-1)))),'Color','r','LineWidth',1)
        semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_t(3,3,1:end-1)))),'Color','g','LineWidth',1)
        semilogy(t_obs(1:end-1)/3600,SQRT_X,'Color','k','LineWidth',2)
        xlabel('time $[hour]$')
        ylabel('$[km]$')
        title('MMX position vector $3\sigma$ envelopes','Interpreter','latex','FontSize',16)
        legend('$3\sigma_{x}$','$3\sigma_{y}$','$3\sigma_{z}$','$3 RMS$','Interpreter','latex','FontSize',14)
        subplot(1,2,2)
        semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_t(4,4,1:end-1)))),'Color','b','LineWidth',1)
        hold on;
        grid on;
        semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_t(5,5,1:end-1)))),'Color','r','LineWidth',1)
        semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_t(6,6,1:end-1)))),'Color','g','LineWidth',1)
        semilogy(t_obs(1:end-1)/3600,SQRT_V,'Color','k','LineWidth',2)
        xlabel('time $[hour]$')
        ylabel('$[km/s]$')
        title('MMX velocity vector $3\sigma$ envelopes','Interpreter','latex','FontSize',16)
        legend('$3\sigma_{\dot{x}}$','$3\sigma_{\dot{y}}$','$3\sigma_{\dot{z}}$','$3 RMS$','Interpreter','latex','FontSize',14)
    
    
    %   Square-Root of the Phobos covariance error
    
        sqx = squeeze(P_t(7,7,1:end-1));
        sqy = squeeze(P_t(8,8,1:end-1));
        sqz = squeeze(P_t(9,9,1:end-1));
        SQRT_X = 3*sqrt(sqx + sqy + sqz);
        sqvx = squeeze(P_t(10,10,1:end-1));
        sqvy = squeeze(P_t(11,11,1:end-1));
        sqvz = squeeze(P_t(12,12,1:end-1));
        SQRT_V = 3*sqrt(sqvx + sqvy + sqvz);
    
    %   3sigma Envelopes
        figure()
        subplot(1,2,1)
        semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(P_t(7,7,1:end-1)))),'Color','b','LineWidth',1)
        hold on
        grid on
        semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_t(8,8,1:end-1)))),'Color','r','LineWidth',1)
        semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_t(9,9,1:end-1)))),'Color','g','LineWidth',1)
        semilogy(t_obs(1:end-1)/3600,SQRT_X,'Color','k','LineWidth',2)
        xlabel('time $[hour]$')
        ylabel('$[km]$')
        title('Phobos position vector $3\sigma$ envelopes','Interpreter','latex','FontSize',16)
        legend('$3\sigma_{x}$','$3\sigma_{y}$','$3\sigma_{z}$','$3 RMS$','Interpreter','latex','FontSize',14)
        subplot(1,2,2)
        semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_t(10,10,1:end-1)))),'Color','b','LineWidth',1)
        hold on
        grid on
        semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_t(11,11,1:end-1)))),'Color','r','LineWidth',1)
        semilogy(t_obs(1:end-1)/3600,3*squeeze(real(sqrt(P_t(12,12,1:end-1)))),'Color','g','LineWidth',1)
        semilogy(t_obs(1:end-1)/3600,SQRT_V,'Color','k','LineWidth',2)
        xlabel('time $[hour]$')
        ylabel('$[km/s]$')
        title('Phobos velocity vector $3\sigma$ envelopes','Interpreter','latex','FontSize',16)
        legend('$3\sigma_{\dot{x}}$','$3\sigma_{\dot{y}}$','$3\sigma_{\dot{z}}$','$3 RMS$','Interpreter','latex','FontSize',14)
    
    %   Harmonics 3sigma Envelopes
    
        place0 = size(Est1.x,1)-pars.nCoeff-pars.nBias;
        corr_label = {'$x$','$y$','$z$','$\dot{x}$','$\dot{y}$','$\dot{z}$',...
            '$x_{Ph}$','$y_{Ph}$','$z_{Ph}$','$\dot{x}_{Ph}$','$\dot{y}_{Ph}$',...
            '$\dot{z}_{Ph}$'};
    
        figure()
        for i = 0:GravField(1)
            subplot(1,GravField(1)+1,i+1)
            for j = 0:i
                place0 = place0+1;
                semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(P_t(place0,place0,1:end-1)))),'LineWidth',1,...
                    'DisplayName',['$C_{',num2str(i),num2str(j),'}$']);
                grid on;
                hold on;
                corr_label{end+1} = ['$C_{',num2str(i),num2str(j),'}$'];
            end
            xlabel('time $[hour]$')
            legend('Interpreter','latex','FontSize',14)
        end
        
    %   Slm coefficients
        figure()
        for i = 1:GravField(1)
            subplot(1,GravField(1),i)
            for j = 1:i
                place0 = place0+1;
                semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(P_t(place0,place0,1:end-1)))),'LineWidth',1,...
                    'DisplayName',['$S_{',num2str(i),num2str(j),'}$'])
                grid on;
                hold on;
                corr_label{end+1} = ['$S_{',num2str(i),num2str(j),'}$'];
            end
            xlabel('time $[hour]$')
            legend('Interpreter','latex','FontSize',14)
        end
    
    %   Biases
        place0 = size(Est1.x,1)-pars.nBias;
        corr_label(end+1) = {'$\rho_b$'};
        corr_label(end+1) = {'$\dot{\rho}_b$'};
        corr_label(end+1) = {'$LIDAR_b$'};
        corr_label(end+1) = {'$Cam1_b$'};
        corr_label(end+1) = {'$Cam2_b$'};
    
        figure()
        subplot(1,pars.nBias,1)
        semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(P_t(place0+1,place0+1,1:end-1)))),'LineWidth',1,...
                    'DisplayName',('$\rho_b$'),'Color','b');
        grid on;
        hold on;
        xlabel('time $[hour]$')
        ylabel('$[km]$')
        subplot(1,pars.nBias,2)
        semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(P_t(place0+2,place0+2,1:end-1)))),'LineWidth',1,...
                    'DisplayName',('$\dot{\rho}_b$'),'Color','r');
        grid on;
        hold on;
        xlabel('time $[hour]$')
        ylabel('$[km/s]$')
        subplot(1,pars.nBias,3)
        semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(P_t(place0+3,place0+3,1:end-1)))),'LineWidth',1,...
                    'DisplayName',('$LIDAR_b$'),'Color','g');
        grid on;
        hold on;
        xlabel('time $[hour]$')
        ylabel('$[km]$')
    
        subplot(1,pars.nBias,4)
        semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(P_t(place0+4,place0+4,1:end-1)))),'LineWidth',1,...
                    'DisplayName',('$Cam1_b$'),'Color','m');
        grid on;
        hold on;
        xlabel('time $[hour]$')
        ylabel('$[rad]$')
    
        subplot(1,pars.nBias,5)
        semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(P_t(place0+5,place0+5,1:end-1)))),'LineWidth',1,...
                    'DisplayName',('$Cam2_b$'),'Color','k');
        grid on;
        hold on;
        xlabel('time $[hour]$')
        ylabel('$[rad]$')
end