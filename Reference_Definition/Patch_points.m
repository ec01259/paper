function [value,isterminal,direction] = Patch_points(~,X)

%   Integration is blocked if the position vector is perpendicular to the
%   velocity vector
    
    value = dot(X(1:3),X(4:6));
    isterminal = 0;
    direction = 0;

end