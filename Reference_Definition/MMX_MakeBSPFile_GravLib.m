function MMX_MakeBSPFile_GravLib(bspfilename, ID, t, X, refcenter, refframe, Model, mkspk_dir)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% MMX_MakeBSPFile(bspfilename, ID, t, X, refcenter, refframe, Model, mkspk_dir)
%
% Create SPICE bsp kernel
%
% INPUT:
% - bspfilename     name of bsp kernel    
% - ID              body NAIF integer
% - t               vector of time elapsed since epoch (-)
% - X               (N x 6) body state vector in t
% - refcenter       Center of reference frame
% - refframe        Reference frame
% - Model           system parameters
% - mkspk_dir       Path to mkspk executable
%
% OUTPUT:
% - NONE
%
% 
% Copyright 2018-19 EQUULEUS FD Team
% 
% Licensed under the Apache License, Version 2.0 (the "License"); you may
% not use this file except in compliance with the License. You may obtain a
% copy of the License <a
% href="matlab:web('http://www.apache.org/licenses/LICENSE-2.0','-browser')">here</a>.
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
% WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
% License for the specific language governing permissions and limitations
% under the License.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


et0    = Model.epoch;
et     = et0 + t;
Xet    = X';

[et,I] = uniquetol(et, 1e-2, 'DataScale',1);
Xet    = Xet(I, :);

    if(strcmp(refframe, 'ECLIPJ2000') || strcmp(refframe, 'J2000') || strcmp(refframe, 'MARSIAU'))
        work_mkspk([et, Xet], bspfilename, ID, refframe, str2double(refcenter), mkspk_dir);
    else
        error('Stop Here Traveller! Check Reference Frames.');
    end

end