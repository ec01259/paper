function [value,isterminal,direction] = Null_Radial_velocity(t,X,par)

%   Integration is blocked if the position vector is perpendicular to the
%   velocity vector

%     value = (mod(t*par.units.tsf,100)>1);
    value       = dot(X(1:3),X(4:6));
    isterminal  = 0;
    direction   = 0;

end