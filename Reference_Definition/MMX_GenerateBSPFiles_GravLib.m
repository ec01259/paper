function MMX_GenerateBSPFiles_GravLib(t, Xmmx, X401, OrbitType, Amplitudes, PropTime, Model, micePATH)



%% Load SPICE
MMX_InitializeSPICE;
et0 = Model.epoch;
et1 = et0 + PropTime;



%% Create MMX Ephemeris file
if(strcmp(OrbitType, 'QSO'))
    amp = sprintf('%03.0f', Amplitudes(1));
else
    amp = sprintf('%03.0f_%03.0f', Amplitudes(1), Amplitudes(2));
end
dates = sprintf('%09.0f_%09.0f', et0, et1);
SHmodel = sprintf('%dx%d',size(Model.pars.SH.Clm,1)-1,size(Model.pars.SH.Clm,2)-1);
bspfilename = ['MMX_',OrbitType,'_',amp,'_',SHmodel,'_',dates,'.bsp'];

if(exist(bspfilename,'file')~=2)
    ID = -34;
    MMX_MakeBSPFile_GravLib(bspfilename, ID, t, Xmmx, '499', 'MARSIAU', Model, [micePATH, '/exe/']);
    movefile(bspfilename, '../MMX_Product/MMX_BSP_Files_GravLib');
else
    fprintf('MMX ephemeris file already exists!\n');
end


%% Create Phobos Ephemeris file

bspfilename = ['Phobos_',dates,'.bsp'];
if(exist(bspfilename,'file')~=2)
    ID = -401;
    MMX_MakeBSPFile_GravLib(bspfilename, ID, t, X401, '499', 'MARSIAU', Model, [micePATH, '/exe/']);
    movefile(bspfilename, '../MMX_Product/MMX_BSP_Files_GravLib');
else
    fprintf('Phobos ephemeris file already exists!\n');
end
