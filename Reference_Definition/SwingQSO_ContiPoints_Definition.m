clear
close all
clc

%%  Definition of the points to start the continuation for the QSO

    warning('off', 'all'); format longG;
    set(0,'DefaultTextInterpreter','latex');
    set(0,'DefaultAxesFontSize', 16);

%   Mac
    addpath('../MMX_Fcn_CovarianceAnalyses');
    addpath(genpath('../../mice/'));
    addpath(genpath('../../generic_kernels/'));
    addpath(genpath('../../useful_functions/'));
    addpath(genpath('../../dynamical_model/'));
    addpath(genpath('../MMX_Product/MMX_BSP_Files/'));
    addpath(genpath('../MMX_Product/'));
    addpath(genpath('../MMX_Fcn_GenerateReferenceTrajectory/'));
    addpath(genpath('../'));

%%  Patch points

%   Qualche costante utile
    alpha       = 13.03;                          %km
    beta        = 11.4;                           %km
    gamma       = 9.14;                           %km
    Geom.alpha  = alpha;
    Geom.beta   = beta;
    Geom.gamma  = gamma;

%   Qualche parametro e units utile
    [pars, units] = MMX_DefineModelParametersAndUnits([2,2]);

    inp.Model.dynamics  = @CRTBPsh;
    inp.Model.pars      = pars;
    inp.Model.units     = units;
    inp.Model.epoch     = '2026-03-16, 00:00:00 (UTC)';
    inp.OrbitType       = 'Swing-QSO';
    inp.Event           = @Null_Radial_velocity;

%   QSO-H
%     inp.Amplitudes      = [198, 0];
%   QSO-M
%     inp.Amplitudes      = [94, 0];
%   QSO-La
%     inp.Amplitudes      = [49, 0];
%   QSO-Lb
%     inp.Amplitudes      = [31, 0];
%   QSO-Lc
    inp.Amplitudes      = [27, 0];


%   Propagazione e caricamento dei dati della orbita di interesse
    filename    = sprintf('SwingQSOs_0%d_%dx%d.mat',round(inp.Amplitudes(1)),...
        size(inp.Model.pars.SH.Clm,1)-1, size(inp.Model.pars.SH.Clm,2)-1);
    load(filename, 'Xqp', 'Wqp', 'Model');
    
    idx     = 1;
    T       = Wqp(1,idx)*units.tsf;

    
%   Integrazione e identificazione dei patch points
    Ic      = Xqp(1:6,idx);
    Model.pars.T    = T;
    Time    = [0, 4*Model.pars.T/units.tsf];
    AbsTol  = 1e-16;
    RelTol  = 1e-14;
    opt     = odeset('RelTol', RelTol, 'AbsTol', AbsTol, 'event',...
        @Null_Radial_velocity);
    [t,x,te,xe] = ode113(@eom, Time, Ic, opt, Model);
%     t_Patch     = te(2:2:end)*units.tsf;
%     X_patch     = xe(2:2:end,1:3)'.*units.lsf;
%     V_patch     = 2/10*xe(2:2:end,4:6)'.*units.vsf;

    t_Patch     = te*units.tsf;
    X_patch     = xe(:,1:3)'.*units.lsf;
    V_patch     = 5/10*xe(:,4:6)'.*units.vsf;
    

%   Plot orbita e patch points
    h = figure();
    PlotTrajectories(h, x, inp.Model);
    hold on;
    plot3(X_patch(1,:),X_patch(2,:),X_patch(3,:),'ro')
    set(0,'DefaultFigureVisible', 'on');



%%  Phobos states

%   Now, this points must be defined with respect to the new model. So we
%   integrate Phobos with the new model, and add the MMX postition with 
%   respect to this new Phobos

%   kernel with the real position of Phobos
    MMX_InitializeSPICE
    cspice_furnsh(which('MARPHOFRZ.txt'));
    cspice_furnsh(which('mar097.bsp'));

%   Model parameters
    [par, units] = MMX_DefineNewModelParametersAndUnits;

%   Initial state
    data = cspice_str2et('2026-03-16, 00:00:00 (UTC)');
    [Ph, par]   = RealPhobos_States_NewModel(data,par);
    Ph          = Ph./units.sfVec2;
    day         = 24*3600;

%   Initial Phobos's state vector
    St0     = [Ph; reshape(eye(par.d2),[par.d2^2,1])];

%   Integration
    tspanp      = t_Patch*units.tsf; 
    opt         = odeset('RelTol',RelTol,'AbsTol',AbsTol);
    [tp,XPhp]   = ode113(@(t,X) MP_CoupledLibration(t,X,par),tspanp,St0,opt);
    tp          = tp/units.tsf;

%%  Definition of the MMX's postion vector wrt Phobos and of the overall vector useful for the optimization


    X_Ph_Model_orbital_plane    = XPhp(:,1)'.*[cos(XPhp(:,3)'); sin(XPhp(:,3)');...
        zeros(1,size(t_Patch,1))]*units.lsf;
    om_t                        = [zeros(2,size(t_Patch,1)); XPhp(:,4)'*units.tsf];

%   Pre-allocation
    V_Ph_Model_orbital_plane    = zeros(3,size(t_Patch,1));
    X_MMX_patch_orbital_plane   = zeros(3,size(t_Patch,1));
    V_MMX_patch_orbital_plane   = zeros(3,size(t_Patch,1));
    X_MMX_patch_MARSIAU         = zeros(3,size(t_Patch,1));
    V_MMX_patch_MARSIAU         = zeros(3,size(t_Patch,1));

   
    for i = 1:size(t_Patch,1)

        r   = [cos(XPhp(i,3)'); sin(XPhp(i,3)'); 0];
        k   = [0;0;1];
        j   = cross(k,r)/norm(cross(k,r));
        NR  = [r,j,k]';
        RN  = NR';

        RB  = [cos(XPhp(i,7)'), sin(XPhp(i,7)'), 0; ...
            -sin(XPhp(i,7)'), cos(XPhp(i,7)'), 0; ...
            0, 0, 1];
        BR  = RB';

        V_Ph_Model_orbital_plane(:,i)   = RN*[XPhp(i,2)*units.vsf; 0; 0] +...
            cross(om_t(:,i),X_Ph_Model_orbital_plane(:,i));
        X_MMX_patch_orbital_plane(:,i)  = RN*(X_patch(:,i)) +...
            X_Ph_Model_orbital_plane(:,i);
        V_MMX_patch_orbital_plane(:,i)  = RN*(V_patch(:,i)) + RN*[XPhp(i,2)*units.vsf; 0; 0] +...
            cross(om_t(:,i), X_Ph_Model_orbital_plane(:,i));
        X_MMX_patch_MARSIAU(:,i)    = par.perifocal2MARSIAU*(X_MMX_patch_orbital_plane(:,i));
        V_MMX_patch_MARSIAU(:,i)    = par.perifocal2MARSIAU*(V_MMX_patch_orbital_plane(:,i));

    end
        
%     figure()
%     plot3(X_MMX_patch_orbital_plane(1,:),X_MMX_patch_orbital_plane(2,:),X_MMX_patch_orbital_plane(3,:));
%     hold on;
%     grid on;
%     plot3(X_Ph_Model_orbital_plane(1,:),X_Ph_Model_orbital_plane(2,:),X_Ph_Model_orbital_plane(3,:))
%     planetPlot('Mars',[0;0;0],1);
%     xlabel('x [km]');
%     ylabel('y [km]');
%     zlabel('z [km]');
%     legend('MMX','Phobos');
    
    

%%  Setup della optimization per la definizione dell'orbita periodica nel nuovo modello


%   Definition of the initial vector
    X0      = [X_MMX_patch_MARSIAU/units.lsf; V_MMX_patch_MARSIAU/units.vsf;...
                [(t_Patch(2:end)-t_Patch(1:end-1)); (t_Patch(end)- t_Patch(end-1))]'/2*units.tsf;...
                [(t_Patch(2)- t_Patch(1)); (t_Patch(2:end)-t_Patch(1:end-1))]'/2*units.tsf;...
                t_Patch'*units.tsf];
    X_Ph0   = XPhp(:,1:8)';
    par.Geom    = Geom;
    par.X_patch = X_patch;

%%  Propagazione della vecchia initial condition
    
    [~] = PlotInCond(X0,[],[],Geom,data);


%%  Ottimizzazione 

%   Definition of the constraints
%   There are no linear inequality constraints nor linear equality
%   constraints, but only non-linear constraints that are specified in the
%   function @Constraints

    options = optimoptions('fmincon','Display','iter','Algorithm','interior-point',...
        'ConstraintTolerance',1e-14,'FunctionTolerance',1e-14,...
        'MaxFunctionEvaluations',1e4,'EnableFeasibilityMode',true,...
        'OutputFcn',@(x,optimValues,state)PlotInCond(x,optimValues,state,Geom,data));
    X_good  = fmincon(@(x) CostFunction(x,Geom,data),X0,[],[],[],[],[],[], ...
        @(x) Constraints_Swing(x,Geom,data,X_patch),options);
    
    save('SwingQSO-Lb.mat','X_good')


%%  Test della nuova initial condition found
   
    [~] = PlotInCond(X_good,[],[],Geom,data);


%%  Ottimizzazione ridotta

%   Definition of the initial vector
%     X0      = [V_MMX_patch_MARSIAU/units.vsf;...
%                 [(t_Patch(2:end)-t_Patch(1:end-1)); (t_Patch(end)- t_Patch(end-1))]'/2*units.tsf;...
%                 [(t_Patch(2)- t_Patch(1)); (t_Patch(2:end)-t_Patch(1:end-1))]'/2*units.tsf;...
%                 t_Patch'*units.tsf];
%     X_Ph0   = [X_MMX_patch_MARSIAU/units.lsf; XPhp(:,1:8)'];
%     par.Geom    = Geom;
%     par.X_patch = X_patch;
% 
% 
%     options = optimoptions('fmincon','Display','iter','Algorithm','interior-point',...
%         'ConstraintTolerance',1e-14,'FunctionTolerance',1e-14,...
%         'MaxFunctionEvaluations',1e4,'EnableFeasibilityMode',true,...
%         'OutputFcn',@(x,optimValues,state) PlotInCond2(x,optimValues,state,X_Ph0,Geom,data));
%     X_good  = fmincon(@(x) CostFunction2(x,X_Ph0,par,units),X0,[],[],[],[],[],[], ...
%         @(x) Constraints_Swing2(x,X_Ph0,par,units),options);
