function DeltaV = CostFunction(X,Geom,data)


    [par, units]    = MMX_DefineNewModelParametersAndUnits;
    [Ph, par]       = RealPhobos_States_NewModel(data,par);
    Ph              = Ph./units.sfVec2;
    par.Geom        = Geom;

    RelTol  = 1e-13;
    AbsTol  = 1e-16;
    St0     = [Ph; reshape(eye(8),[64,1])];
    tspanp      = X(end,:); 
    opt         = odeset('RelTol',RelTol,'AbsTol',AbsTol);
    [~,X_Ph0]   = ode113(@(t,X) MP_CoupledLibration(t,X,par),tspanp,St0,opt);
    X_Ph0 = X_Ph0(:,1:8)';

%   L'ottimizzazione minimizza il DeltaV richiesto nei punti in cui le
%   propagazioni si incontrano per far incontrare le traiettorie

    DeltaV  = 0;
    RelTol  = 1e-13;
    AbsTol  = 1e-16; 
    opt     = odeset('RelTol',RelTol,'AbsTol',AbsTol,'event',@(t,X) landing_Phobos(t,X,par,units));
    
    for i = 2:size(X,2)
        
        St1meno     = [X(1:6,i-1); X_Ph0(1:end,i-1); par.IPhx_bar; par.IPhy_bar; par.IPhz_bar];
        tspan1meno  = [0, X(end-1,i-1)];
        [~,X1meno]  = ode113(@(t,X) Dynamics_MPHandMMX_Inertia(t,X,par,units),tspan1meno,St1meno,opt);
    
        St1plus     = [X(1:6,i); X_Ph0(1:end,i); par.IPhx_bar; par.IPhy_bar; par.IPhz_bar];
        tspan1plus  = [0, X(end-2,i)];
        [~,X1plus]  = ode113(@(t,X) -Dynamics_MPHandMMX_Inertia(t,X,par,units),tspan1plus,St1plus,opt);
    
        DeltaV      = DeltaV + norm(X1meno(end,4:6)' - X1plus(end,4:6)')*units.vsf;
        
%         figure(99)
%         plot3(X1meno(:,1)*units.lsf,X1meno(:,2)*units.lsf,X1meno(:,3)*units.lsf,'Color','b');
%         grid on;
%         hold on;
%         xlim([-1e4 1e4])
%         ylim([-1e4 1e4])
%         planetPlot('Mars',[0;0;0],1);
%         plot3(X(1,:)*units.lsf,X(2,:)*units.lsf,X(3,:)*units.lsf,'gx');
%         plot3(X1meno(end,1)*units.lsf,X1meno(end,2)*units.lsf,X1meno(end,3)*units.lsf,'bo');
%         plot3(X1plus(:,1)*units.lsf,X1plus(:,2)*units.lsf,X1plus(:,3)*units.lsf,'Color','r');
%         plot3(X1plus(end,1)*units.lsf,X1plus(end,2)*units.lsf,X1plus(end,3)*units.lsf,'or');
%         view(2)
%         hold off
        

    end

end