function stop = PlotInCond(x,~,~,Geom,data)

    stop = false;

    [par, units]    = MMX_DefineNewModelParametersAndUnits;
    [Ph, par]       = RealPhobos_States_NewModel(data,par);
    Ph              = Ph./units.sfVec2;
    par.Geom        = Geom;

    RelTol  = 1e-13;
    AbsTol  = 1e-16;
    St0     = [Ph; reshape(eye(8),[64,1])];
    tspanp      = x(end,:); 
    opt         = odeset('RelTol',RelTol,'AbsTol',AbsTol);
    [~,X_Ph0]   = ode113(@(t,X) MP_CoupledLibration(t,X,par),tspanp,St0,opt);
    X_Ph0 = X_Ph0(:,1:8)';


    for j = 1:size(x,2)
    
        X_0opt  = [x(1:6,j); X_Ph0(1:8,j); par.IPhx_bar; par.IPhy_bar; par.IPhz_bar]; 
        tspanp  = [0,x(8,j)];
        tspanm  = [0,x(7,j)];
        opt     = odeset('RelTol',RelTol,'AbsTol',AbsTol,'event',@(t,X) landing_Phobos(t,X,par,units));
        [~,Xp] = ode113(@(t,X) Dynamics_MPHandMMX_Inertia(t,X,par,units),tspanp,X_0opt,opt);
        [~,Xm] = ode113(@(t,X) -Dynamics_MPHandMMX_Inertia(t,X,par,units),tspanm,X_0opt,opt);
        XPhp    = Xp(:,7:14);    
        XPhm    = Xm(:,7:14);    
    
    
        X_PhobosMARSIAU = zeros(3,size(Xp,1)+size(Xm,1));
        X_QSO           = zeros(3,size(Xp,1)+size(Xm,1));
    
    %   Braccio in avanti dal patch point
    
        for i = 1:size(Xp,1)
    
            X_PhobosMARSIAU(:,i+size(Xm,1)) = par.perifocal2MARSIAU*...
                [cos(XPhp(i,3)); sin(XPhp(i,3)); 0]*XPhp(i,1);
    
            k = par.perifocal2MARSIAU*[0; 0; 1];
            r = X_PhobosMARSIAU(:,i+size(Xm,1))/norm(X_PhobosMARSIAU(:,i+size(Xm,1)));
            v = cross(k,r)/norm(cross(k,r));
            Rot2MIAU = [r,v,k]; 
            MIAU2Rot = Rot2MIAU';
    
            X_QSO(:,i+size(Xm,1)) = MIAU2Rot*(Xp(i,1:3)'-X_PhobosMARSIAU(:,i+size(Xm,1)));
    
        end
    
    %   Braccio indietro dal patch point
    
        for i = 1:size(Xm,1)
    
            X_PhobosMARSIAU(:,size(Xm,1)-i+1) = par.perifocal2MARSIAU*...
                [cos(XPhm(i,3)); sin(XPhm(i,3)); 0]*XPhm(i,1);
    
            k = par.perifocal2MARSIAU*[0; 0; 1];
            r = X_PhobosMARSIAU(:,size(Xm,1)-i+1)/norm(X_PhobosMARSIAU(:,size(Xm,1)-i+1));
            v = cross(k,r)/norm(cross(k,r));
            Rot2MIAU = [r,v,k]; 
            MIAU2Rot = Rot2MIAU';
    
            X_QSO(:,size(Xm,1)-i+1) = MIAU2Rot*(Xm(i,1:3)'-X_PhobosMARSIAU(:,size(Xm,1)-i+1));
    
        end
    
    
        X_QSO_patch   = zeros(3,size(x,2));
    
        for i = 1:size(x,2)
    
            k = [0; 0; 1];
            r = [cos(X_Ph0(3,i)); sin(X_Ph0(3,i)); 0];
            v = cross(k,r)/norm(cross(k,r));
            Rot2perifocal = [r,v,k];
            Rot2MIAU = par.perifocal2MARSIAU*Rot2perifocal;
            MIAU2Rot = Rot2MIAU';
    
            X_QSO_patch(:,i) = MIAU2Rot*x(1:3,i)-[X_Ph0(1,i);0;0];
    
        end
    
%         figure(11)
%         plot3(Xp(:,1)*units.lsf,Xp(:,2)*units.lsf,Xp(:,3)*units.lsf);
%         hold on;
%         grid on;
%         axis equal
%         plot3(Xm(:,1)*units.lsf,Xm(:,2)*units.lsf,Xm(:,3)*units.lsf);
%         plot3(x(1,j)*units.lsf,x(2,j)*units.lsf,x(3,j)*units.lsf,'r*');
% %         plot3(X_PhobosMARSIAU(1,:),X_PhobosMARSIAU(2,:),X_PhobosMARSIAU(3,:))
%         planetPlot('Mars',[0;0;0],1);
%         xlabel('x [km]');
%         ylabel('y [km]');
%         zlabel('z [km]');
%         legend('MMX','Patch points','Phobos');
        figure(10)
        plot3(X_QSO(1,:)*units.lsf,X_QSO(2,:)*units.lsf,X_QSO(3,:)*units.lsf);
        hold on;
        grid on;
        plot3(X_QSO_patch(1,j)*units.lsf,X_QSO_patch(2,j)*units.lsf,X_QSO_patch(3,j)*units.lsf,'*');
        planetPlot('Asteroid',[0;0;0],Geom,1);
        xlabel('x [km]');
        ylabel('y [km]');
        zlabel('z [km]');
        axis equal

    
    end 

    hold off
        
end