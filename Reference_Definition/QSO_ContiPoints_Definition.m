clear
close all
clc

%%  Definition of the points to start the continuation for the QSO

    warning('off', 'all'); format longG;
    set(0,'DefaultTextInterpreter','latex');
    set(0,'DefaultAxesFontSize', 16);

%   Mac
    addpath('../MMX_Fcn_CovarianceAnalyses');
    addpath(genpath('../../mice/'));
    addpath(genpath('../../generic_kernels/'));
    addpath(genpath('../../useful_functions/'));
    addpath(genpath('../../dynamical_model/'));
    addpath(genpath('../MMX_Product/MMX_BSP_Files/'));
    addpath(genpath('../'));
    MMX_InitializeSPICE
    cspice_furnsh(which('MARPHOFRZ.txt'));
    cspice_furnsh(which('mar097.bsp'));

%%  Patch points

    alpha       = 13.03;                          %km
    beta        = 11.4;                           %km
    gamma       = 9.14;                           %km
    Geom.alpha  = alpha;
    Geom.beta   = beta;
    Geom.gamma  = gamma;
    muPh        = 7.1139e-4;                      %km^3/s^2
    muM         = 4.2828e4;                       %km^3/s^2
    pPh         = 9377.2;                         %km 
    omega       = sqrt((muM + muPh)/pPh^3);     %rad/s
    eps         = (muPh/muM)^(1/3);
    
    L           = eps*(sqrt(muM)/omega)^(2/3);
    T           = 1/omega;
    
%   Adimensionalized


    Phobos      = struct('alpha',alpha/L,'beta',beta/L,'gamma',gamma/L,...
                    'mu',muPh/L^3*T^2,'omega',omega*T);  

%%    Da runnare una sola volta

%     r0 = [100;0;0]/L;           % km
%     V0 = [0;-2*r0(1);0];        % km/s
%     
%     par.ds          = 1e-3;     % user-defined step-length parameter
%     par.dsMax       = 1e-1;     % maximum value for step-length parameter...see line 94
%     par.Iter        = 15;       % Max. no. of Newton's Method iterations
%     par.Nmax        = 170;      % No. of desired orbits
%     par.Opt         = 5;        % Optimal no. of Newton's Iteration - Useful for adaptive step-length...see line 94
%     par.Tol         = 1e-10;     % Newton's method tolerance
%     
%     X0              = [r0;V0];
%     T0              = 2*pi;
%     dX0_tilde_ds    = [-1;0;0;0;2;0]/sqrt(5);
%     dT_tilde_ds     = 0;
%     dlam_tilde_ds   = 0;
% 
%     [Xpo,Tpo,Lampo,p,q,Lam,bif] = Predictor_Corrector(X0,T0,@HillProb_Sphere_Nd,@landing_Nd,dX0_tilde_ds,dT_tilde_ds,dlam_tilde_ds,Phobos,par);
%     save('Xpo','Xpo')
%     save('Tpo','Tpo')

%%  Patch points

%   Portforlio di orbite
    load("Xpo.mat")
    load("Tpo.mat")
    n_rev = 4;

%   QSO-H
%     idx = 1;

%   QSO-M
%     idx = 53;

%   QSO-La
%     idx = 79;

%   QSO-Lb
    idx = 86;

%   QSO-Lc
%     idx = 90;

%   Definition patch points
    Phi0        = eye(6);
    X0          = Xpo(:,idx);
    theta0      = zeros(6,1);
    X0          = reshape([X0 Phi0 theta0],[48,1]);
    tspan       = [0 n_rev*Tpo(idx)];
    ODE.T       = 1;
    ODE.lambda  = 0;
    RelTol      = 1e-13;
    AbsTol      = 1e-16;
    opt         = odeset('RelTol',RelTol,'AbsTol',AbsTol,'event', @(t,X) Patch_points(t,X));
    [t,X,te,Xe] = ode113(@(t,X) HillProb_Sphere_Nd(t,X,Phobos,ODE),tspan,X0,opt);
    X           = X(:,1:6).*[1,1,1,T,T,T]*L;
    
%     Xe          = (Xe(:,1:6).*[1,1,1,1/T,1/T,1/T]*L)';
%     te          = te(:,1)*T;

    Xe          = (Xe(1:2:end,1:6).*[1,1,1,1/T,1/T,1/T]*L)';
    te          = te(1:2:end,1)*T;

%   Patch points
    X_patch     = Xe(1:3,:);
    V_patch     = Xe(4:6,:);
    t_patch     = te;

%   Plot
    figure()
    hold on
    grid on
    axis equal
    planetPlot('asteroid',[0,0,0],Geom,1);
    xlabel('X')
    ylabel('Y')
    plot3(X(:,1),X(:,2),X(:,3));
    plot3(Xe(1,:),Xe(2,:),Xe(3,:),'r*')


%%  Phobos states

%   Now, this points must be defined with respect to the new model. So we
%   integrate Phobos with the new model, and add the MMX postition with 
%   respect to this new Phobos

%   kernel with the real position of Phobos
   cspice_furnsh(which('mar097.bsp'));

%   Model parameters
    [par, units] = MMX_DefineNewModelParametersAndUnits;

%   Initial state
    data = cspice_str2et('2026-03-16, 00:00:00 (UTC)');
    [Ph, par]   = RealPhobos_States_NewModel(data,par);
    Ph          = Ph./units.sfVec2;
    day         = 24*3600;

%   Initial Phobos's state vector
    St0     = [Ph; reshape(eye(par.d2),[par.d2^2,1])];

%   Integration
    tspanp      = t_patch*units.tsf; 
    opt         = odeset('RelTol',RelTol,'AbsTol',AbsTol);
    [tp,XPhp]   = ode113(@(t,X) MP_CoupledLibration(t,X,par),tspanp,St0,opt);
    tp          = tp/units.tsf;

%   Total momentum
    Iz          = par.IPhz_bar + par.ni*XPhp(:,1).^2;
    Theta1_dot  = XPhp(:,4) + XPhp(:,6);
    K           = Iz.*XPhp(:,4) + par.IPhz_bar*XPhp(:,8);
    Ktot        = K + (par.MMars/par.MPho)*par.IMz_bar*Theta1_dot;

%   Potential energy
    V       = -par.ni./XPhp(:,1).*(1 + 1./(2*XPhp(:,1).^2).*...
        ((par.IMz_bar - par.Is) - .5*par.IPhx_bar - .5*par.IPhz_bar + par.IPhz_bar ...
        + 1.5*(par.IPhy_bar - par.IPhx_bar)*cos(2 * XPhp(:,7))));

%   Kinetic energy
    T       = .5*par.IMz_bar*par.MMars/par.MPho*Theta1_dot.^2 + .5*par.IPhz_bar*XPhp(:,8).^2 + ...
        .5*par.ni*XPhp(:,2).^2 + .5*(par.IPhz_bar + par.ni*XPhp(:,1).^2).*XPhp(:,4).^2 + ...
        par.IPhz_bar*XPhp(:,8).*XPhp(:,4);

%   Total energy
    Etot    = T + V;

%   Integral of motion's plot
%     figure()
%     subplot(2,1,1)
%     plot(tp/3600, Etot*units.lsf^2*units.tsf^2)
%     ylabel('Mechanical energy')
%     xlabel('[hours]')
%     grid on;
%     subplot(2,1,2)
%     plot(tp/3600, Ktot*units.lsf^2*units.tsf)
%     ylabel('Total momentum')
%     xlabel('[hours]')
%     grid on;


%%  Definition of the MMX's postion vector wrt Phobos and of the overall vector useful for the optimization


    X_Ph_Model_orbital_plane    = [XPhp(:,1)'.*cos(XPhp(:,3)'); XPhp(:,1)'.*sin(XPhp(:,3)');...
        zeros(1,size(t_patch,1))]*units.lsf;
    om_t                        = [zeros(2,size(t_patch,1)); XPhp(:,4)'*units.tsf];

%   Pre-allocation
    V_Ph_Model_orbital_plane    = zeros(3,size(t_patch,1));
    X_MMX_patch_orbital_plane   = zeros(3,size(t_patch,1));
    V_MMX_patch_orbital_plane   = zeros(3,size(t_patch,1));
    X_MMX_patch_MARSIAU         = zeros(3,size(t_patch,1));
    V_MMX_patch_MARSIAU         = zeros(3,size(t_patch,1));

   
    for i = 1:size(t_patch,1)

        r   = [cos(XPhp(i,3)'); sin(XPhp(i,3)'); 0];
        k   = [0;0;1];
        j   = cross(k,r)/norm(cross(k,r));
        RN  = [r,j,k];
        NR  = RN';

        V_Ph_Model_orbital_plane(:,i)   = RN*[XPhp(i,2)*units.vsf; 0; 0] +...
            cross(om_t(:,i),X_Ph_Model_orbital_plane(:,i));
        X_MMX_patch_orbital_plane(:,i)  = RN*(X_patch(:,i)) +...
            X_Ph_Model_orbital_plane(:,i);
        V_MMX_patch_orbital_plane(:,i)  = RN*(V_patch(:,i)) + RN*[XPhp(i,2)*units.vsf; 0; 0] +...
            cross(om_t(:,i),X_Ph_Model_orbital_plane(:,i));
        X_MMX_patch_MARSIAU(:,i)    = par.perifocal2MARSIAU*(X_MMX_patch_orbital_plane(:,i));
        V_MMX_patch_MARSIAU(:,i)    = par.perifocal2MARSIAU*(V_MMX_patch_orbital_plane(:,i));

    end
    
    VvectNormMarsIAU = vecnorm(V_MMX_patch_MARSIAU);
    par.patch = X_MMX_patch_MARSIAU;
    
%     figure()
%     plot3(X_MMX_patch_orbital_plane(1,:),X_MMX_patch_orbital_plane(2,:),X_MMX_patch_orbital_plane(3,:));
%     hold on;
%     grid on;
%     plot3(X_Ph_Model_orbital_plane(1,:),X_Ph_Model_orbital_plane(2,:),X_Ph_Model_orbital_plane(3,:))
%     planetPlot('Mars',[0;0;0],1);
%     xlabel('x [km]');
%     ylabel('y [km]');
%     zlabel('z [km]');
%     legend('MMX','Phobos');
    
    

%%  Setup della optimization per la definizione dell'orbita periodica nel nuovo modello

%   Definition of the initial vector
    X0      = [X_MMX_patch_MARSIAU/units.lsf; V_MMX_patch_MARSIAU/units.vsf;...
                (t_patch(2)-t_patch(1))/2*ones(2,size(t_patch,1))*units.tsf;
                t_patch'*units.tsf];
    X_Ph0   = XPhp(:,1:8)';
    par.Geom    = Geom;
    par.X0      = X_MMX_patch_MARSIAU/units.lsf;

%%  Propagazione della vecchia initial condition
    
  [~] = PlotInCond(X0,[],[],Geom,data);


%%  Ottimizzazione 

%   Definition of the constraints
%   There are no linear inequality constraints nor linear equality
%   constraints, but only non-linear constraints that are specified in the
%   function @Constraints

    options = optimoptions('fmincon','Display','iter','Algorithm','interior-point',...
        'ConstraintTolerance',1e-12,'FunctionTolerance',1e-12,...
        'MaxFunctionEvaluations',1e4,'EnableFeasibilityMode',true,...
        'OutputFcn',@(x,optimValues,state)PlotInCond(x,optimValues,state,Geom,data));
    X_good  = fmincon(@(x) CostFunction(x,Geom,data),X0,[],[],[],[],[],[], ...
        @(x) Constraints(x,Geom,data,X_patch),options);
    
    save('QSO-Lb_20rev.mat','X_good')


%%  Test della nuova initial condition found

    [~] = PlotInCond(X_good,[],[],Geom,data);

