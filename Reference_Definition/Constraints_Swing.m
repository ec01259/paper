function [c, ceq] = Constraints_Swing(X,Geom,data,X_patch)

    [par, units]    = MMX_DefineNewModelParametersAndUnits;
    [Ph, par]       = RealPhobos_States_NewModel(data,par);
    Ph              = Ph./units.sfVec2;
    par.Geom        = Geom;

    RelTol  = 1e-13;
    AbsTol  = 1e-16;
    St0     = [Ph; reshape(eye(8),[64,1])];
    tspanp  = X(end,:); 
    opt     = odeset('RelTol',RelTol,'AbsTol',AbsTol);
    [~,X_Ph0]   = ode113(@(t,X) MP_CoupledLibration(t,X,par),tspanp,St0,opt);
    X_Ph0   = X_Ph0(:,1:8)';

%   Non-linear constraint imposing that the two propagations of the two 
%   branches match 

%   Init dei constraints
    DeltaX          = zeros(6*(size(X,2)-2),1);
    Perp            = zeros((size(X,2))-2,1);
    times           = zeros((size(X,2)-1),1);
    boxPrimo_ultimo = zeros(6,1);
    box             = zeros(3*size(X,2),1);

    RelTol  = 1e-13;
    AbsTol  = 1e-16; 
    opt     = odeset('RelTol',RelTol,'AbsTol',AbsTol,'event',@(t,X) landing_Phobos(t,X,par,units));
    

    for i = 2:size(X,2)
        
%       Propagation forward del nodo vecchio
        St1meno     = [X(1:6,i-1); X_Ph0(1:end,i-1); par.IPhx_bar; par.IPhy_bar; par.IPhz_bar];
        tspan1meno  = [0, X(end-1,i-1)];
        [~,X1meno]  = ode113(@(t,X) Dynamics_MPHandMMX_Inertia(t,X,par,units),tspan1meno,St1meno,opt);
    
%       Propagation del nuovo nodo backward
        St1plus     = [X(1:6,i); X_Ph0(1:end,i); par.IPhx_bar; par.IPhy_bar; par.IPhz_bar];
        tspan1plus  = [0, X(end-2,i)];
        [~,X1plus]  = ode113(@(t,X) -Dynamics_MPHandMMX_Inertia(t,X,par,units),tspan1plus,St1plus,opt);
    
        DeltaX(1+(i-2)*6:(6+(i-2)*6)) = (X1meno(end,1:6)' - X1plus(end,1:6)').*units.sfVec;
                
    end

    
%   Constraint imposing that position vector and velocity vector are 
%   perpendicular at patching points

    for i = 1:size(X,2)
        
%       PROVA A DEFINIRTELO NEL SISTEMA ROTANTE APPICCICATO A PHOBOS
        r   = [cos(X_Ph0(3,i)); sin(X_Ph0(3,i)); 0];
        k   = [0;0;1];
        j   = cross(k,r)/norm(cross(k,r));
        RN  = [r,j,k];
%         X_Phobos_i = par.perifocal2MARSIAU*[X_Ph0(1,i)*cos(X_Ph0(3,i)); X_Ph0(1,i)*sin(X_Ph0(3,i)); 0];
%         V_Phobos_i = par.perifocal2MARSIAU*(RN*[X_Ph0(2,i); 0; 0] + cross([0;0;X_Ph0(4,i)],[X_Ph0(1,i)*cos(X_Ph0(3,i)); X_Ph0(1,i)*sin(X_Ph0(3,i)); 0]));
%         X_rel = X(1:3,i) - X_Phobos_i;
%         V_rel = X(4:6,i) - V_Phobos_i;
%         Perp(i) = dot(X_rel, V_rel);
        
        X_MMX_peri = par.MARSIAU2perifocal*X(1:3,i);
        V_MMX_peri = par.MARSIAU2perifocal*X(4:6,i);

        X_rel = RN'*X_MMX_peri - [X_Ph0(1,i); 0; 0];
        V_rel = RN'*V_MMX_peri - RN'*[X_Ph0(2,i); 0; 0] - RN'*cross([0; 0; X_Ph0(4,i)],...
            X_Ph0(1,i)*[cos(X_Ph0(3,i)); sin(X_Ph0(3,i)); 0]);
        Perp(i) = dot(X_rel, V_rel);

        if i==1
            boxPrimo_ultimo(1:3) = abs(X_patch(2,i) - X_rel(2)*units.lsf) - 1*ones(1,1);
        elseif i==size(X,2)
            boxPrimo_ultimo(4:6) = abs(X_patch(2,i) - X_rel(2)*units.lsf) - 1*ones(1,1);
        end

        box(1+(i-1)*3:3+(i-1)*3) = abs(X_patch(:,i) -...
            X_rel*units.lsf) - [5; 5; .5];

    end

 
%   Constraint imposing that forward and backward propagation times are
%   meeting somewhere between half orbit revolution time

    for i = 2:size(X,2)
        
        times(i-1) = (X(end,i) - X(end,i-1) - (X(end-2,i) + X(end-1,i-1)))/units.tsf;

    end

    c   = box;
    c   = [];
    ceq = [DeltaX;  times];

end