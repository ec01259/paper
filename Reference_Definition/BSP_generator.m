clear
close all
clc

%%  Definition of the points to start the continuation for the QSO

    warning('off', 'all'); format longG;
    set(0,'DefaultTextInterpreter','latex');
    set(0,'DefaultAxesFontSize', 16);

%   Mac
    restoredefaultpath
    addpath('../MMX_Fcn_CovarianceAnalyses');
    addpath(genpath('../../mice/'));
    addpath(genpath('../../generic_kernels/'));
    addpath(genpath('../../useful_functions/'));
    addpath(genpath('../../dynamical_model/'));
    addpath(genpath('../'));
    MMX_InitializeSPICE
    cspice_furnsh(which('mar097.bsp'));
    cspice_furnsh('MARPHOFRZ.txt');

    muPh        = 7.1139e-4;                      %km^3/s^2
    muM         = 4.2828e4;                       %km^3/s^2
    pPh         = 9377.2;                         %km 
    omega       = sqrt((muM + muPh)/pPh^3);       %rad/s
    T           = 1/omega;
    load("Tpo.mat")
    [par, units] = MMX_DefineNewModelParametersAndUnits;

%   Initial MMX's state vector
    startdate   = '2026-03-16 00:00:00 (UTC)';
    data        = cspice_str2et(startdate);
    par.et0     = data;
    [Ph, par]   = RealPhobos_States_NewModel(data,par);
    
%   QSO-H
%     load("QSO-H_20rev.mat")
%     idx = 1;
%     n_rev   = 20;

%   QSO-M
%     load("QSO-M_20rev.mat")
%     idx = 53;
%     n_rev   = 20;

%   QSO-Lb
%     load("QSO-Lb_20rev.mat")

% %   If MMX comes from the optimization
%     X_MMX   = X_good(1:6,1);

%   If MMX comes from a .bsp file
    cspice_furnsh(which('MMX_QSO_031_2x2_826891269_828619269.bsp'));
    X_MMX   = cspice_spkezr('-34', data, 'MARSIAU', 'none', '499')./units.sfVec;


%   Initial Phobos's state vector
    Ph0     = Ph./units.sfVec2;

%   Initial state vector
    St0     = [X_MMX; Ph0; par.IPhx_bar; par.IPhy_bar; par.IPhz_bar];

%   Integration
    day     = 3600;
    period_Def = 5*day;
    tspan   = [0, period_Def]*units.tsf;
    RelTol  = 1e-13;
    AbsTol  = 1e-16; 
    opt     = odeset('RelTol',RelTol,'AbsTol',AbsTol);
    [t,X]   = ode113(@(t,X) Dynamics_MPHandMMX_Inertia(t,X,par,units),tspan,St0,opt);
    Xmmx    = X(:,1:6)'.*units.sfVec;
    XPh_per = X(:,7:14).*units.sfVec2';
    t       = t/units.tsf;

%   Definition of Phobos states in the MARSIAU reference frame

    XPh = zeros(6,size(X,1));

    for i = 1:size(X,1)

        RPh         = XPh_per(i,1);
        RPh_dot     = XPh_per(i,2);
        theta       = XPh_per(i,3);
        theta_dot   = XPh_per(i,4);
        r   = [cos(theta); sin(theta); 0];
        k   = [0;0;1];
        j   = cross(k,r)/norm(cross(k,r));
        RN  = [r,j,k];
        XPh(1:3,i)    = par.perifocal2MARSIAU*RPh*r;
        XPh(4:6,i)    = par.perifocal2MARSIAU*(RN*[RPh_dot; 0; 0] +...
            cross([0;0;theta_dot],RPh*r));

    end

%%  BSP Generation

    Model.epoch = data;
    Model.pars  = par;
    Model.units = units;
    micePATH    = '~/Documents/MATLAB/mice/';
    
    TargetFolder = '../MMX_Product/MMX_BSP_Files_GravLib/';
    if (exist(TargetFolder, 'dir') ~= 7)
        command = sprintf('mkdir %s', TargetFolder);
        system(command);
    end
    
    MMX_GenerateBSPFiles_GravLib(t, Xmmx, XPh, 'QSO', [198, 0], period_Def, Model, micePATH)


%%  Test risultati


    cspice_furnsh(which('MMX_QSO_198_2x2_826891269_828619269.bsp'));
    cspice_furnsh(which('Phobos_826891269_828619269.bsp'));

    MMX_BSP     = cspice_spkezr('-34', data+t', 'MarsIAU', 'none', '499');
    Phobos_BSP  = cspice_spkezr('-401', data+t', 'MarsIAU', 'none', '499');

    err_MMX     = Xmmx - MMX_BSP;
    err_Phobos  = XPh - Phobos_BSP;
    
    figure(1)
    subplot(2,3,1)
    plot(t/3600,err_MMX(1,:)*units.lsf)
    ylabel('$\Delta X [km]$',Interpreter='latex')
    grid on
    subplot(2,3,2)
    plot(t/3600,err_MMX(2,:)*units.lsf)
    ylabel('$\Delta Y [km]$',Interpreter='latex')
    grid on
    subplot(2,3,3)
    plot(t/3600,err_MMX(3,:)*units.lsf)
    ylabel('$\Delta Z [km]$',Interpreter='latex')
    grid on
    subplot(2,3,4)
    plot(t/3600,err_MMX(4,:)*units.vsf)
    ylabel('$\Delta Vx [km/s]$',Interpreter='latex')
    grid on
    grid on
    subplot(2,3,5)
    plot(t/3600,err_MMX(5,:)*units.vsf)
    ylabel('$\Delta Vy [km/s]$',Interpreter='latex')
    grid on
    subplot(2,3,6)
    plot(t/3600,err_MMX(6,:)*units.vsf)
    ylabel('$\Delta Vz [km/s]$',Interpreter='latex')
    grid on
    

    figure(2)
    subplot(2,3,1)
    plot(t/3600,err_Phobos(1,:))
    grid on
    subplot(2,3,2)
    plot(t/3600,err_Phobos(2,:))
    grid on
    subplot(2,3,3)
    plot(t/3600,err_Phobos(3,:))
    grid on
    subplot(2,3,4)
    plot(t/3600,err_Phobos(4,:))
    grid on
    subplot(2,3,5)
    plot(t/3600,err_Phobos(5,:))
    grid on
    subplot(2,3,6)
    plot(t/3600,err_Phobos(6,:))
    grid on
    