function dX = eom_sigmapoints(t, X, Model)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Integrate Equations of Motion for a collection of sigma points
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

dyn      = Model.dynamics;
pars     = Model.pars;
N_Update = Model.N_Update;

d        = pars.d;     % No. of states
n        = length(X) / N_Update;

dX = zeros(n*N_Update,1);

% States only are integrated
if(n == d)
    
    for i = 1:1:N_Update
        f                   = dyn(t, X( (n*(i-1)+1):n*i ), pars);
        dX((n*(i-1)+1):n*i) = f;
    end

% STM is being integrated
elseif(n == d*(d+1))
    
    for i = 1:1:N_Update
        [f, Jx] = dyn(t, X( n*(i-1)+1 : n*(i-1)+d), pars);
        
        Phi  = reshape(X( n*(i-1)+(d+1) : n*(i-1)+d*(d+1)), d, d);
        dPhi = Jx*Phi;
        
        dX((n*(i-1)+1):n*i) = [f; dPhi(:)];
    end
    
% Check if error
else
    error('Stop here Traveller! Check the size of your eom inputs!\n');
    
end