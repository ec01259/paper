function varargout = CGM(X,pars)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% [g, G, U, g_C, g_S] = CGM(X,pars)
%
% Calculate gravity vector and partial derivatives in cartesian coordinates
%
% INPUTS:
% - X
% - pars
%       .Re
%       .GM
%       .Clm
%       .Slm
%
% OUTPUT:
% - g       .gravity vector
% - G       .gravity gradient
% - U       .gravity potential
% - g_C     .partial derivatives wrt normalized Clm Stokes coefficients
% - g_S     .partial derivatives wrt normalized Slm Stokes coefficients
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


Re = pars.Re;        % Scale factor
GM = pars.GM;        % Body's Gravitational Parameter
Clm  = pars.Clm;     % Stokes' Clm normalized coefficients
Slm  = pars.Slm;     % Stokes' Slm normalized coefficients
Norm = pars.Norm;    % Normalization Matrix
[L,M] = size(Clm);
L = L-1;
M = M-1;


% Sat position vector
x = X(1);
y = X(2);
z = X(3);
r = norm(X(1:3));
s = x + 1i*y;


% Compute Unnormalized Associated Legendre Polynomials
% Initialization
Vlm = zeros(L+3);      
for mm = 0:L+2
    
    % Index
    id1 = mm + 1;
    
    % Sectorial Terms
    if(mm == 0)
        Vlm(id1,id1) = 1/r; % seed
    else
        Vlm(id1,id1) = (2*mm-1)*Vlm(mm,mm)*s/r^2;
    end
    
    for ll = (mm+1):L+2
        
        % Index
        id2 = ll+1;
        
        % Zonal & Tesseral Terms
        Vlm(id2,id1) = ((2*ll-1)/(ll-mm))*(z/r^2)*Vlm(id2-1,id1);
        if(ll > mm+1)
            Vlm(id2,id1) = Vlm(id2,id1) - ((ll+mm-1)/(ll-mm))*Vlm(id2-2,id1)/r^2;
        end
    end
end


% Complex Conjugates
Vlms = conj(Vlm);


% Compute First and Second Partial Derivatives
% Initialization
VlmX  = zeros(L+1);
VlmY  = zeros(L+1);
VlmZ  = zeros(L+1);
VlmXX = zeros(L+1);
VlmXY = zeros(L+1);
VlmXZ = zeros(L+1);
VlmYY = zeros(L+1);
VlmYZ = zeros(L+1);
VlmZZ = zeros(L+1);
for mm = 0:L
    
    % Col Index
    col = mm+1;
    
    switch mm
        
        %%%% Case mm = 0 %%%%
        case 0
            for ll = mm:L
                
                % Row Index
                row = ll+1;
                
                % First Partials
                VlmX(row,col) = -0.5*(Vlm(row+1,2)+Vlms(row+1,2));
                VlmY(row,col) = 0.5i*(Vlm(row+1,2)-Vlms(row+1,2));
                VlmZ(row,col) = -(ll+1)*Vlm(row+1,1);
                
                % factors
                fct = (ll+2)*(ll+1);
                
                % Second Partials
                VlmXX(row,col) = 0.25*(Vlm(row+2,3) + Vlms(row+2,3)) - 0.5*fct*Vlm(row+2,1);
                VlmYY(row,col) = -0.25*(Vlm(row+2,3) + Vlms(row+2,3)) - 0.5*fct*Vlm(row+2,1);
                VlmZZ(row,col) = (ll-mm+2)*(ll-mm+1)*Vlm(row+2,col);
                
                VlmXY(row,col) = 0.25i*(Vlms(row+2,3)-Vlm(row+2,3));
                VlmXZ(row,col) = 0.5*(ll+1)*(Vlm(row+2,2)+Vlms(row+2,2));
                VlmYZ(row,col) = 0.5i*(ll+1)*(Vlms(row+2,2)-Vlm(row+2,2));
                
            end
           
            
        %%%% Case mm = 1 %%%%
        case 1            
            for ll = mm:L
                
                % Row Index
                row = ll+1;
                
                % factors
                fct = (ll-mm+2)*(ll-mm+1);
                
                % First Partials
                VlmX(row,col) = 0.5*(-Vlm(row+1,col+1)+fct*Vlm(row+1,col-1));
                VlmY(row,col) = 0.5i*(Vlm(row+1,col+1)+fct*Vlm(row+1,col-1));
                VlmZ(row,col) = -(ll-mm+1)*Vlm(row+1,col);
                                
                % factors
                fct1 = (ll+1)*ll;
                fct2 = (ll-mm+3)*(ll-mm+2);
                
                % Second Partials
                VlmXX(row,col) = 0.25*(Vlm(row+2,4) - fct1*Vlms(row+2,2)) - 0.5*fct1*Vlm(row+2,2);
                VlmYY(row,col) = -0.25*(Vlm(row+2,4) - fct1*Vlms(row+2,2)) - 0.5*fct1*Vlm(row+2,2);
                VlmZZ(row,col) = (ll-mm+2)*(ll-mm+1)*Vlm(row+2,col);
                
                VlmXY(row,col) = -0.25i*(Vlm(row+2,4) + fct1*Vlms(row+2,2));
                VlmXZ(row,col) = 0.5*(ll-mm+1)*(Vlm(row+2,col+1) - fct2*Vlm(row+2,col-1));
                VlmYZ(row,col) = -0.5i*(ll-mm+1)*(Vlm(row+2,col+1) + fct2*Vlm(row+2,col-1));
                
            end
            
        %%%% Case mm > 1 %%%%    
        otherwise
            for ll = mm:L
                
                % Row Index
                row = ll+1;
                
                % factors
                fct = (ll-mm+2)*(ll-mm+1);
                
                % First Partials
                VlmX(row,col) = 0.5*(-Vlm(row+1,col+1)+fct*Vlm(row+1,col-1));
                VlmY(row,col) = 0.5i*(Vlm(row+1,col+1)+fct*Vlm(row+1,col-1));
                VlmZ(row,col) = -(ll-mm+1)*Vlm(row+1,col);
                
                % factors
                fcta = (ll-mm+2)*(ll-mm+1);
                fctb = (ll-mm+3)*(ll-mm+2);
                fctc = (ll-mm+4)*(ll-mm+3)*(ll-mm+2)*(ll-mm+1);
                
                % Second Partials
                VlmXX(row,col) = 0.25*(Vlm(row+2,col+2) + fctc*Vlm(row+2,col-2)) - 0.5*fcta*Vlm(row+2,col);
                VlmYY(row,col) = -0.25*(Vlm(row+2,col+2) + fctc*Vlm(row+2,col-2)) - 0.5*fcta*Vlm(row+2,col);
                VlmZZ(row,col) = fcta*Vlm(row+2,col);
                
                VlmXY(row,col) = 0.25i*(-Vlm(row+2,col+2) + fctc*Vlm(row+2,col-2));
                VlmXZ(row,col) = 0.5*(ll-mm+1)*(Vlm(row+2,col+1) - fctb*Vlm(row+2,col-1));
                VlmYZ(row,col) = -0.5i*(ll-mm+1)*(Vlm(row+2,col+1) + fctb*Vlm(row+2,col-1));
        
            end
    end
end



% Normalize Legendre polynomial functions
Vlm = Vlm./Norm;
VlmX  = VlmX./Norm(1:L+1, 1:L+1);
VlmY  = VlmY./Norm(1:L+1, 1:L+1);
VlmZ  = VlmZ./Norm(1:L+1, 1:L+1);
VlmXX = VlmXX./Norm(1:L+1, 1:L+1);
VlmXY = VlmXY./Norm(1:L+1, 1:L+1);
VlmXZ = VlmXZ./Norm(1:L+1, 1:L+1);
VlmYY = VlmYY./Norm(1:L+1, 1:L+1);
VlmYZ = VlmYZ./Norm(1:L+1, 1:L+1);
VlmZZ = VlmZZ./Norm(1:L+1, 1:L+1);



% Acceleration
U = 0;
g = zeros(3,1);
G = zeros(3);
g_C = zeros(3, (L+1)*(L+2)/2);
g_S = zeros(3, (L+1)*L/2);
nc = 0;
ns = 0;
for ll = 0:L
    
    % row index
    row = ll + 1;
    
    for mm = 0:min(ll, M)
        
        % col index
        col = mm + 1;
        
        
        % gravity potential
        U = U + Re^ll*real((Clm(row,col) - 1i*Slm(row,col))*Vlm(row, col));
        
        
        % gravity acceleration
        g = g + Re^ll*real((Clm(row,col) - 1i*Slm(row,col))*[VlmX(row,col);VlmY(row,col);VlmZ(row,col)]);
        
        
        % Gravity gradient
        G = G + Re^ll*real((Clm(row,col) - 1i*Slm(row,col))*[VlmXX(row,col),VlmXY(row,col),VlmXZ(row,col);
                                                             VlmXY(row,col),VlmYY(row,col),VlmYZ(row,col);
                                                             VlmXZ(row,col),VlmYZ(row,col),VlmZZ(row,col)]);
                                                         
                                                         
        % Partials of g with respect to Clm
        nc = nc + 1;
        g_C(:, nc) = Re^ll*real([VlmX(row,col);VlmY(row,col);VlmZ(row,col)]);
        if(mm > 0)
            ns = ns + 1;
            g_S(:, ns) = Re^ll*real(-1i*[VlmX(row,col);VlmY(row,col);VlmZ(row,col)]);
        end
    end
end


% %% DID NOT NOTICE SIGNIFICANT TIME CHANGES WITH THE FOLLOWING:
% gx = sum(Re.^(0:L)'.*sum(real((Clm - 1i*Slm).*VlmX), 2));
% gy = sum(Re.^(0:L)'.*sum(real((Clm - 1i*Slm).*VlmY), 2));
% gz = sum(Re.^(0:L)'.*sum(real((Clm - 1i*Slm).*VlmZ), 2));
% Gxx = sum(Re.^(0:L)'.*sum(real((Clm - 1i*Slm).*VlmXX), 2));
% Gxy = sum(Re.^(0:L)'.*sum(real((Clm - 1i*Slm).*VlmXY), 2));
% Gxz = sum(Re.^(0:L)'.*sum(real((Clm - 1i*Slm).*VlmXZ), 2));
% Gyy = sum(Re.^(0:L)'.*sum(real((Clm - 1i*Slm).*VlmYY), 2));
% Gyz = sum(Re.^(0:L)'.*sum(real((Clm - 1i*Slm).*VlmYZ), 2));
% Gzz = sum(Re.^(0:L)'.*sum(real((Clm - 1i*Slm).*VlmZZ), 2));
% U = sum(Re.^(0:L)'.*sum(real((Clm - 1i*Slm).*Vlm(1:L+1, 1:M+1)), 2));
% 
% 
% g = GM*[gx; gy; gz];
% G = GM*[Gxx, Gxy, Gxz; Gxy, Gyy, Gyz; Gxz, Gyz, Gzz];
% U = GM*U;


U = GM*U;
g = GM*g;
G = GM*G;
g_C = GM*g_C;
g_S = GM*g_S;
varargout{1} = g;
varargout{2} = G;
varargout{3} = U;
varargout{4} = g_C;
varargout{5} = g_S;