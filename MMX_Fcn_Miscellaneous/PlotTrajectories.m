function PlotTrajectories(h, x, Model, varargin)

% Load dimension
d = Model.pars.d;
alp = Model.pars.alp;
bet = Model.pars.bet;
gam = Model.pars.gam;
lsf = Model.units.lsf;



% Retrieve Stability
if(nargin > 3)
    flg = varargin{1};
    if(flg)
        Clr = 'b';
    else
        Clr = 'r';
    end
else
    Clr = 'k';
end


% Plot Trajectories
figure(h)
[Xe, Ye, Ze] = ellipsoid(0, 0, 0, alp, bet, gam);
surf(Xe*lsf, Ye*lsf, Ze*lsf, 'EdgeColor', 'none', 'FaceColor', [0.5, 0.5, 0.5]); hold on;
if d == 6
    % Plot Stable Orbit
    plot3(x(:,1)*lsf, x(:,2)*lsf, x(:,3)*lsf, Clr, 'Linewidth', 1);
    plot3(x(1,1)*lsf, x(1,2)*lsf, x(1,3)*lsf, '.', 'Color', Clr);
    xlabel('$X$ (km)'); ylabel('$Y$ (km)'); zlabel('$Z$ (km)');
    axis equal;
    hold off;
elseif d == 4
    % Plot Stable Orbit
    plot(x(:,1)*lsf, x(:,2)*lsf, Clr, 'Linewidth', 1); 
    plot(x(1,1)*lsf, x(1,2)*lsf, '.', 'Color', Clr);
    xlabel('$X$ (km)'); ylabel('$Y$ (km)');
    axis equal;
    hold off;
end

