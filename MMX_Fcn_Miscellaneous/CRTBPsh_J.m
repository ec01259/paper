function [C, dC, d2CdX2] = CRTBPsh_J(X, pars)

% Problem parameters
d  = pars.d;
d2 = d/2;
a = pars.a;
n = pars.n;
GM1 = pars.GM1;
SH = pars.SH;


X = reshape(X, d, []);


N = size(X, 2);
C = zeros(1, N);
dC = zeros(d, N);
d2CdX2 = zeros(pars.d, pars.d, N);
for ii = 1:N
    
    % Calculate gravity and gravity gradient from primary
    r1 = X(1:d2, ii) + [a; zeros(d2-1,1)];
    R1 = sqrt(r1(1)^2 + r1(2)^2 + r1(3)^2);
    g1 = -GM1*r1/R1^3;
    G1 = -GM1/R1^3*(eye(3) - 3*(r1*r1')/R1^2);
    
    
    % Calculate sh gravity and gravity gradient from secondary
    r2 = X(1:d2, ii);
    [g2, G2, U2] = CGM(r2, SH);
  
    
    % Jacobi Integral
    C(ii) = 0.5*n^2*(X(1, ii)^2 + X(2, ii)^2) + GM1/a^2*X(1, ii) + GM1/R1 + U2 - 0.5*(X(4, ii)^2 + X(5, ii)^2 + X(6, ii)^2);
    dC(:, ii) = [g1(1) + g2(1) + n^2*X(1, ii) + GM1/a^2; g1(2) + g2(2) + n^2*X(2, ii); g1(3) + g2(3); - X(4:6, ii)];
    d2CdX2(:, :, ii) = [G1 + G2 + diag(n^2*[1; 1; 0]), zeros(3); zeros(3), -eye(3)]; 
end