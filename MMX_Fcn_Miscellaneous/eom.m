function dX = eom(t, X, Model)

dyn = Model.dynamics;
pars = Model.pars;
d = pars.d;     % No. of states
p = pars.p;     % No. of parameters
n = length(X);

% States only are integrated
if(n == d)
    f = dyn(t, X, pars);
    dX = f;

% STM is being integrated
elseif(n == d*(d+1))
    
    [f, Jx] = dyn(t, X(1:d), pars);
    
    Phi = reshape(X(d+1:end), d, d);
    dPhi = Jx*Phi;
    
    dX = [f; dPhi(:)];
    
% Mapping parameters
elseif(n == d*(d+p+1))
    
    [f, Jx, Jp] = dyn(t, X(1:d), pars);
    
    Phi = reshape(X(d+1:d*(d+1)), d, d);
    dPhi = Jx*Phi;
    
    Tht = reshape(X(d*(d+1)+1:d*(d+n+1)), d, n);
    dTht = Jx*Tht + Jp;
    
    dX = [f; dPhi(:); dTht(:)];

% Check if error
else
    error('Stop here Traveller! Check the size of your eom inputs!\n');
end