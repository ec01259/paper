function varargout = CRTBPsh(~,X,pars)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% [f, A] = CRTBPsh(~,X,pars)
%
% Calculate the acceleration vector of the Phobos-centred CRTBP problem
% with spherical harmonics secondary
%
% Input:
% .X        State Vector (d x 1)
% .pars     Problem Parameters
%           .d      Dimension of the Problem
%           .a      Secondary semi-major axis 
%           .n      Secondary mean motion 
%           .GM1    Primary gravitational parameter
%           .SH     Secondary Spherical Harmonics Structure
%
% Output:
% .f        Vector field (d x 1)
% .A        Vector field gradient (d x d)
% .B        Partial derivatives wrt system parameters (d x p)
%
% Author: N.Baresi
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Problem parameters
d       = pars.d;
d2      = d/2;
a       = pars.a;
n       = pars.n;
GM1     = pars.GM1;
SH      = pars.SH;


% Relative Pos Vector wrt Primary
r1 = X(1:d2) + [a; zeros(d2-1,1)];
R1 = sqrt(r1(1)^2 + r1(2)^2 + r1(3)^2);
g1 = -GM1*r1/R1^3;
G1 = -GM1/R1^3*(eye(3) - 3*(r1*r1')/R1^2);


% Relative Pos Vector wrt Secondary
r2 = X(1:d2);
[g2, G2] = CGM(r2, SH);


% EOM
f = zeros(d,1);
f(1:d2) = X(d2+1:d);
f(4) = n^2*X(1) + GM1/a^2 + 2*n*X(5) + g1(1) + g2(1);
f(5) = n^2*X(2) - 2*n*X(4) + g1(2) + g2(2);
f(6) = g1(3) + g2(3);


% Vector field gradient
A = zeros(d);
A(1:d2, d2+1:d) = eye(d2);
A(d2+1:d, 1:d2) = diag(n^2*[1, 1, 0]) + G1 + G2;
A(d2+1:d, d2+1:d) = [0, 2*n, 0; -2*n, 0, 0; 0, 0, 0];


% Prepare for Output
varargout{1} = f;
varargout{2} = A;
end
