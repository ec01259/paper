function dX = eom_qpt(t, X, Model)


% Load Parameters
d = Model.pars.d;
N = Model.pars.N;
P = Model.pars.P;
l1 = Model.pars.l1;
l2 = Model.pars.l2;
DT = Model.pars.DT;
Jd = Model.pars.Jd;


% Extract Information from X vector
D = d*N;
Tht = reshape(X(D*(D+1)+1:end), D, 3);
Phi = reshape(X(D+1:D*(D+1)), D, D);
Y = reshape(X(1:D), d, N);
 

F = zeros(d, N);
A = zeros(D, D);
B = zeros(D, 3);
[~, dC, d2CdX2] = Model.Jacobi(Y, Model.pars);
dI1 = reshape(Jd*DT*Y(:), d, N);
for kk = 1:N
    
    [f0, Jx] = Model.dynamics(t, Y(:, kk), Model.pars);
    F(:, kk) = P*f0 + l1*dC(:, kk) + l2*dI1(:, kk);
    
    idx = d*(kk-1)+1:d*kk;
    A(idx, idx) = P*Jx + l1*d2CdX2(:, :, kk);
    B(idx, :) = [f0, dC(:, kk), dI1(:, kk)];
end
A = A + l2*Jd*DT;

dPhi = A*Phi;
dTht = A*Tht + B;

dX = zeros(D*(D+4), 1);
dX(1:D) = F(:);
dX(D+1:D*(D+1)) = dPhi(:);
dX(D*(D+1)+1:end) = dTht(:);