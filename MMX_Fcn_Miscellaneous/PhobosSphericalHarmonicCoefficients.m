function SH = PhobosSphericalHarmonicCoefficients(L,M)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% SH = PhobosSphericalHarmonicCoefficients(L,M)
%
% Create Phobos Spherical Harmonic structure for NBP Integration
%
% INPUT: None
% - L   .Max order
% - M   .Max degree
%
% OUTOUT: 
% - SH
%       .bodyframe  SPICE Frame Name
%       .Re         Mars radius
%       .GM         Mars gravitational parameter
%       .Clm        Normalized Stokes' coefficients 
%       .Slm        Normalized Stokes' coefficients
%
% Author: N. Baresi
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Load MOON_PA kernels
SH.bodyframe = 'IAU_PHOBOS';


% Load Spherical Harmonics Coefficients
fid = fopen('Phobos_sha_Willner2014.tab','r');


% Read Re and GM values
Data = textscan(fid,'%f %f %f %d %d %d %f %f', 1, 'Delimiter', ',');
SH.Re = Data{1};
SH.GM = Data{2};


% Read Clm and Slm values
Data = textscan(fid,'%3.0f %3.0f %f %f %f %f', 'Delimiter', ',');
fclose(fid);


% Allocate Clm and Slm matrices
ind_l = Data{1} + 1;
ind_m = Data{2} + 1;
Clm = Data{3};
Slm = Data{4};
Clm = full(sparse(ind_l, ind_m, Clm)); Clm(1, 1) = 1.0;
Slm = full(sparse(ind_l, ind_m, Slm));


% Set coeff. to zero beyond degree M
Clm = Clm(1:(L+1), 1:(L+1));
Clm(:,(M+2):(L+1)) = 0;

Slm = Slm(1:(L+1), 1:(L+1));
Slm(:,(M+2):(L+1)) = 0;


% Store Normalized SH Coefficients
SH.Clm = Clm;
SH.Slm = Slm;



%% Calculate Normalization Factor
l = repmat((0:L+2)', 1, M+3);
m = repmat(0:M+2, L+3, 1);
A = l+m;
B = l-m;
B(B<0) = 0;
k = 2*ones(size(l));
k(:, 1) = 1;
Norm = sqrt(factorial(A)./factorial(B)./(2*l+1)./k);
SH.Norm = Norm;



% %% Calculate Unnormalized Coefficients
% l = repmat((0:L)', 1, M+1);
% m = repmat(0:M, L+1, 1);
% A = l+m;
% B = l-m;
% B(B<0) = 0;
% k = 2*ones(size(l));
% k(:, 1) = 1;
% Norm = sqrt(factorial(A)./factorial(B)./(2*l+1)./k);
% SH.Clm_un = Clm./Norm;
% SH.Slm_un = Slm./Norm;


