function [t, x] = IntegrateInitialConditions(X0, T0, Model)

if(length(T0) == 1)
    % No. of data points
    N = T0/(30/Model.units.tsf);
    Time = linspace(0, T0, N+1);
else
    Time = T0;
end


% ode113
Ic = X0;
opt = odeset('RelTol', 3e-14, 'AbsTol', 1e-16);
[t, x] = ode113(@eom, Time, Ic, opt, Model);
