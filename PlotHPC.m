%% Confronto geometrie sulla M
    
load('QSOM_PN10_alpha1_30days.mat');
EstQSO  = Est;
load('3DQSOM_PN10_alpha1__30days.mat');
Est3D   = Est;
load('SwingQSOM_PN10_alpha1__30days.mat');
EstSwing = Est;

t_obs   = EstQSO.t(1,:);


[SQRT_X_QSO, SQRT_V_QSO, P_t_C_20_QSO, P_t_C_22_QSO, ~, ~, ~, ~,...
    ~, ~,] = Envelopes_Plot(EstQSO, fine, fineCss);
[SQRT_X_3D, SQRT_V_3D, P_t_C_20_3D, P_t_C_22_3D, ~, ~, ~, ~,...
    ~, ~] = Envelopes_Plot(Est3D, fine, fineCss);
[SQRT_X_Swing, SQRT_V_Swing, P_t_C_20_Swing, P_t_C_22_Swing, ~, ~, ~, ~,...
    ~, ~] = Envelopes_Plot(EstSwing, fine, fineCss);

figure()
subplot(1,2,1)
semilogy(t_obs(1:end-1)/3600,3.*real(sqrt(P_t_C_20_QSO)),'LineWidth',1.2);
grid on;
hold on;
semilogy(t_obs(1:end-1)/3600,3.*real(sqrt(P_t_C_20_3D)),'LineWidth',1.2);
semilogy(t_obs(1:end-1)/3600,3.*real(sqrt(P_t_C_20_Swing)),'LineWidth',1.2);
xlabel('time $[hour]$','Fontsize',26)
ylabel('C_{20}','Fontsize',26)
legend('QSO','3D-QSO','Swing-QSO','Interpreter','latex','Fontsize',26)
subplot(1,2,2)
semilogy(t_obs(1:end-1)/3600,3.*real(sqrt(P_t_C_22_QSO)),'LineWidth',1.2);
grid on;
hold on;
semilogy(t_obs(1:end-1)/3600,3.*real(sqrt(P_t_C_22_3D)),'LineWidth',1.2);
semilogy(t_obs(1:end-1)/3600,3.*real(sqrt(P_t_C_22_Swing)),'LineWidth',1.2);
xlabel('time $[hour]$','Fontsize',26)
ylabel('C_{22}','Fontsize',26)
legend('QSO','3D-QSO','Swing-QSO','Interpreter','latex','Fontsize',26)


figure()
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(EstQSO.P_t(13,13,1:end-1)))),'LineWidth',1.2)
grid on;
hold on;
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(Est3D.P_t(13,13,1:end-1)))),'LineWidth',1.2)
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(EstSwing.P_t(13,13,1:end-1)))),'LineWidth',1.2)
xlabel('Time [hour]','Fontsize',28)
ylabel('$3\sigma_{\Phi_{Ph}}$ [deg]','Fontsize',32)
legend('QSO-M','3DQSO-M','Swing QSO-M','Interpreter','latex','Fontsize',26)

%   MMX Position and velocity

figure()
subplot(1,2,1)
semilogy(t_obs(1:end-1)/3600,SQRT_X_QSO)
hold on;
grid on;
semilogy(t_obs(1:end-1)/3600,SQRT_X_3D)
semilogy(t_obs(1:end-1)/3600,SQRT_X_Swing)
xlabel('time $[hour]$')
ylabel('$[km]$')
title('MMX position vector $3\sigma$ envelopes','Interpreter','latex','FontSize',16)
legend('QSO','3D-QSO','Swing-QSO','Interpreter','latex','FontSize',14)
subplot(1,2,2)
semilogy(t_obs(1:end-1)/3600,SQRT_V_QSO)
hold on;
grid on;
semilogy(t_obs(1:end-1)/3600,SQRT_V_3D)
semilogy(t_obs(1:end-1)/3600,SQRT_V_Swing)
xlabel('time $[hour]$')
ylabel('$[km/s]$')
title('MMX velocity vector $3\sigma$ envelopes','Interpreter','latex','FontSize',16)
legend('QSO','3D-QSO','Swing-QSO','Interpreter','latex','FontSize',14)

% Phobos states

figure()
subplot(2,4,1)
semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(EstQSO.P_t(7,7,1:end-1)))))
grid on;
hold on;
semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est3D.P_t(7,7,1:end-1)))))
semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(EstSwing.P_t(7,7,1:end-1)))))
xlabel('time [hour]')
ylabel('$R_{Ph}$ [km]')
subplot(2,4,2)
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(EstQSO.P_t(9,9,1:end-1)))))
grid on;
hold on;
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(Est3D.P_t(9,9,1:end-1)))))
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(EstSwing.P_t(9,9,1:end-1)))))
xlabel('time [hour]')
ylabel('$\theta_{Ph}$ [deg]')
subplot(2,4,3)
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(EstQSO.P_t(11,11,1:end-1)))))
grid on;
hold on;
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(Est3D.P_t(11,11,1:end-1)))))
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(EstSwing.P_t(11,11,1:end-1)))))
xlabel('time [hour]')
ylabel('$\Phi_{M}$ [deg]')
subplot(2,4,4)
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(EstQSO.P_t(13,13,1:end-1)))))
grid on;
hold on;
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(Est3D.P_t(13,13,1:end-1)))))
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(EstSwing.P_t(13,13,1:end-1)))))
xlabel('time [hour]')
ylabel('$\Phi_{Ph}$ [deg]')

subplot(2,4,5)
semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(EstQSO.P_t(8,8,1:end-1)))))
grid on;
hold on;
semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est3D.P_t(8,8,1:end-1)))))
semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(EstSwing.P_t(8,8,1:end-1)))))
xlabel('time [hour]')
ylabel('$\dot{R}_{Ph}$ [km/s]')
subplot(2,4,6)
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(EstQSO.P_t(10,10,1:end-1)))))
grid on;
hold on;
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(Est3D.P_t(10,10,1:end-1)))))
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(EstSwing.P_t(10,10,1:end-1)))))
xlabel('time [hour]')
ylabel('$\dot{\theta}_{Ph}$ [deg/s]')
subplot(2,4,7)
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(EstQSO.P_t(12,12,1:end-1)))))
grid on;
hold on;
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(Est3D.P_t(12,12,1:end-1)))))
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(EstSwing.P_t(12,12,1:end-1)))))
xlabel('time [hour]')
ylabel('$\dot{\Phi}_{M}$ [deg/s]')
subplot(2,4,8)
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(EstQSO.P_t(14,14,1:end-1)))))
grid on;
hold on;
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(Est3D.P_t(14,14,1:end-1)))))
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(EstSwing.P_t(14,14,1:end-1)))))
xlabel('time [hour]')
ylabel('$\dot{\Phi}_{Ph}$ [deg/s]')



%% Confronto geometrie sulla Lb
    
load('QSOLb_PN_30days.mat');
EstQSO  = Est;
load('3DQSOLb_PN_30days.mat');
Est3D   = Est;
load('SwingQSOLb_PN_30days.mat');
EstSwing = Est;

[par,~] = MMX_DefineNewModelParametersAndUnits;
C_20    = par.SH.Clm(3,1);
C_22    = par.SH.Clm(3,3);

t_obs   = EstQSO.t(1,:);

[SQRT_X_QSO, SQRT_V_QSO, P_t_C_20_QSO, P_t_C_22_QSO, meanQSO, stdQSO, meanVQSO, stdVQSO,...
    meanLibQSO, stdLibQSO,] = Envelopes_Plot(EstQSO, fine, fineCss);
[SQRT_X_3D, SQRT_V_3D, P_t_C_20_3D, P_t_C_22_3D, mean3D, std3D, meanV3D, stdV3D,...
    meanLib3D, stdLib3D] = Envelopes_Plot(Est3D, fine, fineCss);
[SQRT_X_Swing, SQRT_V_Swing, P_t_C_20_Swing, P_t_C_22_Swing, meanSwing, stdSwing, meanVSwing, stdVSwing,...
    meanLibSwing, stdLibSwing] = Envelopes_Plot(EstSwing, fine, fineCss);

C_20_QSO_perc= abs(3*real(sqrt(P_t_C_20_QSO))/C_20);
C_22_QSO_perc= abs(3*real(sqrt(P_t_C_22_QSO))/C_22);

C_20_3D_perc= abs(3*real(sqrt(P_t_C_20_3D))/C_20);
C_22_3D_perc= abs(3*real(sqrt(P_t_C_22_3D))/C_22);

C_20_Swing_perc= abs(3*real(sqrt(P_t_C_20_Swing))/C_20);
C_22_Swing_perc= abs(3*real(sqrt(P_t_C_22_Swing))/C_22);

figure()
subplot(1,2,1)
semilogy(t_obs(1:end-1)/3600,3.*real(sqrt(P_t_C_20_QSO)),'LineWidth',1.2);
grid on;
hold on;
semilogy(t_obs(1:end-1)/3600,3.*real(sqrt(P_t_C_20_3D)),'LineWidth',1.2);
semilogy(t_obs(1:end-1)/3600,3.*real(sqrt(P_t_C_20_Swing)),'LineWidth',1.2);
xlabel('Time $[hour]$','Fontsize',26)
ylabel('$3\sigma_{C_{20}}$','Fontsize',30)
legend('QSO','3D-QSO','Swing-QSO','Interpreter','latex','Fontsize',26)
subplot(1,2,2)
semilogy(t_obs(1:end-1)/3600,3.*real(sqrt(P_t_C_22_QSO)),'LineWidth',1.2);
grid on;
hold on;
semilogy(t_obs(1:end-1)/3600,3.*real(sqrt(P_t_C_22_3D)),'LineWidth',1.2);
semilogy(t_obs(1:end-1)/3600,3.*real(sqrt(P_t_C_22_Swing)),'LineWidth',1.2);
xlabel('Time $[hour]$','Fontsize',26)
ylabel('$3\sigma_{C_{22}}$','Fontsize',30)
legend('QSO','3D-QSO','Swing-QSO','Interpreter','latex','Fontsize',26)

figure()
subplot(1,2,1)
semilogy(t_obs(1:end-1)/3600,C_20_QSO_perc,'LineWidth',1.2);
grid on;
hold on;
semilogy(t_obs(1:end-1)/3600,C_20_3D_perc,'LineWidth',1.2);
semilogy(t_obs(1:end-1)/3600,C_20_Swing_perc,'LineWidth',1.2);
xlabel('Time $[hour]$','Fontsize',26)
ylabel('%','Fontsize',26)
legend('QSO','3D-QSO','Swing-QSO','Interpreter','latex','Fontsize',26)
subplot(1,2,2)
semilogy(t_obs(1:end-1)/3600,C_22_QSO_perc,'LineWidth',1.2);
grid on;
hold on;
semilogy(t_obs(1:end-1)/3600,C_22_3D_perc,'LineWidth',1.2);
semilogy(t_obs(1:end-1)/3600,C_22_Swing_perc,'LineWidth',1.2);
xlabel('Time $[hour]$','Fontsize',26)
ylabel('%','Fontsize',26)
legend('QSO','3D-QSO','Swing-QSO','Interpreter','latex','Fontsize',26)



figure()
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(EstQSO.P_t(13,13,1:end-1)))),'LineWidth',1.2)
grid on;
hold on;
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(Est3D.P_t(13,13,1:end-1)))),'LineWidth',1.2)
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(EstSwing.P_t(13,13,1:end-1)))),'LineWidth',1.2)
title('Libration angle $3\sigma$ envelopes','FontSize',26)
xlabel('Time [hour]','Interpreter','latex','Fontsize',26)
ylabel('$3\sigma_{\Phi_{Ph}}$ [deg]','Interpreter','latex','Fontsize',30)
legend('QSO','3D-QSO','Swing-QSO','Interpreter','latex','Fontsize',26)
ylim([5e-1,1e1])

%   MMX Position and velocity

figure()
subplot(1,2,1)
semilogy(t_obs(1:end-1)/3600,SQRT_X_QSO)
hold on;
grid on;
semilogy(t_obs(1:end-1)/3600,SQRT_X_3D)
semilogy(t_obs(1:end-1)/3600,SQRT_X_Swing)
xlabel('time $[hour]$')
ylabel('$[km]$')
title('MMX position vector $3\sigma$ envelopes','Interpreter','latex','FontSize',16)
legend('QSO','3D-QSO','Swing-QSO','Interpreter','latex','FontSize',14)
subplot(1,2,2)
semilogy(t_obs(1:end-1)/3600,SQRT_V_QSO)
hold on;
grid on;
semilogy(t_obs(1:end-1)/3600,SQRT_V_3D)
semilogy(t_obs(1:end-1)/3600,SQRT_V_Swing)
xlabel('time $[hour]$')
ylabel('$[km/s]$')
title('MMX velocity vector $3\sigma$ envelopes','Interpreter','latex','FontSize',16)
legend('QSO','3D-QSO','Swing-QSO','Interpreter','latex','FontSize',14)

% Phobos states

figure()
subplot(2,4,1)
semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(EstQSO.P_t(7,7,1:end-1)))))
grid on;
hold on;
semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est3D.P_t(7,7,1:end-1)))))
semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(EstSwing.P_t(7,7,1:end-1)))))
xlabel('time [hour]')
ylabel('$R_{Ph}$ [km]')
subplot(2,4,2)
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(EstQSO.P_t(9,9,1:end-1)))))
grid on;
hold on;
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(Est3D.P_t(9,9,1:end-1)))))
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(EstSwing.P_t(9,9,1:end-1)))))
xlabel('time [hour]')
ylabel('$\theta_{Ph}$ [deg]')
subplot(2,4,3)
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(EstQSO.P_t(11,11,1:end-1)))))
grid on;
hold on;
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(Est3D.P_t(11,11,1:end-1)))))
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(EstSwing.P_t(11,11,1:end-1)))))
xlabel('time [hour]')
ylabel('$\Phi_{M}$ [deg]')
subplot(2,4,4)
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(EstQSO.P_t(13,13,1:end-1)))))
grid on;
hold on;
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(Est3D.P_t(13,13,1:end-1)))))
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(EstSwing.P_t(13,13,1:end-1)))))
xlabel('time [hour]')
ylabel('$\Phi_{Ph}$ [deg]')

subplot(2,4,5)
semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(EstQSO.P_t(8,8,1:end-1)))))
grid on;
hold on;
semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(Est3D.P_t(8,8,1:end-1)))))
semilogy(t_obs(1:end-1)/3600,3.*squeeze(real(sqrt(EstSwing.P_t(8,8,1:end-1)))))
xlabel('time [hour]')
ylabel('$\dot{R}_{Ph}$ [km/s]')
subplot(2,4,6)
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(EstQSO.P_t(10,10,1:end-1)))))
grid on;
hold on;
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(Est3D.P_t(10,10,1:end-1)))))
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(EstSwing.P_t(10,10,1:end-1)))))
xlabel('time [hour]')
ylabel('$\dot{\theta}_{Ph}$ [deg/s]')
subplot(2,4,7)
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(EstQSO.P_t(12,12,1:end-1)))))
grid on;
hold on;
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(Est3D.P_t(12,12,1:end-1)))))
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(EstSwing.P_t(12,12,1:end-1)))))
xlabel('time [hour]')
ylabel('$\dot{\Phi}_{M}$ [deg/s]')
subplot(2,4,8)
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(EstQSO.P_t(14,14,1:end-1)))))
grid on;
hold on;
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(Est3D.P_t(14,14,1:end-1)))))
semilogy(t_obs(1:end-1)/3600,180/pi*3.*squeeze(real(sqrt(EstSwing.P_t(14,14,1:end-1)))))
xlabel('time [hour]')
ylabel('$\dot{\Phi}_{Ph}$ [deg/s]')
