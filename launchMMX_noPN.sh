#!/bin/bash
 
#SBATCH --partition=shared
#SBATCH --job-name="MMX_EDO"
#SBATCH --nodes=1
#SBATCH --time=06-23:59:59
#SBATCH --ntasks-per-node=10
#SBATCH --mem=40G   #maximum is 128G on shared partition (or use –-contraint=op and –-exclusive in separate lines)

#SBATCH -o slurm_MMX_EDO.%N.%j.out
#SBATCH -e slurm_MMX_EDO.%N.%j.err
 
# Specify when we should receive e-mail about the job - in this case if it ends or fails
#SBATCH --mail-type=ALL
#SBATCH --mail-user=ec01259@surrey.ac.uk

cd /users/ec01259/MMX_Edo/paper_after_conf

module load matlab
 
matlab -nosplash -nodesktop -nodisplay -r "run('HPC_noPN.m'); exit"
