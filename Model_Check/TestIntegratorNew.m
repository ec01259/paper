clear
close all
clc

%%  Test model

    warning('off', 'all'); format longG;
    set(0,'DefaultTextInterpreter','latex');
    set(0,'DefaultAxesFontSize', 16);

%   Mac
    addpath('../MMX_Fcn_CovarianceAnalyses');
    addpath('../');
    addpath('../../useful_functions/');
    addpath(genpath('../../mice'));
    addpath(genpath('../../generic_kernels/'));
    addpath(genpath('../MMX_Product/MMX_FullEphemeris_BSP_Files/'));
    MMX_InitializeSPICE
    cspice_furnsh(which('mar097.bsp'));
    cspice_furnsh('MARPHOFRZ.txt');

%   Windows
%     addpath('MMX_Fcn_CovarianceAnalyses');
%     addpath('../AOD');
%     addpath(genpath('../MICE/mice'));
%     cspice_furnsh('kernels_IntegratorTest.txt');
    
  
%   Model parameters
    [par, units] = MMX_DefineNewModelParametersAndUnits;

%%  Integration Phobos's new model

%   Initial state

    startdate   = '2026-03-16 00:00:00 (UTC)';
    data        = cspice_str2et(startdate);
    [Ph, par]   = Phobos_States_NewModelPiu(data,par);
    Ph          = Ph./units.sfVec2;
    day         = 24*3600;

%   Initial Phobos's state vector
    St0     = [Ph; reshape(eye(par.d2),[par.d2^2,1])];

%   Integration
    tspan   = [0, 2*day]*units.tsf;
    RelTol  = 1e-13;
    AbsTol  = 1e-16; 
    opt     = odeset('RelTol',RelTol,'AbsTol',AbsTol);
    [t,X]   = ode113(@(t,X) MP_CoupledLibration(t,X,par),tspan,St0,opt);

%%  Integrali del moto senza MMX

%   Total momentum
    Iz          = par.IPhz_bar + par.ni*X(:,1).^2;
    Theta1_dot  = X(:,4) + X(:,6);
    K           = Iz.*X(:,4) + par.IPhz_bar*X(:,8);
    Ktot        = K + par.IMz_bar*Theta1_dot;
    K1          = K(1);

%   Potential energy
    V       = -par.ni./X(:,1).*(1 + 1./(2*X(:,1).^2).*...
        ((par.IMz_bar - par.Is) - .5*par.IPhx_bar - .5*par.IPhy_bar + par.IPhz_bar ...
        + 1.5*(par.IPhy_bar - par.IPhx_bar)*cos(2 * X(:,7))));

%   Kinetic energy
    T       = .5*par.IMz_bar*Theta1_dot.^2 + .5*par.IPhz_bar*X(:,8).^2 + ...
        .5*par.ni*X(:,2).^2 + .5*(par.IPhz_bar + par.ni*X(:,1).^2).*X(:,4).^2 + ...
        par.IPhz_bar*X(:,8).*X(:,4);

%   Total energy
    Etot    = T + V;

%   Integral of motion's plot
    figure(1)
    subplot(2,1,1)
    plot(t/3600, Etot)
    grid on;
    subplot(2,1,2)
    plot(t/3600, Ktot)
    grid on;

%   States' evolution

    theta_t     = mod(X(:,3),2*pi);
    Phi1_t      = mod(X(:,5),2*pi);
    Phi2_t      = mod(X(:,7),2*pi);
    
    figure(2)
    subplot(2,4,1)
    grid on;
    hold on;
    plot(t/units.tsf,X(:,1)*units.lsf);
    ylabel('radius')
    subplot(2,4,5)
    grid on;
    hold on;
    plot(t/units.tsf,X(:,2)*units.vsf);
    ylabel('Rdot')
    subplot(2,4,2)
    grid on;
    hold on;
    plot(t/units.tsf, theta_t);
    ylabel('Theta')
    subplot(2,4,6)
    grid on;
    hold on;
    plot(t/units.tsf, X(:,4)*units.tsf);
    ylabel('Thetadot')
    subplot(2,4,3)
    grid on;
    hold on;
    plot(t/units.tsf, Phi1_t);
    ylabel('Phi1')
    subplot(2,4,7)
    grid on;
    hold on;
    plot(t/units.tsf, X(:,6)*units.tsf);
    ylabel('Phi1dot')
    subplot(2,4,4)
    grid on;
    hold on;
    plot(t/units.tsf, Phi2_t);
    ylabel('Phi2')
    subplot(2,4,8)
    grid on;
    hold on;
    plot(t/units.tsf, X(:,8)*units.tsf);
    ylabel('Phi2dot')
    

%   Difference wrt SPICE
    
    t = t/units.tsf;
    r_Ph_t      = zeros(1,size(t,1));
    rdot_Ph_t   = zeros(1,size(t,1));
    theta_t     = zeros(1,size(t,1));
    thetadot_t  = zeros(1,size(t,1));
    Phi1_t      = zeros(1,size(t,1));
    
    Phi1dot_t   = zeros(1,size(t,1));
    Phi2_t      = zeros(1,size(t,1));
    Phi2dot_t   = zeros(1,size(t,1));
    
    for i = 1:size(t,1)

        time        = data+t(i);
        Phobos_t    = cspice_spkezr('401', time, 'MARSIAU', 'none', '499');
        r           = Phobos_t(1:3);
        v           = Phobos_t(4:6);
        h           = cross(r,v);

        Ph_0        = par.MARSIAU2perifocal*r;
        theta_t(i)  = atan2(Ph_0(2),Ph_0(1));
        thetadot_t(i)   = norm(h)/norm(r)^2;
        rdot_Ph_t(i)    = dot(r,v)/norm(r);
        r_Ph_t(i)       = norm(Phobos_t(1:3));

        Phobos_t_ORB    = cspice_spkezr('401', time, 'MARPHOFRZ', 'none', '499');
        er              = -Phobos_t_ORB(1:3)/norm(Phobos_t_ORB(1:3));
        eh              = cross(Phobos_t_ORB(1:3),Phobos_t_ORB(4:6))/...
            norm(cross(Phobos_t_ORB(1:3),Phobos_t_ORB(4:6)));
        ey              = cross(eh,er);
        NO              = [er,ey,eh];
        no              = [-er,cross(eh,-er),eh];
        
        BN          = cspice_sxform('MARPHOFRZ','IAU_MARS',time);
        BO          = BN(1:3,1:3)*no;
        [Phi1_t(i),ea2,ea3]  = cspice_m2eul(BO,3,2,1);
    
        Wtilde      = -BN(4:6, 1:3)*BN(1:3, 1:3)';
        w_BN        = [-Wtilde(2,3); Wtilde(1,3); -Wtilde(1,2)];
        w_BO        = w_BN - BO*[0; 0; thetadot_t(i)];
        Phi1dot_t(i)= (sin(ea3)*w_BO(2) + cos(ea3)*w_BO(3))/cos(ea2);

        BN          = cspice_sxform('MARPHOFRZ','IAU_PHOBOS',time);
        R_In2PhPA   = par.PB*BN(1:3,1:3);
        BO          = R_In2PhPA*NO;
        [Phi2_t(i),ea2,ea3]  = cspice_m2eul(BO,3,2,1);

        Wtilde      = -BN(4:6, 1:3)*BN(1:3, 1:3)';
        w_BN        = [-Wtilde(2,3); Wtilde(1,3); -Wtilde(1,2)];
        w_BO        = w_BN - BO*[0; 0; thetadot_t(i)];
        Phi2dot_t(i)= (sin(ea3)*w_BO(2) + cos(ea3)*w_BO(3))/cos(ea2);

    end
    
%   Raggio Spice and integration
    figure()
    subplot(2,4,1)
    plot(t/3600,X(:,1)*units.lsf);
    grid on;
    hold on;
    plot(t/3600,r_Ph_t);
    ylabel('$r_{Ph}$ [km]','Fontsize',26)
    xlabel('[hours]','Fontsize',26)
    legend('Integrated','Spice','Fontsize',22)
    subplot(2,4,5)
    plot(t/3600,X(:,1)*units.lsf-r_Ph_t(:));
    grid on;
    ylabel('$\Delta r_{Ph}$ [km]','Fontsize',26)
    xlabel('[hours]','Fontsize',26)

%   R_dot Spice and integration
    subplot(2,4,2)
    plot(t/3600,X(:,2)*units.vsf);
    grid on;
    hold on;
    plot(t/3600,rdot_Ph_t);
    ylabel('$\dot{r}_{Ph}$  [km/s]','Fontsize',26)
    xlabel('[hours]','Fontsize',26)
    legend('Integrated','Spice','Fontsize',22)
    subplot(2,4,6)
    plot(t/3600,X(:,2)*units.vsf-rdot_Ph_t(:));
    grid on;
    ylabel('$\Delta \dot{r}_{Ph}$ [km/s]','Fontsize',26)
    xlabel('[hours]','Fontsize',26)

%   Theta Spice and integration    
    subplot(2,4,3)
    plot(t/3600,mod(X(:,3),2*pi));
    grid on;
    hold on;
    plot(t/3600,mod(theta_t,2*pi));
    ylabel('$\theta$ [rad]','Fontsize',26)
    xlabel('[hours]','Fontsize',26)
    legend('Integrated','Spice','Fontsize',22)
    subplot(2,4,7)
    plot(t/3600,angdiff(X(:,3)',theta_t));
    grid on;
    ylabel('$\Delta \theta$ [rad]','Fontsize',26)
    xlabel('[hours]','Fontsize',26)

%   Theta_dot Spice and integration    
    subplot(2,4,4)
    plot(t/3600,X(:,4)*units.tsf);
    grid on;
    hold on;
    plot(t/3600,thetadot_t);
    ylabel('$\dot{\theta}$ [rad/s]','Fontsize',26)
    xlabel('[hours]','Fontsize',26)
    legend('Integrated','Spice','Fontsize',22)
    subplot(2,4,8)
    plot(t/3600,X(:,4)*units.tsf-thetadot_t(:));
    grid on;
    ylabel('$\Delta \dot{\theta}$ [rad/s]','Fontsize',26)
    xlabel('[hours]','Fontsize',26)

%   Phi1 Spice and integration    
    figure()
    subplot(2,4,1)
    plot(t/3600,mod(X(:,5),2*pi));
    grid on;
    hold on;
    plot(t/3600,mod(Phi1_t,2*pi));
    ylabel('$\Phi_{M}$ [rad]','Fontsize',26)
    xlabel('[hours]','Fontsize',26)
    legend('Integrated','Spice','Fontsize',22)
    subplot(2,4,5)
    plot(t/3600,angdiff(X(:,5)',Phi1_t));
    grid on;
    ylabel('$\Delta \Phi_{M}$ [rad]','Fontsize',26)
    xlabel('[hours]','Fontsize',26)

%   Phi1_dot Spice and integration    
    subplot(2,4,2)
    plot(t/3600,X(:,6)*units.tsf);
    grid on;
    hold on;
    plot(t/3600,Phi1dot_t);
    ylabel('$\dot{\Phi}_{M}$ [rad/s]','Fontsize',26)
    xlabel('[hours]','Fontsize',26)
    legend('Integrated','Spice','Fontsize',22)
    subplot(2,4,6)
    plot(t/3600,X(:,6)*units.tsf-Phi1dot_t(:));
    grid on;
    ylabel('$\Delta \dot{\Phi}_{M}$ [rad/s]','Fontsize',26)
    xlabel('[hours]','Fontsize',26)

%   Phi2 Spice and integration
    subplot(2,4,3)
    plot(t/3600,mod(X(:,7),2*pi));
    grid on;
    hold on;
    plot(t/3600,mod(Phi2_t,2*pi));
    ylabel('$\Phi_{Ph}$ [rad]','Fontsize',26)
    xlabel('[hours]','Fontsize',26)
    legend('Integrated','Spice','Fontsize',22)
    subplot(2,4,7)
    plot(t/3600,angdiff(X(:,7)',Phi2_t));
    grid on;
    ylabel('$\Delta \Phi_{Ph}$ [rad]','Fontsize',26)
    xlabel('[hours]','Fontsize',26)

%   Phi2_dot Spice and integration    
    subplot(2,4,4)
    plot(t/3600,X(:,8)*units.tsf);
    grid on;
    hold on;
    plot(t/3600,Phi2dot_t);
    ylabel('$\dot{\Phi}_{Ph}$ [rad/s]','Fontsize',26)
    xlabel('[hours]','Fontsize',26)
    legend('Integrated','Spice','Fontsize',22)
    subplot(2,4,8)
    plot(t/3600,X(:,8)*units.tsf-Phi2dot_t(:));
    grid on;
    ylabel('$\Delta \dot{\Phi}_{Ph}$ [rad/s]','Fontsize',26)
    xlabel('[hours]','Fontsize',26)


%   Comparison wrt x,y,z in MarsIAU reference frame

% r_SPICE = zeros(3,size(t,1));
% r_Model = zeros(3,size(t,1));
% 
%     for i = 1:size(t,1)
% 
%         time        = data+t(i);
%         Phobos_t    = cspice_spkezr('401', time, 'MARSIAU', 'none', '499');
%         r_SPICE(:,i)= Phobos_t(1:3);
% 
%         theta   = X(i,3);
% %         Phi     = X(i,5);
% %         theta   = theta+Phi;
% %         theta = theta_t(i);
%         r_Model(:,i)= par.MARSIAU2perifocal'*[X(i,1)*cos(theta); X(i,1)*sin(theta); 0]*units.lsf;
% 
%     end
% 
%     figure()
%     subplot(2,3,1)
%     plot(t/3600, r_Model(1,:))
%     grid on;
%     hold on;
%     plot(t/3600, r_SPICE(1,:))
%     xlabel('[hours]')
%     ylabel('X')
%     legend('model','SPICE')
%     subplot(2,3,2)
%     plot(t/3600, r_Model(2,:))
%     grid on;
%     hold on;
%     plot(t/3600, r_SPICE(2,:))
%     xlabel('[hours]')
%     ylabel('Y')
%     legend('model','SPICE')
%     subplot(2,3,3)
%     plot(t/3600, r_Model(3,:))
%     grid on;
%     hold on;
%     plot(t/3600, r_SPICE(3,:))
%     xlabel('[hours]')
%     ylabel('Z')
%     legend('model','SPICE')
%     subplot(2,3,4)
%     plot(t/3600, r_Model(1,:)-r_SPICE(1,:))
%     grid on;
%     hold on;
%     xlabel('[hours]')
%     ylabel('$\Delta$X')
%     subplot(2,3,5)
%     plot(t/3600, r_Model(2,:)-r_SPICE(2,:))
%     grid on;
%     hold on;
%     xlabel('[hours]')
%     ylabel('$\Delta$ Y')
%     subplot(2,3,6)
%     plot(t/3600, r_Model(3,:)-r_SPICE(3,:))
%     grid on;
%     hold on;
%     xlabel('[hours]')
%     ylabel('$\Delta$ Z')
%     


%%  Keplerian model

% %   Propagation
%     Phobos_0    = cspice_spkezr('401', data, 'MARSIAU', 'none', '499');
%     r           = Phobos_0(1:3);
%     v           = Phobos_0(4:6);
% 
%     St0         = [r; v];
%     opt         = odeset('RelTol',RelTol,'AbsTol',AbsTol);
%     [t1,X1]     = ode113(@(t,x) TwoBP(t,x,par),tspan/units.tsf,St0,opt);
% 
% %   Comparison
%     RPh_t       = zeros(size(X,1),1);
%     RPhdot_t    = zeros(size(X,1),1);
%     Theta_t     = zeros(size(X,1),1);
%     Thetadot_t  = zeros(size(X,1),1);
% 
%     for i = 1:size(X,1)
% 
%         RPh_t(i)        = norm(X1(i,1:3));
%         Theta_t(i)      = atan2(X1(i,2),X1(i,1));
%         h               = cross(X1(i,1:3),X1(i,4:6));
%         Thetadot_t(i)   = norm(h)/norm(X1(i,1:3))^2;
%         RPhdot_t(i)     = dot(X1(i,1:3),X1(i,4:6))/norm(X1(i,1:3));
%         
%     end 
% 
% 
% %   Raggio Spice and integration
%     figure()
%     subplot(2,4,1)
%     plot(t/3600,RPh_t(:));
%     grid on;
%     hold on;
%     plot(t/3600,r_Ph_t);
%     ylabel('radius [km]')
%     xlabel('[hours]')
%     legend('Integrated','2BP')
%     subplot(2,4,5)
%     plot(t/3600,RPh_t(:)-r_Ph_t(:));
%     grid on;
%     ylabel('err radius [km]')
%     xlabel('[hours]')
% 
% %   R_dot Spice and integration
%     subplot(2,4,2)
%     plot(t/3600,RPhdot_t(:));
%     grid on;
%     hold on;
%     plot(t/3600,rdot_Ph_t);
%     ylabel('Rdot [km/s]')
%     xlabel('[hours]')
%     legend('Integrated','2BP')
%     subplot(2,4,6)
%     plot(t/3600,RPhdot_t(:)-rdot_Ph_t(:));
%     grid on;
%     ylabel('err Rdot [km/s]')
%     xlabel('[hours]')
%     
% %   Theta_dot Spice and integration    
%     subplot(2,4,3)
%     plot(t/3600,Theta_t(:));
%     grid on;
%     hold on;
%     plot(t/3600,theta_t);
%     ylabel('Theta [rad]')
%     xlabel('[hours]')
%     legend('Integrated','2BP')
%     subplot(2,4,7)
%     plot(t/3600,angdiff(Theta_t',theta_t));
%     grid on;
%     ylabel('err Theta [rad]')
%     xlabel('[hours]')
% 
% %   Theta_dot Spice and integration    
%     subplot(2,4,4)
%     plot(t/3600,Thetadot_t(:));
%     grid on;
%     hold on;
%     plot(t/3600,thetadot_t);
%     ylabel('Thetadot [rad/s]')
%     xlabel('[hours]')
%     legend('Integrated','2BP')
%     subplot(2,4,8)
%     plot(t/3600,Thetadot_t(:)-thetadot_t(:));
%     grid on;
%     ylabel('err Thetadot [rad/s]')
%     xlabel('[hours]')
% 
% 
%     figure()
%     subplot(2,3,1)
%     plot(t/3600, X1(:,1))
%     grid on;
%     hold on;
%     plot(t/3600, r_SPICE(1,:))
%     xlabel('[hours]')
%     ylabel('X')
%     legend('2BP','SPICE')
%     subplot(2,3,2)
%     plot(t/3600, X1(:,2))
%     grid on;
%     hold on;
%     plot(t/3600, r_SPICE(2,:))
%     xlabel('[hours]')
%     ylabel('Y')
%     legend('2BP','SPICE')
%     subplot(2,3,3)
%     plot(t/3600, X1(:,3))
%     grid on;
%     hold on;
%     plot(t/3600, r_SPICE(3,:))
%     xlabel('[hours]')
%     ylabel('Z')
%     legend('2BP','SPICE')
%     subplot(2,3,4)
%     plot(t/3600, X1(:,1)'-r_SPICE(1,:))
%     grid on;
%     hold on;
%     xlabel('[hours]')
%     ylabel('$\Delta$X')
%     subplot(2,3,5)
%     plot(t/3600, X1(:,2)'-r_SPICE(2,:))
%     grid on;
%     hold on;
%     xlabel('[hours]')
%     ylabel('$\Delta$ Y')
%     subplot(2,3,6)
%     plot(t/3600, X1(:,3)'-r_SPICE(3,:))
%     grid on;
%     hold on;
%     xlabel('[hours]')
%     ylabel('$\Delta$ Z')
