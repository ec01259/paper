clear
close all
clc

%%  Test model

    warning('on', 'all'); format longG;
    set(0,'DefaultTextInterpreter','latex');
    set(0,'DefaultAxesFontSize', 16);

%   Mac
    addpath('../');
    addpath('../MMX_Fcn_CovarianceAnalyses');
    addpath('../../useful_functions/');
    addpath(genpath('../../mice'));
    addpath(genpath('../../generic_kernels/'));
    addpath(genpath('../MMX_Product/MMX_FullEphemeris_BSP_Files/'));
    MMX_InitializeSPICE
    cspice_furnsh(which('mar097.bsp'));
    cspice_furnsh(which('Phobos_826891269_828619269.bsp'));
    cspice_furnsh('MARPHOFRZ.txt');
    
%   Model parameters
    [par, units] = MMX_DefineNewModelParametersAndUnits;


%%  Integration Phobos's new model + MMX

%   Initial MMX's state vector
    startdate   = '2026-03-16 00:00:00 (UTC)';
    data        = cspice_str2et(startdate);
    [Ph, par]   = Phobos_States_NewModel(data,par);

%   Initial Phobos's state vector
    Ph02        = Ph./units.sfVec2;

%   Initial MMX's State vector
    Ph01        = cspice_spkezr('401', data, 'MarsIAU', 'none', '499');
    Ph01(1:3)   = par.MARSIAU2perifocal*Ph01(1:3);
    Ph01(4:6)   = par.MARSIAU2perifocal*Ph01(4:6);
    Ph01        = Ph01./[units.lsf; units.lsf; units.lsf; units.vsf; units.vsf; units.vsf];
    
%   Initial state vector
    St0     = [Ph01; Ph02];

%   Integration
    tspan   = (0:500:10*7.7*3600)*units.tsf;
    RelTol  = 1e-13;
    AbsTol  = 1e-16; 
    opt     = odeset('RelTol',RelTol,'AbsTol',AbsTol);
    [t,X]   = ode113(@(t,X) MPh_CoupledLibrationCheck(t,X,par),tspan,St0,opt);

%   States' evolution

    RPh1        = sqrt(X(:,1).^2 + X(:,2).^2 + X(:,3).^2)*units.lsf;
    RPh1_dot    = dot(X(:,1:3)'*units.lsf,X(:,4:6)'*units.vsf)'./...
        (sqrt(X(:,1).^2 + X(:,2).^2 + X(:,3).^2)*units.lsf);
    theta_1     = zeros(size(t,1),1);
    thetadot_1  = zeros(size(t,1),1);
    
    for i = 1:size(t,1)

        r           = X(i,1:3)'*units.lsf;
        v           = X(i,4:6)'*units.vsf;
        h           = cross(r,v);
        
        theta_1(i)      = mod(atan2(r(2),r(1)),2*pi);
        thetadot_1(i)   = norm(h)/norm(r)^2;

    end

    
%   Raggio, R_dot, Theta e derivata

    figure()
    subplot(2,4,1)
    plot(t,X(:,7)*units.lsf)
    grid on;
    hold on;
    plot(t,RPh1)
    legend('Paper Eq.','From Potential')
    ylabel('km')
    subplot(2,4,2)
    plot(t,X(:,8)*units.vsf)
    grid on;
    hold on;
    plot(t,RPh1_dot)
    legend('Paper Eq.','From Potential')
    ylabel('km/s')
    subplot(2,4,3)
    plot(t,mod(X(:,9),2*pi))
    grid on;
    hold on;
    plot(t,theta_1)
    legend('Paper Eq.','From Potential')
    ylabel('rad')
    subplot(2,4,4)
    plot(t,X(:,10)*units.tsf)
    grid on;
    hold on;
    plot(t,thetadot_1)
    legend('Paper Eq.','From Potential')
    ylabel('rad/s')

    subplot(2,4,5)
    plot(t,X(:,7)*units.lsf - RPh1)
    grid on;
    hold on;
    plot(t,mean(X(:,7)*units.lsf - RPh1)*ones(size(t,1)),'.-','Color','r')
    legend('Difference between the Radii results')
    ylabel('km')
    subplot(2,4,6)
    plot(t,X(:,8)*units.vsf - RPh1_dot)
    grid on;
    hold on;
    plot(t,mean(X(:,8)*units.vsf - RPh1_dot)*ones(size(t,1)),'.-','Color','r')
    legend('Difference between the Vr results')
    ylabel('km/s')
    subplot(2,4,7)
    plot(t,angdiff(X(:,9),theta_1))
    grid on;
    hold on;
    plot(t,mean(angdiff(X(:,9),theta_1))*ones(size(t,1)),'.-','Color','r')
    legend('Difference between the theta results')
    ylabel('rad')
    subplot(2,4,8)
    plot(t,X(:,10)*units.tsf - thetadot_1)
    grid on;
    hold on;
    plot(t,mean(X(:,10)*units.tsf - thetadot_1)*ones(size(t,1)),'.-','Color','r')
    legend('Difference between the theta_dot results')
    ylabel('rad/s')
