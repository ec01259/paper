clear
close all
clc

%%  Test model With MMX

    warning('on', 'all'); format longG;
    set(0,'DefaultTextInterpreter','latex');
    set(0,'DefaultAxesFontSize', 16);

%   Mac
    addpath('../');
    addpath('../MMX_Fcn_CovarianceAnalyses');
    addpath(genpath('../../mice/'));
    addpath(genpath('../../generic_kernels/'));
    addpath(genpath('../../useful_functions/'));
    addpath(genpath('../../dynamical_model/'));
    addpath(genpath('../MMX_Product/MMX_BSP_Files_GravLib/'));
    MMX_InitializeSPICE
    cspice_furnsh(which('mar097.bsp'));
    cspice_furnsh(which('MARPHOFRZ.txt'));
    cspice_furnsh(which('MMX_QSO_031_2x2_826891269_828619269.bsp'));
    cspice_furnsh(which('Phobos_826891269_828619269.bsp'));
    
%   Model parameters
    [par, units]= MMX_DefineNewModelParametersAndUnits;
    alpha       = 13.03;                          %km
    beta        = 11.4;                           %km
    gamma       = 9.14;                           %km
    Phobos      = struct('alpha',alpha/units.lsf,'beta',beta/units.lsf,'gamma',gamma/units.lsf);
    
    
%%  Integration Phobos's new model + MMX

%   Initial MMX's state vector
    startdate   = '2026-03-16 00:00:00 (UTC)';
    data        = cspice_str2et(startdate);
    par.et0     = data;

    [Ph, par]   = Phobos_States_NewModel(data,par);

%   Initial Phobos's state vector
    Ph0     = Ph./units.sfVec2;

%   Initial MMX's State vector
    MMX0    = cspice_spkezr('-34', data, 'MARSIAU', 'none', '499')./units.sfVec;

%   Initial state vector
    St0     = [MMX0; Ph0];
    
%   Integration
    day     = 24*3600;
    tspan   = (0:150:day)*units.tsf;
    RelTol  = 1e-13;
    AbsTol  = 1e-16; 
    opt     = odeset('RelTol',RelTol,'AbsTol',AbsTol);
    [t,X]   = ode113(@(t,X) MPhMMX_CoupledLibration(t,X,par,units),tspan,St0,opt);
    t       = t/units.tsf;

%%  Plot

    
%   QSO orbit

    QSO_t   = zeros(size(X,2),3);
    om_t    = [zeros(2,size(t,1)); X(:,10)'*units.tsf];
    
    for i = 1:size(t,1)
    
        r_Phobos    = [X(i,7)*cos(X(i,9))*units.lsf; X(i,7)*sin(X(i,9))*units.lsf; 0];
        
        r   = [cos(X(i,9)'); sin(X(i,9)'); 0];
        k   = [0;0;1];
        j   = cross(k,r)/norm(cross(k,r));
        NR  = [r,j,k];
        RN  = NR';
        
        QSO_t(i,1:3)= RN*(par.MARSIAU2perifocal*X(i,1:3)'*units.lsf-r_Phobos);
        
    end

    figure(1)
    plot3(QSO_t(:,1),QSO_t(:,2),QSO_t(:,3))
    planetPlot('asteroid',[0;0;0],par.Phobos,1);
    hold on;
    grid on;
    
%%  Definizione del PN random acceleration nel caso di full dynamic

    X_dot_Dyn   = zeros(6,1);
    X_dot_NBP   = zeros(6,1);
    Diff        = zeros(6,1);
    
    Clm = [];
    Slm = [];
    
    for i=1:size(par.SH.Clm,1)
        Clm = [Clm; par.SH.Clm(i,1:i)'];
    end
    for i=2:size(par.SH.Clm,1)
        Slm = [Slm; par.SH.Clm(i,2:i)'];
    end
    
    par.Clm     = Clm;
    par.Slm     = Slm;
    par.nCoeff  = 9;
    par.nBias   = 0;

    for i=2:size(X,1)

        X_dot1         = MPhMMX_CoupledLibration(t(i)*units.tsf,X(i,:)',par);
        X_dot_Dyn(:,i) = X_dot1(1:6).*units.sfVec*units.tsf;
        ST_Phobos      = cspice_spkezr('401', data+t(i), 'MarsIAU', 'none', '499')./units.sfVec;
        rPh            = par.MARSIAU2perifocal'*[X(i,7)*cos(X(i,9)); X(i,7)*sin(X(i,9)); 0];
        rsb = (ST_Phobos(1:3) - rPh)*units.lsf;
        X_NBP          = [X(i,1:6)'; ST_Phobos; par.Clm; par.Slm; zeros(21*21,1)];
        X_dot2         = NBP_check(t(i)*units.tsf,X_NBP,par);
        X_dot_NBP(:,i) = X_dot2(1:6).*units.sfVec*units.tsf;

        Diff(:,i) = (X_dot_Dyn(:,i) - X_dot_NBP(:,i));

    end


    figure()
    subplot(2,3,1)
    plot(t/3600,X_dot_Dyn(4,:))
    grid on;
    hold on;
    plot(t/3600,X_dot_NBP(4,:))
    xlabel('[hour]')
    ylabel('$Acc_{xx}$')
    legend('New model','NBP')
    subplot(2,3,2)
    plot(t/3600,X_dot_Dyn(5,:))
    grid on;
    hold on;
    plot(t/3600,X_dot_NBP(5,:))
    xlabel('[hour]')
    ylabel('$Acc_{yy}$')
    legend('New model','NBP')
    subplot(2,3,3)
    plot(t/3600,X_dot_Dyn(6,:))
    grid on;
    hold on;
    plot(t/3600,X_dot_NBP(6,:))
    xlabel('[hour]')
    ylabel('$Acc_{zz}$')
    legend('New model','NBP')
    subplot(2,3,4)
    plot(t/3600,Diff(4,:))
    grid on
    xlabel('[hour]')
    ylabel('$\Delta Acc_{xx}$')
    subplot(2,3,5)
    plot(t/3600,Diff(5,:))
    grid on
    xlabel('[hour]')
    ylabel('$\Delta Acc_{yy}$')
    subplot(2,3,6)
    plot(t/3600,Diff(6,:))
    grid on
    xlabel('[hour]')
    ylabel('$\Delta Acc_{zz}$')


