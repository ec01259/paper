clear
close all
clc

%%  Test model With MMX

    warning('on', 'all'); format longG;
    set(0,'DefaultTextInterpreter','latex');
    set(0,'DefaultAxesFontSize', 16);

%   Mac
    restoredefaultpath
    addpath('../');
    addpath('../MMX_Fcn_CovarianceAnalyses');
    addpath(genpath('../../mice/'));
    addpath(genpath('../../generic_kernels/'));
    addpath(genpath('../../useful_functions/'));
    addpath(genpath('../../dynamical_model/'));
    addpath(genpath('../MMX_Product/MMX_BSP_Files_GravLib/'));
    MMX_InitializeSPICE
    cspice_furnsh(which('MARPHOFRZ.txt'));
    cspice_furnsh(which('mar097.bsp'));
    cspice_furnsh(which('MMX_QSO_031_2x2_826891269_828619269.bsp'));
    cspice_furnsh(which('Phobos_826891269_828619269.bsp'));
    
%   Model parameters
    [par, units]= MMX_DefineNewModelParametersAndUnits;
    alpha       = 13.03;                          %km
    beta        = 11.4;                           %km
    gamma       = 9.14;                           %km
    Phobos      = struct('alpha',alpha/units.lsf,'beta',beta/units.lsf,'gamma',gamma/units.lsf);
    
    
%%  Integration Phobos's new model + MMX

%   Initial MMX's state vector
    startdate   = '2026-03-16 00:00:00 (UTC)';
    data        = cspice_str2et(startdate);
    par.et0     = data;

    [Ph, par]   = Phobos_States_NewModel(data,par);

%   Initial Phobos's state vector
    Ph0     = Ph./units.sfVec2;

%   Initial MMX's State vector
    MMX0    = cspice_spkezr('-34', data, 'MARSIAU', 'none', '499')./units.sfVec;

%   Initial state vector
    St0     = [MMX0; Ph0; par.IPhx_bar; par.IPhy_bar; par.IPhz_bar];
    
%   Integration
    day     = 24*3600;
    tspan   = (0:150:5*day)*units.tsf;
    RelTol  = 1e-13;
    AbsTol  = 1e-16; 
    opt     = odeset('RelTol',RelTol,'AbsTol',AbsTol);
    [t,X]   = ode113(@(t,X) Dynamics_MPHandMMX_Inertia(t,X,par,units),tspan,St0,opt);


%%  Recovering the accelerations

    Acc_NewModel    = zeros(3,size(X,1));
    Acc_NBodyModel  = zeros(3,size(X,1));
    [pars, unit]    = MMX_DefineModelParametersAndUnits([8,8]);
    [pars, unit]    = MMX_CovarianceAnalysisParameters(pars, unit);

    for i = 1:size(X,1)

        St_dot = Dynamics_MPHandMMX_Inertia(t(i),X(i,:)',par,units);
        Acc_NewModel(:,i)   = St_dot(4:6)*units.vsf*units.tsf;
        
        MMX    = cspice_spkezr('-34', data+t(i)/units.tsf, 'MARSIAU', 'none', '499')./unit.sfVec;
        Phobos = cspice_spkezr('-401', data+t(i)/units.tsf, 'MARSIAU', 'none', '499')./unit.sfVec;

        X_i    = [MMX; Phobos; pars.Clm; pars.Slm; 0; 0; 0; 0; 0];
        Xold   = [X_i; reshape(eye(size(X_i,1)),[size(X_i,1)^2,1])];
        St_dot = MP_Circular_all(t,Xold,pars,unit);
        Acc_NBodyModel(:,i)  = St_dot(4:6)*unit.vsf/unit.tsf;

    end

%%  Plot

    figure()
    subplot(2,3,1)
    plot(t/units.tsf,Acc_NewModel(1,:));
    hold on;
    grid on;
    ylabel('$Acc_x [km/s^2]$')
    plot(t/units.tsf,Acc_NBodyModel(1,:));
    legend('New Model','Nbody');
    subplot(2,3,2)
    plot(t/units.tsf,Acc_NewModel(2,:));
    hold on;
    grid on;
    ylabel('$Acc_y [km/s^2]$')
    plot(t/units.tsf,Acc_NBodyModel(2,:));
    legend('New Model','Nbody');
    subplot(2,3,3)
    plot(t/units.tsf,Acc_NewModel(3,:));
    hold on;
    grid on;
    ylabel('$Acc_z [km/s^2]$')
    plot(t/units.tsf,Acc_NBodyModel(3,:));
    legend('New Model','Nbody');
    subplot(2,3,4)
    plot(t/units.tsf,Acc_NewModel(1,:)-Acc_NBodyModel(1,:));
    hold on;
    grid on;
    plot(t/units.tsf,ones(1,size(X,1))*mean(Acc_NewModel(1,:)-Acc_NBodyModel(1,:)));
    ylabel('$\Delta Acc_x [km/s^2]$')
    legend('Difference');
    subplot(2,3,5)
    plot(t/units.tsf,Acc_NewModel(2,:)-Acc_NBodyModel(2,:));
    hold on;
    grid on;
    plot(t/units.tsf,ones(1,size(X,1))*mean(Acc_NewModel(2,:)-Acc_NBodyModel(2,:)));
    ylabel('$\Delta Acc_y [km/s^2]$')
    legend('Difference');
    subplot(2,3,6)
    plot(t/units.tsf,Acc_NewModel(3,:)-Acc_NBodyModel(3,:));
    hold on;
    grid on;
    plot(t/units.tsf,ones(1,size(X,1))*mean(Acc_NewModel(3,:)-Acc_NBodyModel(3,:)));
    ylabel('$\Delta Acc_z [km/s^2]$')
    legend('Difference');
