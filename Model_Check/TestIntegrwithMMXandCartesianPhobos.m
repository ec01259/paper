clear
close all
clc

%%  Test model With MMX

    warning('on', 'all'); format longG;
    set(0,'DefaultTextInterpreter','latex');
    set(0,'DefaultAxesFontSize', 16);

%   Mac
    addpath('../');
    addpath('../MMX_Fcn_CovarianceAnalyses');
    addpath(genpath('../../mice/'));
    addpath(genpath('../../generic_kernels/'));
    addpath(genpath('../../useful_functions/'));
    addpath(genpath('../../dynamical_model/'));
    addpath(genpath('../MMX_Product/MMX_BSP_Files_GravLib/'));
    MMX_InitializeSPICE
    cspice_furnsh(which('mar097.bsp'));
    cspice_furnsh(which('MMX_QSO_031_2x2_826891269_828619269.bsp'));
    cspice_furnsh(which('Phobos_826891269_828619269.bsp'));
    
%   Model parameters
    [par, units]= MMX_DefineNewModelParametersAndUnits;
    alpha       = 13.03;                          %km
    beta        = 11.4;                           %km
    gamma       = 9.14;                           %km
    Phobos      = struct('alpha',alpha/units.lsf,'beta',beta/units.lsf,'gamma',gamma/units.lsf);
    
    
%%  Integration Phobos's new model + MMX

%   Initial MMX's state vector
    startdate   = '2026-03-16 00:00:00 (UTC)';
    data        = cspice_str2et(startdate);
    par.et0     = data;

    [Ph, par]   = Phobos_States_NewModel(data,par);

%   Initial Phobos's state vector
    Ph0    = cspice_spkezr('-401', data, 'MARSIAU', 'none', '499')./units.sfVec;
    Ph0     = [Ph0; Ph(5:end)./[1; units.tsf; 1; units.tsf]];

%   Initial MMX's State vector
    MMX0    = cspice_spkezr('-34', data, 'MARSIAU', 'none', '499')./units.sfVec;

%   Initial state vector
    St0     = [MMX0; Ph0];

%   Integration
    day     = 24*3600;
    tspan   = (0:150:10*day)*units.tsf;
    RelTol  = 1e-13;
    AbsTol  = 1e-16; 
    opt     = odeset('RelTol',RelTol,'AbsTol',AbsTol);
    [t,X]   = ode113(@(t,X) MPhMMX_CoupledLibration_Cartesian(t,X,par,units),tspan,St0,opt);
    t       = t/units.tsf;


%%  Check

    err_r_MMX   = zeros(3,size(t,1));
    err_v_MMX   = zeros(3,size(t,1));
    err_r_Ph    = zeros(3,size(t,1));
    err_v_Ph    = zeros(3,size(t,1));
    
    for i = 1:size(t,1)
        
        time        = data+t(i);
        MMX_t       = cspice_spkezr('-34', time, 'MARSIAU', 'none', '499');
        Phobos_t    = cspice_spkezr('-401', time, 'MARSIAU', 'none', '499');
    
        err_MMX(:,i)= MMX_t-X(i,1:6)'.*units.sfVec;
        err_Ph(:,i) = Phobos_t-X(i,7:12)'.*units.sfVec;
        
        err_r_MMX(:,i)   = err_MMX(1:3,i);
        err_v_MMX(:,i)   = err_MMX(4:6,i);
        err_r_Ph(:,i)    = err_Ph(1:3,i);
        err_v_Ph(:,i)    = err_Ph(4:6,i);

    end

    figure()
    subplot(2,3,1)
    plot(t/3600,err_r_MMX(1,:))
    grid on
    hold on
    subplot(2,3,2)
    plot(t/3600,err_r_MMX(2,:))
    grid on
    hold on
    subplot(2,3,3)
    plot(t/3600,err_r_MMX(3,:))
    grid on
    hold on
    subplot(2,3,4)
    plot(t/3600,err_v_MMX(1,:))
    grid on
    hold on
    subplot(2,3,5)
    plot(t/3600,err_v_MMX(2,:))
    grid on
    hold on
    subplot(2,3,6)
    plot(t/3600,err_v_MMX(3,:))
    grid on
    hold on

    figure()
    subplot(2,3,1)
    plot(t/3600,err_r_Ph(1,:))
    grid on
    hold on
    subplot(2,3,2)
    plot(t/3600,err_r_Ph(2,:))
    grid on
    hold on
    subplot(2,3,3)
    plot(t/3600,err_r_Ph(3,:))
    grid on
    hold on
    subplot(2,3,4)
    plot(t/3600,err_v_Ph(1,:))
    grid on
    hold on
    subplot(2,3,5)
    plot(t/3600,err_v_Ph(2,:))
    grid on
    hold on
    subplot(2,3,6)
    plot(t/3600,err_v_Ph(3,:))
    grid on
    hold on


%   Difference wrt SPICE
    
    r_Ph_t      = zeros(1,size(t,1));
    rdot_Ph_t   = zeros(1,size(t,1));
    theta_t     = zeros(1,size(t,1));
    thetadot_t  = zeros(1,size(t,1));
    Phi1_t      = zeros(1,size(t,1));
    
    Phi1dot_t   = zeros(1,size(t,1));
    Phi2_t      = zeros(1,size(t,1));
    Phi2dot_t   = zeros(1,size(t,1));
    
    for i = 1:size(t,1)

        time        = data+t(i);
        Phobos_t    = cspice_spkezr('401', time, 'MARSIAU', 'none', '499');
        r           = Phobos_t(1:3);
        v           = Phobos_t(4:6);
        h           = cross(r,v);

        Ph_0        = par.MARSIAU2perifocal*r;
        theta_t(i)  = atan2(Ph_0(2),Ph_0(1));
        thetadot_t(i)   = norm(h)/norm(r)^2;
        rdot_Ph_t(i)    = dot(r,v)/norm(r);
        r_Ph_t(i)       = norm(Phobos_t(1:3));

        Phobos_t_ORB    = cspice_spkezr('401', time, 'MARPHOFRZ', 'none', '499');
        er              = -Phobos_t_ORB(1:3)/norm(Phobos_t_ORB(1:3));
        eh              = cross(Phobos_t_ORB(1:3),Phobos_t_ORB(4:6))/...
            norm(cross(Phobos_t_ORB(1:3),Phobos_t_ORB(4:6)));
        ey              = cross(eh,er);
        NO              = [er,ey,eh];
        no              = [-er,cross(eh,-er),eh];
        
        BN          = cspice_sxform('MARPHOFRZ','IAU_MARS',time);
        BO          = BN(1:3,1:3)*no;
        [Phi1_t(i),ea2,ea3]  = cspice_m2eul(BO,3,2,1);
    
        Wtilde      = -BN(4:6, 1:3)*BN(1:3, 1:3)';
        w_BN        = [-Wtilde(2,3); Wtilde(1,3); -Wtilde(1,2)];
        w_BO        = w_BN - BO*[0; 0; thetadot_t(i)];
        Phi1dot_t(i)= (sin(ea3)*w_BO(2) + cos(ea3)*w_BO(3))/cos(ea2);

        BN          = cspice_sxform('MARPHOFRZ','IAU_PHOBOS',time);
        R_In2PhPA   = par.PB*BN(1:3,1:3);
        BO          = R_In2PhPA*NO;
        [Phi2_t(i),ea2,ea3]  = cspice_m2eul(BO,3,2,1);

        Wtilde      = -BN(4:6, 1:3)*BN(1:3, 1:3)';
        w_BN        = [-Wtilde(2,3); Wtilde(1,3); -Wtilde(1,2)];
        w_BO        = w_BN - BO*[0; 0; thetadot_t(i)];
        Phi2dot_t(i)= (sin(ea3)*w_BO(2) + cos(ea3)*w_BO(3))/cos(ea2);

    end


%   Phi1 Spice and integration    
    figure()
    subplot(2,4,1)
    plot(t/3600,mod(X(:,13),2*pi));
    grid on;
    hold on;
    plot(t/3600,mod(Phi1_t,2*pi));
    ylabel('$\Phi_{M}$ [rad]','Fontsize',26)
    xlabel('[hours]','Fontsize',26)
    legend('Integrated','Spice','Fontsize',22)
    subplot(2,4,5)
    plot(t/3600,angdiff(X(:,13)',Phi1_t));
    grid on;
    ylabel('$\Delta \Phi_{M}$ [rad]','Fontsize',26)
    xlabel('[hours]','Fontsize',26)

%   Phi1_dot Spice and integration    
    subplot(2,4,2)
    plot(t/3600,X(:,14)*units.tsf);
    grid on;
    hold on;
    plot(t/3600,Phi1dot_t);
    ylabel('$\dot{\Phi}_{M}$ [rad/s]','Fontsize',26)
    xlabel('[hours]','Fontsize',26)
    legend('Integrated','Spice','Fontsize',22)
    subplot(2,4,6)
    plot(t/3600,X(:,14)*units.tsf-Phi1dot_t(:));
    grid on;
    ylabel('$\Delta \dot{\Phi}_{M}$ [rad/s]','Fontsize',26)
    xlabel('[hours]','Fontsize',26)

%   Phi2 Spice and integration
    subplot(2,4,3)
    plot(t/3600,mod(X(:,15),2*pi));
    grid on;
    hold on;
    plot(t/3600,mod(Phi2_t,2*pi));
    ylabel('$\dot{\Phi}_{Ph}$ [rad]','Fontsize',26)
    xlabel('[hours]','Fontsize',26)
    legend('Integrated','Spice','Fontsize',22)
    subplot(2,4,7)
    plot(t/3600,angdiff(X(:,15)',Phi2_t));
    grid on;
    ylabel('$\Delta \dot{\Phi}_{Ph}$ [rad]','Fontsize',26)
    xlabel('[hours]','Fontsize',26)

%   Phi2_dot Spice and integration    
    subplot(2,4,4)
    plot(t/3600,X(:,16)*units.tsf);
    grid on;
    hold on;
    plot(t/3600,Phi2dot_t);
    ylabel('$\dot{\Phi}_{Ph}$ [rad/s]','Fontsize',26)
    xlabel('[hours]','Fontsize',26)
    legend('Integrated','Spice','Fontsize',22)
    subplot(2,4,8)
    plot(t/3600,X(:,16)*units.tsf-Phi2dot_t(:));
    grid on;
    ylabel('$\Delta \dot{\Phi}_{Ph}$ [rad/s]','Fontsize',26)
    xlabel('[hours]','Fontsize',26)