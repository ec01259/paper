clearvars; close all; clc; format longG;
set(0, 'DefaultTextInterpreter', 'latex');
set(0, 'DefaultAxesFontSize', 16);

% addpath(genpath('~/Documents/MATLAB/MyFunctions'));

MMX_InitializeSPICE;
cspice_furnsh(which('mar097.bsp'));
cspice_furnsh('MARPHOFRZ.txt');
% cspice_furnsh(which('phobos_2014_09_22.bds'));

handle = cspice_dasopr(which('phobos_2014_09_22.bds'));
[dladsc, found] = cspice_dlabfs( handle );
[nv, np] = cspice_dskz02( handle, dladsc );
vrt = cspice_dskv02(handle, dladsc, 1, nv)';
fct = cspice_dskp02(handle, dladsc, 1, np)';
figure()
trisurf(fct, vrt(:, 1), vrt(:, 2), vrt(:, 3), 'EdgeColor', 'none', 'FaceColor', [0.5, 0.5, 0.5]), hold on;
axis equal;
material dull
cam = camlight;


filename = 'Phobos_shape_model.txt';

fid = fopen(filename, 'w');
fprintf(fid, 'v %f %f %f\n', vrt');
fprintf(fid, 'f %d %d %d\n', fct');
fclose(fid)

shape = ReadShapeModel(filename);

shape.PB = [0.999240107370417        0.0378550984643587       0.00928436010779999
       -0.0379006415569816          0.99927008291848       0.00477940934253335
      -0.00909665828350325      -0.00512766070913186         0.999945477465509];

MARS = '499';
PHOBOS = '401';

et0 = cspice_str2et('2026-03-16 00:00:00 (UTC)');
P = 30*86400;
dt = linspace(0, P, 10000);

et = et0 + dt;

Xph = cspice_spkezr(PHOBOS, et, 'MARPHOFRZ', 'NONE', MARS);

rph = Xph(1:3, :);
vph = Xph(4:6, :);
hph = cross(rph, vph);

eR = -rph./vecnorm(rph);
eH = hph./vecnorm(hph);
eT = cross(eH, eR);
eT = eT./vecnorm(eT);


tht_dot = vecnorm(hph)./vecnorm(rph).^2;

BN = cspice_sxform('MARPHOFRZ', 'IAU_PHOBOS', et);

for tt = 1:length(et)
    
    NO = [eR(:, tt), eT(:, tt), eH(:, tt)];
    BO = BN(1:3, 1:3, tt)*NO;
    PO = shape.PB*BO;
            
   
    [ea1, ea2, ea3] = cspice_m2eul(PO, 3, 2, 1);
    
    yaw(tt) = ea1;
    pitch(tt) = ea2;
    roll(tt) = ea3;
    
    
    % Angular velocity
    Wtilde = -BN(4:6, 1:3, tt)*BN(1:3, 1:3, tt)';
    
    w_BN = [-Wtilde(2,3); Wtilde(1,3); -Wtilde(1,2)];

    w_BO = w_BN - BO*[0; 0; tht_dot(tt)];
    
    yaw_dot(tt) = (sin(ea3)*w_BO(2) + cos(ea3)*w_BO(3))/cos(ea2);
    pitch_dot(tt) = cos(ea3)*w_BO(2) - sin(ea3)*w_BO(3);
    roll_dot(tt) = w_BO(1) + tan(ea2)*sin(ea3)*w_BO(2) + tan(ea2)*cos(ea3)*w_BO(3);
    
end



figure()
r2d = 180/pi;

% Yaw
subplot(311);
yyaxis left
plot((et - et(1))/86400, yaw*r2d); hold on;
ylabel('$\psi$ [deg]');
ylim([-3,3]);

yyaxis right
plot((et - et(1))/86400, yaw_dot*r2d); hold on;
ylabel('$\dot{\psi}$ [deg/s]');
ylim([-6,6]*1e-4);
grid minor


% Pitch
subplot(312);
yyaxis left
plot((et - et(1))/86400, pitch*r2d); hold on;
ylabel('$\theta$ [deg]');
grid on


yyaxis right
plot((et - et(1))/86400, pitch_dot*r2d); hold on;
ylabel('$\dot{\theta}$ [deg/s]');


subplot(313);
yyaxis left
plot((et - et(1))/86400, roll*r2d); hold on;
ylabel('$\phi$ [deg]');
grid on

yyaxis right
plot((et - et(1))/86400, roll_dot*r2d); hold on;
ylabel('$\dot{\phi}$ [deg/s]');







