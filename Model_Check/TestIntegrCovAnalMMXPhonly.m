clear
close all
clc

%%  Test model With MMX

      warning('on', 'all'); format longG;
    set(0,'DefaultTextInterpreter','latex');
    set(0,'DefaultAxesFontSize', 16);

%   Mac
    addpath('../');
    addpath('../MMX_Fcn_CovarianceAnalyses');
    addpath(genpath('../../mice/'));
    addpath(genpath('../../generic_kernels/'));
    addpath(genpath('../../useful_functions/'));
    addpath(genpath('../../dynamical_model/'));
    addpath(genpath('../MMX_Product/MMX_BSP_Files_GravLib/'));
    MMX_InitializeSPICE
    cspice_furnsh(which('mar097.bsp'));
    cspice_furnsh(which('MMX_QSO_031_2x2_826891269_828619269.bsp'));
    cspice_furnsh(which('Phobos_826891269_828619269.bsp'));
    
%   Model parameters
    [par, units]= MMX_DefineNewModelParametersAndUnits;
    alpha       = 13.03;                          %km
    beta        = 11.4;                           %km
    gamma       = 9.14;                           %km
    Phobos      = struct('alpha',alpha/units.lsf,'beta',beta/units.lsf,'gamma',gamma/units.lsf);
    
    
%%  Integration Phobos's new model + MMX

%   Initial MMX's state vector
    startdate   = '2026-03-16 00:00:00 (UTC)';
    data        = cspice_str2et(startdate);
    par.et0     = data;

    [Ph, par]   = Phobos_States_NewModel(data,par);

%   Initial Phobos's state vector
    Ph0     = Ph./units.sfVec2;

%   Initial MMX's State vector
    MMX0    = cspice_spkezr('-34', data, 'MARSIAU', 'none', '499')./units.sfVec;

%   Initial state vector
    St0     = [MMX0; Ph0];
    St02    = [MMX0; Ph0; par.IPhx_bar; par.IPhy_bar; par.IPhz_bar];
    
%   Integration
    day     = 24*3600;
    tspan   = (0:150:5*day)*units.tsf;
    RelTol  = 1e-13;
    AbsTol  = 1e-16; 
    opt     = odeset('RelTol',RelTol,'AbsTol',AbsTol);
    [t,X]   = ode113(@(t,X) New_Analysis_Dynamics_smart_MMXPhonly(t,X,par,units),tspan,St0,opt);
    [t1,X1] = ode113(@(t,X) Dynamics_MPHandMMX_Inertia(t,X,par,units),tspan,St02,opt);
    t       = t/units.tsf;
    t1      = t1/units.tsf;

%%  Plot

    
%   QSO orbit

    QSO_t1   = zeros(size(X,2),3);
    QSO_t2   = zeros(size(X,2),3);
    om_t1    = [zeros(2,size(t,1)); X(:,10)'*units.tsf];
    om_t2    = [zeros(2,size(t,1)); X(:,10)'*units.tsf];
    
    for i = 1:size(t,1)
    
        r_Phobos    = [X(i,7)*cos(X(i,9))*units.lsf; X(i,7)*sin(X(i,9))*units.lsf; 0];
        
        r   = [cos(X(i,9)'); sin(X(i,9)'); 0];
        k   = [0;0;1];
        j   = cross(k,r)/norm(cross(k,r));
        NR  = [r,j,k];
        RN  = NR';
        
        QSO_t1(i,1:3)= RN*(par.MARSIAU2perifocal*X(i,1:3)'*units.lsf-r_Phobos);

        r_Phobos    = [X1(i,7)*cos(X1(i,9))*units.lsf; X1(i,7)*sin(X1(i,9))*units.lsf; 0];
        
        r   = [cos(X1(i,9)'); sin(X1(i,9)'); 0];
        k   = [0;0;1];
        j   = cross(k,r)/norm(cross(k,r));
        NR  = [r,j,k];
        RN  = NR';
        
        QSO_t2(i,1:3)= RN*(par.MARSIAU2perifocal*X1(i,1:3)'*units.lsf-r_Phobos);
        
    end

    figure(1)
    plot3(QSO_t1(:,1),QSO_t1(:,2),QSO_t1(:,3))
    hold on;
    grid on;
    plot3(QSO_t2(:,1),QSO_t2(:,2),QSO_t2(:,3))
    planetPlot('asteroid',[0;0;0],par.Phobos,1);
    
    figure(2)
    subplot(2,3,1)
    plot(t/3600, X(:,1)-X1(:,1))
    grid on
    subplot(2,3,2)
    plot(t/3600, X(:,2)-X1(:,2))
    grid on
    subplot(2,3,3)
    plot(t/3600, X(:,3)-X1(:,3))
    grid on
    subplot(2,3,4)
    plot(t/3600, X(:,4)-X1(:,4))
    grid on
    subplot(2,3,5)
    plot(t/3600, X(:,5)-X1(:,5))
    grid on
    subplot(2,3,6)
    plot(t/3600, X(:,6)-X1(:,6))
    grid on

    figure(3)
    subplot(2,3,1)
    plot(t/3600, X(:,1))
    hold on
    grid on
    plot(t1/3600, X1(:,1))
    subplot(2,3,2)
    plot(t/3600, X(:,2))
    hold on
    grid on
    plot(t1/3600, X1(:,2))
    subplot(2,3,3)
    plot(t/3600, X(:,3))
    hold on
    grid on
    plot(t1/3600, X1(:,3))
    subplot(2,3,4)
    plot(t/3600, X(:,4))
    hold on
    grid on
    plot(t1/3600, X1(:,4))
    subplot(2,3,5)
    plot(t/3600, X(:,5))
    hold on
    grid on
    plot(t1/3600, X1(:,5))
    subplot(2,3,6)
    plot(t/3600, X(:,6))
    hold on
    grid on
    plot(t1/3600, X1(:,6))


    figure(4)
    subplot(2,4,1)
    plot(t/3600, X(:,7)-X1(:,7))
    grid on
    subplot(2,4,2)
    plot(t/3600, X(:,8)-X1(:,8))
    grid on
    subplot(2,4,3)
    plot(t/3600, X(:,9)-X1(:,9))
    grid on
    subplot(2,4,4)
    plot(t/3600, X(:,10)-X1(:,10))
    grid on
    subplot(2,4,5)
    plot(t/3600, X(:,11)-X1(:,11))
    grid on
    subplot(2,4,6)
    plot(t/3600, X(:,12)-X1(:,12))
    grid on
    subplot(2,4,7)
    plot(t/3600, X(:,13)-X1(:,13))
    grid on
    subplot(2,4,8)
    plot(t/3600, X(:,14)-X1(:,14))
    grid on