clear
close all
clc

%%  Test model With MMX

      warning('on', 'all'); format longG;
    set(0,'DefaultTextInterpreter','latex');
    set(0,'DefaultAxesFontSize', 16);

%   Mac
    addpath('../');
    addpath('../MMX_Fcn_CovarianceAnalyses');
    addpath(genpath('../../mice/'));
    addpath(genpath('../../generic_kernels/'));
    addpath(genpath('../../useful_functions/'));
    addpath(genpath('../../dynamical_model/'));
    addpath(genpath('../MMX_Product/MMX_BSP_Files_GravLib/'));
    MMX_InitializeSPICE
    cspice_furnsh('MARPHOFRZ.txt');
    cspice_furnsh(which('mar097.bsp'));
%     cspice_furnsh(which('MMX_QSO_198_2x2_826891269_828619269.bsp'));
%     cspice_furnsh(which('MMX_QSO_094_2x2_826891269_828619269.bsp'));
%     cspice_furnsh(which('MMX_QSO_049_2x2_826891269_828619269.bsp'));
%     cspice_furnsh(which('MMX_QSO_031_2x2_826891269_828619269.bsp'));
%     cspice_furnsh(which('MMX_QSO_027_2x2_826891269_828619269.bsp'));

%     cspice_furnsh(which('MMX_SwingQSO_094_006_2x2_826891269_831211269.bsp'));
%     cspice_furnsh(which('MMX_SwingQSO_049_000_2x2_826891269_828619269.bsp'));
%     cspice_furnsh(which('MMX_SwingQSO_031_011_2x2_826891269_831211269.bsp'));

    cspice_furnsh(which('MMX_3DQSO_031_006_2x2_826891269_828619269.bsp'));

    cspice_furnsh(which('Phobos_826891269_828619269.bsp'));
    
%   Model parameters
    [par, units]= MMX_DefineNewModelParametersAndUnits;
    alpha       = 13.03;                          %km
    beta        = 11.4;                           %km
    gamma       = 9.14;                           %km
    Phobos      = struct('alpha',alpha/units.lsf,'beta',beta/units.lsf,'gamma',gamma/units.lsf);
    Geom.alpha  = alpha;
    Geom.beta   = beta;
    Geom.gamma  = gamma;
    
%%  Integration Phobos's new model + MMX

%   Initial MMX's state vector
    startdate   = '2026-03-16 00:00:00 (UTC)';
    data        = cspice_str2et(startdate);
    par.et0     = data;
    par.Geom    = Geom;
    [Ph, par]   = Phobos_States_NewModel(data,par);

%   Initial Phobos's state vector
    Ph0     = Ph./units.sfVec2;

%   Initial MMX's State vector
    MMX0    = cspice_spkezr('-34', data, 'MARSIAU', 'none', '499')./units.sfVec;

%   Initial state vector
    St0     = [MMX0; Ph0];
    St02    = [MMX0; Ph0; par.IPhx_bar; par.IPhy_bar; par.IPhz_bar];
    
%   Integration
    day     = 24*3600;
    tspan   = (0:100:10*day)*units.tsf;
    RelTol  = 1e-13;
    AbsTol  = 1e-16; 
    opt     = odeset('RelTol',RelTol,'AbsTol',AbsTol,'event', @(t,X) landing_Phobos(t,X,par,units));
    [t,X]   = ode113(@(t,X) Dynamics_MPHandMMX_Inertia(t,X,par,units),tspan,St02,opt);
    t       = t/units.tsf;

%%  Plot

    
%   QSO orbit

    QSO_t1   = zeros(size(X,2),3);
    QSO_t2   = zeros(size(X,2),3);
    om_t1    = [zeros(2,size(t,1)); X(:,10)'*units.tsf];
    om_t2    = [zeros(2,size(t,1)); X(:,10)'*units.tsf];
    
    for i = 1:size(t,1)
    
        r_Phobos    = [X(i,7)*cos(X(i,9))*units.lsf; X(i,7)*sin(X(i,9))*units.lsf; 0];
        
        r   = [cos(X(i,9)'); sin(X(i,9)'); 0];
        k   = [0;0;1];
        j   = cross(k,r)/norm(cross(k,r));
        NR  = [r,j,k];
        RN  = NR';
        
        QSO_t1(i,1:3)= RN*(par.MARSIAU2perifocal*X(i,1:3)'*units.lsf-r_Phobos);

        r_Phobos    = [X(i,7)*cos(X(i,9))*units.lsf; X(i,7)*sin(X(i,9))*units.lsf; 0];
        
        r   = [cos(X(i,9)'); sin(X(i,9)'); 0];
        k   = [0;0;1];
        j   = cross(k,r)/norm(cross(k,r));
        NR  = [r,j,k];
        RN  = NR';
        
        QSO_t2(i,1:3)= RN*(par.MARSIAU2perifocal*X(i,1:3)'*units.lsf-r_Phobos);
        
    end

    figure(1)
    plot3(QSO_t1(:,1),QSO_t1(:,2),QSO_t1(:,3))
    hold on;
    grid on;
    planetPlot('asteroid',[0;0;0],par.Phobos,1);
    view([45,45,45])
%     view(2)
    axis equal
    xlabel('Radial [km]','FontSize',26,'Interpreter','latex')
    ylabel('Along Track [km]','FontSize',26,'Interpreter','latex')
    zlabel('Across Track [km]','FontSize',26,'Interpreter','latex')
    legend('3DQSO-Lb','FontSize',26,'Interpreter','latex')