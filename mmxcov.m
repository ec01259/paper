function mmxcov(PhobosGravityField, OrbitType, Amplitudes, PropTime, Epoch, micePATH, genkernPATH, varargin)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% mmxcov(PhobosGravityField, OrbitType, Amplitudes, PropTime, Epoch, micePATH, genkernPATH)
%
% Main function for MMX linear covariance analyses
%
% INPUTS:
% .PhobosGravityField   Order x Degree of Phobos' gravity field (max [8, 8])
% .OrbitType            Desired orbit type ('QSO', '3D-QSO', 'Swing-QSO')
% .Amplitudes           Desired orbit amplitudes (Aqso, Ay/Az) [km, km]
% .PropTime             Desired Propagation Time [s]
% .Epoch                Insert initial epoch [date]
% .micePATH             Path to Mice directory
% .genkernPATH          Path to Generic Kernels
%
% OUTPUTS: None
% 
%
% EXAMPLES:
%
%   mmxtoolbox([4, 4], 'QSO', [26.69, 0], 30*86400, '2025 Jan 10, 00:00:00 (UTC)', '~/Documents/spice')
%
%   mmxtoolbox([8, 8], '3D-QSO', [26.69, 20], 30*86400, '2025 Jan 10, 00:00:00 (UTC)', '~/Documents/spice')
%
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%     close all; clc; 
    warning('off', 'all'); format longG;
    set(0,'DefaultTextInterpreter','latex');
    set(0,'DefaultAxesFontSize', 16);
    set(0,'DefaultFigureVisible', 'off');
    addpath(genpath('./MMX_Fcn_CovarianceAnalyses'));
    addpath(genpath('./MMX_Fcn_GenerateReferenceTrajectory'));
    addpath(genpath('./MMX_Fcn_Miscellaneous'));
    addpath(genpath('./MMX_Product'));

%%%%%%%%%%%%%%%%%%
%%% EDIT PATHS %%%
%%%%%%%%%%%%%%%%%%
    addpath(genpath(micePATH));
    addpath(genpath(genkernPATH));
    
    
    [pars, units] = MMX_DefineModelParametersAndUnits(PhobosGravityField);
    
    
    inp.Model.dynamics  = @CRTBPsh;
    inp.Model.pars      = pars;
    inp.Model.units     = units;
    inp.Model.epoch     = Epoch;
    inp.OrbitType       = OrbitType;
    inp.Amplitudes      = Amplitudes;
    inp.PropTime        = PropTime;
    
    
    [t, x, out] = MMX_GenerateReferenceTrajectory(inp);
    
    
    h = figure();
    PlotTrajectories(h, x, inp.Model);
    set(0,'DefaultFigureVisible', 'on');
    
    
    % Read Phobos orbit elements at epoch
    [X1, Xph] = MMX_CalculateInitialConditionsInInertialFrame(out.X0, inp.Model);



%%  Integrate Initial conditions in Inertial frame
    Model2 = inp.Model;
    Model2.dynamics = @TBPpert;
    Model2.pars.d = 12;
    [t, y] = IntegrateInitialConditions([X1; Xph], t, Model2);



%% Sanity Check
% [z, Err] = MMX_CheckRelativeErrorInSynodicFrame(x, y, inp.Model);



%%  Move origin to Mars-Phobos barycentre
    mu = inp.Model.pars.SH.GM/(inp.Model.pars.GM1 + inp.Model.pars.SH.GM);
    Xbm = mu*y(:, 7:12);
    Xmmx = y(:, 1:6) - Xbm;
    X401 = (y(:, 7:12) - Xbm);
    X499 = -Xbm;
    
    
    TargetFolder = '/MMX_Product/MMX_BSP_Files/';
    if (exist(TargetFolder, 'dir') ~= 7)
        command = sprintf('mkdir %s', TargetFolder);
        system(command);
    end
    
    MMX_GenerateBSPFiles(t, Xmmx, X499, X401, OrbitType, Amplitudes, PropTime, inp.Model, micePATH)

%%  Covariance Analysis Parameters

switch varargin{1}
    case 'SRIF'
        [pars, units] = MMX_CovarianceAnalysisParameters(pars, units);
    case 'SRIF_SNC'
        [pars, units] = MMX_CovarianceAnalysisParameters(pars, units);
    case 'UKF'
        [pars, units] = MMX_CovarianceAnalysisParameters_UKF(pars, units);
end



%%  Kernels loading

    MMX_InitializeSPICE
    
    et0       = cspice_str2et(Epoch);
    et_end    = et0 + PropTime;
    
    if(strcmp(OrbitType, 'QSO'))
        amp = sprintf('%03.0f', Amplitudes(1));
    else
        amp = sprintf('%03.0f_%03.0f', Amplitudes(1), Amplitudes(2));
    end

    dates = sprintf('%09.0f_%09.0f', et0, et_end);
    
    SHmodel = sprintf('%dx%d',size(inp.Model.pars.SH.Clm,1)-1,size(inp.Model.pars.SH.Clm,2)-1);
    addpath(genpath('..\MMX_Product\MMX_BSP_Files'));
    bspfilename = ['MMX_',OrbitType,'_',amp,'_',SHmodel,'_',dates,'.bsp'];
    cspice_furnsh(which(bspfilename));
    bspfilename = ['Phobos_',dates,'.bsp'];
    cspice_furnsh(which(bspfilename));
    bspfilename = ['Mars_',dates,'.bsp'];
    cspice_furnsh(which(bspfilename));

%%  Observations
    fprintf('\nNeed one moment to simulate the observables...');

    switch varargin{1}
        case 'SRIF'
            YObs_Full   = Observations_Generation_Paper(et0, et_end, pars, units);
        case 'SRIF_SNC'
            YObs_Full   = Observations_Generation_Paper(et0, et_end, pars, units);
        case 'UKF'
            YObs_Full   = Observations_Generation_UKF(et0, et_end, pars, units);
    end

    fprintf('\nOk, ready!\n');
    fprintf('\nHere I go with the analysis...');

%%  Initial conditions for the analysis
    MMX     = cspice_spkezr('-33', et0, 'J2000', 'none', 'MARS');
    Phobos  = cspice_spkezr('401', et0, 'J2000', 'none', 'MARS');    
        
    Est0.X      = [MMX./units.sfVec; Phobos./units.sfVec; pars.Clm; pars.Slm; pars.bias];
    Est0.dx     = zeros(size(Est0.X,1),1);
    Est0.P0     = pars.P0;
    Est0.t0     = et0/units.tsf;

%% Analysis

    if ismac
        if ~(exist('Householder.mexmaci64','file')==3)
            cd MMX_Functions/MMX_Fcn_CovarianceAnalyses/
            mex Householder.c
            cd ../..    
        end
    elseif isunix
        if ~(exist('Householder.mexa64','file')==3)
            cd MMX_Functions/MMX_Fcn_CovarianceAnalyses/
            mex Householder.c
            cd ../..
        end
    elseif ispc
        if ~(exist('Householder.mexw64','file')==3)
            cd MMX_Functions/MMX_Fcn_CovarianceAnalyses/
            mex Householder.c
            cd ../..    
        end
    else
        error('Platform not supported')
    end
    

    switch varargin{1}
        case 'SRIF'
            [Est]  = SRIF(Est0,@MP_Circular_all,@Observables_model,...
                pars.R,YObs_Full,pars,units);
        case 'SRIF_SNC'
            [Est]  = SRIF_SNC(Est0,@MP_Circular_all,@Observables_model,...
                pars.R,YObs_Full,pars,units);
        case 'UKF'
            pars.alpha  = 1e0;
            [Est]  = UKF_oldDyn(Est0,@MP_Circular_all,@MP_Circular_all_UKF,...
                @Observables_model_UKF,pars.R,YObs_Full,pars,units);
    end

% Smoothing

%     if Est.flagNO == 0
%     
%         Smooth = Smoothing(Est,pars,units);
%         Est.x_t = Smooth.x_lk_t;
%         Est.P_t = Smooth.P_lk_t;
%     end

%% Results

%     if Est.flagNO == 0
        
        fprintf('\nHere are the results...');
        switch varargin{1}
            case 'SRIF'
                Cov_ResultsPlot(PhobosGravityField, Est, YObs_Full, pars, units);
            case 'SRIF_SNC'
                Cov_ResultsPlot(PhobosGravityField, Est, YObs_Full, pars, units);
            case 'UKF'
                Cov_ResultsPlot_UKF([2,2], Est, YObs_Full, pars, units)
        end

end