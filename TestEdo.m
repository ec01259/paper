%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Test_mmxcov.m
%
% Example script to run and test mmxcov()
%
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear; close all; clc

% Add Path to MICE Library
% Add generic kernels to MATLAB Path

restoredefaultpath
rmpath(genpath('~/MMX_Fcn_CovarianceAnalyses'));
rmpath(genpath('~/MMX_Fcn_GenerateReferenceTrajectory'));
rmpath(genpath('~/MMX_Fcn_Miscellaneous'));
rmpath(genpath('~/MMX_Product'));
addpath('../useful_functions/');

% Edo
micePATH    = '~/Documents/MATLAB/mice/';
genkernPATH = '~/Documents/MATLAB/generic_kernels/';

%% Set integration duration

days = 1e1;

%   Planar QSOs

% %   QSO-H
%     mmxcov([4, 4], 'QSO', [197.69, 0], days*86400, '2025 Jan 10, 00:00:00 (UTC)', micePATH, genkernPATH)
%     mmxcov([1, 1], 'QSO', [197.69, 0], days*86400, '2025 Apr 10, 00:00:00 (UTC)', micePATH, genkernPATH)
% %   QSO-M
%     mmxcov([2, 2], 'QSO', [30.69, 0], days*86400, '2026-03-16, 00:00:00 (UTC)', micePATH, genkernPATH,'SRIF')
% %   QSO-La
%     mmxcov([4, 4], 'QSO', [48.69, 0], days*86400, '2025 Jan 10, 00:00:00 (UTC)', micePATH, genkernPATH)
% %   QSO-Lb
%     mmxcov([4, 4], 'QSO', [30.69, 0], days*86400, '2025 Jan 10, 00:00:00 (UTC)', micePATH, genkernPATH)
%     mmxcov([4, 4], 'QSO', [30.69, 0], days*86400, '2025 Jan 10, 00:00:00 (UTC)', micePATH, genkernPATH,'SNC')
% %   QSO-Lc
%     mmxcov([4, 4], 'QSO', [26.69, 0], days*86400, '2025 Mar 25, 12:00:00 (UTC)', micePATH, genkernPATH)
%     mmxcov([6, 6], 'QSO', [26.69, 0], 30*86400, '2025 Apr 25, 00:00:00 (UTC)', micePATH, genkernPATH)
%     mmxcov([8, 8], 'QSO', [26.69, 0], 10*86400, '2025 Jan 10, 00:00:00 (UTC)', micePATH, genkernPATH)
%  
%     
% %   3D QSOs
%
% %   3D QSO-H
%     mmxcov([2, 2], '3D-QSO', [26.69, 10], days*86400, '2025 Jan 10, 00:00:00 (UTC)', micePATH, genkernPATH)
% 
% %   3D QSO-M
%     mmxcov([2, 2], '3D-QSO', [93.69, 30], days*86400, '2025 Jan 10, 00:00:00 (UTC)', micePATH, genkernPATH)
% 
% %   3D QSO-Lc
%     mmxcov([6, 6], '3D-QSO', [26.69, 10], days*86400, '2025 Jan 10, 00:00:00 (UTC)', micePATH, genkernPATH)
% 
% 
%     
% %   Swing QSOs
% 
% %   Swing QSO-Lb
    mmxcov([2, 2], 'Swing-QSO', [93.69, 10], days*86400, '2025 Jan 10, 00:00:00 (UTC)', micePATH, genkernPATH)
% 
% %   Swing QSO-Lb
%     mmxcov([4, 4], 'Swing-QSO', [30.69, 10], days*86400, '2025 Jan 10, 00:00:00 (UTC)', micePATH, genkernPATH)
% 
% %   Swing QSO-Lb
%     mmxcov([6, 6], 'Swing-QSO', [30.69, 10], days*86400, '2025 Jan 10, 00:00:00 (UTC)', micePATH, genkernPATH)