clear 
close all
clc

%%  Setup

    warning('off', 'all'); format longG;
    set(0,'DefaultTextInterpreter','latex');
    set(0,'DefaultAxesFontSize', 16);

%   Mac
    restoredefaultpath
    addpath('MMX_Fcn_CovarianceAnalyses/');
    addpath('../useful_functions/');
    addpath(genpath('../mice/'));
    addpath(genpath('../generic_kernels/'));
    addpath(genpath('MMX_Product/MMX_BSP_Files_GravLib/'));
    MMX_InitializeSPICE
    cspice_furnsh(which('mar097.bsp'));
    cspice_furnsh(which('MARPHOFRZ.txt'));
%     cspice_furnsh(which('MMX_QSO_031_2x2_826891269_828619269.bsp'));
%     cspice_furnsh(which('MMX_3DQSO_031_009_2x2_826891269_828619269.bsp'));
    cspice_furnsh(which('MMX_SwingQSO_031_011_2x2_826891269_828619269.bsp'));
    cspice_furnsh(which('Phobos_826891269_828619269.bsp'));
    
%   Time of the analysis
    data        = '2026-03-16 00:00:00 (UTC)';
    data        = cspice_str2et(data);
    day         = 86400;
    PropTime    = 10*day;
    date_end    = data+PropTime;
    
%   Model parameters
    [par, units] = MMX_DefineNewModelParametersAndUnits;

%   Covariance analysis parameters
    par.et0      = data;
    [par, units] = New_MMX_CovarianceAnalysisParameters(par, units);

%%  Observations  


%   Observables
    [YObs_Full] = Observations_Generation_New(data, date_end, par, units);
%     YObs_Full(3,:) = NaN;
%     YObs_Full(5,:) = NaN;
%     YObs_Full(7,:) = NaN;
%     YObs_Full(end-2:end,:) = NaN;

%     save('./ObservationsHPC/Observations-QSOM_16_Lidar5Min.mat','YObs_Full');
    save('Observations.mat','YObs_Full');
