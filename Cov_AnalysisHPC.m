clear
close all
clc

%%  Setup

    warning('off', 'all'); format longG;
    set(0,'DefaultTextInterpreter','latex');
    set(0,'DefaultAxesFontSize', 16);

%   Mac
    restoredefaultpath
    addpath('MMX_Fcn_CovarianceAnalyses/');
    addpath('Model_check/');
    addpath('../useful_functions/');
    addpath(genpath('../mice/'));
    addpath(genpath('../dynamical_model/'));
    addpath(genpath('../generic_kernels/'));
    addpath(genpath('./MMX_Product/MMX_BSP_Files_GravLib/'));
    MMX_InitializeSPICE
    cspice_furnsh('../generic_kernels/mar097.bsp');
    cspice_furnsh('./Model_Check/MARPHOFRZ.txt');
 
%     mex ./MMX_Fcn_CovarianceAnalyses/SphericalHarmonicGravityModelFullPartials.c

%%  QSO-H    
    
    cspice_furnsh('./MMX_Product/MMX_BSP_Files_GravLib/MMX_QSO_094_2x2_826891269_828619269.bsp');
    cspice_furnsh('./MMX_Product/MMX_BSP_Files_GravLib/Phobos_826891269_828619269.bsp');
    
    load("./ObservationsHPC/Observations-QSOM_16_Lidar20Min.mat");

%   Model parameters
    [par, units] = MMX_DefineNewModelParametersAndUnits;

%   Time of the analysis
    data        = '2026-03-16 00:00:00 (UTC)';
    data        = cspice_str2et(data);
    par.et0     = data;
    [Ph,par]    = Phobos_States_NewModel(data,par);

%   Covariance analysis parameters
    [par, units] = New_MMX_CovarianceAnalysisParameters(par, units);
    par.interval_lidar = 1200;
    par.sigma    = 1e-10/(units.vsf*units.tsf);
    par.sigmaPh  = 0/(units.vsf*units.tsf);

%   Initial Phobos's state vector
    Ph0     = Ph./units.sfVec2;

%   Initial MMX's State vector
    MMX0   = cspice_spkezr('-34', data, 'MarsIAU', 'none', '499');
    MMX0   = MMX0./units.sfVec;

%   Analysis initial state vector
    St0    = [MMX0; Ph0; par.I2; par.bias];

    Est0.X  = St0;
    Est0.dx = zeros(size(St0,1),1);
    Est0.P0 = par.P0;
    Est0.t0 = data*units.tsf;
  
    par.alpha   = 1;

%     [Est] = UKF_CovarianceAnalysis(Est0,@Dynamics_MPHandMMX_Inertia,...
%         @Cov_Dynamics_Good, @New_Observables_model,...
%         par.R,YObs_Full,par,units);
%     
%     save('QSOM_PN10_alpha1_16_Lidar20Min.mat','Est');
%     movefile('QSOM_PN10_alpha1_16_Lidar20Min.mat','./ResultsHPC/')

%%  QSO-M    
    
%     cspice_furnsh('./MMX_Product/MMX_BSP_Files_GravLib/MMX_QSO_049_2x2_826891269_828619269.bsp');    
    load("./ObservationsHPC/Observations-QSOM_16_Lidar5Min.mat");

%   Model parameters
    [par, units] = MMX_DefineNewModelParametersAndUnits;

%   Time of the analysis
    data        = '2026-03-16 00:00:00 (UTC)';
    data        = cspice_str2et(data);
    par.et0     = data;
    [Ph,par]    = Phobos_States_NewModel(data,par);

%   Covariance analysis parameters
    [par, units] = New_MMX_CovarianceAnalysisParameters(par, units);
    par.interval_lidar = 300;
    par.sigma    = 1e-10/(units.vsf*units.tsf);
    par.sigmaPh  = 0/(units.vsf*units.tsf);

%   Initial Phobos's state vector
    Ph0     = Ph./units.sfVec2;

%   Initial MMX's State vector
    MMX0   = cspice_spkezr('-34', data, 'MarsIAU', 'none', '499');
    MMX0   = MMX0./units.sfVec;

%   Analysis initial state vector
    St0    = [MMX0; Ph0; par.I2; par.bias];

    Est0.X  = St0;
    Est0.dx = zeros(size(St0,1),1);
    Est0.P0 = par.P0;
    Est0.t0 = data*units.tsf;
  
    par.alpha   = 1;

%     [Est] = UKF_CovarianceAnalysis(Est0,@Dynamics_MPHandMMX_Inertia,...
%         @Cov_Dynamics_Good, @New_Observables_model,...
%         par.R,YObs_Full,par,units);
%     
%     save('QSOM_PN10_alpha1_16_Lidar5Min.mat','Est');
%     movefile('QSOM_PN10_alpha1_16_Lidar5Min.mat','./ResultsHPC/')

%%  QSO-La    
    
%     cspice_furnsh('./MMX_Product/MMX_BSP_Files_GravLib/MMX_QSO_027_2x2_826891269_828619269.bsp');    
    load("./ObservationsHPC/Observations-QSOM_16_Lidar10Min.mat");

%   Model parameters
    [par, units] = MMX_DefineNewModelParametersAndUnits;

%   Time of the analysis
    data        = '2026-03-16 00:00:00 (UTC)';
    data        = cspice_str2et(data);
    par.et0     = data;
    [Ph,par]    = Phobos_States_NewModel(data,par);

%   Covariance analysis parameters
    [par, units] = New_MMX_CovarianceAnalysisParameters(par, units);
    par.interval_lidar = 600;
    par.sigma    = 1e-10/(units.vsf*units.tsf);
    par.sigmaPh  = 0/(units.vsf*units.tsf);

%   Initial Phobos's state vector
    Ph0     = Ph./units.sfVec2;

%   Initial MMX's State vector
    MMX0   = cspice_spkezr('-34', data, 'MarsIAU', 'none', '499');
    MMX0   = MMX0./units.sfVec;

%   Analysis initial state vector
    St0    = [MMX0; Ph0; par.I2; par.bias];

    Est0.X  = St0;
    Est0.dx = zeros(size(St0,1),1);
    Est0.P0 = par.P0;
    Est0.t0 = data*units.tsf;
  
    par.alpha   = 1;

%     [Est] = UKF_CovarianceAnalysis(Est0,@Dynamics_MPHandMMX_Inertia,...
%         @Cov_Dynamics_Good, @New_Observables_model,...
%         par.R,YObs_Full,par,units);
%     
%     save('QSOM_PN10_alpha1_16_Lidar10Min.mat','Est');
%     movefile('QSOM_PN10_alpha1_16_Lidar10Min.mat','./ResultsHPC/')

%%  QSO-Lb    
    
%     cspice_furnsh('./MMX_Product/MMX_BSP_Files_GravLib/MMX_QSO_031_2x2_826891269_828619269.bsp');    
    load("./ObservationsHPC/Observations-QSOM_16_Lidar1Min.mat");

%   Model parameters
    [par, units] = MMX_DefineNewModelParametersAndUnits;

%   Time of the analysis
    data        = '2026-03-16 00:00:00 (UTC)';
    data        = cspice_str2et(data);
    par.et0     = data;
    [Ph,par]    = Phobos_States_NewModel(data,par);

%   Covariance analysis parameters
    [par, units] = New_MMX_CovarianceAnalysisParameters(par, units);
    par.interval_lidar = 60;
    par.sigma    = 1e-10/(units.vsf*units.tsf);
    par.sigmaPh  = 0/(units.vsf*units.tsf);

%   Initial Phobos's state vector
    Ph0     = Ph./units.sfVec2;

%   Initial MMX's State vector
    MMX0   = cspice_spkezr('-34', data, 'MarsIAU', 'none', '499');
    MMX0   = MMX0./units.sfVec;

%   Analysis initial state vector
    St0    = [MMX0; Ph0; par.I2; par.bias];

    Est0.X  = St0;
    Est0.dx = zeros(size(St0,1),1);
    Est0.P0 = par.P0;
    Est0.t0 = data*units.tsf;
  
    par.alpha   = 1;

    [Est] = UKF_CovarianceAnalysis(Est0,@Dynamics_MPHandMMX_Inertia,...
        @Cov_Dynamics_Good, @New_Observables_model,...
        par.R,YObs_Full,par,units);
    
    save('QSOM_PN10_alpha1_16_Lidar1Min.mat','Est');
    movefile('QSOM_PN10_alpha1_16_Lidar1Min.mat','./ResultsHPC/')

%%  QSO-Lc    
    
%     cspice_furnsh('./MMX_Product/MMX_BSP_Files_GravLib/MMX_QSO_049_2x2_826891269_828619269.bsp');    
    load("./ObservationsHPC/Observations-QSOM_16.mat");

%   Model parameters
    [par, units] = MMX_DefineNewModelParametersAndUnits;

%   Time of the analysis
    data        = '2026-03-16 00:00:00 (UTC)';
    data        = cspice_str2et(data);
    par.et0     = data;
    [Ph,par]    = Phobos_States_NewModel(data,par);

%   Covariance analysis parameters
    [par, units] = New_MMX_CovarianceAnalysisParameters(par, units);
    par.interval_Range_rate = 600;
    par.sigma    = 1e-10/(units.vsf*units.tsf);
    par.sigmaPh  = 0/(units.vsf*units.tsf);

%   Initial Phobos's state vector
    Ph0     = Ph./units.sfVec2;

%   Initial MMX's State vector
    MMX0   = cspice_spkezr('-34', data, 'MarsIAU', 'none', '499');
    MMX0   = MMX0./units.sfVec;

%   Analysis initial state vector
    St0    = [MMX0; Ph0; par.I2; par.bias];

    Est0.X  = St0;
    Est0.dx = zeros(size(St0,1),1);
    Est0.P0 = par.P0;
    Est0.t0 = data*units.tsf;
  
    par.alpha   = 1;

    [Est] = UKF_CovarianceAnalysis(Est0,@Dynamics_MPHandMMX_Inertia,...
        @Cov_Dynamics_Good, @New_Observables_model,...
        par.R,YObs_Full,par,units);
    
    save('QSOM_PN10_alpha1_16_Rate10Min.mat','Est');
    movefile('QSOM_PN10_alpha1_16_Rate10Min.mat','./ResultsHPC/')


%%  QSO-H    
    
%     cspice_furnsh('./MMX_Product/MMX_BSP_Files_GravLib/MMX_QSO_198_2x2_826891269_828619269.bsp');    
    load("./ObservationsHPC/Observations-QSOM_16_Rate5Min.mat");

%   Model parameters
    [par, units] = MMX_DefineNewModelParametersAndUnits;

%   Time of the analysis
    data        = '2026-03-16 00:00:00 (UTC)';
    data        = cspice_str2et(data);
    par.et0     = data;
    [Ph,par]    = Phobos_States_NewModel(data,par);

%   Covariance analysis parameters
    [par, units] = New_MMX_CovarianceAnalysisParameters(par, units);
    par.interval_rate = 300;
    par.sigma    = 1e-10/(units.vsf*units.tsf);
    par.sigmaPh  = 0/(units.vsf*units.tsf);

%   Initial Phobos's state vector
    Ph0     = Ph./units.sfVec2;

%   Initial MMX's State vector
    MMX0   = cspice_spkezr('-34', data, 'MarsIAU', 'none', '499');
    MMX0   = MMX0./units.sfVec;

%   Analysis initial state vector
    St0    = [MMX0; Ph0; par.I2; par.bias];

    Est0.X  = St0;
    Est0.dx = zeros(size(St0,1),1);
    Est0.P0 = par.P0;
    Est0.t0 = data*units.tsf;
  
    par.alpha   = 1;

    [Est] = UKF_CovarianceAnalysis(Est0,@Dynamics_MPHandMMX_Inertia,...
        @Cov_Dynamics_Good, @New_Observables_model,...
        par.R,YObs_Full,par,units);
    
    save('QSOM_PN10_alpha1_16_Rate5Min.mat','Est');
    movefile('QSOM_PN10_alpha1_16_Rate5Min.mat','./ResultsHPC/')

%%  QSO-M    
    
%     cspice_furnsh('./MMX_Product/MMX_BSP_Files_GravLib/MMX_QSO_094_2x2_826891269_828619269.bsp');    
    load("./ObservationsHPC/Observations-QSOM_16_Rate1Min.mat");

%   Model parameters
    [par, units] = MMX_DefineNewModelParametersAndUnits;

%   Time of the analysis
    data        = '2026-03-16 00:00:00 (UTC)';
    data        = cspice_str2et(data);
    par.et0     = data;
    [Ph,par]    = Phobos_States_NewModel(data,par);

%   Covariance analysis parameters
    [par, units] = New_MMX_CovarianceAnalysisParameters(par, units);
    par.interval_Range_rate = 60;
    par.interval_lidar = 60;
    par.sigma    = 1e-10/(units.vsf*units.tsf);
    par.sigmaPh  = 0/(units.vsf*units.tsf);

%   Initial Phobos's state vector
    Ph0     = Ph./units.sfVec2;

%   Initial MMX's State vector
    MMX0   = cspice_spkezr('-34', data, 'MarsIAU', 'none', '499');
    MMX0   = MMX0./units.sfVec;

%   Analysis initial state vector
    St0    = [MMX0; Ph0; par.I2; par.bias];

    Est0.X  = St0;
    Est0.dx = zeros(size(St0,1),1);
    Est0.P0 = par.P0;
    Est0.t0 = data*units.tsf;
  
    par.alpha   = 1;

    [Est] = UKF_CovarianceAnalysis(Est0,@Dynamics_MPHandMMX_Inertia,...
        @Cov_Dynamics_Good, @New_Observables_model,...
        par.R,YObs_Full,par,units);
    
    save('QSOM_PN10_alpha1_16_Rate1Min.mat','Est');
    movefile('QSOM_PN10_alpha1_16_Rate1Min.mat','./ResultsHPC/')



%%  QSO-La    
    
%     cspice_furnsh('./MMX_Product/MMX_BSP_Files_GravLib/MMX_QSO_049_2x2_826891269_828619269.bsp');    
    load("./ObservationsHPC/Observations-QSOM_16_Camera20Min.mat");

%   Model parameters
    [par, units] = MMX_DefineNewModelParametersAndUnits;

%   Time of the analysis
    data        = '2026-03-16 00:00:00 (UTC)';
    data        = cspice_str2et(data);
    par.et0     = data;
    [Ph,par]    = Phobos_States_NewModel(data,par);

%   Covariance analysis parameters
    [par, units] = New_MMX_CovarianceAnalysisParameters(par, units);
    pars.interval_camera = 1200;     % s
    par.sigma    = 1e-10/(units.vsf*units.tsf);
    par.sigmaPh  = 0/(units.vsf*units.tsf);

%   Initial Phobos's state vector
    Ph0     = Ph./units.sfVec2;

%   Initial MMX's State vector
    MMX0   = cspice_spkezr('-34', data, 'MarsIAU', 'none', '499');
    MMX0   = MMX0./units.sfVec;

%   Analysis initial state vector
    St0    = [MMX0; Ph0; par.I2; par.bias];

    Est0.X  = St0;
    Est0.dx = zeros(size(St0,1),1);
    Est0.P0 = par.P0;
    Est0.t0 = data*units.tsf;
  
    par.alpha   = .8;

    [Est] = UKF_CovarianceAnalysis(Est0,@Dynamics_MPHandMMX_Inertia,...
        @Cov_Dynamics_Good, @New_Observables_model,...
        par.R,YObs_Full,par,units);
    
    save('QSOM_PN10_alpha81_16_Camera20Min.mat','Est');
    movefile('QSOM_PN10_alpha81_16_Camera20Min.mat','./ResultsHPC/')



%%  QSO-Lb    
    
%     cspice_furnsh('./MMX_Product/MMX_BSP_Files_GravLib/MMX_QSO_031_2x2_826891269_828619269.bsp');    
    load("./ObservationsHPC/Observations-QSOM_16_Camera20Min.mat");

%   Model parameters
    [par, units] = MMX_DefineNewModelParametersAndUnits;

%   Time of the analysis
    data        = '2026-03-16 00:00:00 (UTC)';
    data        = cspice_str2et(data);
    par.et0     = data;
    [Ph,par]    = Phobos_States_NewModel(data,par);

%   Covariance analysis parameters
    [par, units] = New_MMX_CovarianceAnalysisParameters(par, units);
    pars.interval_camera = 600;     % s
    par.sigma    = 1e-10/(units.vsf*units.tsf);
    par.sigmaPh  = 0/(units.vsf*units.tsf);

%   Initial Phobos's state vector
    Ph0     = Ph./units.sfVec2;

%   Initial MMX's State vector
    MMX0   = cspice_spkezr('-34', data, 'MarsIAU', 'none', '499');
    MMX0   = MMX0./units.sfVec;

%   Analysis initial state vector
    St0    = [MMX0; Ph0; par.I2; par.bias];

    Est0.X  = St0;
    Est0.dx = zeros(size(St0,1),1);
    Est0.P0 = par.P0;
    Est0.t0 = data*units.tsf;
  
    par.alpha   = .8;

    [Est] = UKF_CovarianceAnalysis(Est0,@Dynamics_MPHandMMX_Inertia,...
        @Cov_Dynamics_Good, @New_Observables_model,...
        par.R,YObs_Full,par,units);
    
    save('QSOM_PN10_alpha81_16_Camera10Min.mat','Est');
    movefile('QSOM_PN10_alpha81_16_Camera10Min.mat','./ResultsHPC/')


%%  QSO-Lc    
    
%     cspice_furnsh('./MMX_Product/MMX_BSP_Files_GravLib/MMX_QSO_027_2x2_826891269_828619269.bsp');    
    load("./ObservationsHPC/Observations-QSOM_16_Camera2Min.mat");

%   Model parameters
    [par, units] = MMX_DefineNewModelParametersAndUnits;

%   Time of the analysis
    data        = '2026-03-16 00:00:00 (UTC)';
    data        = cspice_str2et(data);
    par.et0     = data;
    [Ph,par]    = Phobos_States_NewModel(data,par);

%   Covariance analysis parameters
    [par, units] = New_MMX_CovarianceAnalysisParameters(par, units);
    pars.interval_camera = 100;     % s
    par.sigma    = 1e-10/(units.vsf*units.tsf);
    par.sigmaPh  = 0/(units.vsf*units.tsf);

%   Initial Phobos's state vector
    Ph0     = Ph./units.sfVec2;

%   Initial MMX's State vector
    MMX0   = cspice_spkezr('-34', data, 'MarsIAU', 'none', '499');
    MMX0   = MMX0./units.sfVec;

%   Analysis initial state vector
    St0    = [MMX0; Ph0; par.I2; par.bias];

    Est0.X  = St0;
    Est0.dx = zeros(size(St0,1),1);
    Est0.P0 = par.P0;
    Est0.t0 = data*units.tsf;
  
    par.alpha   = .8;

    [Est] = UKF_CovarianceAnalysis(Est0,@Dynamics_MPHandMMX_Inertia,...
        @Cov_Dynamics_Good, @New_Observables_model,...
        par.R,YObs_Full,par,units);
    
    save('QSOM_PN10_alpha81_16_Camera2Min.mat','Est');
    movefile('QSOM_PN10_alpha81_16_Camera2Min.mat','./ResultsHPC/')
