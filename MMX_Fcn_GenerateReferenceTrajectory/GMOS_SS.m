function [Xqp, Wqp, lqp, Bqp,Zqp,Sqp] = GMOS_SS(Xqp0, Wqp0, Zqp0, GMOS, Model, J)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% [Xqp,Wqp,lqp,Bqp,Zqp,Sqp] = GMOS_SS(Xqp0,Wqp0,Zqp0,EOM,f,dfdx,J, pars)
%
% Compute several members of a 2D quasi-periodic invariant tori family
% using the single-shooting version of the GMOS algorithm
%
% INPUT:
% VARIABLE      TYPE               DESCRIPTION
% - Xqp0        2D double array    Initial guess of QP Torus
% - Wqp0        1D array           Initial guess of torus frequencies
% - Zqp0        1D array           Initial guess of family tangent 
% - @EOM        function           Equations of Motion
% - @f          function           Vector field
% - @dfdx       function           Partial derivatives of Vector field
% - J           matrix             Matrix of canonical transformation
% - pars        struct             List of Parameters
%   .d          int                number of states
%   .GMOS
%    .ds        double             initial step-length
%    .dsMax     double             maximum step-length allowed
%    .Iter      int                No. of Newton's method interations allowed
%    .N         int                No. of GMOS solution points
%    .Nmax      int                No. of quasi-periodic tori to be computed
%    .M         int                No. of nodes in time-domain
%    .Opt       int                optimal number of Newton's iteration
%    .Plt       int                Flag: 1 to enable plotting functions, 0 ow.
%    .Tol       double             Convergence Tolerance
%
% OUTPUT:
% VARIABLE      TYPE               DESCRIPTION
% - Xqp         3D double array    QP tori computed with the algorithm      
% - Wqp         2D double array    Frequencies of QP tori
% - lqp         2D double array    Unfolding parameters
% - Bqp         3D double array    Floquet Matrices 
% - Zqp         2D double array    Family tangent
% - Sqp         1D double array    Step-lengths
%
% DEPENDENCIES:
% - FourierMatrix.m
% - RotationMatrix.m
%
% AUTHOR: N. Baresi
%
% REFERENCES:
% [1] Olikara, Z. P. and Scheeres, D. J., JAS 2012 
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Parameters
d     = Model.pars.d;             % No. of states
ds    = GMOS.ds;
dsMax = GMOS.dsMax;
Iter  = GMOS.Iter;
N     = GMOS.N;
Nmax  = GMOS.Nmax;
M     = GMOS.M;
Opt   = GMOS.Opt;
Plt   = GMOS.Plt;
Tol   = GMOS.Tol;
switch GMOS.ContinuationMethod
    case 'Jacobi'
        C0 = GMOS.Jacobi;
    case 'Period'
        P0 = GMOS.Period;
    otherwise
        error('Stop Here Traveller! Wrong Inputs\n');
end
h = GMOS.JacobiFnc;
D = d*N;



%% Create Figures and their handles
if(Plt)
    fig98 = figure(98); ax98 = gca; 
    fig99 = figure(99); ax99 = gca; 
end




%% Fourier Matrices
[DFT,IDFT,DT] = FourierMatrix(d,N);



%% Calculate Matrix of Canonical Transformation
Jd  = zeros(D);
for ii = 1:N
    idx = d*(ii-1)+1:d*ii;
    Jd(idx,idx) = J;
end



%% Store Matrices in pars
Model.Jacobi = h;
Model.pars.N = N;
Model.pars.DT = DT;
Model.pars.Jd = Jd;



%% Initial Guess
fprintf('\nLoad Initial Guess:\n');

% Define Initial Guess
U0     = reshape(Xqp0,d,N);
P0     = Wqp0(1);                        % Stroboscopic time 
p0     = Wqp0(2);                        % Rotation number
w10    = Wqp0(3);
w20    = Wqp0(4);
l10    = 0;
l20    = 0;


% Approximate family tangent
dz0    = Zqp0;
dU0    = reshape(dz0(1:end-4),d,N);
dP0    = dz0(end-3);
dp0    = dz0(end-2);
dl10   = dz0(end-1);
dl20   = dz0(end);



%% GMOS Algorithm
fprintf('GMOS Algorithm (Single-shooting):\n');

% Initialize Matrices
Xqp    = zeros(D, Nmax);
Wqp    = zeros(4, Nmax);
lqp    = zeros(2, Nmax);
Bqp    = zeros(D, D, Nmax);
Zqp    = zeros(D+4, Nmax);
Sqp    = zeros(1, Nmax);

% Compute up to Nmax family members
for ii = 1:Nmax
    
    % Tracking feedback
    fprintf('Family member No. %d, ds = %e:\n', ii, ds);
    
    
    % Partial Derivatives for Phase Constraints
    dUT1 = reshape(DT*U0(:),d,N);
    dUT0 = zeros(d,N);
    [~, dC0] = h(U0, Model.pars);
    dI10 = reshape(Jd*DT*U0(:), d, N);
    for kk = 1:N
        f0 = Model.dynamics([], U0(:,kk), Model.pars);
        dUT0(:,kk) = 1/w10 * (P0*f0 + l10*dC0(:, kk) + l20*dI10(:, kk) - w20*dUT1(:,kk));
    end
        
    
    % Predictor
    U  = U0 + ds*dU0;
    P  = P0 + ds*dP0;
    p  = p0 + ds*dp0;
    l1 = l10 + ds*dl10;
    l2 = l20 + ds*dl20;
    
    
    % Corrector
    for jj = 1:Iter
        
        % Calculate Rotation Matrix
        R    = RotationMatrix(p,d,N);
        R    = IDFT*R*DFT;               



        %% Integrate qp trajectories
        time = linspace(0, 1, M);
        Ic = [U(:); reshape(eye(D), D*D, 1); zeros(3*D, 1)];
        opt = odeset('Reltol', 3e-12, 'Abstol', 1e-14);
        Model.pars.P = P;
        Model.pars.l1 = l1;
        Model.pars.l2 = l2;
        [~, u] = ode113(@eom_qpt, time, Ic, opt, Model);
        
        
        % Store matrices
        Ut = reshape(u(end, 1:D), d, N);        
        Phi = reshape(u(end, D+1:D*(D+1)), D, D);
        Tht = reshape(u(end, D*(D+1)+1:end), D, 3);
        ft = reshape(Tht(:, 1), d, N);
        dUtdl1 = reshape(Tht(:, 2), d, N);
        dUtdl2 = reshape(Tht(:, 3), d, N);


        % Additional Constraints
        phs0 = dot(U(:) - U0(:), dUT0(:))/N;
        phs1 = dot(U(:), dUT1(:))/N;
        if strcmp(GMOS.ContinuationMethod,'Period')
            prme  = P - P0;
            dpmre = zeros(1, D+4);
            dpmre(1, end-3) = 1;
        elseif strcmp(GMOS.ContinuationMethod,'Jacobi')
            [C, dC] = h(U, Model.pars);
            prme = sum(C)/N - C0;
            dpmre = zeros(1, D+4);
            dpmre(1, 1:D) = dC(:)'/N;
        end
            
        arcl = dot(U(:) - U0(:), dU0(:))/N + (P - P0)*dP0 + (p - p0)*dp0 - ds;
        
        
        % Rotate Points
        Ur   = reshape(R*Ut(:),d,N);
         
                
        % Plot
        if(Plt)
            figure(fig99);
            plot3(U(1,:),U(2,:),U(3,:),'ob','Parent',ax99); hold on;
            plot3(Ut(1,:),Ut(2,:),Ut(3,:),'or','Parent',ax99);
            plot3(Ur(1,:),Ur(2,:),Ur(3,:),'og','Parent',ax99);
            plot3(U(1,1),U(2,1),U(3,1),'ob','Markerfacecolor','b','Parent',ax99);
            plot3(Ut(1,1),Ut(2,1),Ut(3,1),'or','Markerfacecolor','r','Parent',ax99);
            plot3(Ur(1,1),Ur(2,1),Ur(3,1),'og','Markerfacecolor','g','Parent',ax99);
            hold off;
            drawnow;
        end
        
        
                
        %% Error Vector 
        F = [Ur(:) - U(:); phs0; phs1; prme; arcl];
        
        
        
        %% Error Jacobian 
        DF                   = zeros(D+4, D+4); 
        DF(1:end-4, 1:end-4) = R*Phi - eye(d*N);
        DF(1:end-4, end-3)   = R*ft(:);
        DF(1:end-4, end-2)   = -DT*Ur(:);           % d R(U)/dp
        DF(1:end-4, end-1)   = R*dUtdl1(:);
        DF(1:end-4, end)     = R*dUtdl2(:);
        
        
        DF(end-3,1:end-4)   = dUT0(:)'/N;          % d phs0/du
        DF(end-2,1:end-4)   = dUT1(:)'/N;          % d phs1/du
        
        DF(end-1, :)        = dpmre;                   % d prd/dP
        
        DF(end, 1:end-4)    = dU0(:)'/N;           % d arcl/du
        DF(end, end-3)      = dP0;                 % d arcl/dP
        DF(end, end-2)      = dp0;                 % d arcl/dp
        DF(end, end-1)      = dl10;                % d arcl/dl10
        DF(end, end)        = dl20;                % d arcl/dl20
        
        
        
        %% Newton's Update
        z = -DF\F;
        test1 = sqrt(dot(F(1:end-4),F(1:end-4))/N + dot(F(end-3:end),F(end-3:end)));
        test2 = sqrt(dot(z(1:end-4),z(1:end-4))/N + dot(z(end-3:end),z(end-3:end)));
        fprintf('|F|* = %.4e, |z|* = %.4e, |F| = %.4e, |z| = %.4e, arc = %.4e, l1 = %.4e, l2 = %.4e\n',test1, test2, norm(F), norm(z), arcl, l1, l2);
        
        if(test1 < Tol || test2 < Tol)
            fprintf('Quasi-periodic Torus has been found!\n\n');
            if(abs(l1) > Tol || abs(l2) > Tol)
                fprintf('However, unfolding parameters are too high!\n\n');
                Xqp(:, ii:end) = [];
                Wqp(:, ii:end) = [];
                lqp(:, ii:end) = [];
                Bqp(:,:, ii:end) = [];
                Zqp(:, ii:end) = [];
                Sqp(ii:end) = [];
                return;
            end
            
            
%             % Create QP torus from cell
%             IJK         = cell2mat(Xt);
%             Xt          = sparse(IJK(1,:),IJK(2,:),IJK(3,:));
%             Xt          = full(Xt);
            Xt = u(:, 1:D)';


            % Plot
            if(Plt)
                figure(fig98);
                clr  = linspace(0, 1, GMOS.Nmax);
                for kk = 1:M
                    xt = reshape(Xt(:,kk),d,N);
                    if(d == 4)
                        plot(xt(1,:),xt(2,:),'.','Color',[0,clr(ii),1-clr(ii)],'Parent',ax98); hold on;
                    else
                        plot3(xt(1,:),xt(2,:),xt(3,:),'.','Color',[0,clr(ii),1-clr(ii)],'Parent',ax98); hold on;
                    end
                end
                axis equal;
                hold off;
                drawnow;
            end
            
            
            % Stability
            B   = R*Phi;
                        
            
            % Compute Family Tangent
            [sz, vz, dz] = svd(DF(1:end-1, 1:end-2));
            dzt = [dz(:, end); 0; 0];
            dzt = dzt/sqrt(dot(dzt(1:end-4), dzt(1:end-4))/N + dot(dzt(end-3:end), dzt(end-3:end)));
%             dz0 = [U(:)-U0(:); P-P0; p-p0; l1 - l10; l2 - l20];
%             dz0 = dz0/sqrt(dot(U(:) - U0(:), U(:) - U0(:))/N + (P - P0)^2 + (p - p0)^2 + (l1 - l10)^2 + (l2 - l20)^2);
            if(dot(dzt, dz0) > 0)
                dz0 = dzt;
            else
                dz0 = -dzt;
            end
            dU0 = reshape(dz0(1:end-4),d,N);
            dP0 = dz0(end-3);
            dp0 = dz0(end-2);
            dl10 = dz0(end-1);
            dl20 = dz0(end);

            
            % Step-size Controller
            Eps = Opt/jj;
            if(Eps > 2)
                Eps = 2;
            elseif(Eps < 0.5)
                Eps = 0.5;
            end
            ds = min(Eps*ds,dsMax);
            
            
            % Store Results
            Xqp(:, ii) = U(:);
            Wqp(:,ii) = [P; p; 2*pi/P; p/P];
            lqp(:, ii) = [l1; l2];
            Bqp(:, :, ii) = B;
            Zqp(:, ii) = dz0;
            Sqp(ii) = ds;
            
            
            % Update Old Solution
            U0 = U;
            P0 = P;
            p0 = p;
            l10 = l1;
            l20 = l2;
            break;
        else
            % Update Points & Frequency Vector
            U = U + reshape(z(1:end-4),d,N);
            P = P + z(end-3);
            p = p + z(end-2);
            l1 = l1 + z(end-1);
            l2 = l2 + z(end);
        end
    
    
        if(jj == Iter || test1 > 1e4 || test2 > 1e4 || isnan(norm(F)) || isnan(norm(z)))
            fprintf('Quasi-periodic Torus could not be found!\n\n');
            Xqp(:, ii:end) = [];
            Wqp(:, ii:end) = [];
            lqp(:, ii:end) = [];
            Bqp(:,:, ii:end) = [];
            Zqp(:, ii:end) = [];
            Sqp(ii:end) = [];
            return;
        end
    end
end
    