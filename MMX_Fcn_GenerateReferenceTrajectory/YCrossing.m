function [phs, dphs] = YCrossing(X,~)

[d,N] = size(X);
phs   = X(1);
dphs  = [1 0 0 0 0 0, repmat(zeros(1,d),1,N-1)];