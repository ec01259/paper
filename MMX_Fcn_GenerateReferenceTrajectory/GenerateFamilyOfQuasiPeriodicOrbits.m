function GenerateFamilyOfQuasiPeriodicOrbits(Xpo, Tpo, Vpo, Epo, filename, Model)


% Continue Quasi-periodic Trajectories
GMOS.ds = 1/(10*Model.units.lsf);
GMOS.N = 31;
GMOS.dsMax = 5/Model.units.lsf;
GMOS.Iter = 15;
GMOS.Nmax = 100;
GMOS.M = 100;
GMOS.Opt = 5;
GMOS.Plt = 1;
GMOS.Tol = 1e-10;
GMOS.ContinuationMethod = 'Jacobi';
GMOS.Jacobi = CRTBPsh_J(Xpo, Model.pars);
GMOS.JacobiFnc = @CRTBPsh_J;


% Calculate Quasi-periodic Invariant Tori
J = [ 0,  2,  0, -1,  0,  0;
     -2,  0,  0,  0, -1,  0;
      0,  0,  0,  0,  0, -1;
      1,  0,  0,  0,  0,  0;
      0,  1,  0,  0,  0,  0;
      0,  0,  1,  0,  0,  0];  % Canonical transf. matrix


[Xqp0,Wqp0,Zqp0] = GMOS_SS_InitialGuess(Xpo, Tpo, Vpo, Epo, GMOS, Model);
[Xqp, Wqp, lqp, Bqp, Zqp, Sqp] = GMOS_SS(Xqp0, Wqp0, Zqp0, GMOS, Model, J);
save(filename, 'Xqp', 'Wqp', 'lqp', 'Bqp', 'Zqp', 'Sqp', 'GMOS', 'Model', 'J');
movefile(filename, 'MMX_Product');