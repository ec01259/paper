function GenerateFamilyOfPeriodicOrbits(filename, Model)


% Initial Guess
A = 150/Model.units.lsf;
X0 = [0; 2*A; 0; A; 0; 0];
T0 = 2*pi;

% Family Tangent initial guess
dX0ds = [0; -2; 0; -1; 0; 0];
dT0ds = 0;
Z0 = [dX0ds; dT0ds]/norm([dX0ds; dT0ds]);

% Differential corrector options
options.ds = 1/(10*Model.units.lsf);
options.dsMax = 5/Model.units.lsf;
options.PhaseCondition = 'User-Defined';
options.PhaseConditionFcn = @YCrossing;
options.ContinuationMethod = 'Pseudo-Arclength';
options.FamilyTangent = Z0;
options.Nmax = 1000;
options.ExitCondition = true;
options.ExitConditionFcn = @CheckAltitude;

% Calculate Family of Periodic Orbits
[Xpo,Tpo,Mpo,Epo, FLAG, Zpo, Spo] = PO_SS(X0, T0, options, Model);
save(filename, 'Xpo', 'Tpo', 'Mpo', 'Epo', 'FLAG', 'Zpo', 'Spo', 'options', 'Model');
movefile(filename, 'MMX_Product');