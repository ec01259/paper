%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Rotation Matrix
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Q = RotationMatrix(p,n,N)

Q = zeros(n*N);
K = -(N-1)/2:1:(N-1)/2;
I = find(K == 0);
K = [K(I:end),K(1:I-1)];

kk = 1;
for ii = 2:2:(N-1)
    
    % column index
    kk = kk + 1;
    
    % Indexing
    row1 = n*(ii-1)+1:n*ii;
    row2 = n*ii+1:n*(ii+1);
    
    % Rotating Fourier Coefficients
    Q(row1,row1) = eye(n)*cos(K(kk)*p);
    Q(row1,row2) = -eye(n)*sin(K(kk)*p);
    Q(row2,row1) = eye(n)*sin(K(kk)*p);
    Q(row2,row2) = eye(n)*cos(K(kk)*p);
end
Q(1:n,1:n) = eye(n);
end