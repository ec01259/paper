%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Fourier Matrix
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [R,IR,DR] = FourierMatrix(n,N)

% -- Initialization --
D  = zeros(n*N);
R  = zeros(n*N);
IR = zeros(n*N);
DR = zeros(n*N);

% Order
K = -(N-1)/2:1:(N-1)/2;
I = find(K == 0);
K = [K(I:end),K(1:I-1)];

for ii = 1:N
    
    % k index
    kk = 1;
    
    % Row Index
    id1 = n*(ii-1)+1:n*ii;
    
    for jj = 1:N
        
        % Column Index
        id2 = n*(jj-1)+1:n*jj;
        
        % DFT matrix
        D(id1,id2)  = eye(n)*exp(2*pi*K(ii)*K(jj)*1j/N);
        
        % IR matrix
        if(mod(jj,2) == 0)
            kk = kk + 1;
            IR(id1,id2) = cos(2*pi*(ii-1)*K(kk)/N)*eye(n);
            DR(id1,id2) = -K(kk)*sin(2*pi*(ii-1)*K(kk)/N)*eye(n);
        else
            IR(id1,id2) = sin(2*pi*(ii-1)*K(kk)/N)*eye(n);
            DR(id1,id2) = K(kk)*cos(2*pi*(ii-1)*K(kk)/N)*eye(n);
        end
    end
    
    % A0
    IR(id1,1:n) = eye(n);
end


% From complex to real
kk = 0;
for ii = 2:2:(N-1)
    
    % column index
    kk = kk + 1;
    
    % Indexing
    row1 = n*(ii-1)+1:n*ii;
    row2 = n*ii+1:n*(ii+1);
    col1 = n*kk+1:n*(kk+1);
    col2 = n*(N-kk)+1:n*(N-kk+1);
    
    % Definitions
    R(row1,col1) = eye(n)/N;
    R(row1,col2) = eye(n)/N;
    R(row2,col1) = -1i*eye(n)/N;
    R(row2,col2) = 1i*eye(n)/N;
end
R(1:n,1:n) = eye(n)/N;
R = R*D;
if(max(max(imag(R))) > 1e-16)
    fprintf('Error in R!\n');
    return;
end
R = real(R);

% Derivative Matrix
DR = DR*R;
if(max(max(imag(DR))) > 1e-16)
    fprintf('Error in DR!\n');
    return;
end
DR = real(DR);
end