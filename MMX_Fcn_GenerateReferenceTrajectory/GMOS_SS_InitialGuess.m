function [Xqp0,Wqp0,Zqp0] = GMOS_SS_InitialGuess(Xpo, Tpo, Vpo, Epo, GMOS, Model)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% [Xqp0,Wqp0,Zqp0] = GMOS_SS_InitialGuess(Xpo, Tpo, Vpo, Epo, GMOS, Model)
%
% Generate Initial Guess for the GMOS single-shooting Method
% 
% INPUTs:
% VARIABLE     TYPE         DESCRIPTION
% - Xpo        double       Periodic Orbit Initial Conditions
% - Tpo        double       Periodic Orbit Period
% - Vpo        double       Center subspace Eigenvector
% - Epo        double       Center subspace Eigenvalue
% - pars       struct       List of Parameters
%   .d         int          No. of states
%   .GMOS
%    .ds       double       Initial displacement along center subspace
%    .N        int          No. of GMOS solution points
%
% OUTPUTS:
% VARIABLE     TYPE                 DESCRIPTION
% Xqp0         2D double array      QP Torus initial guess
% Wqp0         1D double array      QP Torus frequencies initial guess
% Zqp0         1D double array      Approximated Family Tangent
%
% DEPENDENCIES:
% - none
%
% AUTHOR: N. Baresi
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Problem parameters
d     = Model.pars.d;


% GMOS parameters
ds    = GMOS.ds;
N     = GMOS.N;


% Check Inputs
assert(isreal(Epo) == 0,'Please check input eigenvalues!');



%% Initialization %%%%
% Initial Guess 
fprintf('\nGenerating Initial Guess:\n');
tht    = linspace(0,2*pi,N+1); tht(end) = [];
THT    = repmat(tht,d,1); 
THT    = THT(:);
dU0    = (repmat(real(Vpo),N,1).*cos(THT) - repmat(imag(Vpo),N,1).*sin(THT));
U0     = repmat(Xpo,N,1) + ds*dU0;

% Stroboscopic time
P0     = Tpo;                         
dP0    = 0;
w10    = 2*pi/Tpo;


% Rotation number
p0     = atan2(imag(Epo),real(Epo));     
dp0    = 0;
w20    = p0/P0;              


% QP Torus Initial Guess
Xqp0   = U0;
Wqp0   = [P0; p0; w10; w20];

% Approximate family tangent
Zqp0   = [dU0; dP0; dp0; 0; 0]/sqrt(dU0(:)'*dU0(:)/N + dP0^2 + dp0^2); 
end 


