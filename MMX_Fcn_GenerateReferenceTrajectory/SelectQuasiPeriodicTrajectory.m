function [X0, T0] = SelectQuasiPeriodicTrajectory(Xpo, Xqp, Wqp, GMOS, Model, OrbitType, Amplitudes)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% [X0, T0] = SelectQuasiPeriodicTrajectory(Xpo, Xqp, Wqp, GMOS, inp)
%
% Selecte Quasi-periodic trajectory based on Input Amplitudes
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Initialize Amplitudes
A = zeros(2, size(Xqp, 2)); 


% Loop over QPT and store y and z amplitudes
for ii = 1:size(Xqp, 2)
    dXqp0 = reshape(Xqp(:, ii), Model.pars.d, GMOS.N) - Xpo;
    A(:, ii) = [max(dXqp0(2, :)); max(dXqp0(3, :))];
end


% Select 3D QSO
if(strcmp(OrbitType, '3D-QSO'))
    Az = Amplitudes(2);
    if(Az >= min(A(2, :)) && Az <= max(A(2, :)))
        X0 = spline(A(2, :), Xqp, Az);
        T0 = spline(A(2, :), Wqp(1:2, :), Az);
    else
        fprintf('Inserted Az amplitude does not fit within range of 3D-QSO family (%f, %f) [km x km]...\n', min(A(2, :))*Model.units.lsf, max(A(2, :))*Model.units.lsf);
        fprintf('Selecting last family member within the family!\n');
        X0 = Xqp(:, end);
        T0 = Wqp(1:2, end);
    end
  
    
% Select Swing QSO
elseif(strcmp(OrbitType, 'Swing-QSO'))
    Ay = Amplitudes(2);
    if(Ay >= min(A(1, :)) && Ay <= max(A(1, :)))
        X0 = spline(A(1, :), Xqp, Ay);
        T0 = spline(A(1, :), Wqp(1:2, :), Ay);
    else
        fprintf('Inserted Ay amplitude does not fit within range of Swing-QSO family (%f, %f) [km x km]...\n', min(A(1, :))*Model.units.lsf, max(A(1, :))*Model.units.lsf);
        fprintf('Selecting last family member within the family!\n');
        X0 = Xqp(:, end);
        T0 = Wqp(1:2, end);
    end
else
    error('Stop here traveller! You need to specify either "QSO", "3D-QSO", or "Swing-QSO"\n');
end