function [Vqp, Eqp, filename] = CalculateCenterEigs(Mpo, Epo, inp)

OrbitType = inp.OrbitType;
Amplitudes = inp.Amplitudes;


% No. of states
d = size(Mpo);

% SVD
[~, ~, d1] = svd(Mpo - Epo(1)*eye(d)); d1 = d1(:, end);
[~, ~, d2] = svd(Mpo - Epo(3)*eye(d)); d2 = d2(:, end);


% 3D or Swing QSOs
if(abs(dot(d1, [0, 0, 1, 0, 0, 1])) > abs(dot(d2, [0, 0, 1, 0, 0, 1])))
    e3 = Epo(1);
    d3 = d1;
    es = Epo(3);
    ds = d2;
else
    e3 = Epo(3);
    d3 = d2;
    es = Epo(1);
    ds = d1;
end


% 3D QSO
if(strcmp(OrbitType, '3D-QSO'))
    Vqp = d3;
    Eqp = e3;
    filename = sprintf('3DQSOs_%03.0f_%dx%d.mat', Amplitudes(1), size(inp.Model.pars.SH.Clm,1)-1, size(inp.Model.pars.SH.Clm,2)-1);
    
    % Swing QSO
elseif(strcmp(OrbitType, 'Swing-QSO'))
    Vqp = ds;
    Eqp = es;
    filename = sprintf('SwingQSOs_%03.0f_%dx%d.mat', Amplitudes(1), size(inp.Model.pars.SH.Clm,1)-1, size(inp.Model.pars.SH.Clm,2)-1);
    
else
    error('Stop here traveller! You need to specify either "QSO", "3D-QSO", or "Swing-QSO"\n');
end