function opt = CheckOptions(opt)

% check Continuation Method 
if(~isfield(opt,'ContinuationMethod'))
    fprintf('Stop here traveller! You need to specify a method among "Pseudo-Arclength", "Jacobi", or "Period"\n');
else
    switch opt.ContinuationMethod
        case 'Pseudo-Arclength'
            assert(isfield(opt,'FamilyTangent'),'Please specify your family tangent!');
        case 'Jacobi'
            assert(isfield(opt,'Jacobi'),'Please insert Jacobi value');
            assert(isfield(opt,'JacobiFnc'),'Please specify Jacobi function');
        case 'Period'
            assert(isfield(opt,'Period'),'Please insert period value');
    end
end


% check Phase Condition
if(~isfield(opt,'PhaseCondition'))
    opt.PhaseCondition = 'Default';
else
    if strcmp(opt.PhaseCondition,'User-Defined')
        assert(isfield(opt,'PhaseConditionFcn'),'Please Specify Phase Condition Function');
    end
end


% Set field ds (Default = 1e-3)
if(~isfield(opt,'ds'))
    opt.ds    = 1e-3;
end

% Set field dsMax (Default = 1)
if(~isfield(opt,'dsMax'))
    opt.dsMax = 1;
end

% Set field Iter (Default = 15)
if(~isfield(opt,'Iter'))
    opt.Iter  = 15;
end

% Set field Nmax (Default = 1)
if(~isfield(opt,'Nmax'))
    opt.Nmax  = 1;
end

% Set field Opt (Default = 5)
if(~isfield(opt,'Opt'))
    opt.Opt  = 5;
end

% Set field Plt (Default = 1)
if(~isfield(opt,'Plt'))
    opt.Plt  = 1;
end

% Set field Tol (Default = 1e-10)
if(~isfield(opt,'Tol'))
    opt.Tol  = 1e-10;
end


% Set field Exit Condition (Default = false)
if(~isfield(opt,'ExitCondition'))
    opt.ExitCondition = false;
else
    if opt.ExitCondition
        assert(isfield(opt,'ExitConditionFcn'),'Please Specify Exit Condition Function');
    end
end


