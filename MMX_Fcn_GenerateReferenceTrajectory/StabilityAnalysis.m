function [E, Flg] = StabilityAnalysis(M,pars)

% Load dimension
d = pars.d;


% Stability Analysis
if d == 6
    
    % Trace & Det
    alp  = 2 - trace(M);
    bet  = 1 + 0.5 * (alp^2 - trace(M^2));
    p    = (alp + sqrt(alp^2 - 4 * bet + 8))/2;
    q    = (alp - sqrt(alp^2 - 4 * bet + 8))/2;
    
    % Store Eigenvalues
    E(1) = 0.5 * (-p + sqrt(p^2 - 4));
    E(2) = 0.5 * (-p - sqrt(p^2 - 4));
    E(3) = 0.5 * (-q + sqrt(q^2 - 4));
    E(4) = 0.5 * (-q - sqrt(q^2 - 4));
                
    if(isreal(-p) && isreal(-q) && abs(-p) < 2 && abs(-q) < 2)
        % Stable Flag
        Flg = true;
    else
        % Unstable Flag
        Flg = false;
    end
elseif d == 4
    
    % Trace & Det
    alp  = 2 - trace(M);
    bet  = det(M);
    p    = (alp + sqrt(alp^2 - 4 * bet))/2;
    q    = (alp - sqrt(alp^2 - 4 * bet))/2;
    
    % Store Eigenvalues
    E(1) = -p;
    E(2) = -q;
    
    if(isreal(p + q) && abs(p + q) < 2)
        % Stable Flag
        Flg = true;
    else
        % Unstable Flag
        Flg = false;
    end
end