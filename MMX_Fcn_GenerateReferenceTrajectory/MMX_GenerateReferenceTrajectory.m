function [t, x, out] = MMX_GenerateReferenceTrajectory(inp)

Model = inp.Model;
OrbitType = inp.OrbitType;
Amplitudes = inp.Amplitudes/Model.units.lsf;
PropTime = inp.PropTime/Model.units.tsf;


% Create filename
filename = sprintf('QSOs_%dx%d.mat',size(Model.pars.SH.Clm,1)-1,size(Model.pars.SH.Clm,2)-1);



%% Check if Filename exists and if Model is the same
if(exist(filename, 'file') == 2)
    IsSameModel = CheckModel(filename, Model);
    CalculateOrbitFlag = ~IsSameModel;
else
    CalculateOrbitFlag = true;
end



%% Decide whether you can load orbits or calculate new ones
if(CalculateOrbitFlag)
    GenerateFamilyOfPeriodicOrbits(filename, Model);
end
load(filename, 'Xpo', 'Tpo', 'options', 'Model');



%% Select and Refine desired Periodic Orbit
H = Amplitudes(1);
Alt = Xpo(2, :);

% Check if desired altitude is within range, else load last family member
if(H >= min(Alt) && H <= max(Alt))
    X0 = spline(Alt, Xpo, Amplitudes(1));
    T0 = spline(Alt, Tpo, Amplitudes(1));
else
    X0 = Xpo(:, end);
    T0 = Tpo(end);
end
options.PhaseCondition = 'User-Defined';
options.PhaseConditionFcn = @YCrossing;
options.ContinuationMethod = 'Period';
options.Period = T0;
options.Nmax = 1;
options.Plt = false;
[X0, T0, M0, E0, ~] = PO_SS(X0, T0, options, Model);



%% Quasi-peridic? Y/N
if(~strcmp(OrbitType, 'QSO'))

    
    % Select quasi-periodic eigenvector
    [Vqp, Eqp, filename] = CalculateCenterEigs(M0, E0, inp);
    
    
    if(exist(filename, 'file') == 2)
        IsSameModel = CheckModel(filename, Model);
        CalculateOrbitFlag = ~IsSameModel;
    else
        CalculateOrbitFlag = true;
    end
    
          
    % Load or Generate Family of QPT
    if(exist(filename, 'file') ~= 2 || CalculateOrbitFlag)
        GenerateFamilyOfQuasiPeriodicOrbits(X0, T0, Vqp, Eqp, filename, Model);
    end
    load(filename, 'Xqp', 'Wqp', 'GMOS', 'Model');
    

    % Select Quasi-periodic Trajectory
    [X0, T0] = SelectQuasiPeriodicTrajectory(X0, Xqp, Wqp, GMOS, Model, OrbitType, Amplitudes);
end


%% Save and output initial conditions for modules that integrate eom in synodic frame
out.X0 = X0;
out.T0 = T0;
[t, x] = IntegrateInitialConditions(X0(1:6), PropTime, Model);
end
