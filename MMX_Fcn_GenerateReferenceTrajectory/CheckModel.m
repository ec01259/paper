function ModelFlag = CheckModel(filename, InputModel)

load(filename, 'Model');
ModelFlag = isequal(InputModel.dynamics, Model.dynamics) & ...
    isequal(InputModel.pars, Model.pars) & ...
    isequal(InputModel.units, Model.units);




