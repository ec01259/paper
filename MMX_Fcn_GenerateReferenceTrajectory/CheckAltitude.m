function exit = CheckAltitude(X, T, Model)

Time = [0, T];
Ic = X;
opt = odeset('RelTol',3e-14,'AbsTol',1e-16);
[~, x] = ode113(@eom,Time,Ic,opt, Model);

Alt = sqrt(x(:, 1).^2 + x(:, 2).^2 + x(:, 3).^2);

if(min(Alt) < 18/Model.units.lsf)
    exit = true;
else
    exit = false;
end