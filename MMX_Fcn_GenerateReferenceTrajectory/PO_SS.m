function [Xpo,Tpo,Mpo,Epo,FLAG,varargout] = PO_SS(X0, T0, opt, Model)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% [Xpo,Tpo,Mpo,Epo,FLAG,varargout] = PO_SS(X0,T0,opt,f,dfdx,pars)
% 
% Calculate Periodic Orbits with the Single-Shooting Method
%
% Input:
% .X0       State vector initial guess (d x 1)
% .T0       Period initial guess
% .opt      Single-Shooting parameters
%           - ds (default, 1e-3)
%           - dsMax (default, 1)
%           - Iter (default, 15)
%           - Nmax (default, 1)
%           - Opt (default, 5)
%           - Plt (default, 1)
%           - Tol (default, 1e-10)
%           - PhaseCondition ('Default','User-Defined')
%             if 'User-Defined, specify .@PhaseConditionFcn
%           - ContinuationMethod ('Pseudo-Arclength','Jacobi','Period')
%             if 'Pseudo-Arclength', specify .FamilyTangent
%             if 'Jacobi', specify .Jacobi and .@JacobiFcn
%             if 'Period', specify .Period
%           - ExitCondition (default, 0)
%             if true, specify .@ExitConditionFcn
% .Model    Problem Parameters
%           - dynamics
%           - pars
%           - units
%            
% Output:
% .Xpo      Vector of Periodic Initial Conditions (d x Nmax)
% .Tpo      Vector of Periods (1 x Nmax)
% .Mpo      Monodromy Matrices (d x d x Nmax)
% .Epo      Monodromy Matrix Eigenvalues (d-2 x Nmax)
% .FLAG     Stability Flag (1 x Nmax)
% .Zpo      if 'Pseudo-Arclength', Family Tangents (d+1 x Nmax)
% .Spo      if 'Pseudo-Arclength', Step length (1 x Nmax)
%
% Author: N. Baresi
% Dependencies: aux/CheckOptions.m, aux/PlotTrajectories,
% aux/StabilityAnalysis.m
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Load parameters
dyn   = Model.dynamics;
pars  = Model.pars;
d     = pars.d;
assert(d == 4 || d == 6,'Stop here Traveller! PO_SS can only calculate periodic orbits in DOF = 2 and DOF = 3 systems!');


% Check Options
opt   = CheckOptions(opt);
ds    = opt.ds;
dsMax = opt.dsMax;
Iter  = opt.Iter;
Nmax  = opt.Nmax;
Opt   = opt.Opt;
Plt   = opt.Plt;
if(Plt)
    if(isfield(opt,'gcf'))
        fig = figure(opt.gcf);
    else
        fig = figure(99);
    end
end
Tol   = opt.Tol;
if strcmp(opt.PhaseCondition,'User-Defined')
    g = opt.PhaseConditionFcn;
end

Z0    = zeros(d,1);
switch opt.ContinuationMethod
    case 'Pseudo-Arclength'
        Z0 = opt.FamilyTangent;
    case 'Jacobi'
        C0 = opt.Jacobi;
        h  = opt.JacobiFnc;
    case 'Period'
        P0 = opt.Period;
    otherwise
        error('Stop Here Traveller! Wrong Inputs\n');
end


% Initialization
Xpo   = zeros(d,Nmax);
Tpo   = zeros(1,Nmax);
Mpo   = zeros(d,d,Nmax);
Epo   = zeros(d-2,Nmax);
FLAG  = false(1,Nmax);
if strcmp(opt.ContinuationMethod,'Pseudo-Arclength')
    Zpo   = zeros(d+1,Nmax);
    Spo   = zeros(1,Nmax);
end


% Continue over family branch
for jj = 1:Nmax
    fprintf('Family Member No. %d, ds = %.4e:\n',jj,ds);
    
    % Predictor
    X = X0 + ds*Z0(1:d);
    T = T0 + ds*Z0(end);
    
    % Corrector
    for ii = 1:Iter
        
        % Integrate Trajectory
        Time  = [0, T];
        Ic    = [X; reshape(eye(d),d*d,1)];
        [t,x] = ode113(@eom,Time,Ic,odeset('RelTol',3e-14,'AbsTol',1e-16), Model);
        
        
        % STM
        M     = reshape(x(end,d+1:end),d,d);
        
        
        % Phase Condition
        switch opt.PhaseCondition
            case 'Default'
                phs          = dot(X - X0, f(0,X0,pars));
                dphsdx       = dyn(0, X0, pars)';
            case 'User-Defined'
                [phs,dphsdx] = g(X,pars);
        end
        Dphs               = [dphsdx, zeros(size(phs,1), 1)];
        
        
        % Parametrizing Equation
        switch opt.ContinuationMethod
            case 'Pseudo-Arclength'
                arc        = dot(X - X0, Z0(1:d)) + (T - T0)*Z0(end) - ds;
                Darc       = [Z0(1:d)', Z0(end)];
            case 'Jacobi'
                [C,DC]     = h(X,pars);
                arc        = C - C0;
                Darc       = [reshape(DC,1,d), 0];
            case 'Period'
                arc        = T - P0;
                Darc       = [zeros(1,d), 1];
        end
        
        
        % Error Vector and Jacobian
        F     = [x(end,1:d)' - X; phs; arc];
        DF    = [M - eye(d), dyn(t(end),x(end,1:d)',pars); Dphs; Darc];
        
        
        % Newton's update
        dy    = -DF\F;        
        fprintf('|F| = %.4e, |dy| = %.4e\n',norm(F),norm(dy));
       
        
        % Check if converged
        if(norm(F) < Tol || norm(dy) < Tol)          
            
            % Stability Analysis
            [E, Flg] = StabilityAnalysis(M,pars);
            
            
            % Plot Trajectory
            if(Plt)
                PlotTrajectories(fig, x, Model, Flg)
            end
            
                        
            % Store Results
            Xpo(:,jj)   = X;
            Tpo(jj)     = T;
            Mpo(:,:,jj) = M;
            Epo(:,jj)   = E;
            FLAG(jj)    = Flg;
            
                
            if strcmp(opt.ContinuationMethod,'Pseudo-Arclength')
                % Compute family tangent
                Zt        = [X - X0; T - T0];
                Z         = Zt/norm(Zt);
%                 [~,~,Z]   = svd(DF(1:end-1,:)); % disp(diag(lam));
%                 Z         = Z(:,end)/norm(Z(:,end));
                if(dot(Z(1:3),Z0(1:3)) < 0)
                    Z = -Z;
                end
                
                
                % Step size controller
                Eps = Opt/ii;
                if(Eps > 2)
                    Eps = 2;
                elseif(Eps < 0.5)
                    Eps = 0.5;
                end
                ds  = min(Eps*ds,dsMax);
            
                
                % Store Results
                Zpo(:,jj)   = Z;
                Spo(jj)     = ds;
                
                
                % Update Old Solution & Family Tangent
                X0          = X;
                T0          = T;
                Z0          = Z;
            end         
            break;
        else
            % Newton's Update
            X = X + dy(1:d);
            T = T + dy(end);
        end
    end
    
    
    if(opt.ExitCondition)
        exit = opt.ExitConditionFcn(X, T, Model);
    else
        exit = false;
    end
    
        
    % Quit if Last Iteration
    if (ii == Iter || exit)
        fprintf('Periodic Orbit could not be found!\n\n');
        Xpo(:,jj:Nmax)   = [];
        Tpo(jj:Nmax)     = [];
        Mpo(:,:,jj:Nmax) = [];
        Epo(:,jj:Nmax)   = [];
        FLAG(jj:Nmax)    = [];
        if strcmp(opt.ContinuationMethod,'Pseudo-Arclength')
            Zpo(:,jj:Nmax)   = [];
            Spo(:,jj:Nmax)   = [];
        end
        break;
    else
        fprintf('Periodic Orbit has been found!\n\n');
    end
end
if strcmp(opt.ContinuationMethod,'Pseudo-Arclength')
    varargout{1} = Zpo;
    varargout{2} = Spo;
end
return;