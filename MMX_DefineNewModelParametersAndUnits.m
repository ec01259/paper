function [pars, units] = MMX_DefineNewModelParametersAndUnits
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% [pars, units] = MMX_DefineModelParametersAndUnits(PhobosGravityField)
%
% Define model parameters and units for the MMX Toolbox
% 
% Inputs:
% PhobosGravityField    Order x Degree of Phobos' gravity field (max [8, 8])
% 
% Outputs:
% pars.d        No. of States
% pars.p        No. of model parameters
% pars.Body     Secondary's body structure:
%          .alp Secondary's largest semi-axis [Normalized Units]
%          .bet Secondary's intermediate semi-axis [Normalized Units]
%          .gam Secondary's smallest semi-axis [Normalized Units]
%
% units.lsf     Length unit
% units.tsf     Time unit
% units.vsf     Velocity unit
% units.sfVec   Vector of normalizing units [lsf, lsf, lsf, vsf, vsf, vsf]
%
% Author: STAG Team
% Date: Jun 2021
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 



%   Parameters
    G           = astroConstants(1);
    pars.GM2    = cspice_bodvrd('401','GM',1);                %[kg]
    pars.GM1    = cspice_bodvrd('499','GM',1);                %[kg]
    r_Ph        = cspice_bodvrd('401','RADII',3);             %[km]
    alpha       = r_Ph(1);
    r_Ph        = r_Ph/alpha;
    a_Ph        = r_Ph(1);
    b_Ph        = r_Ph(2);                                    %[km]
    c_Ph        = r_Ph(3);                                    %[km]
    pars.RPhmean = sqrt(3/(a_Ph+b_Ph+c_Ph));
    r_M         = cspice_bodvrd('499','RADII',3);             %[km]
    r_M         = r_M/alpha;
    a_M         = r_M(1);                                     %[km]
    b_M         = r_M(2);                                     %[km]
    c_M         = r_M(3);                                     %[km]
    
    units.lsf       = alpha;                                      % Unit of lenght [km]
    units.tsf       = sqrt((pars.GM1 + pars.GM2)/units.lsf^3);    % Unit of time [1/s]
    units.vsf       = units.lsf*units.tsf;
    units.GMsf      = units.lsf^3*units.tsf^2;
    
    units.sfVec     = [units.lsf; units.lsf; units.lsf; units.vsf; units.vsf; units.vsf];
    units.sfVec2    = [units.lsf; units.vsf; 1; units.tsf; 1; units.tsf; 1; units.tsf];
    units.M         = pars.GM2/astroConstants(1);                  % Unit of mass [km]

%   Bodies Geometry
    pars.Phobos.alpha = a_Ph*units.lsf;
    pars.Phobos.beta  = b_Ph*units.lsf;
    pars.Phobos.gamma = c_Ph*units.lsf;
    
    pars.Mars.alpha = a_M*units.lsf;
    pars.Mars.beta  = b_M*units.lsf;
    pars.Mars.gamma = c_M*units.lsf;

%   Mass fractions
    pars.m      = pars.GM2^2/(pars.GM1 + pars.GM2);
    pars.mu     = (pars.GM1 + pars.GM2);
    pars.ni     = pars.GM1/(pars.GM1 + pars.GM2);
    
    pars.G      = G/(units.lsf^3*units.tsf^2/units.M);
    pars.MMars  = pars.GM1/(G*units.M);
    pars.MPho   = pars.GM2/(G*units.M);
    
%   Inertia parameters
    pars.IPhx_bar   = .2*(b_Ph^2 + c_Ph^2);  
    pars.IPhy_bar   = .2*(a_Ph^2 + c_Ph^2);
    pars.IPhz_bar   = .2*(a_Ph^2 + b_Ph^2);

    pars.IMx_bar    = .2*pars.GM1*(b_M^2 + c_M^2)/(a_Ph^2*pars.GM1);   
    pars.IMy_bar    = pars.IMx_bar;
    pars.Is         = pars.IMx_bar;
    pars.IMz_bar    = .2*pars.GM1*(a_M^2 + b_M^2)/(a_Ph^2*pars.GM1);

%   Phobos gravitational harmonics WRT principal axis of inertia

    pars.SH = PhobosSphericalHarmonicCoefficients(2,2);
    pars.SH.Re = pars.SH.Re/units.lsf;
    pars.SH.GM = pars.SH.GM/units.GMsf;

%   par.MMars gravitational harmonics WRT principal axis of inertia
    pars.RMmean  = mean(r_M);
    C_20_M       = (pars.IMx_bar - pars.IMz_bar)/(pars.MMars*pars.RMmean^2);
    pars.J2      = - C_20_M;

%   Some other useful parameters
    pars.d       = 6;        % MMX's States
    pars.d2      = 8;        % Phobos's States

%   Ellipsoidal Phobos gravitational harmonics WRT principal axis of inertia
    C_22_Ph     = .25*(pars.IPhy_bar - pars.IPhx_bar);
    C_20_Ph     = -.5*(2*pars.IPhz_bar - pars.IPhx_bar - pars.IPhy_bar);

    pars.SH.Clm  = [1, 0, 0; 0, 0, 0; C_20_Ph, 0, C_22_Ph]./pars.SH.Norm(1:3,1:3);
    pars.SH.Slm  = zeros(3,3);



end