clear
close all
clc

%%  Setup

    warning('off', 'all'); format longG;
    set(0,'DefaultTextInterpreter','latex');
    set(0,'DefaultAxesFontSize', 16);

%   Mac
    restoredefaultpath
    addpath('MMX_Fcn_CovarianceAnalyses/');
    addpath('Model_check/');
    addpath('../useful_functions/');
    addpath(genpath('../mice/'));
    addpath(genpath('../generic_kernels/'));
    addpath(genpath('../paper_after_conf/MMX_Product/MMX_BSP_Files_GravLib/'));
    MMX_InitializeSPICE
    cspice_furnsh('../generic_kernels/mar097.bsp');
    cspice_furnsh('./Model_Check/MARPHOFRZ.txt');
 
%%  QSO-H    
    
%     cspice_furnsh('./MMX_Product/MMX_BSP_Files_GravLib/MMX_QSO_198_2x2_826891269_831211269.bsp');
    cspice_furnsh('./MMX_Product/MMX_BSP_Files_GravLib/Phobos_826891269_831211269.bsp');
%     
%     load("./ObservationsHPC/Observations-QSOH_30days_25.mat");
% 
% %   Model parameters
%     [par, units] = MMX_DefineNewModelParametersAndUnits;
% 
% %   Time of the analysis
%     data        = '2026-03-25 00:00:00 (UTC)';
%     data        = cspice_str2et(data);
%     par.et0     = data;
%     [Ph,par]    = Phobos_States_NewModel(data,par);
% 
% %   Covariance analysis parameters
%     [par, units] = New_MMX_CovarianceAnalysisParameters(par, units);
%     par.sigma    = 1e-10/(units.vsf*units.tsf);
%     par.sigmaPh  = 0/(units.vsf*units.tsf);
% 
% %   Initial Phobos's state vector
%     Ph0     = Ph./units.sfVec2;
% 
% %   Initial MMX's State vector
%     MMX0   = cspice_spkezr('-34', data, 'MarsIAU', 'none', '499');
%     MMX0   = MMX0./units.sfVec;
% 
% %   Analysis initial state vector
%     St0    = [MMX0; Ph0; par.I2; par.bias];
% 
%     Est0.X  = St0;
%     Est0.dx = zeros(size(St0,1),1);
%     Est0.P0 = par.P0;
%     Est0.t0 = data*units.tsf;
%   
%     par.alpha   = 1e0;
% 
%     [Est] = UKF_CovarianceAnalysis(Est0,@Dynamics_MPHandMMX_Inertia,...
%         @Cov_Dynamics_Good, @New_Observables_model,...
%         par.R,YObs_Full,par,units);
%     
%     save('QSOH_PN10_alpha1_30days_25.mat','Est');
%     movefile('QSOH_PN10_alpha1_30days_25.mat','./ResultsHPC/')

%%  QSO-M    
    
%     cspice_furnsh('./MMX_Product/MMX_BSP_Files_GravLib/MMX_QSO_094_2x2_826891269_831211269.bsp');    
%     load("./ObservationsHPC/Observations-QSOM_30days_25.mat");
% 
% %   Model parameters
%     [par, units] = MMX_DefineNewModelParametersAndUnits;
% 
% %   Time of the analysis
%     data        = '2026-03-25 00:00:00 (UTC)';
%     data        = cspice_str2et(data);
%     par.et0     = data;
%     [Ph,par]    = Phobos_States_NewModel(data,par);
% 
% %   Covariance analysis parameters
%     [par, units] = New_MMX_CovarianceAnalysisParameters(par, units);
%     par.sigma    = 1e-10/(units.vsf*units.tsf);
%     par.sigmaPh  = 0/(units.vsf*units.tsf);
% 
% %   Initial Phobos's state vector
%     Ph0     = Ph./units.sfVec2;
% 
% %   Initial MMX's State vector
%     MMX0   = cspice_spkezr('-34', data, 'MarsIAU', 'none', '499');
%     MMX0   = MMX0./units.sfVec;
% 
% %   Analysis initial state vector
%     St0    = [MMX0; Ph0; par.I2; par.bias];
% 
%     Est0.X  = St0;
%     Est0.dx = zeros(size(St0,1),1);
%     Est0.P0 = par.P0;
%     Est0.t0 = data*units.tsf;
%   
%     par.alpha   = 1e0;
% 
%     [Est] = UKF_CovarianceAnalysis(Est0,@Dynamics_MPHandMMX_Inertia,...
%         @Cov_Dynamics_Good, @New_Observables_model,...
%         par.R,YObs_Full,par,units);
%     
%     save('QSOM_PN10_alpha1_30days_25.mat','Est');
%     movefile('QSOM_PN10_alpha1_30days_25.mat','./ResultsHPC/')

%%  QSO-La    
    
%     cspice_furnsh('./MMX_Product/MMX_BSP_Files_GravLib/MMX_QSO_049_2x2_826891269_831211269.bsp');    
%     load("./ObservationsHPC/Observations-QSOLa_30days_25.mat");
% 
% %   Model parameters
%     [par, units] = MMX_DefineNewModelParametersAndUnits;
% 
% %   Time of the analysis
%     data        = '2026-03-25 00:00:00 (UTC)';
%     data        = cspice_str2et(data);
%     par.et0     = data;
%     [Ph,par]    = Phobos_States_NewModel(data,par);
% 
% %   Covariance analysis parameters
%     [par, units] = New_MMX_CovarianceAnalysisParameters(par, units);
%     par.sigma    = 1e-10/(units.vsf*units.tsf);
%     par.sigmaPh  = 0/(units.vsf*units.tsf);
% 
% %   Initial Phobos's state vector
%     Ph0     = Ph./units.sfVec2;
% 
% %   Initial MMX's State vector
%     MMX0   = cspice_spkezr('-34', data, 'MarsIAU', 'none', '499');
%     MMX0   = MMX0./units.sfVec;
% 
% %   Analysis initial state vector
%     St0    = [MMX0; Ph0; par.I2; par.bias];
% 
%     Est0.X  = St0;
%     Est0.dx = zeros(size(St0,1),1);
%     Est0.P0 = par.P0;
%     Est0.t0 = data*units.tsf;
%   
%     par.alpha   = 1e0;
% 
%     [Est] = UKF_CovarianceAnalysis(Est0,@Dynamics_MPHandMMX_Inertia,...
%         @Cov_Dynamics_Good, @New_Observables_model,...
%         par.R,YObs_Full,par,units);
%     
%     save('QSOLa_PN10_alpha1_30days_25.mat','Est');
%     movefile('QSOLa_PN10_alpha1_30days_25.mat','./ResultsHPC/')


%%  QSO-Lb    
    
%     cspice_furnsh('./MMX_Product/MMX_BSP_Files_GravLib/MMX_QSO_031_2x2_826891269_831211269.bsp');
%     cspice_furnsh('./MMX_Product/MMX_BSP_Files_GravLib/Phobos_826891269_831211269.bsp');
%     load("./ObservationsHPC/Observations-QSOM_30days_25.mat");
% 
% %   Model parameters
%     [par, units] = MMX_DefineNewModelParametersAndUnits;
% 
% %   Time of the analysis
%     data        = '2026-03-25 00:00:00 (UTC)';
%     data        = cspice_str2et(data);
%     par.et0     = data;
%     [Ph,par]    = Phobos_States_NewModel(data,par);
% 
% %   Covariance analysis parameters
%     [par, units] = New_MMX_CovarianceAnalysisParameters(par, units);
%     par.sigma    = 1e-10/(units.vsf*units.tsf);
%     par.sigmaPh  = 0/(units.vsf*units.tsf);
% 
% %   Initial Phobos's state vector
%     Ph0     = Ph./units.sfVec2;
% 
% %   Initial MMX's State vector
%     MMX0   = cspice_spkezr('-34', data, 'MarsIAU', 'none', '499');
%     MMX0   = MMX0./units.sfVec;
% 
% %   Analysis initial state vepar.alpha   = 7e-1;ctor
%     St0    = [MMX0; Ph0; par.I2; par.bias];
% 
%     Est0.X  = St0;
%     Est0.dx = zeros(size(St0,1),1);
%     Est0.P0 = par.P0;
%     Est0.t0 = data*units.tsf;
%   
%     par.alpha   = 1e0;
% 
%     [Est] = UKF_CovarianceAnalysis(Est0,@Dynamics_MPHandMMX_Inertia,...
%         @Cov_Dynamics_Good, @New_Observables_model,...
%         par.R,YObs_Full,par,units);
%     
%     save('QSOLb_PN10_alpha1_30days_25.mat','Est');
%     movefile('QSOLb_PN10_alpha1_30days_25.mat','./ResultsHPC/')
% 
%%  QSO-Lc    
%     
    cspice_furnsh('./MMX_Product/MMX_BSP_Files_GravLib/MMX_QSO_027_2x2_826891269_831211269.bsp');    
    load("./ObservationsHPC/Observations-QSOLc_30days_25.mat");

%   Model parameters
    [par, units] = MMX_DefineNewModelParametersAndUnits;

%   Time of the analysis
    data        = '2026-03-28 00:00:00 (UTC)';
    data        = cspice_str2et(data);
    par.et0     = data;
    [Ph,par]    = Phobos_States_NewModel(data,par);

%   Covariance analysis parameters
    [par, units] = New_MMX_CovarianceAnalysisParameters(par, units);
    par.sigma    = 1e-10/(units.vsf*units.tsf);
    par.sigmaPh  = 0/(units.vsf*units.tsf);

%   Initial Phobos's state vector
    Ph0     = Ph./units.sfVec2;

%   Initial MMX's State vector
    MMX0   = cspice_spkezr('-34', data, 'MarsIAU', 'none', '499');
    MMX0   = MMX0./units.sfVec;

%   Analysis initial state vector
    St0    = [MMX0; Ph0; par.I2; par.bias];

    Est0.X  = St0;
    Est0.dx = zeros(size(St0,1),1);
    Est0.P0 = par.P0;
    Est0.t0 = data*units.tsf;
  
    par.alpha   = 1e0;

    [Est] = UKF_CovarianceAnalysis(Est0,@Dynamics_MPHandMMX_Inertia,...
        @Cov_Dynamics_Good, @New_Observables_model,...
        par.R,YObs_Full,par,units);
    
    save('QSOLc_PN10_alpha1_30days_25.mat','Est');
    movefile('QSOLc_PN10_alpha1_30days_25.mat','./ResultsHPC/')

%%  SwingQSO-Lb    
    
%     cspice_furnsh('./MMX_Product/MMX_BSP_Files_GravLib/MMX_SwingQSO_031_011_2x2_826891269_831211269.bsp');    
%     load("./ObservationsHPC/Observations-SwingQSOLb_30days.mat");
% 
% %   Model parameters
%     [par, units] = MMX_DefineNewModelParametersAndUnits;
% 
% %   Time of the analysis
%     data        = '2026-03-20 00:00:00 (UTC)';
%     data        = cspice_str2et(data);
%     par.et0     = data;
%     [Ph,par]    = Phobos_States_NewModel(data,par);
% 
% %   Covariance analysis parameters
%     [par, units] = New_MMX_CovarianceAnalysisParameters(par, units);
%     par.sigma    = 1e-10/(units.vsf*units.tsf);
%     par.sigmaPh  = 0/(units.vsf*units.tsf);
% 
% %   Initial Phobos's state vector
%     Ph0     = Ph./units.sfVec2;
% 
% %   Initial MMX's State vector
%     MMX0   = cspice_spkezr('-34', data, 'MarsIAU', 'none', '499');
%     MMX0   = MMX0./units.sfVec;
% 
% %   Analysis initial state vector
%     St0    = [MMX0; Ph0; par.I2; par.bias];
% 
%     Est0.X  = St0;
%     Est0.dx = zeros(size(St0,1),1);
%     Est0.P0 = par.P0;
%     Est0.t0 = data*units.tsf;
%   
%     par.alpha   = 1e0;
% 
%     [Est] = UKF_CovarianceAnalysis(Est0,@Dynamics_MPHandMMX_Inertia,...
%         @Cov_Dynamics_Good, @New_Observables_model,...
%         par.R,YObs_Full,par,units);
%     
%     save('SwingQSOLb_PN10_alpha1_30days.mat','Est');
%     movefile('SwingQSOLb_PN10_alpha1_30days.mat','./ResultsHPC/')
